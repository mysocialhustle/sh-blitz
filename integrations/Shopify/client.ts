const API_URL = `https://${process.env.BLITZ_PUBLIC_SHOPIFY_STORE_URL}/api/2022-04/graphql.json`
const ACCESS_TOKEN = `${process.env.BLITZ_PUBLIC_SHOPIFY_ACCESS_TOKEN}`

interface vars {
    variables?: {}
}

export default async function fetchAPI(query, { variables }: vars = {}) {
    const headers = {
        'X-Shopify-Storefront-Access-Token': ACCESS_TOKEN,
        'Content-Type': 'application/json',
    }

    const res = await fetch(API_URL, {
        method: 'POST',
        headers,
        body: JSON.stringify({
            query,
            variables,
        }),
    })

    const json = await res.json()
    if (json.errors) {
        console.error(json.errors)
        throw new Error('Failed to fetch API')
    }
    return json.data
}
