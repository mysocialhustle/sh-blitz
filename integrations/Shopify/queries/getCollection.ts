import fetchAPI from '../client'
import productsQuery from '../constants/productsQuery'

export async function getCollection(handle: string) {
    const data = await fetchAPI(
        `
        query CollectionQuery($handle: String = "") {
            collection(handle: $handle) {
              image {
                altText
                url(transform: {preferredContentType: WEBP})
                width
                height
              }
              title
              ${productsQuery('CREATED')}
            }
          }          
        `,
        {
            variables: { handle },
        }
    )

    return data?.collection
}
