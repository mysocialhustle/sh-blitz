import fetchAPI from '../client'
import productsQuery from '../constants/productsQuery'

export async function getAllProducts() {
    const data = await fetchAPI(
        `
        query AllProductsQuery {
              ${productsQuery('CREATED_AT')}
        }          
        `
    )

    return data?.products
}
