const productsQuery = (sortkey = 'ID') => `
products(first: 24, sortKey: ${sortkey}) {
    edges {
      node {
        featuredImage {
          altText
          url(transform: {preferredContentType: WEBP})
          width
          height
        }
        handle
        onlineStoreUrl
        id
        priceRange {
          minVariantPrice {
            amount
          }
          maxVariantPrice {
            amount
          }
        }
        title
      }
    }
  }
`

export default productsQuery
