import { z } from 'zod'
import { ComponentType } from 'react'
import { FormProps } from 'types'
import withHubspot from 'integrations/Hubspot/withHubspot'

interface Props<S extends z.ZodType<any, any>, T extends unknown> {
    id: string
    hotLead?: boolean
    formProps?: Partial<FormProps<S, T>>
    Component: ComponentType
}

export default function HubspotForm<
    S extends z.ZodType<any, any>,
    T extends unknown
>({ id, hotLead = true, formProps, Component }: Props<S, T>) {
    return withHubspot(Component, {
        hotLead,
        id,
        ...formProps,
    })
}
