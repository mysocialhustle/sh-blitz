export default function HubspotMeeting({ url }) {
    return (
        <div
            dangerouslySetInnerHTML={{
                __html: `<iframe src="${url}?embed=true&parentHubspotUtk=59ee24d10088537afd4789d85e25ab84" width="100%" style="min-width: 312px; min-height: 516px; height: 756px; border: none;"></iframe>`,
            }}
        />
    )
}
