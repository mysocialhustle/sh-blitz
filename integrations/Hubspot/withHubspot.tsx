import { ReactNode } from 'react'
import Form from 'app/core/components/Form/Form/Form'
import hasValue from 'app/core/utils/hasValue'
import { getCookie } from 'app/core/utils/cookies'
import useCurrentUrl from 'app/core/hooks/useCurrentUrl'
import { FormProps } from 'types'
import { z } from 'zod'
import submit from './submit'

export default function withHubspot(Component, props) {
    const { id, hotLead = true, url, ...rest } = props
    const hubspotutk = getCookie('hubspotutk')

    const urlfallback =
        typeof window !== 'undefined' ? window.location.href : ''

    const onSubmit = async (data) => {
        //console.log(data)
        const entries = Object.entries(data)
            .map(([key, value]) => {
                return {
                    name: key,
                    value: value,
                }
            })
            .filter((entry) => hasValue(entry.value))
        if (entries.length > 0 && hotLead === true) {
            entries.push({
                name: 'hs_lead_status',
                value: 'Hot Lead',
            })
        }
        let formData = {
            submittedAt: Date.now(),
            fields: entries,
            context: {
                pageUri: url ? url : urlfallback,
            },
        }
        if (hubspotutk !== '') {
            formData.context['hutk'] = hubspotutk
        }
        //console.log(formData)
        await submit(id, formData)
            .then((response) => response.json())
            .then((data) => {
                return data
            })
    }

    return id ? <Component onSubmit={onSubmit} {...rest} /> : null
}
