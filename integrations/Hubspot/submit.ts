export default async function submit(id, data) {
    const url = `https://api.hsforms.com/submissions/v3/integration/submit/${process.env.BLITZ_PUBLIC_HUBSPOT_PORTAL_ID}/${id}`
    return await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
}
