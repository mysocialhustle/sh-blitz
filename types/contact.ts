export interface Contact {
    phone: string
    email: string
    address: {
        street: string
        city: string
        state: string
        zip: string
    }
}
