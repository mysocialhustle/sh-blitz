import { ReactNode, PropsWithoutRef } from 'react'
import { UseFormProps } from 'react-hook-form'
import { z } from 'zod'
import { ButtonProps } from 'types'

interface OnSubmitResult {
    FORM_ERROR?: string
    [prop: string]: any
}

export interface FormProps<S extends z.ZodType<any, any>, T extends unknown>
    extends Omit<PropsWithoutRef<JSX.IntrinsicElements['form']>, 'onSubmit'> {
    /** All your form fields */
    children?: ReactNode
    /** Text to display in the submit button */
    submitText?: string
    schema?: S
    onSubmit: (values: z.infer<S>) => Promise<void | OnSubmitResult>
    initialValues?: UseFormProps<z.infer<S>>['defaultValues']
    buttonProps?: Partial<ButtonProps<T>>
    successMessage?: string | null
    resetOnSubmit?: boolean
}
