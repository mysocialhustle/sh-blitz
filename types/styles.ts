import { ResponsiveProp } from './responsive'

export interface StyledProps extends ResponsiveProp {
    readonly active?: boolean
    readonly color?: string
    readonly backgroundColor?: string
    readonly hoverColor?: string
    readonly hoverBackgroundColor?: string
    readonly backgroundImage?: string
    readonly alignY?: string
    readonly alignX?: string
    readonly direction?: string
    readonly gutter?: string
    readonly index?: number
    readonly fullHeight?: boolean
    readonly fluid?: boolean
    readonly reverse?: boolean
    readonly position?: string
    readonly size?: string | number
    readonly centered?: boolean
    readonly aspectRatio?: number
    readonly inverted?: boolean
}
