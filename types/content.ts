import { ServiceCategory } from './services'

export interface ContentImage {
    featured?: string | null
    transparent?: string | null
}

export interface ContentItem {
    title: string
    slug: string
    images?: ContentImage
}

export interface TestimonialItem extends Partial<ContentItem> {
    author?: string
    categories: string[]
    body: string
}

export interface PortfolioItem extends ContentItem {
    category: ServiceCategory
    placement: string[]
    bgColor: string
    tags: string[]
    tagline?: string
}

export interface Certification {
    name: string
    title: string
    category: 'Awards' | 'Certifications'
    type: 'svg' | 'svg'
    imageSrc: string
    homepage: boolean
}
