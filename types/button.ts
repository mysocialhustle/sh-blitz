import React from 'react'
import { RouteUrlObject } from 'blitz'

/** Typescript generic component
 *  https://medium.com/reactbrasil/make-your-react-component-generic-with-typescript-497378515667
 *
 * */

export interface ButtonProps<T> {
    children: React.ReactNode
    href?: string | RouteUrlObject
    element?: any
    color?: string
    backgroundColor?: string
    hoverColor?: string
    hoverBackgroundColor?: string
    centered?: boolean
    onClick?: () => void
    style?: React.CSSProperties
    disabled?: boolean
    props?: T
}
