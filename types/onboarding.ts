import { Campaign, Content, Website, Company } from 'db'

export interface OnboardingSession {
    project: Partial<Campaign | Content | Website>
    company: Partial<Company>
}
