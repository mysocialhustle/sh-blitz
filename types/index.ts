import React, { DOMAttributes } from 'react'
import { DefaultCtx, SessionContext, SimpleRolesIsAuthorized } from 'blitz'
import { User, UserRole, Company } from 'db'
import { OnboardingSession } from './onboarding'

// Note: You should switch to Postgres and then use a DB enum for role type
export type Role = UserRole

export * from './responsive'
export * from './dimensions'
export * from './menu'
export * from './themes'
export * from './styles'
export * from './inputs'
export * from './content'
export * from './forms'
export * from './button'
export * from './contact'
export * from './team'
export * from './services'
export * from './counters'
export * from './account'
export * from './checkbox'
export * from './modelProperties'
export * from './onboarding'

declare module 'blitz' {
    export interface Ctx extends DefaultCtx {
        session: SessionContext
    }
    export interface Session {
        isAuthorized: SimpleRolesIsAuthorized<Role>
        PublicData: {
            userId: User['id']
            role: Role
            company?: Partial<Company>
            onboarding?: OnboardingSession[]
        }
    }
}
