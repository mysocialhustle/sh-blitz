export enum CounterSize {
    small = 1,
    medium = 2.5,
    large = 5,
}

export interface Counter {
    start?: number
    digit: number
    textBefore?: string
    textAfter?: string
    style?: React.CSSProperties
    round?: number
    duration?: number
    size?: string
}
