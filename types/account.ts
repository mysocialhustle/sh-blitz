import { UserRole } from 'db'

export type UserPrivilegeLabel =
    | 'editAdmins'
    | 'deleteUsers'
    | 'editUsers'
    | 'editCompanies'
    | 'editCampaigns'
    | 'editWebsites'
    | 'editContents'

export type UserPrivileges = Record<UserPrivilegeLabel, UserRole[]>
