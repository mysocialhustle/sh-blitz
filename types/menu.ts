import { DimensionObject } from './dimensions'

export type MenuType = 'mega' | 'external'

export interface MenuItem {
    name: string
    link?: string | null
    icon?: any
    type?: MenuType
    submenu?: any[] | null
}

export interface MegaMenuItem {
    name: string
    submenu: MenuItem[]
}

export interface MenuNav {
    name: string
    type?: MenuType
    submenu?: Array<MenuItem | MegaMenuItem>
}

export type Menu = Array<MenuNav | MenuItem>

export interface ActiveMenuItem extends DimensionObject {
    index: number
}
