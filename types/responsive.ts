export type BreakpointKey = 'xs' | 'sm' | 'md' | 'lg'

export type ResponsiveProp = Partial<
    Record<BreakpointKey, number | string | boolean>
>
