import { ComponentType } from 'react'
import { ContentItem } from './content'
import { Counter } from './counters'
import { ThemeType } from './themes'

export type ServiceCategory = 'Marketing' | 'Creative'

export interface ServiceHero {
    type: 'image' | 'svg'
    theme?: ThemeType
    imgHeight?: number
    imgWidth?: number
    inlineImg?: boolean | string
}

interface ServiceCounter extends Counter {
    text: string
}

export interface ServiceCounters {
    theme?: ThemeType
    counters: ServiceCounter[]
}

export interface ServiceIcon {
    icon: string
    title: string
    text: string
}
export interface ServiceIcons {
    theme?: ThemeType
    headline?: string
    icons: ServiceIcon[]
}

export interface ServiceItem extends ContentItem {
    category: ServiceCategory
    hero: ServiceHero
    counters?: ServiceCounters
    icons?: ServiceIcons
    testimonials?: {
        theme: ThemeType
    }
    portfolio?: {
        theme?: ThemeType
        title: string
    }
    custom?: ComponentType
    form?: {
        theme?: ThemeType
        title?: string
    }
    formId?: string
}
