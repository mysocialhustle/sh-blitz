export type ThemeType =
    | 'dark'
    | 'light'
    | 'grey'
    | 'darkGrey'
    | 'transparentLight'
    | 'transparentDark'
    | 'none'

export interface Theme {
    color: string
    background: string
}
export type Themes = Partial<Record<ThemeType, Theme>>
