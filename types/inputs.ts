import {
    PropsWithoutRef,
    ComponentPropsWithoutRef,
    ComponentProps,
} from 'react'
export interface InputProps
    extends PropsWithoutRef<JSX.IntrinsicElements['input']> {
    /** Field name. */
    name: string
    /** Field label. */
    label: string
    /** Field type. Doesn't include radio buttons and checkboxes */
    type?: 'text' | 'password' | 'email' | 'number' | 'tel' | 'url'
    //outerProps?: PropsWithoutRef<JSX.IntrinsicElements['div']>
    labelProps?: ComponentPropsWithoutRef<'label'>
    outerProps?: ComponentPropsWithoutRef<'div'>
    element?: any
}

export type InputOrTextareaProps = InputProps &
    PropsWithoutRef<JSX.IntrinsicElements['textarea']>

export interface FileInputProps {
    label?: string
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
    children: React.ReactNode
    name: string
    uploadPath?: string
}

export interface FileInputState {
    file: any
    stage: 'empty' | 'pending' | 'signed' | 'complete' | 'error'
    fileLink: string | null
    disabled: boolean
    message: string | null
    presignedPost: any
}

export interface S3SignedPost {
    url: string
    fields: {
        key: string
        bucket: string
    }
}
