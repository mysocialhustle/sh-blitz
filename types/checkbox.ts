export interface CheckboxInput {
    label: string
    value: string
    name?: string
    onChange?: (value: any) => void
    checked?: boolean
}

export interface CheckboxProps {
    inputs: CheckboxInput[] | string[]
    name: string
    onChange?: (value: any) => void
}
