export interface Note {
    label: string
    value: string
}

export type Notes = Note[]
