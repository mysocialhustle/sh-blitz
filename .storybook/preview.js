import { addDecorator } from '@storybook/react'
import GlobalStyle from '../app/styles/global'

addDecorator((s) => (
    <>
        <GlobalStyle />
        {s()}
    </>
))

export const parameters = {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
}
