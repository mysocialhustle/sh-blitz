-- AlterTable
ALTER TABLE "Campaign" ADD COLUMN     "onboarded" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "Content" ADD COLUMN     "onboarded" BOOLEAN NOT NULL DEFAULT false;

-- AlterTable
ALTER TABLE "Website" ADD COLUMN     "onboarded" BOOLEAN NOT NULL DEFAULT false;
