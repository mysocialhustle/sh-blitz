/*
  Warnings:

  - The values [photo,vid] on the enum `ContentType` will be removed. If these variants are still used in the database, this will fail.

*/
-- CreateEnum
CREATE TYPE "WebsiteType" AS ENUM ('wordpress', 'ecommerce', 'custom');

-- AlterEnum
BEGIN;
CREATE TYPE "ContentType_new" AS ENUM ('photography', 'graphicDesign', 'videography');
ALTER TABLE "Content" ALTER COLUMN "type" TYPE "ContentType_new" USING ("type"::text::"ContentType_new");
ALTER TYPE "ContentType" RENAME TO "ContentType_old";
ALTER TYPE "ContentType_new" RENAME TO "ContentType";
DROP TYPE "ContentType_old";
COMMIT;

-- AlterTable
ALTER TABLE "Website" ADD COLUMN     "type" "WebsiteType";
