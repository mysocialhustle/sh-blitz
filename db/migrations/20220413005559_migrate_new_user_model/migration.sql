/*
  Warnings:

  - The `databox` column on the `Campaign` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Campaign" ALTER COLUMN "notes" DROP NOT NULL,
ALTER COLUMN "budget" DROP NOT NULL,
ALTER COLUMN "goals" DROP NOT NULL,
ALTER COLUMN "logins" DROP NOT NULL,
DROP COLUMN "databox",
ADD COLUMN     "databox" JSONB,
ALTER COLUMN "reports" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Company" ALTER COLUMN "notes" DROP NOT NULL,
ALTER COLUMN "contactInfo" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Content" ALTER COLUMN "notes" DROP NOT NULL;

-- AlterTable
ALTER TABLE "Website" ALTER COLUMN "notes" DROP NOT NULL,
ALTER COLUMN "url" DROP NOT NULL,
ALTER COLUMN "logins" DROP NOT NULL;
