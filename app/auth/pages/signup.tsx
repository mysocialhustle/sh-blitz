import { useRouter, BlitzPage, Routes } from 'blitz'
import Layout from 'app/core/layouts/Layout'
import { SignupForm } from 'app/auth/components/SignupForm'
import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'

const SignupPage: BlitzPage = () => {
    const router = useRouter()

    return (
        <Section>
            <Container>
                <Row alignX="center">
                    <Column sm={55}>
                        <SignupForm
                            onSuccess={() => router.push(Routes.AccountPage())}
                        />
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}

SignupPage.redirectAuthenticatedTo = '/account'
SignupPage.getLayout = (page) => <Layout title="Sign Up">{page}</Layout>

export default SignupPage
