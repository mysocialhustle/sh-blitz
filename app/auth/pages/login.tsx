import { useRouter, BlitzPage } from 'blitz'
import Layout from 'app/core/layouts/Layout'
import { LoginForm } from 'app/auth/components/LoginForm'
import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'

const LoginPage: BlitzPage = () => {
    const router = useRouter()

    return (
        <Section>
            <Container>
                <Row alignX="center">
                    <Column sm={55}>
                        <LoginForm
                            onSuccess={(_user) => {
                                const next = router.query.next
                                    ? decodeURIComponent(
                                          router.query.next as string
                                      )
                                    : '/'
                                router.push(next)
                            }}
                        />
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}

LoginPage.redirectAuthenticatedTo = '/account'
LoginPage.getLayout = (page) => <Layout title="Log In">{page}</Layout>

export default LoginPage
