import { useMutation } from 'blitz'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import signup from 'app/auth/mutations/signup'
import { Signup } from 'app/auth/validations'
import Headings from 'app/styles/headings'
import Password from 'app/core/components/Form/Password/Password'

type SignupFormProps = {
    onSuccess?: () => void
}

export const SignupForm = (props: SignupFormProps) => {
    const [signupMutation] = useMutation(signup)

    return (
        <div>
            <Headings.H2 as="h1">Create an Account</Headings.H2>

            <Form
                submitText="Signup"
                schema={Signup}
                initialValues={{ email: '', password: '' }}
                buttonProps={{
                    style: { width: `100%` },
                }}
                onSubmit={async (values) => {
                    try {
                        await signupMutation(values)
                        props.onSuccess?.()
                    } catch (error: any) {
                        if (
                            error.code === 'P2002' &&
                            error.meta?.target?.includes('email')
                        ) {
                            // This error comes from Prisma
                            return { email: 'This email is already being used' }
                        } else {
                            return { [FORM_ERROR]: error.toString() }
                        }
                    }
                }}
            >
                <Input name="email" label="Email" />
                <Password name="password" label="Password" />
            </Form>
        </div>
    )
}

export default SignupForm
