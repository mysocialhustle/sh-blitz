import { BlitzApiRequest, BlitzApiResponse } from 'blitz'
import aws from 'aws-sdk'

const s3 = new aws.S3({
    region: 'us-east-1',
    credentials: {
        accessKeyId: 'AKIAXJZOCD6OEOMY3EAW',
        secretAccessKey: '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',
    },
})
const S3_BUCKET = 'socialhustle-blitz-static'

let presignedPost

export default async function handler(
    req: BlitzApiRequest,
    res: BlitzApiResponse
) {
    const { name, type } = req.body

    if (name && type) {
        const s3Params = {
            Bucket: S3_BUCKET,
            Fields: {
                key: name,
            },
            Conditions: [['content-length-range', 0, 100000000]],
            ContentType: type,
        }

        presignedPost = await s3.createPresignedPost(s3Params)
    }

    res.statusCode = 200
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ ...presignedPost }))
}
