import { BlitzApiRequest, BlitzApiResponse, Ctx } from 'blitz'
import db from 'db'

export default async function handler(
    req: BlitzApiRequest,
    res: BlitzApiResponse
) {
    const INSTAGRAM_ID = process.env.BLITZ_PUBLIC_INSTAGRAM_ID
    const INSTAGRAM_ACCESS_TOKEN =
        process.env.BLITZ_PUBLIC_INSTAGRAM_ACCESS_TOKEN
    const MAX_POSTS = 20
    const FIELDS = `media_url,thumbnail_url,caption,media_type,like_count,shortcode,timestamp,comments_count,username,children{media_url},permalink`

    const ENDPOINT = `https://graph.facebook.com/v7.0/${INSTAGRAM_ID}/media?fields=${FIELDS}&limit=${MAX_POSTS}&access_token=${INSTAGRAM_ACCESS_TOKEN}`

    const fetcher = await fetch(ENDPOINT, {
        method: 'GET',
    })
    const response = await fetcher.json()
    const data = { data: response?.data }
    const name = 'instagram'

    if (fetcher.ok) {
        const dataFeed = await db.dataFeed.update({ where: { name }, data })
        console.log(dataFeed)
    }

    res.statusCode = fetcher.status
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ data: response?.data }))
}
