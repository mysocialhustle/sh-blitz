import { BlitzApiRequest, BlitzApiResponse, Ctx } from 'blitz'
import db from 'db'

export default async function handler(
    req: BlitzApiRequest,
    res: BlitzApiResponse
) {
    const GOOGLE_ADS_PLAYLIST_ID =
        process.env.BLITZ_PUBLIC_GOOGLE_ADS_PLAYLIST_ID
    const PROMOTIONAL_VIDEOS_PLAYLIST_ID =
        process.env.BLITZ_PUBLIC_PROMOTIONAL_VIDEOS_PLAYLIST_ID
    const YOUTUBE_API_KEY = process.env.BLITZ_PUBLIC_YOUTUBE_API_KEY
    const YOUTUBE_CHANNEL_ID = process.env.BLITZ_PUBLIC_YOUTUBE_CHANNEL_ID

    const defaults = {
        maxResults: 1,
        order: `date`,
        part: `snippet,id`,
    }

    const vars = {
        youtube: {
            query: `search`,
            params: {
                ...defaults,
                channelId: YOUTUBE_CHANNEL_ID,
            },
        },
        googleAdsAcademyPlaylist: {
            query: `playlistItems`,
            params: {
                ...defaults,
                playlistId: GOOGLE_ADS_PLAYLIST_ID,
            },
        },
        promotionalVideosPlaylist: {
            query: `playlistItems`,
            params: {
                ...defaults,
                playlistId: PROMOTIONAL_VIDEOS_PLAYLIST_ID,
            },
        },
    }

    const getUrl = (key) => {
        const queryParams = vars[key]
        const { query, params } = queryParams
        const queryStringMap = Object.entries(params).map(([prop, val]) => {
            return `${prop}=${val}`
        })
        const url = `https://www.googleapis.com/youtube/v3/${query}?key=${YOUTUBE_API_KEY}&${queryStringMap.join(
            '&'
        )}`
        return url
    }

    const fetchAll = async (keys: string[]) => {
        const res = await Promise.all(
            keys.map((u) => fetch(u, { method: 'GET' }))
        )
        const jsons = await Promise.all(res.map((r) => r.json()))

        return jsons
    }

    const urls = Object.keys(vars).map((key) => getUrl(key))

    fetchAll(urls).then((res) => console.log(res))

    /**if (fetcher.ok) {
        const dataFeed = await db.dataFeed.update({ where: { name }, data })
        console.log(dataFeed)
    }**/

    res.statusCode = 200
    res.setHeader('Content-Type', 'application/json')
    res.end(JSON.stringify({ data: 'ok' }))
}
