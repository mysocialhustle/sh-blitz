import styled from 'styled-components'

export const Table = styled.table`
    width: 100%;
    vertical-align: middle;
    border: medium none;
    border-spacing: 0px;
    table-layout: auto;
    border-collapse: collapse;
    margin-bottom: 20px;
`

export const TableHeading = styled.th`
    font-weight: bold;
    text-align: left;
    padding: 10px 15px;
    border: 1px solid rgba(0, 0, 0, 0.3);
`

export const HeadingInner = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`

export const TableCell = styled.td`
    vertical-align: middle;
    padding: 15px;
    border: 1px solid rgba(0, 0, 0, 0.3);
    position: relative;

    & > a {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }
`
