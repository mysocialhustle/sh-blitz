import styled from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import fonts from 'app/core/constants/fonts'

const headingBase = `
    font-family: ${fonts.headings};
    line-height: 1.1em;
    word-wrap: break-word;
`

export const h1 = headingBase.concat(`
    margin-bottom: 50px;
    word-wrap: normal;
    font-size: 3rem;
    font-weight: 800;
    letter-spacing: -0.05em;
    ${mediaQueries.md} {
        font-size: 6rem;
    }
`)

export const h2 = headingBase.concat(`
margin-bottom: 50px;
word-wrap: normal;
font-size: 2rem;
font-weight: 800;
letter-spacing: -0.05em;
${mediaQueries.md} {
    font-size: 4rem;
}
`)

export const h3 = headingBase.concat(`
font-size: 1rem;
font-weight: 700;
letter-spacing: -0.025em;
${mediaQueries.md} {
    font-size: 2rem;
}

`)

export const h4 = headingBase.concat(`
font-size: 1rem;
font-weight: 600;
letter-spacing: 0.1em;
${mediaQueries.md} {
    font-size: 1.5rem;
}
`)

export const h5 = headingBase.concat(`
font-size: 0.875rem;
font-weight: 700;
letter-spacing: 0.025em;
${mediaQueries.md} {
    font-size: 1rem;
}
`)

export const h6 = headingBase.concat(`
font-size: 0.75rem;
font-weight: 700;
letter-spacing: 0.025em;
${mediaQueries.md} {
    font-size: 0.875rem;
}
`)

const Headings = {
    H1: styled.h1`
        ${h1}
    `,
    H2: styled.h2`
        ${h2}
    `,
    H3: styled.h3`
        ${h3}
    `,
    H4: styled.h4`
        ${h4}
    `,
    H5: styled.h5`
        ${h5}
    `,
    H6: styled.h6`
        ${h6}
    `,
}

export default Headings
