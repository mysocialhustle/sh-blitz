import styled from 'styled-components'
import headerHeight from 'app/core/constants/headerHeight'

export const PageWrapper = styled.div`
    margin-top: ${headerHeight}px;
`

export const Divider = styled.span`
    &:after {
        content: '|';
        text-align: center;
        display: inline-block;
        width: 1rem;
    }
`
