import styled from "styled-components"
import { gutter, maxWidth, columns } from "app/core/constants/grid"
import mediaQueries from "app/core/constants/mediaQueries"
import breakpoints from "app/core/constants/breakpoints"
import { StyledProps } from "types"

const getColumnWidth = (number) => (parseInt(number) / columns) * 100 + "%"

function makeColumn(breakpoint, columnCount) {
  const columnWidth = getColumnWidth(columnCount)

  return `
		${mediaQueries[breakpoint]} {
			width: ${columnWidth}
		}
	`
}

export const Container = styled.div<StyledProps>`
  max-width: ${(props) => (props.fluid ? "100%" : `${maxWidth}px`)};
  padding-right: ${gutter}px;
  padding-left: ${gutter}px;
  margin-left: auto;
  margin-right: auto;
  width: 100%;
`

export const Row = styled.div<StyledProps>`
  display: flex;
  flex-flow: row wrap;
  margin-right: -${gutter}px;
  margin-left: -${gutter}px;
  justify-content: ${(props) => (props.alignX ? props.alignX : "flex-start")};
  align-items: ${(props) => (props.alignY ? props.alignY : "flex-start")};

  ${(props) =>
    props.reverse &&
    `
      
        ${mediaQueries.sm} {
            flex-direction: row-reverse;
        }

    `}
`

export const Column = styled.div<StyledProps>`
  padding-right: ${gutter}px;
  padding-left: ${gutter}px;
  min-height: 1px;
  position: relative;
  width: ${(props) => (props.xs ? getColumnWidth(props.xs) : `100%`)};

  ${(props) =>
    Object.keys(props)
      .filter((p) => Object.keys(breakpoints).includes(p))
      // Make column with breakpoint & column count params
      .map((p) => makeColumn(p, props[p]))}
`
