import { Suspense } from 'react'
import { useQuery } from 'blitz'
import getDataFeed from 'app/data-feeds/queries/getDataFeed'
import { ErrorBoundary } from 'blitz'
import Video from 'app/data-feeds/components/YouTube/Video/Video'
import { Styled } from '../Video/Video.styles'

function PlaylistVideo({ playlistKey }: { playlistKey: string }) {
    const [playlist] = useQuery(getDataFeed, { name: playlistKey })
    const { data }: { data: any } = playlist
    const { items } = data
    const [video] = items

    return (
        <Video
            id={video?.snippet?.resourceId?.videoId}
            title={video?.snippet?.title}
        />
    )
}

export default function Playlist({ playlistKey }: { playlistKey: string }) {
    return (
        <ErrorBoundary
            FallbackComponent={(error) => (
                <div>Error: {JSON.stringify(error)}</div>
            )}
        >
            <Suspense fallback={<Styled.Placeholder />}>
                <PlaylistVideo playlistKey={playlistKey} />
            </Suspense>
        </ErrorBoundary>
    )
}
