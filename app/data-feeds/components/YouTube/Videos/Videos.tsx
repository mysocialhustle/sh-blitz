import { Suspense } from 'react'
import { useQuery } from 'blitz'
import getDataFeed from 'app/data-feeds/queries/getDataFeed'
import { ErrorBoundary } from 'blitz'
import Video from 'app/data-feeds/components/YouTube/Video/Video'
import { Styled } from '../Video/Video.styles'

function SingleVideo() {
    const [videos] = useQuery(getDataFeed, { name: 'youtube' })
    const { data }: { data: any } = videos
    const { items } = data
    const [video] = items

    return <Video id={video?.id?.videoId} title={video?.snippet?.title} />
}

export default function Videos() {
    return (
        <ErrorBoundary
            FallbackComponent={(error) => (
                <div>Error: {JSON.stringify(error)}</div>
            )}
        >
            <Suspense fallback={<Styled.Placeholder />}>
                <SingleVideo />
            </Suspense>
        </ErrorBoundary>
    )
}
