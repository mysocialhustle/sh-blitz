import React from 'react'
import LiteYouTubeEmbed from 'react-lite-youtube-embed'
import { Styled } from './Video.styles'

export default function Video({ id, title, ...rest }) {
    return (
        <Styled.Video key={id}>
            <LiteYouTubeEmbed id={id} title={title} {...rest} />
        </Styled.Video>
    )
}
