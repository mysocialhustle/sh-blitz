/* eslint-disable jsx-a11y/alt-text */
import { useState, useCallback, useMemo, useEffect } from 'react'
import useDimensions from 'app/core/hooks/useDimensions'
import { wrap } from 'popmotion'
import { Styled } from './Carousel.styles'
import Image from '../Image/Image'
import { useQuery } from 'blitz'
import getDataFeed from 'app/data-feeds/queries/getDataFeed'
import { useAnimation } from 'framer-motion'

export function PlaceholderCarousel() {
    const [trackRef, dimensions] = useDimensions()

    const visibleItems = Math.ceil(dimensions.width / 200)

    return (
        <Styled.Wrapper>
            <Styled.Carousel ref={trackRef}>
                {Array.from(Array(visibleItems).keys()).map((index) => (
                    <div
                        key={index}
                        style={{ width: `${(1 / visibleItems) * 100}%` }}
                    >
                        <Styled.Placeholder />
                    </div>
                ))}
            </Styled.Carousel>
        </Styled.Wrapper>
    )
}

export function Carousel({ captionLength }) {
    const [feed] = useQuery(getDataFeed, { name: 'instagram' })
    const { data: items }: { data: any } = feed
    const [page, setPage] = useState(0)
    const [trackRef, dimensions] = useDimensions()
    const paginate = useCallback(
        function (direction) {
            const newPage = wrap(0, items.length, page + direction)
            setPage(newPage)
        },
        [page, items.length]
    )
    const visibleItems = Math.min(
        Math.ceil(dimensions.width / 200),
        items.length
    )
    const itemWidth = useMemo(() => {
        const finalWidth = (1 / visibleItems) * 100
        return isNaN(finalWidth) ? 0 : finalWidth
    }, [visibleItems])

    const controls = useAnimation()

    useEffect(() => {
        const scrollToPage = wrap(0, visibleItems, page)
        const scrollPercent = itemWidth * -scrollToPage
        controls.start({
            x: `${isNaN(scrollPercent) ? 0 : scrollPercent}%`,
        })
    }, [visibleItems, page, itemWidth, controls])

    return (
        <Styled.Wrapper>
            <Styled.Arrow position="left" onClick={() => paginate(1)} />
            <Styled.Arrow position="right" onClick={() => paginate(-1)} />
            <Styled.Carousel
                ref={trackRef}
                animate={controls}
                transition={{
                    duration: 0.4,
                    ease: [0.37, 0, 0.63, 1],
                }}
            >
                {items.length &&
                    items.map((node, index) => (
                        <Styled.Item
                            key={index}
                            style={{ width: `${itemWidth}%` }}
                        >
                            <Image item={node} caption={captionLength} />
                        </Styled.Item>
                    ))}
            </Styled.Carousel>
        </Styled.Wrapper>
    )
}
