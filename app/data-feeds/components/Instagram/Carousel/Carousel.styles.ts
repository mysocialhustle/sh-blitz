import styled from 'styled-components'
import { StyledProps } from 'types'
import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export namespace Styled {
    export const Placeholder = styled.div`
        padding-bottom: 100%;
        background-color: ${colors.lightGrey};
        width: 100%;
    `
    export const Wrapper = styled.div`
        overflow: hidden;
        position: relative;
    `
    export const Carousel = styled(motion.div)`
        display: flex;
        flex-flow: row nowrap;
    `

    export const Arrow = styled.div<StyledProps>`
        position: absolute;
        transform: translateY(-50%);
        top: 50%;
        color: white;
        padding: 10px;
        background: black;
        z-index: 50;
        line-height: 1;
        opacity: 0.5;
        transition: opacity 0.2s ease-in;
        cursor: pointer;
        &:hover {
            opacity: 1;
        }
        ${(props) =>
            props.position === `left` &&
            `
        left: 20px;
        &:after{
            content: '\\2190'
        }
    `}
        ${(props) =>
            props.position === `right` &&
            `
        right: 20px;
        &:after{
            content: '\\2192'
        }
    `}
    `
    export const Item = styled.div`
        flex: 1 0 auto;
    `
}
