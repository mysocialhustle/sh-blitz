import { Styled } from './Image.styles'
import Icon from 'app/core/components/Icon/Icon'

export default function MediaIcon({ type }) {
    return type === 'VIDEO' ? (
        <Styled.MediaIcon>
            <Icon name="video" />
        </Styled.MediaIcon>
    ) : type === 'CAROUSEL_ALBUM' ? (
        <Styled.MediaIcon>
            <Icon name="carousel_album" />
        </Styled.MediaIcon>
    ) : null
}
