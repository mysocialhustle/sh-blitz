import styled from 'styled-components'

export namespace Styled {
    export const ImageWrapper = styled.div`
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        transform-origin: center center;
        transition: all 0.5s;
    `

    export const MediaIcon = styled.span`
        position: absolute;
        filter: drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.4));
        color: rgba(255, 255, 255, 0.8);
        top: 0;
        right: 0;
        padding: 10px;
        font-size: 1.25em;
        z-index: 1;
        line-height: 1;
    `

    export const InfoWrapper = styled.div`
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background-color: rgba(0, 0, 0, 0.6);
        opacity: 0;
        transition: opacity 0.2s ease-in-out;
        display: flex;
        flex-flow: column wrap;
        justify-content: space-evenly;
        text-align: center;
        font-size: 12px;
        color: white;
        z-index: 1;
        padding: 5px;

        span,
        svg {
            display: inline-block;
            margin: 5px 2px;
        }
    `

    export const Info = styled.div`
        width: 100%;
    `

    export const Link = styled.a`
        flex: 1 0 auto;
        display: block;
        position: relative;
        overflow: hidden;
        padding-bottom: 100%;

        &:hover {
            ${InfoWrapper} {
                opacity: 1;
            }
            ${ImageWrapper} {
                transform: scale(1.05);
            }
        }
    `
}
