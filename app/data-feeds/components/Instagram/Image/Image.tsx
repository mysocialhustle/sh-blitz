import { Styled } from './Image.styles'
import { Image as BlitzImage } from 'blitz'
import MediaIcon from './MediaIcon'
import truncateString from 'app/core/utils/truncateString'
import Icon from 'app/core/components/Icon/Icon'

interface Props {
    item: any
    caption?: boolean | number
    likes?: boolean
    comments?: boolean
}

export default function Image({
    item,
    caption = false,
    likes = true,
    comments = true,
}: Props) {
    const imageUrl =
        item?.media_type === 'VIDEO' ? item?.thumbnail_url : item?.media_url

    return (
        <Styled.Link href={item?.permalink} target="_blank">
            <Styled.ImageWrapper>
                <MediaIcon type={item?.media_type} />
                <Styled.InfoWrapper>
                    {caption && typeof caption === 'number' && (
                        <Styled.Info>
                            <span>
                                {truncateString(item?.caption, caption)}
                            </span>
                        </Styled.Info>
                    )}
                    <Styled.Info>
                        {likes && (
                            <>
                                <Icon name="heart" />
                                <span>{item?.like_count}</span>
                            </>
                        )}
                        {comments && (
                            <>
                                <Icon name="comment" />
                                <span>{item?.comments_count}</span>
                            </>
                        )}
                    </Styled.Info>
                </Styled.InfoWrapper>
                <BlitzImage
                    src={imageUrl}
                    alt={item?.caption}
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center center"
                />
            </Styled.ImageWrapper>
        </Styled.Link>
    )
}
