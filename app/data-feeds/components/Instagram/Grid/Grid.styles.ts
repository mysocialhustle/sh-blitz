import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import { Column as GridColumn } from 'app/styles/grid'
import { gutter } from 'app/core/constants/grid'

export namespace Styled {
    export const Column = styled(GridColumn)`
        padding-right: ${gutter / 2}px;
        padding-left: ${gutter / 2}px;
        margin-bottom: ${gutter}px;
    `
    export const Placeholder = styled.div`
        padding-bottom: 100%;
        background-color: ${colors.lightGrey};
    `
}
