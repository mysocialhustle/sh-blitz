/* eslint-disable jsx-a11y/alt-text */
import { useQuery } from 'blitz'
import getDataFeed from 'app/data-feeds/queries/getDataFeed'
import { Row } from 'app/styles/grid'
import Image from '../Image/Image'
import { Styled } from './Grid.styles'
import { ResponsiveProp } from 'types'

interface Props extends ResponsiveProp {
    count: number
}

export function PlaceholderGrid({ count, ...rest }) {
    return (
        <Row>
            {Array.from(Array(count).keys()).map((index) => {
                return (
                    <Styled.Column key={index} {...rest}>
                        <Styled.Placeholder />
                    </Styled.Column>
                )
            })}
        </Row>
    )
}

export function Grid({ count, ...rest }: Props) {
    const [feed] = useQuery(getDataFeed, { name: 'instagram' })
    const { data: items }: { data: any } = feed
    const slice = items?.slice(0, count)

    return (
        <Row>
            {slice?.map((item, index) => {
                return (
                    <Styled.Column key={index} {...rest}>
                        <Image key={index} item={item} />
                    </Styled.Column>
                )
            })}
        </Row>
    )
}
