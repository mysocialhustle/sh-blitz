import { ComponentType, Suspense } from 'react'
import { ErrorBoundary } from 'blitz'

type ComponentProps = {
    [key: string]: string | number | boolean
}

interface Props {
    Component: ComponentType
    Placeholder: ComponentType
    props?: ComponentProps
}

export function Feed({ Component, Placeholder, props = {} }: Props) {
    return (
        <ErrorBoundary
            FallbackComponent={(error) => (
                <div>Error: {JSON.stringify(error)}</div>
            )}
        >
            <Suspense fallback={<Placeholder {...props} />}>
                <Component {...props} />
            </Suspense>
        </ErrorBoundary>
    )
}
