import { resolver, NotFoundError } from 'blitz'
import db from 'db'
import { z } from 'zod'

const GetDataFeed = z.object({
    // This accepts type of undefined, but is required at runtime
    name: z.string(),
})

export default resolver.pipe(resolver.zod(GetDataFeed), async ({ name }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const dataFeed = await db.dataFeed.findFirst({ where: { name } })

    if (!dataFeed) throw new NotFoundError()

    return dataFeed
})
