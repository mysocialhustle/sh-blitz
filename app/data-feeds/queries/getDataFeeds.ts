import { paginate, resolver } from 'blitz'
import db, { Prisma } from 'db'

interface GetDataFeedsInput
    extends Pick<
        Prisma.DataFeedFindManyArgs,
        'where' | 'orderBy' | 'skip' | 'take'
    > {}

export default resolver.pipe(
    resolver.authorize(),
    async ({ where, orderBy, skip = 0, take = 100 }: GetDataFeedsInput) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const {
            items: dataFeeds,
            hasMore,
            nextPage,
            count,
        } = await paginate({
            skip,
            take,
            count: () => db.dataFeed.count({ where }),
            query: (paginateArgs) =>
                db.dataFeed.findMany({ ...paginateArgs, where, orderBy }),
        })

        return {
            dataFeeds,
            nextPage,
            hasMore,
            count,
        }
    }
)
