import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const UpdateDataFeed = z.object({
    name: z.string(),
    data: z.any(),
})

export default resolver.pipe(
    resolver.zod(UpdateDataFeed),
    resolver.authorize(),
    async ({ name, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const dataFeed = await db.dataFeed.update({ where: { name }, data })

        return dataFeed
    }
)
