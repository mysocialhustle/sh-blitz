import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Social({ variants }) {
    return (
        <svg viewBox="0 0 64 59" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 0.000000)">
                    <g transform="translate(57.000000, 23.500000)">
                        <motion.polyline
                            variants={variants}
                            points="3 13 3 5 0 5"
                        ></motion.polyline>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="13"
                            x2="6"
                            y2="13"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="3"
                            y1="2"
                            x2="3"
                            y2="0"
                        ></motion.line>
                    </g>
                    <g transform="translate(3.000000, 8.500000)">
                        <motion.polyline
                            variants={variants}
                            points="3 12 3 0 8 0"
                        ></motion.polyline>
                        <motion.line
                            variants={variants}
                            x1="6"
                            y1="6"
                            x2="0"
                            y2="6"
                        ></motion.line>
                    </g>
                    <rect x="0" y="4.5" width="14" height="19"></rect>
                    <g transform="translate(18.000000, 13.500000)">
                        <motion.polyline
                            variants={variants}
                            strokeLinejoin="round"
                            points="28 24 30 24 36 30 36 23"
                        ></motion.polyline>
                        <motion.polyline
                            variants={variants}
                            points="36 23 36 0 0 0 0 24 28 24"
                        ></motion.polyline>
                        <motion.polyline
                            variants={variants}
                            points="0 0 18 12 36 0"
                        ></motion.polyline>
                        <g transform="translate(13.000000, 17.500000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="2"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="4"
                                y1="0.5"
                                x2="6"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="8"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                        </g>
                    </g>
                    <g
                        transform="translate(0.000000, 29.500000)"
                        stroke={colors.red}
                    >
                        <motion.polyline
                            variants={variants}
                            points="12 20 36 20 36 11"
                        ></motion.polyline>
                        <motion.polyline
                            variants={variants}
                            strokeLinejoin="round"
                            points="4 22 4 26 10 20 12 20"
                        ></motion.polyline>
                        <motion.polyline
                            variants={variants}
                            points="15 0 0 0 0 20 4 20 4 22"
                        ></motion.polyline>
                        <g transform="translate(3.000000, 3.500000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="12"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="4.5"
                                x2="12"
                                y2="4.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="8.5"
                                x2="30"
                                y2="8.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="12"
                                y1="12.5"
                                x2="30"
                                y2="12.5"
                            ></motion.line>
                        </g>
                    </g>
                    <g transform="translate(41.000000, 46.500000)">
                        <motion.path
                            variants={variants}
                            d="M9,9 C9,10.656 7.657,12 6,12 C4.343,12 3,10.656 3,9 L3,0"
                        ></motion.path>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="3"
                            x2="6"
                            y2="3"
                        ></motion.line>
                    </g>
                    <g transform="translate(51.000000, 4.500000)">
                        <motion.line
                            variants={variants}
                            x1="3"
                            y1="0"
                            x2="3"
                            y2="6"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="3"
                            x2="6"
                            y2="3"
                        ></motion.line>
                    </g>
                    <g
                        transform="translate(22.000000, 0.000000)"
                        stroke={colors.red}
                    >
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="0.5"
                            x2="16"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="4.5"
                            x2="8"
                            y2="4.5"
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
