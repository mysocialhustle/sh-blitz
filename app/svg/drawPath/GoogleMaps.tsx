import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function GoogleMaps({ variants }) {
    return (
        <svg viewBox="0 0 99 112">
            <g
                strokeWidth="1"
                fill="none"
                fillRule="evenodd"
                strokeLinecap="round"
                strokeLinejoin="round"
            >
                <g transform="translate(1.000000, 1.000000)">
                    <motion.path
                        variants={variants}
                        d="M48,0 C31.4314575,0 18,13.3115338 18,29.7321429 C18,50.4321429 48,90 48,90 C48,90 78,50.4321429 78,29.7321429 C78,13.3115338 64.5685425,0 48,0 Z"
                    ></motion.path>
                    <motion.circle
                        stroke={colors.red}
                        variants={variants}
                        cx="48.5"
                        cy="30.5"
                        r="18.5"
                    ></motion.circle>
                    <motion.polyline
                        variants={variants}
                        points="69.7116982 56 84.5889 60.6296633 97 106.910221 71.613659 96.4774037 48.5 109 25.386341 96.4774037 0 106.910221 12.4111 60.6296633 27.2883018 56"
                    ></motion.polyline>
                    <motion.line
                        variants={variants}
                        x1="71"
                        y1="97"
                        x2="65"
                        y2="64"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="31"
                        y1="64"
                        x2="25"
                        y2="97"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="48"
                        y1="110"
                        x2="48"
                        y2="90"
                    ></motion.line>
                </g>
            </g>
        </svg>
    )
}
