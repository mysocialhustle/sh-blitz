import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Communication({ variants }) {
    return (
        <svg viewBox="0 0 48 64" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <g transform="translate(0.000000, 30.000000)">
                        <g
                            transform="translate(12.000000, 0.000000)"
                            stroke={colors.red}
                        >
                            <motion.circle
                                cx="11"
                                cy="5"
                                r="5"
                                variants={variants}
                            ></motion.circle>
                            <motion.path
                                d="M18,30 L22,30 L22,20 C22,18.465 21.414,16.929 20.242,15.758 C19.071,14.586 17.535,14 16,14 L15,14 L11,18 L7,14 L6,14 C4.465,14 2.929,14.586 1.758,15.758 C0.586,16.929 0,18.465 0,20 L0,30 L4,30"
                                variants={variants}
                            ></motion.path>
                            <motion.line
                                x1="18"
                                y1="33"
                                x2="18"
                                y2="27"
                                variants={variants}
                            ></motion.line>
                            <motion.line
                                x1="4"
                                y1="27"
                                x2="4"
                                y2="33"
                                variants={variants}
                            ></motion.line>
                            <motion.line
                                x1="11"
                                y1="21"
                                x2="11"
                                y2="23"
                                variants={variants}
                            ></motion.line>
                            <motion.line
                                x1="11"
                                y1="25"
                                x2="11"
                                y2="27"
                                variants={variants}
                            ></motion.line>
                        </g>
                        <g transform="translate(0.000000, 4.000000)">
                            <motion.circle
                                cx="8"
                                cy="4"
                                r="4"
                                variants={variants}
                                strokeDasharray="24.491737365722656"
                            ></motion.circle>
                            <motion.path
                                d="M9,12 L4,12 C2.977,12 1.952,12.391 1.172,13.172 C0.391,13.953 0,14.977 0,16 L0,26 L3,26 L3,29"
                                variants={variants}
                                strokeDasharray="27.283607482910156"
                            ></motion.path>
                        </g>
                        <g transform="translate(34.000000, 4.000000)">
                            <motion.circle
                                cx="4"
                                cy="4"
                                r="4"
                                variants={variants}
                            ></motion.circle>
                            <motion.path
                                d="M3,12 L8,12 C9.023,12 10.048,12.391 10.828,13.172 C11.609,13.953 12,14.977 12,16 L12,26 L9,26 L9,29"
                                variants={variants}
                            ></motion.path>
                        </g>
                    </g>
                    <g transform="translate(18.000000, 25.500000)">
                        <motion.line
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                    </g>
                    <motion.path
                        d="M30,26 L45,26 C45.256,26 45.512,25.902 45.707,25.707 C45.902,25.512 46,25.256 46,25 L46,1 C46,0.744 45.902,0.488 45.707,0.293 C45.512,0.098 45.256,0 45,0 L1,0 C0.744,0 0.488,0.098 0.293,0.293 C0.098,0.488 0,0.744 0,1 L0,31.586 C0,31.979 0.232,32.35 0.617,32.51 C1.002,32.669 1.43,32.571 1.707,32.293 L8,26 L16,26"
                        variants={variants}
                    ></motion.path>
                </g>
            </g>
        </svg>
    )
}
