import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function CRO({ variants }) {
    return (
        <svg viewBox="0 0 102 102">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 0.000000)" strokeWidth="1">
                    <motion.polyline
                        points="101 101 0 101 0 0"
                        variants={variants}
                    ></motion.polyline>
                    <motion.path
                        d="M10,65.5753425 L22.7692308,65.5753425 L22.7692308,91 L10,91 L10,65.5753425 Z M33.5570292,34.4520548 L46.3262599,34.4520548 L46.3262599,91 L33.5570292,91 L33.5570292,34.4520548 Z M56.8938992,59 L69.66313,59 L69.66313,91 L56.8938992,91 L56.8938992,59 Z M80.2307692,27 L93,27 L93,90.7808219 L80.2307692,90.7808219 L80.2307692,27 Z"
                        variants={variants}
                    ></motion.path>
                    <motion.polyline
                        stroke={colors.red}
                        points="14 43 40.672043 16.1837838 64.0376344 38.3459459 96 2"
                        variants={variants}
                    ></motion.polyline>
                </g>
            </g>
        </svg>
    )
}
