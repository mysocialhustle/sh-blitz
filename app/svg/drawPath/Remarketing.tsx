import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Remarketing({ variants }) {
    return (
        <svg viewBox="0 0 127 122">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)" strokeWidth="1">
                    <motion.line
                        variants={variants}
                        x1="22"
                        y1="32"
                        x2="22"
                        y2="12"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="102"
                        y1="12"
                        x2="102"
                        y2="32"
                    ></motion.line>
                    <motion.polyline
                        variants={variants}
                        points="103 100 103 120 22 120 22 100"
                    ></motion.polyline>
                    <motion.line
                        variants={variants}
                        x1="22"
                        y1="80"
                        x2="22"
                        y2="52"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="102"
                        y1="52"
                        x2="102"
                        y2="80"
                    ></motion.line>
                    <motion.rect
                        variants={variants}
                        x="22"
                        y="0"
                        width="81"
                        height="12"
                    ></motion.rect>
                    <motion.line
                        variants={variants}
                        x1="30"
                        y1="6"
                        x2="34"
                        y2="6"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="38"
                        y1="6"
                        x2="42"
                        y2="6"
                    ></motion.line>
                    <motion.line
                        variants={variants}
                        x1="46"
                        y1="6"
                        x2="50"
                        y2="6"
                    ></motion.line>
                    <g transform="translate(0.000000, 32.000000)">
                        <motion.rect
                            variants={variants}
                            stroke={colors.red}
                            x="112.903226"
                            y="0"
                            width="12.0967742"
                            height="12.1764706"
                        ></motion.rect>
                        <motion.polyline
                            variants={variants}
                            points="112.903226 6.08823529 106.854839 6.08823529 98.7903226 14.2058824 92.7419355 14.2058824"
                        ></motion.polyline>
                        <motion.rect
                            variants={variants}
                            stroke={colors.red}
                            x="0"
                            y="0"
                            width="12.0967742"
                            height="12.1764706"
                        ></motion.rect>
                        <motion.polyline
                            variants={variants}
                            points="12.0967742 6.08823529 18.1451613 6.08823529 26.2096774 14.2058824 32.2580645 14.2058824"
                        ></motion.polyline>
                        <g transform="translate(92.741935, 54.794118)">
                            <motion.rect
                                variants={variants}
                                stroke={colors.red}
                                x="20.1612903"
                                y="2.02941176"
                                width="12.0967742"
                                height="12.1764706"
                            ></motion.rect>
                            <motion.polyline
                                variants={variants}
                                points="20.1612903 8.11764706 14.1129032 8.11764706 6.0483871 0 0 0"
                            ></motion.polyline>
                        </g>
                        <g transform="translate(0.000000, 54.794118)">
                            <motion.rect
                                variants={variants}
                                stroke={colors.red}
                                x="0"
                                y="2.02941176"
                                width="12.0967742"
                                height="12.1764706"
                            ></motion.rect>
                            <motion.polyline
                                variants={variants}
                                points="12.0967742 8.11764706 18.1451613 8.11764706 26.2096774 0 32.2580645 0"
                            ></motion.polyline>
                        </g>
                    </g>
                    <g transform="translate(30.000000, 22.000000)">
                        <motion.rect
                            variants={variants}
                            x="2.03125"
                            y="24.2696629"
                            width="60.9375"
                            height="40.4494382"
                        ></motion.rect>
                        <motion.rect
                            variants={variants}
                            x="2.03125"
                            y="0"
                            width="60.9375"
                            height="8.08988764"
                        ></motion.rect>
                        <motion.line
                            variants={variants}
                            x1="65"
                            y1="16.1797753"
                            x2="0"
                            y2="16.1797753"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="65"
                            y1="72.8089888"
                            x2="24.375"
                            y2="72.8089888"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="65"
                            y1="80.8988764"
                            x2="24.375"
                            y2="80.8988764"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="65"
                            y1="88.988764"
                            x2="0"
                            y2="88.988764"
                        ></motion.line>
                        <motion.polygon
                            stroke={colors.red}
                            variants={variants}
                            points="26.40625 36.4044944 28.4375 36.4044944 42.65625 44.494382 28.4375 52.5842697 26.40625 52.5842697"
                        ></motion.polygon>
                        <motion.rect
                            variants={variants}
                            x="2.03125"
                            y="72.8089888"
                            width="16.25"
                            height="8.08988764"
                        ></motion.rect>
                        <motion.line
                            variants={variants}
                            x1="54.84375"
                            y1="0"
                            x2="54.84375"
                            y2="8.08988764"
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
