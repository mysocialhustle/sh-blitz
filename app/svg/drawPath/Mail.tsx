import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Mail({ variants }) {
    return (
        <svg viewBox="0 0 64 53" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <g transform="translate(0.000000, 11.000000)">
                        <motion.path
                            variants={variants}
                            d="M12,0 L7,0 C6.488,0 5.977,0.195 5.586,0.586 C5.195,0.977 5,1.488 5,2 L5,34"
                        ></motion.path>
                        <motion.path
                            variants={variants}
                            d="M50,0 L55,0 C55.512,0 56.023,0.195 56.414,0.586 C56.805,0.977 57,1.488 57,2 L57,34"
                        ></motion.path>
                        <motion.path
                            variants={variants}
                            d="M39,34 L37,36 L25,36 L23,34 L0,34 L0,36 C0,37.023 0.391,38.047 1.172,38.828 C1.953,39.609 2.977,40 4,40 L58,40 C59.023,40 60.048,39.609 60.828,38.828 C61.609,38.047 62,37.023 62,36 L62,34 L39,34 Z"
                        ></motion.path>
                        <motion.polyline
                            variants={variants}
                            points="50 4 53 4 53 30 9 30 9 4 12 4"
                        ></motion.polyline>
                    </g>
                    <g
                        transform="translate(15.000000, 0.000000)"
                        stroke={colors.red}
                    >
                        <motion.rect
                            variants={variants}
                            x="0"
                            y="0"
                            width="32"
                            height="22"
                        ></motion.rect>
                        <motion.polyline
                            variants={variants}
                            points="0 0 11 11 21 11 32 0"
                        ></motion.polyline>
                        <motion.line
                            variants={variants}
                            x1="11"
                            y1="11"
                            x2="0"
                            y2="22"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="21"
                            y1="11"
                            x2="32"
                            y2="22"
                        ></motion.line>
                        <g transform="translate(11.000000, 17.500000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="2"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="4"
                                y1="0.5"
                                x2="6"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="8"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                        </g>
                    </g>
                    <g
                        transform="translate(23.500000, 27.000000)"
                        stroke={colors.red}
                    >
                        <motion.line
                            variants={variants}
                            x1="0.5"
                            y1="3"
                            x2="0.5"
                            y2="9"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="14.5"
                            y1="3"
                            x2="14.5"
                            y2="9"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="7.5"
                            y1="0"
                            x2="7.5"
                            y2="6"
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
