import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function LocalServices({ variants }) {
    return (
        <svg viewBox="-15 0 121 157">
            <g
                strokeWidth="1"
                fill="none"
                fillRule="evenodd"
                strokeLinejoin="round"
            >
                <g transform="translate(1.000000, 1.000000)">
                    <motion.path
                        variants={variants}
                        d="M89,143.798828 C89,149.985205 83.9337285,155 77.6838488,155 L11.3161512,155 C5.06627148,155 0,149.985205 0,143.798828 L0,11.2011719 C0,5.01479492 5.06627148,0 11.3161512,0 L77.6838488,0 C83.9337285,0 89,5.01479492 89,11.2011719 L89,143.798828 Z"
                    ></motion.path>
                    <motion.line
                        stroke={colors.red}
                        variants={variants}
                        x1="27"
                        y1="10.5"
                        x2="61"
                        y2="10.5"
                        strokeLinecap="round"
                    ></motion.line>
                    <motion.circle
                        stroke={colors.red}
                        variants={variants}
                        cx="44.5"
                        cy="137.5"
                        r="7.5"
                    ></motion.circle>
                    <motion.rect
                        variants={variants}
                        x="10"
                        y="20"
                        width="67"
                        height="101"
                    ></motion.rect>
                </g>
            </g>
        </svg>
    )
}
