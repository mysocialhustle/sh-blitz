import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Advertise({ variants }) {
    return (
        <svg
            viewBox="0 0 54 62"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
        >
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <motion.polyline
                        variants={variants}
                        points="0 8 0 60 52 60 52 8"
                    ></motion.polyline>
                    <motion.rect
                        variants={variants}
                        x="0"
                        y="0"
                        width="52"
                        height="8"
                    ></motion.rect>
                    <g transform="translate(3.000000, 3.500000)">
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                        ></motion.line>
                    </g>
                    <motion.line
                        variants={variants}
                        x1="15"
                        y1="4"
                        x2="49"
                        y2="4"
                    ></motion.line>
                    <g transform="translate(3.000000, 11.500000)">
                        <g transform="translate(0.500000, 0.000000)">
                            <motion.polyline
                                variants={variants}
                                points="44.5 23.5 44.5 24.5 43.5 24.5"
                            ></motion.polyline>
                            <motion.line
                                variants={variants}
                                x1="41.5"
                                y1="24.5"
                                x2="2.5"
                                y2="24.5"
                                stroke-dasharray="2,2"
                            ></motion.line>
                            <motion.polyline
                                variants={variants}
                                points="1.5 24.5 0.5 24.5 0.5 23.5"
                            ></motion.polyline>
                            <motion.line
                                variants={variants}
                                x1="0.5"
                                y1="21.5"
                                x2="0.5"
                                y2="2.5"
                                stroke-dasharray="2,2"
                            ></motion.line>
                            <motion.polyline
                                variants={variants}
                                points="0.5 1.5 0.5 0.5 1.5 0.5"
                            ></motion.polyline>
                            <motion.line
                                variants={variants}
                                x1="3.5"
                                y1="0.5"
                                x2="42.5"
                                y2="0.5"
                                stroke-dasharray="2,2"
                            ></motion.line>
                            <motion.polyline
                                variants={variants}
                                points="43.5 0.5 44.5 0.5 44.5 1.5"
                            ></motion.polyline>
                            <motion.line
                                variants={variants}
                                x1="44.5"
                                y1="3.5"
                                x2="44.5"
                                y2="22.5"
                                stroke-dasharray="2,2"
                            ></motion.line>
                        </g>
                        <g transform="translate(1.000000, 28.500000)">
                            <motion.rect
                                variants={variants}
                                x="0"
                                y="0"
                                width="8"
                                height="8"
                            ></motion.rect>
                            <motion.rect
                                variants={variants}
                                x="12"
                                y="0"
                                width="8"
                                height="8"
                            ></motion.rect>
                            <motion.rect
                                variants={variants}
                                x="24"
                                y="0"
                                width="8"
                                height="8"
                            ></motion.rect>
                            <motion.rect
                                variants={variants}
                                x="36"
                                y="0"
                                width="8"
                                height="8"
                            ></motion.rect>
                        </g>
                        <g transform="translate(0.000000, 40.000000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="2"
                                y1="4.5"
                                x2="8"
                                y2="4.5"
                            ></motion.line>
                        </g>
                        <g transform="translate(12.000000, 40.000000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="2"
                                y1="4.5"
                                x2="8"
                                y2="4.5"
                            ></motion.line>
                        </g>
                        <g transform="translate(24.000000, 40.000000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="2"
                                y1="4.5"
                                x2="8"
                                y2="4.5"
                            ></motion.line>
                        </g>
                        <g transform="translate(36.000000, 40.000000)">
                            <motion.line
                                variants={variants}
                                x1="0"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="2"
                                y1="4.5"
                                x2="8"
                                y2="4.5"
                            ></motion.line>
                        </g>
                        <g
                            transform="translate(15.000000, 6.500000)"
                            stroke={colors.red}
                        >
                            <motion.path d="M16,9 C16,9.768 15.707,10.535 15.121,11.121 C14.535,11.707 13.768,12 13,12 L10,12 L10,0 L13,0 C13.768,0 14.535,0.293 15.121,0.879 C15.707,1.465 16,2.232 16,3 L16,9 Z"></motion.path>
                            <g>
                                <motion.path
                                    d="M6,12 L6,3 C6,2.232 5.707,1.465 5.121,0.879 C4.535,0.293 3.768,0 3,0 C2.232,0 1.465,0.293 0.879,0.879 C0.293,1.465 0,2.232 0,3 L0,12"
                                    stroke-linecap="square"
                                ></motion.path>
                                <motion.line
                                    variants={variants}
                                    x1="0"
                                    y1="6"
                                    x2="6"
                                    y2="6"
                                ></motion.line>
                            </g>
                        </g>
                        <g
                            transform="translate(8.000000, 12.000000)"
                            stroke={colors.red}
                        >
                            <motion.line
                                variants={variants}
                                x1="4"
                                y1="0.5"
                                x2="0"
                                y2="0.5"
                            ></motion.line>
                            <motion.line
                                variants={variants}
                                x1="30"
                                y1="0.5"
                                x2="26"
                                y2="0.5"
                            ></motion.line>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    )
}
