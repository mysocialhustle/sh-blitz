import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Ecommerce({ variants }) {
    return (
        <svg viewBox="0 0 50 64" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <g transform="translate(5.000000, 0.000000)">
                        <motion.polyline
                            points="5 27 5 52 33 52 33 27"
                            variants={variants}
                        ></motion.polyline>
                        <g transform="translate(14.000000, 4.500000)">
                            <motion.line
                                x1="4"
                                y1="0.5"
                                x2="10"
                                y2="0.5"
                                variants={variants}
                            ></motion.line>
                            <motion.line
                                x1="0"
                                y1="0.5"
                                x2="2"
                                y2="0.5"
                                variants={variants}
                                strokeDasharray="2"
                            ></motion.line>
                        </g>
                        <motion.line
                            x1="16"
                            y1="57"
                            x2="22"
                            y2="57"
                            stroke={colors.red}
                            variants={variants}
                        ></motion.line>
                        <motion.path
                            d="M38,9 L38,4 C38,1.8 36.2,0 34,0 L4,0 C1.8,0 0,1.8 0,4 L0,9"
                            variants={variants}
                        ></motion.path>
                        <motion.path
                            d="M38,27 L38,58 C38,60.2 36.2,62 34,62 L4,62 C1.8,62 0,60.2 0,58 L0,27"
                            variants={variants}
                            strokeDasharray="104.56022644042969"
                        ></motion.path>
                    </g>
                    <motion.path
                        d="M39,10 L9,10 L0,20 C0,22.209 1.791,24 4,24 C6.209,24 8,22.209 8,20 C8,22.209 9.791,24 12,24 C14.209,24 16,22.209 16,20 C16,22.209 17.791,24 20,24 C22.209,24 24,22.209 24,20 C24,22.209 25.791,24 28,24 C30.209,24 32,22.209 32,20 C32,22.209 33.791,24 36,24 C38.209,24 40,22.209 40,20 C40,22.209 41.791,24 44,24 C46.209,24 48,22.209 48,20 L39,10 Z"
                        variants={variants}
                        strokeDasharray="132.31544494628906"
                    ></motion.path>
                    <motion.circle
                        stroke={colors.red}
                        cx="24"
                        cy="38"
                        r="10"
                        variants={variants}
                        strokeDasharray="62.42696762084961"
                    ></motion.circle>
                    <g
                        transform="translate(20.000000, 34.000000)"
                        stroke={colors.red}
                    >
                        <motion.line
                            x1="0"
                            y1="8"
                            x2="8"
                            y2="0"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="8"
                            y1="7"
                            x2="6"
                            y2="7"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="2"
                            y1="1"
                            x2="0"
                            y2="1"
                            variants={variants}
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
