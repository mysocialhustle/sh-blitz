import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Eye({ variants }) {
    return (
        <svg viewBox="0 0 64 64" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <g transform="translate(26.000000, 6.500000)">
                        <motion.line
                            variants={variants}
                            x1="10"
                            y1="0.5"
                            x2="8"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="6"
                            y1="0.5"
                            x2="4"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="2"
                            y1="0.5"
                            x2="0"
                            y2="0.5"
                        ></motion.line>
                    </g>
                    <g transform="translate(26.000000, 54.500000)">
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                        ></motion.line>
                    </g>
                    <motion.path
                        variants={variants}
                        d="M31.004,14 C18,14 7,21.438 0,31 C7,40.563 18,48 31.004,48 C44,48 55,40.563 62,31 C55,21.438 44,14 31.004,14 Z"
                        strokeLinejoin="round"
                    ></motion.path>
                    <motion.path
                        variants={variants}
                        d="M31,62 C45.35,62 57.479,52.24 61,39"
                    ></motion.path>
                    <motion.path
                        variants={variants}
                        d="M1,23 C4.521,9.76 16.65,0 31,0"
                    ></motion.path>
                    <motion.circle
                        variants={variants}
                        cx="31"
                        cy="31"
                        r="17"
                    ></motion.circle>
                    <g
                        transform="translate(28.000000, 22.000000)"
                        stroke={colors.red}
                    >
                        <motion.path
                            variants={variants}
                            d="M6,6.001 C6,5.233 5.707,4.466 5.121,3.88 C4.536,3.294 3.767,3.001 3,3.001 C2.233,3.001 1.465,3.294 0.879,3.88 C0.293,4.466 0,5.233 0,6.001 C0,6.769 0.293,7.536 0.879,8.122 C1.465,8.708 2.233,9.001 3,9.001"
                        ></motion.path>
                        <motion.path
                            variants={variants}
                            d="M0,12.001 C0,12.769 0.293,13.536 0.879,14.122 C1.465,14.708 2.233,15.001 3,15.001 C3.767,15.001 4.536,14.708 5.121,14.122 C5.707,13.536 6,12.769 6,12.001 C6,11.233 5.707,10.466 5.121,9.88 C4.536,9.294 3.767,9.001 3,9.001"
                        ></motion.path>
                        <motion.line
                            variants={variants}
                            x1="3"
                            y1="3.001"
                            x2="3"
                            y2="3.55271368e-15"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="3"
                            y1="18"
                            x2="3"
                            y2="15.001"
                        ></motion.line>
                    </g>
                    <motion.path
                        variants={variants}
                        d="M19,31 C19,37.627 24.373,43 31,43 L32,43"
                        stroke={colors.red}
                    ></motion.path>
                    <motion.path
                        variants={variants}
                        d="M43,31 C43,24.373 37.627,19 31,19 L30,19"
                        stroke={colors.red}
                    ></motion.path>
                </g>
            </g>
        </svg>
    )
}
