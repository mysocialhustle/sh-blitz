import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Creativity({ variants }) {
    return (
        <svg viewBox="0 0 52 64" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(0.000000, 1.000000)">
                    <g transform="translate(5.221000, 0.000000)">
                        <motion.path
                            d="M-3.55271368e-15,18 C1.449,7.82 10.199,0 20.779,0 C31.359,0 40.109,7.82 41.559,18"
                            variants={variants}
                        ></motion.path>
                        <motion.path
                            d="M0.159,25 C1.509,32 6.34,37.76 12.779,40.41 L12.779,46 L28.779,46 L28.779,40.41 C35.219,37.76 40.049,32 41.399,25"
                            variants={variants}
                        ></motion.path>
                        <motion.polyline
                            points="28.779 54 28.779 58 12.779 58 12.779 54"
                            variants={variants}
                        ></motion.polyline>
                        <motion.polyline
                            points="9.779 46 20.779 46 31.779 46"
                            variants={variants}
                        ></motion.polyline>
                        <motion.polyline
                            points="9.779 50 20.779 50 31.779 50"
                            variants={variants}
                        ></motion.polyline>
                        <motion.polyline
                            points="9.779 54 20.779 54 31.779 54"
                            variants={variants}
                        ></motion.polyline>
                        <motion.path
                            d="M23.779,58 L23.779,60 C23.779,60.512 23.584,61.023 23.193,61.414 C22.802,61.805 22.291,62 21.779,62 L19.779,62 C19.267,62 18.756,61.805 18.365,61.414 C17.974,61.023 17.779,60.512 17.779,60 L17.779,58"
                            variants={variants}
                        ></motion.path>
                    </g>
                    <g
                        transform="translate(42.000000, 20.500000)"
                        stroke={colors.red}
                    >
                        <motion.line
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                    </g>
                    <g
                        transform="translate(0.000000, 20.500000)"
                        stroke={colors.red}
                    >
                        <motion.line
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                            variants={variants}
                        ></motion.line>
                    </g>
                    <g transform="translate(10.286000, 5.286000)">
                        <motion.path
                            d="M28.712,15.714 C28.714,14.156 29.907,12.89 31.427,12.741 C31.012,10.534 30.152,8.486 28.93,6.699 C27.751,7.675 26.009,7.624 24.906,6.522 C23.804,5.418 23.754,3.675 24.727,2.497 C22.94,1.276 20.892,0.415 18.685,0 C18.539,1.521 17.273,2.714 15.714,2.714 C14.155,2.714 12.89,1.521 12.743,0 C10.536,0.415 8.487,1.276 6.7,2.497 C7.675,3.676 7.624,5.419 6.522,6.521 C5.418,7.625 3.676,7.674 2.497,6.7 C1.276,8.487 0.415,10.536 0,12.743 C1.521,12.89 2.714,14.156 2.715,15.714 C2.714,17.273 1.521,18.538 0.001,18.686 C0.415,20.893 1.276,22.941 2.497,24.728 C3.677,23.753 5.419,23.804 6.522,24.906 C7.624,26.01 7.674,27.753 6.7,28.931 C8.487,30.152 10.536,31.013 12.743,31.428 C12.89,29.907 14.155,28.714 15.714,28.712 C17.273,28.714 18.539,29.907 18.686,31.427 C20.893,31.013 22.942,30.152 24.729,28.931 C23.753,27.752 23.804,26.009 24.906,24.906 C26.01,23.804 27.753,23.754 28.931,24.727 C30.153,22.941 31.013,20.892 31.428,18.685 C29.907,18.538 28.714,17.272 28.712,15.714 Z"
                            stroke={colors.red}
                            strokeLinejoin="round"
                            variants={variants}
                        ></motion.path>
                        <motion.circle
                            cx="15.714"
                            cy="15.714"
                            r="8"
                            variants={variants}
                        ></motion.circle>
                        <motion.circle
                            cx="15.714"
                            cy="15.714"
                            r="3"
                            variants={variants}
                        ></motion.circle>
                    </g>
                    <g transform="translate(22.500000, 42.000000)">
                        <motion.line
                            x1="0.5"
                            y1="4"
                            x2="0.5"
                            y2="0"
                            variants={variants}
                        ></motion.line>
                        <motion.line
                            x1="6.5"
                            y1="0"
                            x2="6.5"
                            y2="4"
                            variants={variants}
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
