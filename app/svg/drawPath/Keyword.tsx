import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

export default function Keyword({ variants }) {
    return (
        <svg viewBox="0 0 64 64" version="1.1">
            <g strokeWidth="1" fill="none" fillRule="evenodd">
                <g transform="translate(1.000000, 1.000000)">
                    <g transform="translate(17.500000, 14.000000)">
                        <motion.line
                            variants={variants}
                            x1="0.5"
                            y1="12"
                            x2="0.5"
                            y2="9"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="4.5"
                            y1="12"
                            x2="4.5"
                            y2="4"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="8.5"
                            y1="12"
                            x2="8.5"
                            y2="7"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="12.5"
                            y1="12"
                            x2="12.5"
                            y2="3"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="16.5"
                            y1="12"
                            x2="16.5"
                            y2="0"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="20.5"
                            y1="12"
                            x2="20.5"
                            y2="4"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="24.5"
                            y1="12"
                            x2="24.5"
                            y2="7"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="28.5"
                            y1="12"
                            x2="28.5"
                            y2="1"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="32.5"
                            y1="12"
                            x2="32.5"
                            y2="5"
                        ></motion.line>
                    </g>
                    <g transform="translate(12.000000, 7.000000)">
                        <motion.rect
                            variants={variants}
                            x="0"
                            y="0"
                            width="34"
                            height="4"
                        ></motion.rect>
                        <motion.rect
                            variants={variants}
                            x="34"
                            y="0"
                            width="4"
                            height="4"
                        ></motion.rect>
                    </g>
                    <motion.path
                        variants={variants}
                        d="M58,0 C60.209,0 62,1.791 62,4 L62,7 L57,7"
                    ></motion.path>
                    <motion.path
                        variants={variants}
                        d="M46,58 L46,55 L0,55 L0,58 C0,60.209 1.791,62 4,62 L50,62 C47.791,62 46,60.209 46,58 Z"
                    ></motion.path>
                    <motion.line
                        variants={variants}
                        x1="8"
                        y1="55"
                        x2="8"
                        y2="42"
                    ></motion.line>
                    <motion.path
                        variants={variants}
                        d="M54,26 L54,4 C54,2.977 54.391,1.953 55.172,1.172 C55.952,0.391 56.977,0 58,0 L12,0 C10.977,0 9.952,0.391 9.172,1.172 C8.391,1.953 8,2.977 8,4 L8,20"
                    ></motion.path>
                    <motion.path
                        variants={variants}
                        d="M54,42 L54,58 C54,59.023 53.609,60.047 52.828,60.828 C52.048,61.609 51.023,62 50,62"
                    ></motion.path>
                    <g
                        transform="translate(0.000000, 23.000000)"
                        stroke={colors.red}
                    >
                        <motion.circle
                            variants={variants}
                            cx="8"
                            cy="8"
                            r="8"
                        ></motion.circle>
                        <motion.polyline
                            variants={variants}
                            points="46 10 46 14 50 14 50 16 54 16 54 10"
                        ></motion.polyline>
                        <motion.polyline
                            variants={variants}
                            points="16 10 58 10 58 6 16 6"
                        ></motion.polyline>
                        <motion.circle
                            variants={variants}
                            cx="10"
                            cy="8"
                            r="2"
                        ></motion.circle>
                    </g>
                    <g transform="translate(26.000000, 36.500000)">
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="0.5"
                            x2="2"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="4"
                            y1="0.5"
                            x2="6"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="8"
                            y1="0.5"
                            x2="10"
                            y2="0.5"
                        ></motion.line>
                    </g>
                    <g transform="translate(11.000000, 42.500000)">
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="0.5"
                            x2="40"
                            y2="0.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="4.5"
                            x2="40"
                            y2="4.5"
                        ></motion.line>
                        <motion.line
                            variants={variants}
                            x1="0"
                            y1="8.5"
                            x2="20"
                            y2="8.5"
                        ></motion.line>
                    </g>
                </g>
            </g>
        </svg>
    )
}
