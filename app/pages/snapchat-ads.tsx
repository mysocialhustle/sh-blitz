import Layout from 'app/core/layouts/Layout'
import SnapchatAds from 'app/data/services/snapchat-ads'
import ServiceTemplate from 'app/core/templates/Services'

export default function SnapchatAdsPage(props) {
    return <ServiceTemplate page={props} />
}

SnapchatAdsPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: '7fa4f56b-6094-41d1-95da-e97f0194cd57',
            ...SnapchatAds,
        }, // will be passed to the page component as props
    }
}
