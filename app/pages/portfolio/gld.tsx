import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Headings from 'app/styles/headings'
import { Container, Row, Column } from 'app/styles/grid'

export default function GLDPortfolio() {
    return (
        <Section>
            <Container>Content</Container>
        </Section>
    )
}

GLDPortfolio.getLayout = (page) => <Layout title="">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
