import Layout from 'app/core/layouts/Layout'
import Team from 'app/core/sections/OurCompany/Team/Team'
import Intro from 'app/core/sections/OurCompany/Intro/Intro'
import Values from 'app/core/sections/OurCompany/Values/Values'
import { PageWrapper } from 'app/styles/utilities'
import Story from 'app/core/sections/OurCompany/Story/Story'

export default function CompanyPage() {
    return (
        <PageWrapper>
            <Intro />
            <Values />
            <Team />
            <Story />
        </PageWrapper>
    )
}

CompanyPage.getLayout = (page) => <Layout title="Our Company">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
