import Layout from 'app/core/layouts/Layout'
import AwardsLogo from 'app/core/blocks/AwardsLogo/AwardsLogo'
import { Container, Row, Column } from 'app/styles/grid'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import certifications from 'app/data/certifications'

export default function AwardsPage() {
    return (
        <Section>
            <Container>
                <ScrollSpy>
                    <h2>Awards</h2>
                </ScrollSpy>
                <Row>
                    {certifications
                        .filter((item) => item.category === 'Awards')
                        .map((item, index) => (
                            <Column key={index} sm={33}>
                                <AwardsLogo {...item} />
                            </Column>
                        ))}
                </Row>
                <ScrollSpy>
                    <h2>Certifications</h2>
                </ScrollSpy>
                <Row>
                    {certifications
                        .filter((item) => item.category === 'Certifications')
                        .map((item, index) => (
                            <Column key={index} sm={25}>
                                <AwardsLogo {...item} />
                            </Column>
                        ))}
                </Row>
            </Container>
        </Section>
    )
}

AwardsPage.getLayout = (page) => (
    <Layout title="Awards & Certifications">{page}</Layout>
)

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
