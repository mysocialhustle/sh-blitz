import Layout from 'app/core/layouts/Layout'
import SEO from 'app/data/services/search-engine-optimization'
import ServiceTemplate from 'app/core/templates/Services'
import LocalSEO from 'app/core/sections/ServicePages/CustomSections/LocalSEO'

export default function SEOPage(props) {
    return <ServiceTemplate page={props} CustomComponent={LocalSEO} />
}

SEOPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: '18b2853f-141e-44f2-b2c7-9fea85250460',
            ...SEO,
        }, // will be passed to the page component as props
    }
}
