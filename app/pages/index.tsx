import React from 'react'
import VideoBanner from 'app/core/components/VideoBanner/VideoBanner'
import Layout from 'app/core/layouts/Layout'
import { Container, Row, Column } from 'app/styles/grid'
import Section from 'app/core/components/Section/Section'
import Counter from 'app/core/components/Counter/Counter'
import HomepageAbout from 'app/core/sections/Homepage/HomepageAbout/HomepageAbout'
import Services from 'app/core/sections/Services/Services'
import Testimonials from 'app/core/sections/Testimonials/Testimonials'
import testimonials from 'app/data/testimonials'
import Contact from 'app/core/sections/Contact/Contact'
import Portfolio from 'app/core/sections/Portfolio/Portfolio'
import portfolio from 'app/data/portfolio'
import ClientLogos from 'app/core/sections/ClientLogos/ClientLogos'
import AwardsLogo from 'app/core/blocks/AwardsLogo/AwardsLogo'
import certifications from 'app/data/certifications'
import { columns } from 'app/core/constants/grid'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Button from 'app/core/components/Button/Button'
import WebHosting from 'app/core/sections/WebHosting/WebHosting'
import YouTubePlaylists from 'app/core/sections/Homepage/YouTubePlaylist/YouTubePlaylist'
import {
    PlaceholderCarousel,
    Carousel,
    Feed,
} from 'app/data-feeds/components/Instagram'

const awards = certifications.filter((item) => item.homepage === true)

export default function Home() {
    return (
        <>
            <VideoBanner
                mp4="https://socialhustle-blitz-static.s3.amazonaws.com/homepage/Social-Hustle-Website-Edit-v1.5.mp4"
                webm="https://socialhustle-blitz-static.s3.amazonaws.com/homepage/Social-Hustle-Website-Edit-v1.5.webm"
                poster="https://socialhustle-blitz-static.s3.amazonaws.com/homepage/new-reel-poster.webp"
            >
                <div className="animation__mask">
                    <div className="animation__el">We Build</div>
                </div>
                <div
                    className="animation__mask"
                    style={{ animationDelay: '0.75s' }}
                >
                    <div
                        className="animation__el"
                        style={{ animationDelay: '0.75s' }}
                    >
                        Businesses.
                    </div>
                </div>
            </VideoBanner>
            <Section style={{ fontSize: '1.15rem' }}>
                <Container style={{ textAlign: 'center' }}>
                    <Row alignX="space-between">
                        <Column md={33}>
                            <Counter digit={548} textAfter="%" />
                            AVG. INCREASE IN CLIENT TRAFFIC
                        </Column>
                        <Column md={33}>
                            <Counter
                                digit={800}
                                textBefore="$"
                                textAfter="M+"
                            />
                            CLIENT REVENUE GAINED
                        </Column>
                        <Column md={33}>
                            <Counter digit={25} textAfter="+" />
                            INDUSTRIES WORKED IN
                        </Column>
                    </Row>
                </Container>
            </Section>
            <HomepageAbout />
            <Services />
            <Testimonials
                nodes={testimonials.filter((item) =>
                    item.categories.includes('homepage')
                )}
            />

            <Portfolio
                nodes={portfolio?.filter((item) =>
                    item?.placement?.includes('homepage')
                )}
                title={`Businesses We've Helped Build`}
            />
            <ClientLogos />
            <Section>
                <Container>
                    <ScrollSpy>
                        <h2 style={{ textAlign: 'center' }}>
                            Awards &amp; Certifications
                        </h2>
                    </ScrollSpy>
                    <Row>
                        {awards.map((item, index) => (
                            <Column key={index} sm={columns / awards.length}>
                                <AwardsLogo {...item} />
                            </Column>
                        ))}
                    </Row>
                    <ScrollSpy>
                        <Button href="/award-certifications/" centered>
                            View More
                        </Button>
                    </ScrollSpy>
                </Container>
            </Section>
            <WebHosting theme="transparentDark" />
            <YouTubePlaylists />
            <Contact />
            <Feed
                Component={Carousel}
                Placeholder={PlaceholderCarousel}
                props={{ captionLength: 75 }}
            />
        </>
    )
}

Home.getLayout = (page) => <Layout title="Home">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
