import Layout from 'app/core/layouts/Layout'
import TikTokAds from 'app/data/services/tiktok-ads'
import ServiceTemplate from 'app/core/templates/Services'

export default function TiktokAdsPage(props) {
    return <ServiceTemplate page={props} />
}

TiktokAdsPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: '87392ee3-a03d-4607-ad68-cbc72b57399d',
            ...TikTokAds,
        }, // will be passed to the page component as props
    }
}
