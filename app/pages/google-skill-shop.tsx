import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Image } from 'blitz'
import { Container } from 'app/styles/grid'
import Button from 'app/core/components/Button/Button'
import colors from 'app/core/constants/colors'

export default function GoogleSkillsPage() {
    return (
        <Section>
            <Container>
                <ScrollSpy>
                    <Image
                        src="https://socialhustle-blitz-static.s3.amazonaws.com/skillshop-homebanner-01.png"
                        alt="Google Skill Workshop"
                        width="2257"
                        height="1080"
                    />
                </ScrollSpy>
                <ScrollSpy>
                    <Button
                        centered
                        href="https://skillshop.withgoogle.com/"
                        backgroundColor={colors.red}
                        hoverBackgroundColor="#000"
                        hoverColor="#fff"
                        props={{ target: '_blank' }}
                    >
                        START YOUR TRAINING
                    </Button>
                </ScrollSpy>
            </Container>
        </Section>
    )
}

GoogleSkillsPage.getLayout = (page) => <Layout title="Careers">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
