import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getUser from 'app/admin/users/queries/getUser'
import deleteUser from 'app/admin/users/mutations/deleteUser'
import { Container } from 'app/styles/grid'
import BackButton from 'app/admin/components/BackButton/BackButton'
import Account from 'app/core/layouts/Account/Account'
import {
    UserForm,
    FORM_ERROR,
} from 'app/admin/users/components/UserForm/UserForm'
import updateUser from 'app/admin/users/mutations/updateUser'
import DeleteButton from 'app/admin/components/DeleteModelButton/DeleteButton'
import Button from 'app/core/components/Button/Button'
import { useState } from 'react'
import Message from 'app/core/components/Form/Messages/Messages'
import generatePassword from 'app/core/utils/generatePassword'
import updateUserPassword from 'app/admin/users/mutations/updateUserPassword'
import sendUserInvitation from 'app/core/utils/sendUserInvitation'

const UserOverviewPage: BlitzPage = () => {
    const router = useRouter()
    const userId = useParam('userId', 'number')
    const [deleteUserMutation] = useMutation(deleteUser)
    const [updateUserPasswordMutation] = useMutation(updateUserPassword)
    const [user, { setQueryData }] = useQuery(getUser, { id: userId })
    const [updateUserMutation] = useMutation(updateUser)
    const [emailSent, setEmailSent] = useState(false)

    const title =
        user.firstName || user.lastName
            ? `${user.firstName || ``} ${user.lastName || ``}`
            : user.email
    const changePassword = async () => {
        const password = generatePassword()
        try {
            const updated = await updateUserPasswordMutation({
                id: user.id,
                password,
            })
            if (updated) {
                sendEmail()
            }
        } catch (error: any) {
            console.error(error)
            alert(error.toString())
        }
    }

    const sendEmail = async () => {
        const password = generatePassword()
        const [fetcher, response] = await sendUserInvitation(
            user.email,
            password
        )
        if (fetcher.ok && response?.status === 'success') setEmailSent(true)
    }

    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>

            <Container>
                <BackButton href={Routes.UsersPage()} label="BACK TO USERS" />
                <h2>{title}</h2>
                {!user.confirmed && (
                    <>
                        <h5>
                            This user is not confirmed. Click below to resend
                            their confirmation email.
                        </h5>
                        <Button
                            element="button"
                            style={{
                                marginBottom: 20,
                            }}
                            onClick={
                                // TO-DO: Send confirmation
                                //() => setEmailSent(true)
                                () => changePassword()
                            }
                        >
                            Re-Send Confirmation
                        </Button>
                    </>
                )}
                {emailSent && (
                    <Message type="success">
                        Success! A new confirmation email has been sent.
                    </Message>
                )}
                <h3>Edit User Details</h3>
                <UserForm
                    submitText="Update User"
                    // TODO use a zod schema for form validation
                    //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                    //         then import and use it here
                    // schema={UpdateUser}
                    initialValues={user}
                    buttonProps={{
                        style: {
                            marginBottom: 20,
                        },
                    }}
                    onSubmit={async (values) => {
                        //console.log(values)
                        try {
                            const updated = await updateUserMutation({
                                id: user.id,
                                ...values,
                            })
                            await setQueryData(updated)
                            router.push(
                                Routes.UserOverviewPage({ userId: updated.id })
                            )
                        } catch (error: any) {
                            console.error(error)
                            return {
                                [FORM_ERROR]: error.toString(),
                            }
                        }
                    }}
                />
                {/**<pre>{JSON.stringify(user, null, 2)}</pre>**/}

                <DeleteButton
                    onClick={async () => {
                        if (
                            window.confirm(
                                'Are you sure you want to delete this user?'
                            )
                        ) {
                            await deleteUserMutation({ id: user.id })
                            router.push(Routes.UsersPage())
                        }
                    }}
                    style={{ marginLeft: '0.5rem' }}
                >
                    Delete User
                </DeleteButton>
            </Container>
        </>
    )
}

UserOverviewPage.authenticate = true
UserOverviewPage.getLayout = (page) => <Account>{page}</Account>

export default UserOverviewPage
