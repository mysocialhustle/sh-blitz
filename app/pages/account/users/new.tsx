import { Link, useRouter, useMutation, BlitzPage, Routes } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import createUser from 'app/admin/users/mutations/createUser'
import {
    UserForm,
    FORM_ERROR,
} from 'app/admin/users/components/UserForm/UserForm'
import { Container } from 'app/styles/grid'
import BackButton from 'app/admin/components/BackButton/BackButton'

const NewUserPage: BlitzPage = () => {
    const router = useRouter()
    const [createUserMutation] = useMutation(createUser)

    return (
        <Container>
            <BackButton href={Routes.UsersPage()} label="BACK TO USERS" />

            <h2>Create New User</h2>

            <UserForm
                submitText="Create User"
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                // schema={CreateUser}
                // initialValues={{}}
                onSubmit={async (values) => {
                    try {
                        const req = await createUserMutation(values)
                        if (req?.response?.status === 'success') {
                            router.push(
                                Routes.UserOverviewPage({
                                    userId: req?.user.id,
                                })
                            )
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            />
        </Container>
    )
}

NewUserPage.authenticate = true
NewUserPage.getLayout = (page) => (
    <Account title={'Create New User'}>{page}</Account>
)

export default NewUserPage
