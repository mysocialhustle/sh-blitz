import { Link, BlitzPage, Routes } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import UsersList from 'app/admin/users/components/UserList/UserList'
import { Container } from 'app/styles/grid'
import Button from 'app/core/components/Button/Button'

const UsersPage: BlitzPage = () => {
    return (
        <Container>
            <h2>Manage Users</h2>
            <Button href={Routes.NewUserPage()}>Create User</Button>

            <UsersList pagination={100} />
        </Container>
    )
}

UsersPage.authenticate = true
UsersPage.getLayout = (page) => <Account title="Users">{page}</Account>

export default UsersPage
