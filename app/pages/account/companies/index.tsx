import {
    Head,
    Link,
    usePaginatedQuery,
    useRouter,
    BlitzPage,
    Routes,
} from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import Account from 'app/core/layouts/Account/Account'
import Button from 'app/core/components/Button/Button'
import { Container } from 'app/styles/grid'
import CompaniesList from 'app/admin/companies/components/CompaniesList/CompaniesList'

const CompaniesPage: BlitzPage = () => {
    return (
        <>
            <Head>
                <title>Companies</title>
            </Head>

            <Container>
                <h2>Companies</h2>
                <p>
                    <Button href={Routes.NewCompanyPage()}>
                        Create a New Company
                    </Button>
                </p>
                <CompaniesList pagination={100} />
            </Container>
        </>
    )
}

CompaniesPage.authenticate = true
CompaniesPage.getLayout = (page) => <Account>{page}</Account>

export default CompaniesPage
