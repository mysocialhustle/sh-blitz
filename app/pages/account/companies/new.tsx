import { Link, useRouter, useMutation, BlitzPage, Routes } from 'blitz'
import Layout from 'app/core/layouts/Layout'
import createCompany from 'app/admin/companies/mutations/createCompany'
import {
    CompanyForm,
    FORM_ERROR,
} from 'app/admin/companies/components/CompanyForm'

const NewCompanyPage: BlitzPage = () => {
    const router = useRouter()
    const [createCompanyMutation] = useMutation(createCompany)

    return (
        <div>
            <h1>Create New Company</h1>

            <CompanyForm
                submitText="Create Company"
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                // schema={CreateCompany}
                // initialValues={{}}
                onSubmit={async (values) => {
                    try {
                        const company = await createCompanyMutation(values)
                        router.push(
                            Routes.CompanyOverviewPage({
                                companyId: company.id,
                            })
                        )
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            />

            <p>
                <Link href={Routes.CompaniesPage()}>
                    <a>Companies</a>
                </Link>
            </p>
        </div>
    )
}

NewCompanyPage.authenticate = true
NewCompanyPage.getLayout = (page) => (
    <Layout title={'Create New Company'}>{page}</Layout>
)

export default NewCompanyPage
