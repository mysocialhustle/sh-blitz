import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCompany from 'app/admin/companies/queries/getCompany'
import deleteCompany from 'app/admin/companies/mutations/deleteCompany'
import Account from 'app/core/layouts/Account/Account'
import DeleteButton from 'app/admin/components/DeleteModelButton/DeleteButton'
import { Container } from 'app/styles/grid'
import EditButton from 'app/admin/components/EditButton/EditButton'
import BackButton from 'app/admin/components/BackButton/BackButton'
import { Divider } from 'app/styles/utilities'
import Notes from 'app/admin/companies/components/Notes/Notes'
import ContactInfo from 'app/admin/companies/components/ContactInfo/ContactInfo'
import Section from 'app/core/components/Section/Section'
import RelatedModels from 'app/admin/companies/components/RelatedModels/RelatedModels'
import CompanyNavigation from 'app/admin/companies/components/Navigation'

const CompanyOverviewPage: BlitzPage = () => {
    const router = useRouter()
    const companyId = useParam('companyId', 'number')
    const [deleteCompanyMutation] = useMutation(deleteCompany)
    const [company] = useQuery(getCompany, { id: companyId })
    console.log(company)

    return (
        <>
            <Head>
                <title>{company.name} | Company Page</title>
            </Head>

            <Container>
                <BackButton
                    href={Routes.CompaniesPage()}
                    label="BACK TO COMPANIES"
                />
                <CompanyNavigation />
                <h1>{company.name}</h1>
                <p>
                    <EditButton>Edit this company</EditButton>
                    <Divider />
                    <DeleteButton
                        onClick={async () => {
                            if (
                                window.confirm(
                                    'Are you sure you want to delete this company?'
                                )
                            ) {
                                await deleteCompanyMutation({ id: company.id })
                                router.push(Routes.CompaniesPage())
                            }
                        }}
                    >
                        Delete
                    </DeleteButton>
                </p>
                <Section theme="transparentLight" size="tiny">
                    <ContactInfo company={company} />
                    <Notes company={company} />
                </Section>
            </Container>
        </>
    )
}

CompanyOverviewPage.authenticate = true
CompanyOverviewPage.getLayout = (page) => <Account>{page}</Account>

export default CompanyOverviewPage
