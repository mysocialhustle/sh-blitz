import ModelNavigation from 'app/admin/components/ModelNavigation/ModelNavigation'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCompany from 'app/admin/companies/queries/getCompany'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import Account from 'app/core/layouts/Account/Account'
import CompanyNavigation from 'app/admin/companies/components/Navigation'
import { Container } from 'app/styles/grid'
import BackButton from 'app/admin/components/BackButton/BackButton'
import EditButton from 'app/admin/components/EditButton/EditButton'
import RelatedModels from 'app/admin/companies/components/RelatedModels/RelatedModels'

export default function CompanyCampaigns() {
    const companyId = useParam('companyId', 'number')
    const [company] = useQuery(getCompany, { id: companyId })
    const [{ campaigns }] = useQuery(getCampaigns, {
        where: { companyId: companyId },
    })

    return (
        <>
            <Head>
                <title>{company.name} Campaigns</title>
            </Head>
            <Container>
                <BackButton
                    href={Routes.CompaniesPage()}
                    label="BACK TO COMPANIES"
                />
                <CompanyNavigation />
                <h1>{company.name} Campaigns</h1>

                <RelatedModels items={campaigns} modelType="campaigns" />
            </Container>
        </>
    )
}

CompanyCampaigns.authenticate = true
CompanyCampaigns.getLayout = (page) => <Account>{page}</Account>
