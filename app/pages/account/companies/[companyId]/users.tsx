import ModelNavigation from 'app/admin/components/ModelNavigation/ModelNavigation'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCompany from 'app/admin/companies/queries/getCompany'
import getUsers from 'app/admin/users/queries/getUsers'
import Account from 'app/core/layouts/Account/Account'
import CompanyNavigation from 'app/admin/companies/components/Navigation'
import { Container } from 'app/styles/grid'
import BackButton from 'app/admin/components/BackButton/BackButton'
import EditButton from 'app/admin/components/EditButton/EditButton'
import List from 'app/admin/users-on-companies/components/List/List'
import UserOnCompanyForm from 'app/admin/users-on-companies/components/UserOnCompanyForm'
import getUsersOnCompanies from 'app/admin/users-on-companies/queries/getUsersOnCompanies'

export default function CompanyUsers() {
    const companyId = useParam('companyId', 'number')
    const [company] = useQuery(getCompany, { id: companyId })
    const [{ usersOnCompanies }] = useQuery(getUsersOnCompanies, {
        where: { companyId: companyId },
    })
    const map = usersOnCompanies.map(({ userId }) => {
        return userId
    })
    const [{ users }] = useQuery(getUsers, {
        where: { id: { in: [...map] } },
    })

    return (
        <>
            <Head>
                <title>{company.name} Users</title>
            </Head>
            <Container>
                <BackButton
                    href={Routes.CompaniesPage()}
                    label="BACK TO COMPANIES"
                />
                <CompanyNavigation />
                <h1>{company.name} Users</h1>
                <p>
                    <EditButton>Edit this company</EditButton>
                </p>
                <List users={users} company={company} />
                <UserOnCompanyForm company={company} />
            </Container>
        </>
    )
}

CompanyUsers.authenticate = true
CompanyUsers.getLayout = (page) => <Account>{page}</Account>
