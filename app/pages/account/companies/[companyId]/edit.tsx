import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useMutation,
    useParam,
    BlitzPage,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getCompany from 'app/admin/companies/queries/getCompany'
import updateCompany from 'app/admin/companies/mutations/updateCompany'
import {
    CompanyForm,
    FORM_ERROR,
} from 'app/admin/companies/components/CompanyForm'

export const EditCompany = () => {
    const router = useRouter()
    const companyId = useParam('companyId', 'number')
    const [company, { setQueryData }] = useQuery(
        getCompany,
        { id: companyId },
        {
            // This ensures the query never refreshes and overwrites the form data while the user is editing.
            staleTime: Infinity,
        }
    )
    const [updateCompanyMutation] = useMutation(updateCompany)

    return (
        <>
            <Head>
                <title>Edit Company {company.id}</title>
            </Head>

            <div>
                <h1>Edit Company {company.id}</h1>
                <pre>{JSON.stringify(company, null, 2)}</pre>

                <CompanyForm
                    submitText="Update Company"
                    // TODO use a zod schema for form validation
                    //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                    //         then import and use it here
                    // schema={UpdateCompany}
                    initialValues={company}
                    onSubmit={async (values) => {
                        try {
                            const updated = await updateCompanyMutation({
                                id: company.id,
                                ...values,
                            })
                            await setQueryData(updated)
                            router.push(
                                Routes.CompanyOverviewPage({
                                    companyId: updated.id,
                                })
                            )
                        } catch (error: any) {
                            console.error(error)
                            return {
                                [FORM_ERROR]: error.toString(),
                            }
                        }
                    }}
                />
            </div>
        </>
    )
}

const EditCompanyPage: BlitzPage = () => {
    return (
        <div>
            <Suspense fallback={<div>Loading...</div>}>
                <EditCompany />
            </Suspense>

            <p>
                <Link href={Routes.CompaniesPage()}>
                    <a>Companies</a>
                </Link>
            </p>
        </div>
    )
}

EditCompanyPage.authenticate = true
EditCompanyPage.getLayout = (page) => <Layout>{page}</Layout>

export default EditCompanyPage
