import { BlitzPage, Routes } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import { SignupForm } from 'app/auth/components/SignupForm'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import Layout from 'app/core/layouts/Layout'

const AccountPage: BlitzPage = () => {
    /**const currentUser = useCurrentUser()

    return <div>{currentUser && <h1>Hello, {currentUser.name}</h1>}</div>**/
    return null
}

AccountPage.authenticate = { redirectTo: '/login' }
AccountPage.getLayout = (page) => <Account title="Your Account">{page}</Account>

export default AccountPage
