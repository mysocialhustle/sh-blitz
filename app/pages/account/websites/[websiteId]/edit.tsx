import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useMutation,
    useParam,
    BlitzPage,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getWebsite from 'app/admin/websites/queries/getWebsite'
import updateWebsite from 'app/admin/websites/mutations/updateWebsite'
import {
    WebsiteForm,
    FORM_ERROR,
} from 'app/admin/websites/components/WebsiteForm'

export const EditWebsite = () => {
    const router = useRouter()
    const websiteId = useParam('websiteId', 'number')
    const [website, { setQueryData }] = useQuery(
        getWebsite,
        { id: websiteId },
        {
            // This ensures the query never refreshes and overwrites the form data while the user is editing.
            staleTime: Infinity,
        }
    )
    const [updateWebsiteMutation] = useMutation(updateWebsite)

    return (
        <>
            <Head>
                <title>Edit Website {website.id}</title>
            </Head>

            <div>
                <h1>Edit Website {website.id}</h1>
                <pre>{JSON.stringify(website, null, 2)}</pre>

                <WebsiteForm
                    submitText="Update Website"
                    // TODO use a zod schema for form validation
                    //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                    //         then import and use it here
                    // schema={UpdateWebsite}
                    initialValues={website}
                    onSubmit={async (values) => {
                        try {
                            const updated = await updateWebsiteMutation({
                                id: website.id,
                                ...values,
                            })
                            await setQueryData(updated)
                            router.push(
                                Routes.WebsiteOverviewPage({
                                    websiteId: updated.id,
                                })
                            )
                        } catch (error: any) {
                            console.error(error)
                            return {
                                [FORM_ERROR]: error.toString(),
                            }
                        }
                    }}
                />
            </div>
        </>
    )
}

const EditWebsitePage: BlitzPage = () => {
    return (
        <div>
            <Suspense fallback={<div>Loading...</div>}>
                <EditWebsite />
            </Suspense>

            <p>
                <Link href={Routes.WebsitesPage()}>
                    <a>Websites</a>
                </Link>
            </p>
        </div>
    )
}

EditWebsitePage.authenticate = true
EditWebsitePage.getLayout = (page) => <Layout>{page}</Layout>

export default EditWebsitePage
