import { Link, useRouter, useMutation, BlitzPage, Routes } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import createWebsite from 'app/admin/websites/mutations/createWebsite'
import {
    WebsiteForm,
    FORM_ERROR,
} from 'app/admin/websites/components/WebsiteForm'
import { Container } from 'app/styles/grid'

const NewWebsitePage: BlitzPage = () => {
    const router = useRouter()
    const [createWebsiteMutation] = useMutation(createWebsite)

    return (
        <Container>
            <h2>Create New Website</h2>

            <WebsiteForm
                submitText="Create Website"
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                // schema={CreateWebsite}
                // initialValues={{}}
                onSubmit={async (values) => {
                    try {
                        const website = await createWebsiteMutation(values)
                        router.push(
                            Routes.WebsiteOverviewPage({
                                websiteId: website.id,
                            })
                        )
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            />

            <p>
                <Link href={Routes.WebsitesPage()}>
                    <a>Websites</a>
                </Link>
            </p>
        </Container>
    )
}

NewWebsitePage.authenticate = true
NewWebsitePage.getLayout = (page) => (
    <Account title={'Create New Website'}>{page}</Account>
)

export default NewWebsitePage
