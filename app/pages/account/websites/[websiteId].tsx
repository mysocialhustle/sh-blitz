import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getWebsite from 'app/admin/websites/queries/getWebsite'
import deleteWebsite from 'app/admin/websites/mutations/deleteWebsite'

export const Website = () => {
    const router = useRouter()
    const websiteId = useParam('websiteId', 'number')
    const [deleteWebsiteMutation] = useMutation(deleteWebsite)
    const [website] = useQuery(getWebsite, { id: websiteId })

    return (
        <>
            <Head>
                <title>Website {website.id}</title>
            </Head>

            <div>
                <h1>Website {website.id}</h1>
                <pre>{JSON.stringify(website, null, 2)}</pre>

                <Link href={Routes.EditWebsitePage({ websiteId: website.id })}>
                    <a>Edit</a>
                </Link>

                <button
                    type="button"
                    onClick={async () => {
                        if (window.confirm('This will be deleted')) {
                            await deleteWebsiteMutation({ id: website.id })
                            router.push(Routes.WebsitesPage())
                        }
                    }}
                    style={{ marginLeft: '0.5rem' }}
                >
                    Delete
                </button>
            </div>
        </>
    )
}

const WebsiteOverviewPage: BlitzPage = () => {
    return (
        <div>
            <p>
                <Link href={Routes.WebsitesPage()}>
                    <a>Websites</a>
                </Link>
            </p>

            <Suspense fallback={<div>Loading...</div>}>
                <Website />
            </Suspense>
        </div>
    )
}

WebsiteOverviewPage.authenticate = true
WebsiteOverviewPage.getLayout = (page) => <Layout>{page}</Layout>

export default WebsiteOverviewPage
