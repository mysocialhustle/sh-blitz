import { Head, BlitzPage, Routes } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import { Container } from 'app/styles/grid'
import WebsitesList from 'app/admin/websites/components/WebsitesList/WebsitesList'
import Button from 'app/core/components/Button/Button'

const WebsitesPage: BlitzPage = () => {
    return (
        <>
            <Head>
                <title>Websites</title>
            </Head>

            <Container>
                <h2>Websites</h2>
                <p>
                    <Button href={Routes.NewWebsitePage()}>
                        Create a New Website
                    </Button>
                </p>

                <WebsitesList pagination={100} />
            </Container>
        </>
    )
}

WebsitesPage.authenticate = true
WebsitesPage.getLayout = (page) => <Account>{page}</Account>

export default WebsitesPage
