import Account from 'app/core/layouts/Account/Account'
import { Container } from 'app/styles/grid'
import HubspotMeeting from 'integrations/Hubspot/HubspotMeeting'
import Accordion from 'app/core/components/Accordion/Accordion'

export default function AccountBookmeetingPage() {
    return (
        <Container>
            <h2>Click Below to Schedule a Meeting</h2>
            <Accordion title="Marketing & Consultation">
                <HubspotMeeting
                    url={`https://meetings.hubspot.com/dalin-bernard`}
                />
            </Accordion>
            <Accordion title="Website Development">
                <HubspotMeeting
                    url={`https://meetings.hubspot.com/christian213`}
                />
            </Accordion>
            <Accordion title="Content Creation">
                <HubspotMeeting
                    url={`https://meetings.hubspot.com/christian213`}
                />
            </Accordion>
        </Container>
    )
}

AccountBookmeetingPage.authenticate = { redirectTo: '/login' }
AccountBookmeetingPage.suppressFirstRenderFlicker = true
AccountBookmeetingPage.getLayout = (page) => (
    <Account title="Book a Meeting">{page}</Account>
)
