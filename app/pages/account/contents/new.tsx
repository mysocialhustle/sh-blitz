import { Link, useRouter, useMutation, BlitzPage, Routes } from 'blitz'
import Layout from 'app/core/layouts/Layout'
import createContent from 'app/admin/contents/mutations/createContent'
import {
    ContentForm,
    FORM_ERROR,
} from 'app/admin/contents/components/ContentForm'

const NewContentPage: BlitzPage = () => {
    const router = useRouter()
    const [createContentMutation] = useMutation(createContent)

    return (
        <div>
            <h1>Create New Content</h1>

            <ContentForm
                submitText="Create Content"
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                // schema={CreateContent}
                // initialValues={{}}
                onSubmit={async (values) => {
                    try {
                        const content = await createContentMutation(values)
                        router.push(
                            Routes.ContentOverviewPage({
                                contentId: content.id,
                            })
                        )
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            />

            <p>
                <Link href={Routes.ContentsPage()}>
                    <a>Contents</a>
                </Link>
            </p>
        </div>
    )
}

NewContentPage.authenticate = true
NewContentPage.getLayout = (page) => (
    <Layout title={'Create New Content'}>{page}</Layout>
)

export default NewContentPage
