import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getContent from 'app/admin/contents/queries/getContent'
import deleteContent from 'app/admin/contents/mutations/deleteContent'

export const Content = () => {
    const router = useRouter()
    const contentId = useParam('contentId', 'number')
    const [deleteContentMutation] = useMutation(deleteContent)
    const [content] = useQuery(getContent, { id: contentId })

    return (
        <>
            <Head>
                <title>Content {content.id}</title>
            </Head>

            <div>
                <h1>Content {content.id}</h1>
                <pre>{JSON.stringify(content, null, 2)}</pre>

                <Link href={Routes.EditContentPage({ contentId: content.id })}>
                    <a>Edit</a>
                </Link>

                <button
                    type="button"
                    onClick={async () => {
                        if (window.confirm('This will be deleted')) {
                            await deleteContentMutation({ id: content.id })
                            router.push(Routes.ContentsPage())
                        }
                    }}
                    style={{ marginLeft: '0.5rem' }}
                >
                    Delete
                </button>
            </div>
        </>
    )
}

const ContentOverviewPage: BlitzPage = () => {
    return (
        <div>
            <p>
                <Link href={Routes.ContentsPage()}>
                    <a>Contents</a>
                </Link>
            </p>

            <Suspense fallback={<div>Loading...</div>}>
                <Content />
            </Suspense>
        </div>
    )
}

ContentOverviewPage.authenticate = true
ContentOverviewPage.getLayout = (page) => <Layout>{page}</Layout>

export default ContentOverviewPage
