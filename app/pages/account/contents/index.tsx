import { Suspense } from 'react'
import {
    Head,
    Link,
    usePaginatedQuery,
    useRouter,
    BlitzPage,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getContents from 'app/admin/contents/queries/getContents'

const ITEMS_PER_PAGE = 100

export const ContentsList = () => {
    const router = useRouter()
    const page = Number(router.query.page) || 0
    const [{ contents, hasMore }] = usePaginatedQuery(getContents, {
        orderBy: { id: 'asc' },
        skip: ITEMS_PER_PAGE * page,
        take: ITEMS_PER_PAGE,
    })

    const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
    const goToNextPage = () => router.push({ query: { page: page + 1 } })

    return (
        <div>
            <ul>
                {contents.map((content) => (
                    <li key={content.id}>
                        <Link
                            href={Routes.ContentOverviewPage({
                                contentId: content.id,
                            })}
                        >
                            <a>{content.name}</a>
                        </Link>
                    </li>
                ))}
            </ul>

            <button disabled={page === 0} onClick={goToPreviousPage}>
                Previous
            </button>
            <button disabled={!hasMore} onClick={goToNextPage}>
                Next
            </button>
        </div>
    )
}

const ContentsPage: BlitzPage = () => {
    return (
        <>
            <Head>
                <title>Contents</title>
            </Head>

            <div>
                <p>
                    <Link href={Routes.NewContentPage()}>
                        <a>Create Content</a>
                    </Link>
                </p>

                <Suspense fallback={<div>Loading...</div>}>
                    <ContentsList />
                </Suspense>
            </div>
        </>
    )
}

ContentsPage.authenticate = true
ContentsPage.getLayout = (page) => <Layout>{page}</Layout>

export default ContentsPage
