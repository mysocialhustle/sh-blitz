import { Suspense } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useMutation,
    useParam,
    BlitzPage,
    Routes,
} from 'blitz'
import Layout from 'app/core/layouts/Layout'
import getContent from 'app/admin/contents/queries/getContent'
import updateContent from 'app/admin/contents/mutations/updateContent'
import {
    ContentForm,
    FORM_ERROR,
} from 'app/admin/contents/components/ContentForm'

export const EditContent = () => {
    const router = useRouter()
    const contentId = useParam('contentId', 'number')
    const [content, { setQueryData }] = useQuery(
        getContent,
        { id: contentId },
        {
            // This ensures the query never refreshes and overwrites the form data while the user is editing.
            staleTime: Infinity,
        }
    )
    const [updateContentMutation] = useMutation(updateContent)

    return (
        <>
            <Head>
                <title>Edit Content {content.id}</title>
            </Head>

            <div>
                <h1>Edit Content {content.id}</h1>
                <pre>{JSON.stringify(content, null, 2)}</pre>

                <ContentForm
                    submitText="Update Content"
                    // TODO use a zod schema for form validation
                    //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                    //         then import and use it here
                    // schema={UpdateContent}
                    initialValues={content}
                    onSubmit={async (values) => {
                        try {
                            const updated = await updateContentMutation({
                                id: content.id,
                                ...values,
                            })
                            await setQueryData(updated)
                            router.push(
                                Routes.ContentOverviewPage({
                                    contentId: updated.id,
                                })
                            )
                        } catch (error: any) {
                            console.error(error)
                            return {
                                [FORM_ERROR]: error.toString(),
                            }
                        }
                    }}
                />
            </div>
        </>
    )
}

const EditContentPage: BlitzPage = () => {
    return (
        <div>
            <Suspense fallback={<div>Loading...</div>}>
                <EditContent />
            </Suspense>

            <p>
                <Link href={Routes.ContentsPage()}>
                    <a>Contents</a>
                </Link>
            </p>
        </div>
    )
}

EditContentPage.authenticate = true
EditContentPage.getLayout = (page) => <Layout>{page}</Layout>

export default EditContentPage
