import { useMutation } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Container, Row, Column } from 'app/styles/grid'
import Company from 'app/core/sections/SalesOrderForm/Company'
import SalesOrderForm from 'app/core/blocks/Forms/SalesOrderForm'

export default function SalesOrderPage() {
    return (
        <Container>
            <SalesOrderForm />
        </Container>
    )
}

SalesOrderPage.authenticate = { redirectTo: '/login' }
SalesOrderPage.suppressFirstRenderFlicker = true
SalesOrderPage.getLayout = (page) => (
    <Account title="Sales Order Form">{page}</Account>
)
