import { BlitzPage, Routes, useMutation } from 'blitz'
import Account from 'app/core/layouts/Account/Account'
import { SignupForm } from 'app/auth/components/SignupForm'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import Password from 'app/core/components/Form/Password/Password'
import changePassword from 'app/auth/mutations/changePassword'

export default function AccountSettingsPage() {
    const currentUser = useCurrentUser()
    const [changePWMutation] = useMutation(changePassword)

    return (
        <Container>
            <h2>Edit Your Account Details</h2>
            <Form
                onSubmit={async (e) => console.log(e)}
                style={{ marginBottom: 50 }}
            >
                <Row>
                    <Column sm={50}>
                        <Input name="firstname" label="First Name" required />
                    </Column>
                    <Column sm={50}>
                        <Input name="lastname" label="Last Name" required />
                    </Column>
                    <Column sm={50}>
                        <Input
                            name="email"
                            label="Email Address"
                            type="email"
                            defaultValue={currentUser?.email}
                        />
                    </Column>
                    <Column sm={50}>
                        <Input name="phone" label="Telephone" type="tel" />
                    </Column>
                </Row>
            </Form>
            <h2>Change Your Password</h2>
            <Form
                onSubmit={async (values) => {
                    try {
                        const newPw = await changePWMutation(values)
                        console.log(newPw)
                    } catch (error: any) {
                        return {
                            [FORM_ERROR]:
                                'Sorry, we had an unexpected error. Please try again. - ' +
                                error.toString(),
                        }
                    }
                }}
            >
                <Row>
                    <Column sm={50}>
                        <Password
                            name="currentPassword"
                            label="Current Password"
                            required
                        />
                    </Column>
                    <Column sm={50}>
                        <Password
                            name="newPassword"
                            label="New Password"
                            required
                        />
                    </Column>
                </Row>
            </Form>
        </Container>
    )
}

AccountSettingsPage.authenticate = { redirectTo: '/login' }
AccountSettingsPage.suppressFirstRenderFlicker = true
AccountSettingsPage.getLayout = (page) => (
    <Account title="Your Account Settings">{page}</Account>
)
