import {
    Head,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import deleteCampaign from 'app/admin/campaigns/mutations/deleteCampaign'
import Account from 'app/core/layouts/Account/Account'
import BackButton from 'app/admin/components/BackButton/BackButton'
import { Container } from 'app/styles/grid'
import EditButton from 'app/admin/components/EditButton/EditButton'
import CampaignNavigation from 'app/admin/campaigns/components/Navigation'
import Icon from 'app/core/components/Icon/Icon'
import { campaigns } from 'app/data/campaigns'
import Section from 'app/core/components/Section/Section'
import Notes from 'app/admin/campaigns/components/Notes/Notes'
import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import Goals from 'app/admin/campaigns/components/Goals/Goals'
import Budget from 'app/admin/campaigns/components/Budget/Budget'
import Logins from 'app/admin/campaigns/components/Logins/Logins'
import DeleteButton from 'app/admin/components/DeleteModelButton/DeleteButton'
import { Divider } from 'app/styles/utilities'
import RelatedCompany from 'app/admin/components/RelatedCompany/RelatedCompany'

const CampaignOverviewPage: BlitzPage = () => {
    const { mode } = useContext(Context)
    const router = useRouter()
    const campaignId = useParam('campaignId', 'number')
    const [campaign] = useQuery(getCampaign, { id: campaignId })
    const [deleteCampaignMutation] = useMutation(deleteCampaign)
    const { type, budget, logins, goals } = campaign

    return (
        <>
            <Head>
                <title>{campaign.name}</title>
            </Head>
            <Container>
                <BackButton
                    href={Routes.CampaignsPage()}
                    label="BACK TO CAMPAIGNS"
                />
                <CampaignNavigation />
                <h1>{campaign.name}</h1>
                <RelatedCompany model={campaign} />
                <p>
                    <EditButton>Edit this campaign</EditButton>
                    <Divider />
                    <DeleteButton
                        onClick={async () => {
                            if (
                                window.confirm(
                                    'Are you sure you want to delete this campaign? This CANNOT be undone.'
                                )
                            ) {
                                await deleteCampaignMutation({
                                    id: campaign.id,
                                })
                                router.push(Routes.CampaignsPage())
                            }
                        }}
                    >
                        Delete
                    </DeleteButton>
                </p>
                <Section theme="transparentLight" size="tiny">
                    <Goals campaign={campaign} />
                    <Budget campaign={campaign} />
                    <Logins campaign={campaign} />
                    <Notes campaign={campaign} />
                </Section>
            </Container>
        </>
    )
}

CampaignOverviewPage.authenticate = true
CampaignOverviewPage.getLayout = (page) => <Account>{page}</Account>

export default CampaignOverviewPage
