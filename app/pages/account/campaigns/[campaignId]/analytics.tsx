import { useContext } from 'react'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import Account from 'app/core/layouts/Account/Account'
import BackButton from 'app/admin/components/BackButton/BackButton'
import { Container } from 'app/styles/grid'
import EditButton from 'app/admin/components/EditButton/EditButton'
import DataBox from 'app/admin/campaigns/components/Databox/Databox'
import CampaignNavigation from 'app/admin/campaigns/components/Navigation'
import Section from 'app/core/components/Section/Section'

export default function CampaignAnalyticsPage() {
    const campaignId = useParam('campaignId', 'number')
    const [campaign] = useQuery(getCampaign, { id: campaignId })
    return (
        <>
            <Head>
                <title>{campaign.name} Analytics</title>
            </Head>
            <Container>
                <BackButton
                    href={Routes.CampaignsPage()}
                    label="BACK TO CAMPAIGNS"
                />
                <CampaignNavigation />
                <h1>{campaign.name}</h1>
                <p>
                    <EditButton>Edit this campaign</EditButton>
                </p>
                <Section theme="transparentLight" size="tiny">
                    <DataBox campaign={campaign} />
                </Section>
            </Container>
        </>
    )
}

CampaignAnalyticsPage.authenticate = true
CampaignAnalyticsPage.getLayout = (page) => <Account>{page}</Account>
