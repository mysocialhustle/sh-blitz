import {
    Head,
    Link,
    useRouter,
    useQuery,
    useMutation,
    useParam,
    BlitzPage,
    Routes,
} from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import UpdateCampaign from 'app/admin/campaigns/components/Forms/UpdateCampaign'
import Account from 'app/core/layouts/Account/Account'
import BackButton from 'app/admin/components/BackButton/BackButton'
import { Container } from 'app/styles/grid'

export const EditCampaign = () => {
    const router = useRouter()
    const campaignId = useParam('campaignId', 'number')
    const [campaign, { setQueryData }] = useQuery(
        getCampaign,
        { id: campaignId },
        {
            // This ensures the query never refreshes and overwrites the form data while the user is editing.
            staleTime: Infinity,
        }
    )
    const [updateCampaignMutation] = useMutation(updateCampaign)

    return (
        <>
            <Head>
                <title>Edit {campaign.name}</title>
            </Head>

            <div>
                <h2>Edit {campaign.name}</h2>
                <pre>{JSON.stringify(campaign, null, 2)}</pre>
                <UpdateCampaign
                    campaign={campaign}
                    setQueryData={setQueryData}
                />
            </div>
        </>
    )
}

const EditCampaignPage: BlitzPage = () => {
    return (
        <Container>
            <EditCampaign />
        </Container>
    )
}

EditCampaignPage.authenticate = true
EditCampaignPage.getLayout = (page) => <Account>{page}</Account>

export default EditCampaignPage
