import Account from 'app/core/layouts/Account/Account'
import { Link, useRouter, useMutation, BlitzPage, Routes } from 'blitz'
import { Container, Row, Column } from 'app/styles/grid'
import createCampaign from 'app/admin/campaigns/mutations/createCampaign'
import CreateCampaign, {
    FORM_ERROR,
} from 'app/admin/campaigns/components/Forms/CreateCampaign'
import BackButton from 'app/admin/components/BackButton/BackButton'

const NewCampaignPage: BlitzPage = () => {
    const router = useRouter()
    const [createCampaignMutation] = useMutation(createCampaign)

    return (
        <Container>
            <BackButton href={Routes.CampaignsPage()} />
            <h2>Create a New Campaign</h2>

            <CreateCampaign
                submitText="Create Campaign"
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                // schema={CreateCampaign}
                // initialValues={{}}
                onSubmit={async (values) => {
                    try {
                        const campaign = await createCampaignMutation(values)
                        router.push(
                            Routes.CampaignOverviewPage({
                                campaignId: campaign.id,
                            })
                        )
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            />
        </Container>
    )
}

NewCampaignPage.authenticate = true
NewCampaignPage.getLayout = (page) => (
    <Account title={'Create New Campaign'}>{page}</Account>
)

export default NewCampaignPage
