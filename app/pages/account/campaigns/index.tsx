import Account from 'app/core/layouts/Account/Account'
import { Container } from 'app/styles/grid'
import { BlitzPage, Routes } from 'blitz'
import CampaignsList from 'app/admin/campaigns/components/CampaignsList/CampaignsList'
import Button from 'app/core/components/Button/Button'

const CampaignsPage: BlitzPage = () => {
    return (
        <Container>
            <h2>Campaigns</h2>
            <p>
                View your analytics & access your reports for each campaign
                here.
            </p>
            <p>
                <Button href={Routes.NewCampaignPage()}>
                    Create a New Campaign
                </Button>
            </p>
            <CampaignsList pagination={100} />
        </Container>
    )
}

CampaignsPage.authenticate = true
CampaignsPage.getLayout = (page) => <Account title="Campaigns">{page}</Account>

export default CampaignsPage
