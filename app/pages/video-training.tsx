import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Container } from 'app/styles/grid'
import Video from 'app/data-feeds/components/YouTube/Video/Video'
import Button from 'app/core/components/Button/Button'

export default function VideoTrainingPage() {
    return (
        <Section>
            <Container>
                <ScrollSpy>
                    <h1>Video Training</h1>
                </ScrollSpy>
                <ScrollSpy>
                    <p>
                        We believe that there is only a benefit to be gained
                        from sharing what we know. With over 25 years of
                        collective experience in our office, we have made the
                        decision to give back to our community both local and
                        digital. The following videos are free resources that we
                        have made available to everything.
                    </p>
                    <p>
                        They will walk you through the basics of every service
                        that we offer. We even have gone as far as to teach you
                        how to manage your own marketing accounts should you be
                        unable to partner with us at this time. Our hope is that
                        this knowledge and understanding will better your
                        ability to collaborate with us in the future. We believe
                        that better educated and informed partners create longer
                        laster and more rewarding partnerships.
                    </p>
                </ScrollSpy>
                <ScrollSpy>
                    <h2>
                        Dive Into Our Google Ads Academy By Watching The Videos
                        Below:
                    </h2>
                </ScrollSpy>
                <ScrollSpy>
                    <Video
                        id={process.env.BLITZ_PUBLIC_GOOGLE_ADS_PLAYLIST_ID}
                        title="Google Ads Playlist"
                        playlist={true}
                        playlistCoverId="GPYz1KCnS4I"
                    />
                    <Button
                        centered
                        href="https://www.youtube.com/channel/UCCGV8tK3JN4InG8UMuRoQ4A"
                        props={{
                            target: '_blank',
                        }}
                        style={{ maxWidth: 400, marginTop: 50 }}
                    >
                        VIEW OUR COMPLETE LIBRARY ON YOUTUBE
                    </Button>
                </ScrollSpy>
            </Container>
        </Section>
    )
}

VideoTrainingPage.getLayout = (page) => (
    <Layout title="Video Training">{page}</Layout>
)

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
