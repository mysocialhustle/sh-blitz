import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Headings from 'app/styles/headings'
import CareersForm from 'app/core/blocks/Forms/Careers'
import HubspotForm from 'integrations/Hubspot/HubspotForm'
import { Container, Row, Column } from 'app/styles/grid'

export default function CareersPage() {
    return (
        <Section>
            <Container>
                <Row alignX="center">
                    <Column sm={60}>
                        <ScrollSpy>
                            <h1>Careers</h1>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Headings.H4 as="h2">
                                Inquire about career opportunities at Social
                                Hustle.
                            </Headings.H4>
                        </ScrollSpy>
                        <ScrollSpy>
                            <HubspotForm
                                Component={CareersForm}
                                id="a00abc05-2103-43fd-b76f-27b7ae231968"
                                hotLead={false}
                            />
                        </ScrollSpy>
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}

CareersPage.getLayout = (page) => <Layout title="Careers">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
