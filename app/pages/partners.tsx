import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import Partners from 'app/core/sections/Partners/Partners'
import Headings from 'app/styles/headings'
import { Container, Row, Column } from 'app/styles/grid'

export default function PartnersPage() {
    return <Partners />
}

PartnersPage.getLayout = (page) => <Layout title="Partners">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
