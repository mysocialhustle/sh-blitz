import Layout from 'app/core/layouts/Layout'
import Contact from 'app/core/sections/Contact/Contact'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import HubspotMeeting from 'integrations/Hubspot/HubspotMeeting'

export default function ContactPage() {
    return (
        <>
            <Contact />
            <ScrollSpy>
                <HubspotMeeting
                    url={`https://meetings.hubspot.com/dalin-bernard`}
                />
            </ScrollSpy>
        </>
    )
}

ContactPage.getLayout = (page) => <Layout title="Contact">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
