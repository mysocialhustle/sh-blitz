import Layout from 'app/core/layouts/Layout'
import Consultation from 'app/data/services/consulting'
import ServiceTemplate from 'app/core/templates/Services'

export default function ConsultingPage(props) {
    return <ServiceTemplate page={props} />
}

ConsultingPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: 'd6321d48-5370-4b02-be4d-d4283895ab2d',
            ...Consultation,
        }, // will be passed to the page component as props
    }
}
