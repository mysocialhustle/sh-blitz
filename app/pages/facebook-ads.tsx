import Layout from 'app/core/layouts/Layout'
import FacebookAds from 'app/data/services/facebook-ads'
import ServiceTemplate from 'app/core/templates/Services'
import Instagram from 'app/core/sections/ServicePages/CustomSections/Instagram'

export default function FacebookAdsPage(props) {
    return <ServiceTemplate page={props} CustomComponent={Instagram} />
}

FacebookAdsPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: 'b556ddd3-0f6a-4af3-b0b2-5dc9fe9ff67a',
            ...FacebookAds,
        }, // will be passed to the page component as props
    }
}
