import Layout from 'app/core/layouts/Layout'
import GoogleAds from 'app/data/services/google-ads'
import ServiceTemplate from 'app/core/templates/Services'
import BingAds from 'app/core/sections/ServicePages/CustomSections/BingAds'
export default function GoogleAdsPage(props) {
    return <ServiceTemplate page={props} CustomComponent={BingAds} />
}

GoogleAdsPage.getLayout = (page) => (
    <Layout title={page?.props?.title} theme={page?.props?.hero?.theme}>
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    return {
        props: {
            formId: 'd6d13444-f8a3-4136-a125-286ccfe1bc73',
            ...GoogleAds,
        }, // will be passed to the page component as props
    }
}
