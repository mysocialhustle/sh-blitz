import { getAllProducts } from 'integrations/Shopify'
import Layout from 'app/core/layouts/Layout'
import Banner from 'app/core/components/Banner/Banner'
import ProductFeed from 'app/core/sections/Products/Feed/ProductFeed'

export default function AllProducts({ products }) {
    return (
        <>
            <Banner
                title="Shop"
                image={{
                    src: `https://socialhustle-blitz-static.s3.amazonaws.com/SH-Logo-White-Gang-01.jpg`,
                    alt: `Social Hustle Shop`,
                }}
            />
            <ProductFeed nodes={products?.edges} />
            {/**JSON.stringify(products)**/}
        </>
    )
}

AllProducts.getLayout = (page) => (
    <Layout title="Products" theme="dark">
        {page}
    </Layout>
)

export async function getStaticProps(context) {
    const products = await getAllProducts()

    return {
        props: {
            products: products,
        }, // will be passed to the page component as props
    }
}
