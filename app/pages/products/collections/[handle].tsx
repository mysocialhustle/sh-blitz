import { getCollection } from 'integrations/Shopify'
import Layout from 'app/core/layouts/Layout'
import Banner from 'app/core/components/Banner/Banner'
import ProductFeed from 'app/core/sections/Products/Feed/ProductFeed'

export default function ProductCollection({ collection }) {
    return (
        <>
            <Banner
                title={collection?.title}
                image={{
                    src: collection?.image?.url,
                    alt: collection?.image?.altText,
                }}
            />
            <ProductFeed nodes={collection?.products?.edges} />
        </>
    )
}

ProductCollection.getLayout = (page) => (
    <Layout title={page?.props?.collection?.title || 'Products'} theme="dark">
        {page}
    </Layout>
)

export async function getStaticProps({ params: { handle } }) {
    const collection = await getCollection(handle)

    return {
        props: {
            collection: collection,
        }, // will be passed to the page component as props
    }
}

export async function getStaticPaths() {
    return {
        paths: [],
        fallback: 'blocking',
    }
}
