import Layout from 'app/core/layouts/Layout'
import Feedback from 'app/core/sections/Feedback/Feedback'
import Section from 'app/core/components/Section/Section'
import { Container } from 'app/styles/grid'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Headings from 'app/styles/headings'

export default function FeedbackPage() {
    return (
        <Section>
            <Container
                style={{
                    textAlign: `center`,
                }}
            >
                <ScrollSpy>
                    <h1>Feedback</h1>
                </ScrollSpy>
                <ScrollSpy>
                    <Headings.H4 as="h2">
                        How would you rate your experience working with the
                        Social Hustle team?
                    </Headings.H4>
                </ScrollSpy>
            </Container>
            <Feedback />
        </Section>
    )
}

FeedbackPage.getLayout = (page) => <Layout title="Feedback">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
