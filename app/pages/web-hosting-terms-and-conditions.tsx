import Layout from 'app/core/layouts/Layout'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Headings from 'app/styles/headings'
import { Container, Row, Column } from 'app/styles/grid'

export default function HostingTaCPage() {
    return (
        <Section>
            <Container>
                <h1>Web Hosting Terms and Conditions</h1>
                <h2>Web Hosting – Terms Of Service</h2>
                <p>
                    These Terms of Service (the “Agreement”) set forth the terms
                    and conditions of Your Use of hosting and related services
                    (“Services”). In this Agreement “You” and “Your” refer to
                    You as the user of Our Services, or any agent, employee,
                    servant or person authorized to act on Your behalf. “We”,
                    “us” and “our” refer to Social Hustle, as well as its
                    subsidiaries and sister companies (“My Social Hustle”). This
                    Agreement explains Our obligations to You, and explains Your
                    obligations to Us for the various services or products
                    offered by Social Hustle (“Services”). When You use Your
                    account or permit someone else to use it to purchase or
                    otherwise acquire access to additional Services or to cancel
                    Your Services (even if We were not notified of such
                    authorization), You signify Your agreement to the terms and
                    conditions contained in this Agreement.
                </p>
                <h2>Term Of Agreement; Modification</h2>
                <p>
                    You agree that Social Hustle may modify this Agreement and
                    the Services it offers to You from time to time. You agree
                    to be bound by any changes Social Hustle may reasonably make
                    to this Agreement when such changes are made. If You have
                    purchased Services from Social Hustle, the terms and
                    conditions of this Agreement shall continue in full force
                    and effect as long as You take advantage of and use the
                    Services. By continuing to use the Services after any
                    revision to this Agreement or change in services, you agree
                    to abide by and be bound by any such revisions or changes.
                </p>
                <h2>Accurate Information</h2>
                <p>
                    You agree to maintain accurate information by providing
                    updates to Social Hustle, as needed, while You are using the
                    Services. You agree You will notify Social Hustle within
                    five (5) business days when any change of the information
                    You provided as part of the application and/or registration
                    process changes. Failure by You, for whatever reason, to
                    respond within five (5) business days to any inquiries made
                    by Social Hustle to determine the validity of information
                    provided by You will constitute a material breach of this
                    Agreement. If You provide any information that is
                    inaccurate, not current, false, misleading or incomplete, or
                    if Social Hustle has reasonable grounds to suspect that Your
                    information is inaccurate, not current, false, misleading or
                    incomplete, Social Hustle has the absolute right, in its
                    sole discretion, to terminate its Services and close Your
                    account.
                </p>
                <h2>Privacy</h2>
                <p>
                    Social Hustle ‘s Privacy Policy, which is incorporated
                    herein by reference, is applicable to all Services. The
                    Privacy Policy sets out Your rights and Social Hustle ‘s
                    responsibilities with regard to Your personal information.
                    Social Hustle will not use Your information in any way
                    inconsistent with the purposes and limitations provided in
                    the Privacy Policy. You agree that Social Hustle, in its
                    sole discretion, may modify the Privacy Policy, and You
                    further agree that, by using the Services after such
                    modifications become effective, You have agreed to these
                    modifications. You acknowledge that if you do not agree to
                    any such modification, you may terminate this Agreement.
                    Social Hustle will not refund any fees paid by You if You
                    terminate your Agreement under this provision. You represent
                    and warrant that You have provided notice to, and obtained
                    consent from, any third party individuals whose personal
                    data You supply to Social Hustle as part of the Services
                    with regard to: (i) the purposes for which such third
                    party’s personal data has been collected; (ii) the intended
                    recipients or categories of recipients of the third party’s
                    personal data; (iii) which parts of the third party’s data
                    are obligatory and which parts, if any, are voluntary; and
                    (iv) how the third party can access and, if necessary,
                    rectify the data held about them. You further agree to
                    provide such notice and obtain such consent with regard to
                    any third party personal data You supply to Social Hustle in
                    the future. Social Hustle is not responsible for any
                    consequences resulting from Your failure to provide notice
                    or receive consent from such individuals nor for Your
                    providing outdated, incomplete or inaccurate data.
                </p>
                <h2>Accepted Use Policy</h2>
                <p>
                    Social Hustle’s Accepted Use Policy (“AUP”), which is
                    incorporated herein by reference, is applicable to all
                    Services. You should use all Services for lawful purposes
                    only. You agree to maintain Your website in full compliance
                    with the terms and conditions set forth in the AUP. By using
                    any Services, You agree:
                </p>
                <p>
                    not to violate the laws, regulations, ordinances or other
                    such requirements of any applicable Federal, State or local
                    government.
                </p>
                <p>
                    not to transmit any unsolicited commercial or bulk email,
                    not to be engaged in any activity known or considered to be
                    spamming or Mail Bombing.
                </p>
                <p>
                    not to make any illegal communication to any Newsgroup,
                    Mailing List, Chat Facility, or another Internet Forum.
                </p>
                <p>
                    not to make, attempt or allow any unauthorized access to
                    Social Hustle website, servers, your own hosting account or
                    the account of any other customers of Social Hustle.
                </p>
                <p>
                    not to allow any remote code execution of malicious software
                    through the hosting account provided by Social Hustle.
                </p>
                <p>
                    not to cause denial of service attacks, port scans or other
                    endangering and invasive procedures against Social Hustle
                    servers and facilities or the servers and facilities of
                    other network hosts or Internet users.
                </p>
                <p>
                    not to forge the signature or other identifying mark or code
                    of any other person or engage in any activity to attempt to
                    deceive other persons regarding the true identity of the
                    User.
                </p>
                <p>
                    not to use Social Hustle services to host any website, other
                    content, links or advertisements of websites that: infringe
                    any copyright, trademark, patent, trade secret, or other
                    proprietary rights of any third party information; contain
                    nudity, pornography or other content deemed adult related;
                    profess hatred for particular social, ethnical, religious or
                    other group; contain viruses, Trojan horses, worms, time
                    bombs, corrupted files, or any other similar software or
                    programs that may damage the operation of a computer or a
                    person’s property; contain warez; contain any kind of proxy
                    server or other traffic relaying programs; promote money
                    making schemes, multi-level marketing or similar activities;
                    contain lottery, gambling, casino; contain torrent trackers,
                    torrent Portals or similar software; violent or encouraging
                    violence.
                </p>
                <p>
                    not to upload unacceptable material which includes: IRC
                    bots, warez, image, file storage, mirror, or banner-ad
                    services, topsites, streaming, Escrow, High-Yield Interest
                    Programs (HYIP) or related sites, investment sites (FOREX,
                    E-Gold Exchange, etc), bitcoin miners, sale of any
                    controlled substances without providing proof of appropriate
                    permit(s) in advance, AutoSurf sites, Bank Debentures, Bank
                    Debenture Trading Programs, Prime Banks Programs, lottery
                    sites, muds / rpg’s, hate sites, hacking focused
                    sites/archives/programs, or sites promoting illegal
                    activities, IP Scanners, Brute Force Programs, Mail Bombers
                    and Spam Scripts.
                </p>
                <p>
                    not to engage in or to instigate actions that cause harm to
                    Social Hustle or other customers. Such actions include, but
                    are not limited to, actions resulting in blacklisting any of
                    Our IPs by any online spam database, actions resulting in
                    DDOS attacks for any servers, etc. Social Hustle reserves
                    the right to refuse service to anyone upon Our discretion.
                    Any material that in Social Hustle judgment, is either
                    obscene or threatening is strictly prohibited and will be
                    removed from Social Hustle servers immediately with or
                    without prior notice and may lead to possible warning,
                    suspension or immediate account termination with no refund.
                    You agree that We have the sole right to decide what
                    constitutes a violation of the acceptable policy use
                    described above as well as what is the appropriate severity
                    of any corrective action to be applied. In the event that a
                    violation of Our Acceptable Use Policy is found, Social
                    Hustle will take corrective action upon our own discretion
                    and will notify You. Social Hustle decision in such a case
                    is binding and final, and cannot be a subject of a further
                    change. Social Hustle cannot and shall not be liable for any
                    loss or damage arising from Our measures against actions
                    causing harm to Social Hustle or any other third party. We
                    have the right to terminate each and any hosting account
                    that has been suspended for any reason for more than 14
                    calendar days after the suspension date, unless You have
                    taken corrective measures to remove the initial suspension
                    threat or violation. Any backup copies of the hosting
                    account will be permanently deleted upon termination and no
                    refund will be due. Social Hustle will not be liable for any
                    loss or damages in such cases.
                </p>
                <p>
                    not to violate the Ryan Haight Online Pharmacy Consumer
                    Protection Act of 2008 or similar legislation, or promote,
                    encourage or engage in the sale or distribution of
                    prescription medication without a valid prescription.
                </p>
                <p>
                    At its discretion, Social Hustle reserves the right to
                    investigate the use of its services for violations of its
                    policies. This includes all hosting packages and services.
                    Social Hustle further reserves the right to remove any
                    content we determine to be prohibited by this agreement or
                    our Terms and Conditions. No backups will be kept of removed
                    content.
                </p>
                <h2>Storage And Security</h2>
                <p>
                    At all times, You shall bear full risk of loss and damage to
                    Your server and all of Your server content. You are entirely
                    responsible for maintaining the confidentiality of Your
                    password and account information. You acknowledge and agree
                    that You are solely responsible for all acts, omissions and
                    use under and charges incurred with Your account or password
                    or in connection with the server or any of Your server
                    content displayed, linked, transmitted through or stored on
                    the server. You shall be solely responsible for undertaking
                    measures to: (i) prevent any loss or damage to Your server
                    content; (ii) maintain independent archival and backup
                    copies of Your server content; (iii) ensure the security,
                    confidentiality and integrity of Your server content
                    transmitted through or stored on Social Hustle servers; and
                    (iv) ensure the confidentiality of Your password. Social
                    Hustle services are not intended to be used for data backup
                    or archiving purposes. Using an account as an online storage
                    space for archiving electronic files is prohibited and will
                    result in termination of hosting services without prior
                    notice. We reserve the right to delete Your archives if they
                    affect Our overall server performance and Social Hustle
                    shall have no liability to You or any other person for loss,
                    damage or destruction of any of Your content. The services
                    offered by Social Hustle are not intended to provide a PCI
                    (Payment Card Industry) compliant environment and therefore
                    should not be utilized as such without further compliance
                    activity. Social Hustle shall have no liability to You or
                    any other person for Your use of Social Hustle products
                    and/or services in violation of these terms.
                </p>
                <h2>Ownership</h2>
                <p>
                    Except as otherwise set forth herein, all right, title and
                    interest in and to all, (i) registered and unregistered
                    trademarks, service marks and logos; (ii) patents, patent
                    applications, and patentable ideas, inventions, and/or
                    improvements; (iii) trade secrets, proprietary information,
                    and know-how; (iv) all divisions, continuations, reissues,
                    renewals, and extensions thereof now existing or hereafter
                    filed, issued, or acquired; (v) registered and unregistered
                    copyrights including, without limitation, any forms, images,
                    audiovisual displays, text, software and (vi) all other
                    intellectual property, proprietary rights or other rights
                    related to intangible property which are used, developed,
                    comprising, embodied in, or practiced in connection with any
                    of the Services identified herein (“IP rights”) are owned by
                    Social Hustle or its third party partners, and you agree to
                    make no claim of interest in or ownership of any such IP
                    rights. You acknowledge that no title to the IP rights is
                    transferred to you, and that You do not obtain any rights,
                    express or implied, in the Services, other than the rights
                    expressly granted in this Agreement. To the extent that you
                    create any Derivative Work (any work that is based upon one
                    or more preexisting versions of a work provided to you, such
                    as an enhancement or modification, revision, translation,
                    abridgement, condensation, expansion, collection,
                    compilation or any other form in which such preexisting
                    works may be recast, transformed or adapted) such Derivative
                    Work shall be owned by Social Hustle and all right, title
                    and interest in and to each such Derivative Work shall
                    automatically vest in Social Hustle. Social Hustle shall
                    have no obligation to grant You any right in any such
                    Derivative Work.
                </p>
                <h2>Non-Exclusive Right To Use</h2>
                <p>
                    If You have purchased and/or been given permission to use
                    software from Social Hustle, Social Hustle grants You a
                    limited, non-exclusive, nontransferable and non-assignable
                    right and ability to use the software for such purposes as
                    are ordinary and customary. You are free to use the software
                    on any computer, but not on two or more computers at one
                    time. You agree to not alter or modify the software. You
                    agree You are not authorized to combine the software with
                    any other software program, create derivative works based
                    upon the software, nor are You authorized to integrate any
                    plug-in or enhancement which uses or relies upon the
                    software. You further agree not to reverse engineer,
                    decompile or otherwise attempt to uncover the source code.
                    Social Hustle reserves all rights to the software. The
                    software and any copies You are authorized to make are the
                    intellectual property of Social Hustle. The source code and
                    its organization are the exclusive property of Social Hustle
                    and the software is protected by copyright law. Except as
                    expressly provided for in this section, this Agreement does
                    not grant You any rights in the software and all rights are
                    reserved by Social Hustle.
                </p>
                <p>
                    Any such software and Services are provided to You “as is”
                    without warranty of any kind either express or implied,
                    including but not limited to the implied warranties or
                    conditions of merchantability or fitness for a particular
                    purpose.
                </p>
                <h2>Third-Party Software</h2>
                <p>
                    Social Hustle provides some third-party software to You for
                    easier account management including, but is not limited to
                    cPanel, etc. Such software is provided on an “as is” as
                    available basis. We do not guarantee that any specific
                    results can be obtained by using such software. Social
                    Hustle does not take responsibility for any faults in such
                    software functioning.
                </p>
                <p>
                    By using Social Hustle Services that include such
                    third-party software, you specifically agree to the relevant
                    third-party terms of service and that You shall use the
                    services in accordance with such terms. The following are
                    examples of where you attest that You agree to third-party
                    software terms. The examples are not exhaustive. If You
                    order cPanel to use for Your hosting services, Your order
                    constitutes explicit agreement with cPanel’s terms and You
                    authorize Social Hustle to agree to such terms on Your
                    behalf. The terms of service for cPanel may be found here.
                    If You purchase Microsoft Windows for a dedicated server and
                    request that Social Hustle manually assist You with
                    installation of this product, Your request constitutes
                    explicit agreement with Microsoft Windows’ terms of service
                    and You authorize Social Hustle to agree to such terms on
                    Your behalf. Microsoft Windows’ terms of service may be
                    found here. Your failure to abide by any third-party license
                    may result in the immediate termination of Your Services by
                    Social Hustle.
                </p>
                <p>
                    You can add and use third-party software on Your account
                    only if it is compatible with Our servers and is approved by
                    Social Hustle. Your use of any third party software is at
                    Your own risk. Social Hustle does not control and therefore
                    cannot be responsible for any third party software
                    performance and provides no guarantees that its use will
                    result in any particular outcome or result. Social Hustle
                    will have no liability or responsibility for any damage,
                    loss of data, loss of use or other loss occurring in
                    connection with Your use of third party software or
                    products. Social Hustle reserves the right, at its sole
                    discretion, to terminate, suspend, cancel or alter Your
                    access to third-party software at any time.
                </p>
                <p>
                    You are solely responsible for any license and other fees
                    required by the software providers, for using any
                    third-party software installed on Your account apart from
                    the initial account setup.
                </p>
                <h2>Third-Party Content</h2>
                <p>
                    If You elect to sell or resell advertising or web space to a
                    third party then You will be responsible for the contents of
                    that advertising and the actions of that third party. Social
                    Hustle has the absolute right to reject any advertising or
                    other third party content that is illegal, offensive or
                    otherwise in breach of the then current Social Hustle policy
                    or agreement. Such content may result in the suspension or
                    in the immediate termination of Your account. You are
                    responsible for monitoring all service renewals and orders.
                    In the event that an error occurs the account holder must
                    notify Social Hustle immediately of the error. In no event
                    shall Social Hustle be liable to the Account Holder for any
                    damages resulting from or related to any failure or delay of
                    domain registration, transfer or renewal.
                </p>
                <h2>Billing And Payment</h2>
                <p>
                    All fees for the Services shall be in accordance with Social
                    Hustle ‘s fee schedule then in effect, the terms of which
                    are incorporated herein by reference, and shall be due at
                    the times provided therein. Fees for renewal periods after
                    the Initial Term shall be due and owing immediately upon the
                    first day of such renewal period. Social Hustle may impose a
                    debt service charge equal to one and one-half percent (1.5%)
                    of the overdue balance for each month or fraction thereof
                    the overdue amount remains unpaid. In addition, in the event
                    that any amount due Social Hustle remains unpaid seven (7)
                    days after such payment is due for shared hosting packages
                    and three (3) days for VPS and dedicated servers, Social
                    Hustle, in its sole discretion, may immediately terminate
                    this agreement, and/or withhold or suspend Services. There
                    will be a $15.00 fee to reinstate accounts that have been
                    suspended or terminated. All taxes, fees and governmental
                    charges relating to the Services provided hereunder shall be
                    paid by You.
                </p>
                <p>
                    If You signed up for a monthly payment plan, Your monthly
                    billing date will be determined based on the day of the
                    month You purchased the products or Services. If You signed
                    up for an annual (or longer) payment plan, and You elected
                    the automatic renewal option, Social Hustle will
                    automatically renew Your Services when they come up for
                    renewal and will take payment in accordance with the
                    designated payment method at Social Hustle ‘s then current
                    rates.
                </p>
                <p>
                    If you improperly charge back for web hosting services
                    rendered, we may disable the ability to transfer any other
                    your Social Hustle services away from Social Hustle.
                </p>
                <h2>Termination &amp; Cancellation Policy</h2>
                <p>
                    The initial term of Your agreement with Social Hustle shall
                    be as set forth in Your Order Form. The Initial Term shall
                    begin upon commencement of the Services in the Order Form.
                    After the Initial Term, your agreement with Social Hustle
                    shall automatically renew for successive terms of equal
                    length as the Initial Term, unless terminated or cancelled
                    by either party as provided in this section.
                </p>
                <p>
                    This agreement may be terminated: (i) by You by submitting a
                    helpdesk ticket under Billing Issues category at least three
                    (3) working days before the account is due to renew; or (ii)
                    by Social Hustle at any time, without prior notice, if, in
                    Social Hustle ‘s judgment, You are in violation of any terms
                    or conditions herein; or (iii) in Social Hustle ‘s sole
                    judgment, Your use of the Services places or is likely to
                    place unreasonable demands upon Social Hustle or could
                    disrupt Social Hustle ‘s business operations; or (iv) by
                    Social Hustle if it so determines that You are or are
                    alleged to be violating the terms and conditions of any
                    other agreement entered into by You and either Social Hustle
                    or Social Hustle.
                </p>
                <p>
                    In the event of termination or suspension of Services under
                    the above circumstances, You agree (a) that no pre-paid fees
                    will be refunded to You; and (b) that Social Hustle may take
                    control of any domain name associated with the terminated
                    Services, provided such domain name was registered through
                    the domain name registration of Social Hustle.
                </p>
                <p>
                    You may receive a refund if: i) Your Standard Hosting or
                    Premium Hosting account is cancelled within the initial 30
                    days after sign-up. Only first-time Standard Hosting or
                    Premium Hosting clients are eligible for the 30-day
                    money-back guarantee. If the account holder cancels after
                    the time period specified, there will be no refund given.
                    Refunds do NOT apply to dedicated IP addresses, SSL
                    certificates, renewals, any third party product (WordFence
                    Premium, Databox) that are not included into the package and
                    are purchased at additional fee, domain registration related
                    fees; (iii) Your Dedicated Clearance Server or Dedicated New
                    Server account is cancelled within the initial 7 days after
                    sign-up. Only first-time Dedicated Server accounts are
                    eligible for the 7-day money-back guarantee. If the account
                    holder cancels after the time period specified, there will
                    be no refund given. Refunds do NOT apply to dedicated IP
                    addresses, SSL certificates, renewals, any third party
                    product (WordFence Premium, Databox) that are not included
                    into the package and are purchased at additional fee, domain
                    registration related fees.
                </p>
                <p>
                    In the event of termination of this Agreement caused by your
                    default hereunder, you shall bear all costs of termination,
                    including any reasonable costs Social Hustle incurs in
                    closing your account. You agree to pay any and all costs
                    incurred by Social Hustle in enforcing your compliance with
                    this Section. Upon termination, you shall destroy any copy
                    of the materials provided to you hereunder and referenced
                    herein. You agree that upon termination or discontinuance
                    for any reason, Social Hustle may delete all information
                    related to you on the Services.
                </p>
                <h2>Customer Support</h2>
                <p>
                    Social Hustle provides customer support to You at no
                    additional fee for issues related to Social Hustle service
                    only. Social Hustle has the right to decide what is a
                    service related issue and to charge additional fees or
                    refuse support for non-service related issues. Any fees paid
                    by You for providing non-service related support are
                    non-refundable.
                </p>
                <p>
                    You can request customer support via email by sending a
                    request to{' '}
                    <a href="mailto:christian@mysocialhustle.com">
                        christian@mysocialhustle.com
                    </a>{' '}
                    You acknowledge that by asking our customer support
                    representatives for assistance, You authorize their
                    intervention and operation in Your account.
                </p>
                <p>
                    You must provide Social Hustle with all information and
                    access to facilities that Social Hustle may reasonably
                    require to provide the requested customer support.
                </p>
                <p>
                    You are solely liable for performing and storing a back-up
                    copy of your data, files and hosting account prior to
                    requesting customer support and agreeing to any interference
                    or operation, provided by Social Hustle. In the event You
                    are not satisfied with the outcome of any action You shall
                    be solely responsible for restoring the back-up copies of
                    Your data.
                </p>
                <p>
                    In the event You require server support from us for Your VPS
                    or Dedicated server with User-Responsible or Basic types of
                    Server Management, Social Hustle reserves a right to decide
                    whether or not to provide server support, based on the
                    results of preliminary server check. Should Social Hustle
                    decide, in its sole discretion, to provide support, this
                    service will be provided at an additional cost according to
                    the pricing indicated on the New Servers Page Server
                    Management Section.
                </p>
                <h2>Legal Purposes</h2>
                <p>
                    The web hosting and reseller hosting account and/or related
                    electronic services can only be used for legal purposes
                    under all applicable international, federal, provincial, and
                    municipal laws. The intent of Social Hustle is to provide
                    space to serve web documents, not as an off-site storage
                    area for electronic files and is governed by the AUP.
                    Violations of the AUP or any other provisions of this
                    Agreement may result in termination of the Services provided
                    by Social Hustle, with or without the grant of a notice or
                    cure period, such notice or cure period to be granted at the
                    sole discretion of Social Hustle based upon the severity of
                    the violation. Social Hustle reserves the right to refuse
                    Service if any of the content within, or any links from, the
                    Your website is deemed illegal, misleading, or obscene, or
                    is otherwise in breach of Social Hustle ‘s AUP, in the sole
                    and absolute opinion of Social Hustle. You agree that Social
                    Hustle shall not be liable to you for loss or damages that
                    may result from its refusal to host your website or provided
                    the Services under this Agreement.
                </p>
                <h2>Account Use</h2>
                <p>
                    You agree to follow generally accepted rules of “Netiquette”
                    when sending e-mail messages or posting to newsgroups. You
                    are responsible for the security of Your password. Social
                    Hustle will not change passwords to any account without
                    proof of identification, which is satisfactory to Social
                    Hustle, which may include written authorization with
                    signature. In the event of any partnership break-up, divorce
                    or other legal problems that includes You, You understand
                    that Social Hustle will remain neutral and may put the
                    account on hold until the situation has been resolved. Under
                    no circumstances will Social Hustle be liable for any losses
                    incurred by You during this time of determination of
                    ownership, or otherwise. You agree to defend (through
                    counsel of Our choosing), indemnify and hold harmless Social
                    Hustle from any and all claims arising from such ownership
                    disputes. If you are required to supply or transmit
                    sensitive information to Social Hustle you should take all
                    due precautions to provide any sensitive information over a
                    secure communication channel.
                </p>
                <h2>Currency</h2>
                <p>
                    While all purchases are processed in US dollars, Social
                    Hustle may provide an estimated conversion price to
                    currencies other than US dollars. You acknowledge and agree
                    that the pricing displayed during the checkout process is an
                    estimate. Due to potential slight time delays between actual
                    purchase and the payment settlement, the actual price
                    charged may fluctuate. Accordingly, Social Hustle makes no
                    representations or warranties that the actual price will be
                    the same or substantially similar to the actual price You
                    will pay and You waive any and all claims based upon any
                    discrepancy between the estimate and the actual price. In
                    addition, You may be charged VAT, based on the country
                    indicated in Your billing address section. Any amounts to be
                    charged will appear during the checkout process.
                </p>
                <h2>Limitation Of Liability; Waiver And Release</h2>
                <p>
                    The Services offered by Social Hustle are being provided on
                    an “AS IS” and Social Hustle expressly disclaims any and all
                    warranties, whether express or implied, including without
                    limitation any implied warranties of merchantability or
                    fitness for a particular purpose and non-infringment, to the
                    fullest extent permitted or authorized by law. Without
                    limitation of the foregoing, Social Hustle expressly does
                    not warrant that the Social Hustle Services will meet Your
                    requirements, function as intended, or that the use of the
                    provided Services will be uninterrupted or error free. You
                    understand and agree that any material and/or data
                    downloaded or otherwise obtained through the use of the
                    Services is done at your own discretion and risk and that
                    you will be solely responsible for any damage to your
                    computer system or loss of data that results from the
                    download of such material and/or data. No advice or
                    information, whether oral or written, obtained by you from
                    Social Hustle shall create any warranty not expressly made
                    herein. You agree that Social Hustle will not be liable for
                    any (i) suspension or loss of the Services, except to the
                    limited extent that a remedy is provided under this
                    Agreement; (ii) interruption of business; (iii) access
                    delays or access interruptions to the website(s) provided
                    through or by the Services; (iv) loss or liability resulting
                    from acts of god; (v) data non-delivery, mis-delivery,
                    corruption, destruction or other modification; (vi) events
                    beyond the control of Social Hustle; (vii) the processing of
                    Your application for Services; or (viii) loss or liability
                    resulting from the unauthorized use or misuse of Your
                    account identifier or password.
                </p>
                <p>
                    In no event shall Social Hustle be liable for any or all
                    direct, indirect, incidental, special, exemplary or
                    consequential damages (including, but not limited to,
                    procurement of substitute goods or services; loss of use,
                    data, or profits; or business interruption) however caused
                    and on any theory of liability, whether in contract, strict
                    liability, or tort (including, but not limited to,
                    negligence or otherwise) arising in any way out of the use
                    of the Services, even if Social Hustle is aware of or has
                    been advised of the possibility of such damages.
                </p>
                <p>
                    In addition, You specifically acknowledge and agree that any
                    cause of action arising out of or related to Social Hustle
                    or the Services provided by Social Hustle must be commenced
                    within one (1) year after the cause of action accrues,
                    otherwise such cause of action shall be permanently barred.
                </p>
                <p>
                    In addition, You specifically acknowledge and agree that in
                    no event shall Social Hustle’s total aggregate liability
                    exceed the total amount paid by You for the particular
                    Services that are the subject of the cause of action.
                </p>
                <p>
                    The foregoing limitations shall apply to the fullest extent
                    permitted by law, and shall survive any termination or
                    expiration of these Terms of Service or Your use of Social
                    Hustle or its Services offered.
                </p>
                <p>
                    No waiver of any provision of this Agreement shall be
                    effective unless it is in writing and signed by an
                    authorized representative of Social Hustle.
                </p>
                <h2>Indemnification</h2>
                <p>
                    Accordingly, You for Yourself and all of Your heirs,
                    personal representatives, predecessors, successors and
                    assigns, hereby fully release, remise, and forever discharge
                    Social Hustle and all affiliates of Social Hustle, and all
                    officers, agents, employees, and representatives of Social
                    Hustle, and all of their heirs, personal representatives,
                    predecessors, successors and assigns, for, from and against
                    any and all claims, liens, demands, causes of action,
                    controversies, offsets, obligations, losses, damages and
                    liabilities of every kind and character whatsoever,
                    including, but not limited to, any action omission,
                    misrepresentation or other basis of liability founded either
                    in tort or contract and the duties arising thereunder,
                    whether known or unknown, relating to or arising out of, or
                    in any way connected with or resulting from, the Services
                    and Your acquisition and use thereof, including, but not
                    limited to, the provision of the Social Hustle products
                    and/or services by Social Hustle and its agents and
                    employees. Further, You agree to defend, indemnify and hold
                    harmless Social Hustle and any of its contractors, agents,
                    employees, officers, directors, shareholders, affiliates and
                    assigns from any loss, liability, damages or expense,
                    including reasonable attorneys’ fees, arising out of (i) any
                    breach of any representation or warranty provided in this
                    Agreement, or as provided by Social Hustle’s AUP or any
                    other agreement that has been incorporated by reference
                    herein; (ii) the Services or your use of the Services,
                    including without limitation infringement or dilution by You
                    or by another using the Services from Your computer; (iii)
                    any intellectual property or other proprietary right of any
                    person or entity; (iv) any information or data You supplied
                    to Social Hustle, including, without limitation, any
                    misrepresentation in Your application, if applicable; (v)
                    the inclusion of metatags or other elements in any website
                    created for you or by you via the Services; (vi) any
                    information, material, or services available on your Social
                    Hustle Hosted website; or (vii), any negligence or willful
                    misconduct by You, or any allegation that Your account
                    infringes a third person’s copyright, trademark or
                    proprietary or intellectual property right, or
                    misappropriates a third person’s trade secrets.
                </p>
                <p>
                    This indemnification is in addition to any indemnification
                    required of You elsewhere. Should Social Hustle be notified
                    of a pending law suit, or receive notice of the filing of a
                    law suit, Social Hustle may seek a written confirmation from
                    You concerning Your obligation to defend, indemnify Social
                    Hustle. Such written confirmation may include the posting of
                    performance bonds or other guarantees. Your failure to
                    provide such a confirmation may be considered a breach of
                    this agreement. You agree that Social Hustle shall have the
                    right to participate in the defense of any such claim
                    through counsel of its own choosing. You agree to notify
                    Social Hustle of any such claim promptly in writing and to
                    allow Social Hustle to control the proceedings. You agree to
                    cooperate fully with Social Hustle during such proceedings.
                    The terms of this section will survive any termination or
                    cancellation of this Agreement.
                </p>
                <h2>Trademark Or Copyright Claims</h2>
                <p>
                    Social Hustle is a service provider and respects the
                    copyrights and other intellectual property rights of others
                    [and herein incorporates its Copyright Infringement Policy].
                    To the extent Social Hustle receives a proper notice of
                    infringement of copyright, trademark or other intellectual
                    property, Social Hustle reserves the right to access,
                    preserve and disclose to third parties any of Your
                    information or data (including personally identifiable
                    information and private communications) related to a written
                    complaint of infringement if Social Hustle believes in its
                    sole discretion that such access, preservation, or
                    disclosure is necessary or useful to respond to or otherwise
                    address such complaint.
                </p>
                <p>
                    Social Hustle expressly reserves the right to terminate in
                    appropriate circumstances an account or the access rights of
                    a subscriber for repeated copyright infringement. Social
                    Hustle also reserve the right to terminate an account or
                    subscriber for even one instance of infringement.
                </p>
                <p>
                    Proper notice of infringement shall include the following
                    information in writing to Social Hustle’s designated agent:
                </p>
                <p>
                    the electronic or physical signature of the rights holder or
                    the person authorized to act on behalf of that person;
                </p>
                <p>identification of the work that has been infringed;</p>
                <p>
                    an identification of the material that is claimed to be
                    infringing, and information reasonably sufficient to permit
                    Social Hustle to locate the material (for example, by
                    providing a URL to the material); or, if applicable,
                    identification of the reference or link to material or
                    activity claimed to be infringing, and information
                    reasonably sufficient to permit Social Hustle to locate that
                    reference or link;
                </p>
                <p>Your name, address, telephone number, and email address;</p>
                <p>
                    a statement by You that You have a good faith belief that
                    the disputed use is not authorized by the rights holder, its
                    agent, or the law; and
                </p>
                <p>
                    a statement that the information in Your notification is
                    accurate and a statement, made under penalty of perjury,
                    that You are the rights holder or are authorized to act on
                    the behalf of the rights holder.
                </p>
                <p>
                    Notice of infringement must be sent to Social Hustle’s
                    designated agent to receive notification of claimed
                    infringement as follows: Attn: Legal Department, Social
                    Hustle, 4600 East Washington Street, Suite 305, Phoenix, AZ
                    85034, facsimile: (310)-312-9513.
                </p>
                <h2>Additional Reservation Of Rights</h2>
                <p>
                    Social Hustle expressly reserves the right to deny, cancel,
                    terminate, suspend, lock, or modify access to (or control
                    of) any account or any Services (including the right to
                    cancel or transfer any domain name registration) for any
                    reason (as determined by Social Hustle in its sole and
                    absolute discretion), including but not limited to the
                    following: (i) to correct mistakes made by Social Hustle in
                    offering or delivering any Services (including any domain
                    name registration); (ii) to protect the integrity and
                    stability of, and correct mistakes made by, any domain name
                    registry; (iii) to assist with our fraud and abuse detection
                    and prevention efforts; (iv) to comply with applicable
                    local, state, national and international laws, rules and
                    regulations; (v) to comply with requests of law enforcement,
                    including subpoena requests; (vi) to comply with any dispute
                    resolution process; (vii) to defend any legal acction or
                    threatened legal action without consideration for whether
                    such legal action or threatened legal action is eventually
                    determined to be with or without merit, or (viii) to avoid
                    any civil or criminal liability on the part of Social
                    Hustle, its officers, directors, employees and agents, as
                    well as Social Hustle’s affiliates.
                </p>
                <p>
                    In the event that Social Hustle need exercise any of its
                    rights expressed herein to investigate any potential breach
                    or violation of the terms and conditions of this Agreement,
                    service fees may continue to accrue on your accounts, and
                    you will continue to remain responsible for the payment of
                    any service fees that accrue during the relevant period.
                </p>
                <p>Governing Law And Jurisdiction For Disputes</p>
                <p>
                    Except as otherwise set forth in the UDRP or any similar
                    policy with respect to any dispute regarding the Services
                    provided under this Agreement, Your rights and obligations
                    and all actions contemplated by this Agreement shall be
                    governed by the laws of the United States of America and the
                    State of Arizona. You agree that any action to enforce this
                    agreement or any matter relating to Your use of the Services
                    must be brought exclusively in the United States District
                    Court of Arizona, or if there is no jurisdiction in such
                    court, then in a state court in Maricopa County, State of
                    Arizona.
                </p>
                <h2>Notices</h2>
                <p>
                    You agree that any notices required to be given under this
                    Agreement by Us to You will be deemed to have been given if
                    delivered in accordance with the account and/or domain name
                    Whois information You have provided. You acknowledge that it
                    is Your responsibility to maintain current contact
                    information in the account and/or domain name Whois
                    information You have provided.
                </p>
                <h2>Legal Age</h2>
                <p>
                    You attest that you are of legal age (18 or over) to enter
                    into this Agreement.
                </p>
                <h2>Final Agreement</h2>
                <p>
                    This Agreement, together with all modifications, constitutes
                    the complete and exclusive agreement between You and Us, and
                    supersede and govern all prior proposals, agreements, or
                    other communications. This Agreement may not be amended or
                    modified by You except by means of a written document signed
                    by both You and an authorized representative of Us. By
                    applying for Social Hustle’s services through the online
                    application process or otherwise, or by using the Services
                    under this Agreement, you acknowledge that you have read and
                    agree to be bound by all terms and conditions of this
                    Agreement and documents incorporated by reference.
                </p>
                <h2>No Agency Relationship</h2>
                <p>
                    Nothing contained in this Agreement shall be construed as
                    creating any agency, partnership, or other form of joint
                    enterprise between the parties hereto. Each party shall
                    ensure that the foregoing persons shall not represent to the
                    contrary, either expressly, implicitly, by appearance or
                    otherwise.
                </p>
                <h2>Enforceability</h2>
                <p>
                    In the event that any provision of this Agreement shall be
                    unenforceable or invalid under any applicable law or be so
                    held by applicable court decision, such unenforceability or
                    invalidity shall not render this Agreement unenforceable or
                    invalid as a whole. We will amend or replace such provision
                    with one that is valid and enforceable and which achieves,
                    to the extent possible, our original objectives and intent
                    as reflected in the original provision.
                </p>
                <h2>Assignment And Resale</h2>
                <p>
                    Except as otherwise set forth herein, Your rights under this
                    Agreement are not assignable or transferable. Any attempt by
                    Your creditors to obtain an interest in Your rights under
                    this Agreement, whether by attachment, levy, garnishment or
                    otherwise, renders this Agreement voidable at Our option.
                    You agree not to reproduce, duplicate, copy, sell, resell or
                    otherwise exploit for any commercial purposes any of the
                    Services (or portion thereof) without Social Hustle’s prior
                    express written consent.
                </p>
                <h2>Force Majeure</h2>
                <p>
                    Neither party shall be deemed in default hereunder, nor
                    shall it hold the other party responsible for, any
                    cessation, interruption or delay in the performance of its
                    obligations hereunder due to causes beyond its control
                    including, but not limited to: earthquake; flood; fire;
                    storm; natural disaster; act of God; war; terrorism; armed
                    conflict; labor strike; lockout; boycott; supplier failures,
                    shortages, breaches, or delays; or any law, order
                    regulation, direction, action or request of the government,
                    including any federal, state and local governments having or
                    claiming jurisdiction over Social Hustle, or of any
                    department, agency, commission, bureau, corporation or other
                    instrumentality of any federal, state, or local government,
                    or of any civil or military authority; or any other cause or
                    circumstance, whether of a similar or dissimilar nature to
                    the foregoing, beyond the reasonable control of the affected
                    party, provided that the party relying upon this section (i)
                    shall have given the other party written notice thereof
                    promptly and, in any event, within five (5) days of
                    discovery thereof and (ii) shall take all steps reasonably
                    necessary under the circumstances to mitigate the effects of
                    the force majeure event upon which such notice is based;
                    provided further, that in the event a force majeure event
                    described in this Section extends for a period in excess of
                    thirty (30) days in the aggregate, Social Hustle may
                    immediately terminate this Agreement.
                </p>
                <h2>Headings</h2>
                <p>
                    The section headings appearing in this Agreement are
                    inserted only as a matter of convenience and in no way
                    define, limit, construe or describe the scope or extent of
                    such section or in any way affect such section.
                </p>
            </Container>
        </Section>
    )
}

HostingTaCPage.getLayout = (page) => <Layout title="">{page}</Layout>

export async function getStaticProps(context) {
    return {
        props: {}, // will be passed to the page component as props
    }
}
