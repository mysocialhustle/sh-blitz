import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const UpdateCampaign = z.object({
    id: z.number(),
    databox: z
        .array(
            z.object({
                box: z.string(),
                label: z.string(),
            })
        )
        .optional(),
    reports: z
        .array(
            z.object({
                name: z.string(),
                link: z.string(),
            })
        )
        .optional(),
    notes: z
        .array(
            z.object({
                label: z.string(),
                value: z.string(),
            })
        )
        .optional(),
    budget: z.string().optional(),
    goals: z.string().optional(),
    onboarded: z.boolean().optional(),
    logins: z
        .array(
            z.object({
                website: z.string().optional(),
                username: z.string().optional(),
                password: z.string().optional(),
            })
        )
        .optional(),
})

export default resolver.pipe(
    resolver.zod(UpdateCampaign),
    resolver.authorize(),
    async ({ id, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const campaign = await db.campaign.update({ where: { id }, data })

        return campaign
    }
)
