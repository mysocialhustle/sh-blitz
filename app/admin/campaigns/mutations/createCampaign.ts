import { resolver } from 'blitz'
import db, { CampaignType } from 'db'
import { z } from 'zod'

const CreateCampaign = z.object({
    name: z.string(),
    type: z.nativeEnum(CampaignType),
    companyId: z.number(),
})

export default resolver.pipe(
    resolver.zod(CreateCampaign),
    resolver.authorize(),
    async (input) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const campaign = await db.campaign.create({ data: input })

        return campaign
    }
)
