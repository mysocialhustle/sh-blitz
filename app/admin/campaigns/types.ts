export interface CampaignDatabox {
    box: string
    label: string
}

export interface CampaignReport {
    name: string
    link: string
}

export interface CampaignNote {
    label: string
    value: string
}

export interface CampaignLogin {
    website?: string
    username: string
    password: string
}
