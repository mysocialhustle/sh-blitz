import { Campaign } from 'db'
import Edit from './Edit'
import { Row } from 'app/styles/grid'
import Box from './Box'
import { CampaignDatabox } from '../../types'

interface Props {
    campaign: Campaign
}

export default function DataBox({ campaign }: Props) {
    const databox = campaign.databox as unknown as CampaignDatabox[]

    return (
        <>
            {databox && Array.isArray(databox) && databox?.length > 0 && (
                <>
                    <h3>Analytics</h3>
                    <p>
                        Track the metrics &amp; KPIs for {campaign.name} here.
                    </p>
                    <Row>
                        {databox.map((box, index) => (
                            <Box
                                key={index}
                                index={index}
                                campaign={campaign}
                                {...box}
                            />
                        ))}
                    </Row>
                </>
            )}
            <Edit campaign={campaign} />
        </>
    )
}
