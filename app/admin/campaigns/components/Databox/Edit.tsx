import { Context } from 'app/admin/contexts'
import { useContext, useState } from 'react'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import { Campaign } from 'db'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import hasValue from 'app/core/utils/hasValue'
import { CampaignDatabox } from '../../types'
import AddDeleteButtons from 'app/admin/components/AddDeleteButtons/AddDeleteButtons'

interface Props {
    campaign: Campaign
}

export default function Edit({ campaign }: Props) {
    const { mode } = useContext(Context)
    const [boxCount, setBoxCount] = useState(1)
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const databox = campaign.databox as unknown as CampaignDatabox[]

    return mode === 'edit' ? (
        <EditBox>
            <h3>Embed a Databox</h3>
            <Form
                submitText="Submit"
                successMessage={null}
                buttonProps={{
                    style: {
                        marginBottom: 20,
                    },
                }}
                onSubmit={async (values: { databox: CampaignDatabox[] }) => {
                    //console.log(databox, values?.databox)
                    const nonEmpty = values?.databox.filter((box) =>
                        hasValue(box)
                    )
                    const currentValues =
                        databox && Array.isArray(databox) ? databox : []

                    try {
                        const updated = await updateCampaignMutation({
                            id: campaign.id,
                            databox: [...currentValues, ...nonEmpty],
                        })
                        if (updated) {
                            invalidateQuery(getCampaign)
                            setBoxCount(1)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            >
                {Array.from(Array(boxCount).keys()).map((index) => (
                    <div key={index}>
                        <Input
                            label="Give the databox a name or label."
                            name={`databox[${index}][label]`}
                            required
                        />
                        <Input
                            label="Paste the databox embed code here."
                            element="textarea"
                            name={`databox[${index}][box]`}
                            required
                            rows={4}
                        />
                    </div>
                ))}
                <AddDeleteButtons
                    add={() => setBoxCount(boxCount + 1)}
                    remove={() => {
                        const num = boxCount <= 1 ? 1 : boxCount - 1
                        setBoxCount(num)
                    }}
                />
            </Form>
        </EditBox>
    ) : null
}
