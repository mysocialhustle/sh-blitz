import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import { Styled } from './Databox.styles'
import Icon from 'app/core/components/Icon/Icon'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import CloseButton from 'app/admin/components/CloseButton/CloseButton'
import { CampaignDatabox } from '../../types'

export default function Box({ box, label, campaign, index }) {
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const { mode } = useContext(Context)
    const deleteBox = async () => {
        let copy: CampaignDatabox[] = [...campaign.databox]
        copy.splice(index, 1)
        //console.log(copy, campaign.databox)
        if (window.confirm('Are you sure you want to delete this databox?')) {
            const updated = await updateCampaignMutation({
                id: campaign.id,
                databox: copy,
            })
            if (updated) {
                invalidateQuery(getCampaign)
            }
        }
    }

    return (
        <Styled.Column sm={50} md={33}>
            <Styled.Heading as="h4">{label}</Styled.Heading>
            <Styled.BoxWrapper>
                <CloseButton
                    onClick={() => deleteBox()}
                    style={{ left: -10 }}
                />
                <Styled.Box
                    dangerouslySetInnerHTML={{
                        __html: box,
                    }}
                />
            </Styled.BoxWrapper>
        </Styled.Column>
    )
}
