import styled, { keyframes } from 'styled-components'
import colors from 'app/core/constants/colors'
import { Column as GridColumn } from 'app/styles/grid'
import Headings from 'app/styles/headings'

const fadeIn = keyframes`
    0% {
        opacity: 0;

    }
    100% {
        opacity: 1;
    }
`

export namespace Styled {
    export const Close = styled.span`
        display: inline-block;
        position: absolute;
        z-index: 1;
        background: white;
        line-height: 0;
        padding: 5px;
        border-radius: 50%;
        top: -5px;
        left: 0px;
        filter: drop-shadow(2px 4px 6px rgba(0, 0, 0, 0.25));
        transition: background-color 0.25s ease;
        cursor: pointer;
        font-size: 0.75em;
    `

    export const Column = styled(GridColumn)`
        margin-bottom: 20px;
        animation: ${fadeIn} 0.5s ease;

        &:hover {
            ${Close} {
                background-color: ${colors.red};
            }
        }
    `

    export const Box = styled.div`
        margin-bottom: 20px;
    `

    export const BoxWrapper = styled.div`
        position: relative;
    `

    export const Heading = styled(Headings.H5)`
        font-weight: 600;
        text-align: center;
        letter-spacing: 0.5px;
        text-transform: uppercase;
        margin-bottom: 10px;
        margin-top: 20px;
    `
}
