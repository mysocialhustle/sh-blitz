import { Form } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { z } from 'zod'
export { FORM_ERROR } from 'app/core/components/Form'
import { CampaignType } from 'db'
import Select from 'app/core/components/Form/Select/Select'
import camelCaseToString from 'app/core/utils/camelCaseToString'
import { useQuery, Routes, Link } from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import colors from 'app/core/constants/colors'

export function CampaignForm(props) {
    const [companies] = useQuery(getCompanies, {})

    return (
        <Form {...props}>
            <p>
                <em>
                    Select which company this campaign is for. If the company
                    does not exist, you need to{' '}
                    <Link href={Routes.NewCompanyPage()}>
                        <a
                            style={{
                                color: colors.red,
                                textDecoration: 'underline',
                            }}
                        >
                            create it
                        </a>
                    </Link>{' '}
                    and then come back here.
                </em>
            </p>
            <Select
                name="companyId"
                options={companies.companies.map((el) => ({
                    value: el.id,
                    label: el.name,
                }))}
                label="Select a company"
            />
            <p>
                <em>Choose which kind of campaign this will be.</em>
            </p>
            <Select
                label="Type of campaign"
                name="type"
                options={Object.keys(CampaignType).map((type) => {
                    return {
                        label: camelCaseToString(type),
                        value: type,
                    }
                })}
            />
            <p>
                <em>
                    {`Give this campaign a name (unless you have a good reason not to, please use the naming convention [company name] [campaign type]. E.g. "Social Hustle TikTok Ads").`}
                </em>
            </p>

            <Input name="name" label="Campaign Name" required />
        </Form>
    )
}
