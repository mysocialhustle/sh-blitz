import { invalidateQuery, useMutation, Routes } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import deleteCampaign from 'app/admin/campaigns/mutations/deleteCampaign'
import Account from 'app/core/layouts/Account/Account'
import BackButton from 'app/admin/components/BackButton/BackButton'
import { Container } from 'app/styles/grid'
import EditButton from 'app/admin/components/EditButton/EditButton'
import CampaignNavigation from 'app/admin/campaigns/components/Navigation'
import Icon from 'app/core/components/Icon/Icon'
import { campaigns } from 'app/data/campaigns'
import Section from 'app/core/components/Section/Section'
import { Row, Column } from 'app/styles/grid'
import Notes from 'app/admin/campaigns/components/Notes/Notes'
import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import hasValue from 'app/core/utils/hasValue'

export default function Budget({ campaign }) {
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const { budget } = campaign
    const { mode, setMode } = useContext(Context)
    const show = hasValue(budget) || mode === 'edit'

    return (
        <>
            {show && (
                <Row alignY={mode === 'edit' ? 'flex-start' : 'center'}>
                    <Column sm={40}>
                        <h3>Budget</h3>
                    </Column>

                    <Column sm={60}>
                        {mode === 'edit' ? (
                            <EditBox>
                                <h3>Add/Edit Budget</h3>
                                <Form
                                    submitText="Submit"
                                    successMessage={null}
                                    onSubmit={async (values: {
                                        budget: string
                                    }) => {
                                        try {
                                            const updated =
                                                await updateCampaignMutation({
                                                    id: campaign.id,
                                                    budget: values.budget,
                                                })
                                            if (updated) {
                                                invalidateQuery(getCampaign)
                                                setMode('view')
                                            }
                                        } catch (error: any) {
                                            console.error(error)
                                            return {
                                                [FORM_ERROR]: error.toString(),
                                            }
                                        }
                                    }}
                                >
                                    <Input
                                        name="budget"
                                        label="Campaign Budget"
                                        defaultValue={budget}
                                        required
                                    />
                                </Form>
                            </EditBox>
                        ) : (
                            <p>{budget}</p>
                        )}
                    </Column>
                </Row>
            )}
        </>
    )
}
