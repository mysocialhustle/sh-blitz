import ModelNavigation from 'app/admin/components/ModelNavigation/ModelNavigation'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'

export default function CampaignNavigation() {
    const campaignId = useParam('campaignId', 'number')
    const [campaign] = useQuery(getCampaign, { id: campaignId })

    return (
        <ModelNavigation
            items={[
                {
                    name: 'Campaign Overview',
                    link: Routes.CampaignOverviewPage({
                        campaignId: campaign.id,
                    }),
                },
                {
                    name: 'Real-Time Analytics',
                    link: Routes.CampaignAnalyticsPage({
                        campaignId: campaign.id,
                    }),
                },
                {
                    name: 'Reports',
                    link: Routes.CampaignReportsPage({
                        campaignId: campaign.id,
                    }),
                },
            ]}
        />
    )
}
