import Edit from './Edit'
import { Campaign } from 'db'
import Report from './Report'
import { Styled } from './ReportList.styles'
import { CampaignReport } from '../../types'
import Section from 'app/core/components/Section/Section'

interface Props {
    campaign: Campaign
}

export default function ReportList({ campaign }: Props) {
    const reports = campaign?.reports as unknown as CampaignReport[]

    return (
        <>
            {reports && Array.isArray(reports) && reports?.length > 0 && (
                <>
                    <h3>Reports</h3>
                    <p>View all your marketing reports here.</p>
                    <Styled.List>
                        {reports.map((report, index) => (
                            <Report
                                key={index}
                                index={index}
                                campaign={campaign}
                                {...report}
                            />
                        ))}
                    </Styled.List>
                </>
            )}

            <Edit campaign={campaign} />
        </>
    )
}
