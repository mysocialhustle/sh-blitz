import styled, { keyframes } from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import colors from 'app/core/constants/colors'

export namespace Styled {
    const scaleIn = keyframes`
    0% {
        transform: scaleY(0);
        opacity: 0;

    }
    100% {
        transform: scaleY(1);
        opacity: 1;
    }
`
    export const Buttons = styled.div`
        .arrow {
            stroke: currentColor;
            width: auto;
            height: 0.75em;
            vertical-align: middle;
            margin-left: 20px;
        }

        ${mediaQueries.md} {
            transform: translateX(100%);
            opacity: 0;
            transition: all 0.35s ease-in;
            display: inline-block;
        }
    `

    export const Edit = styled.div`
        cursor: pointer;
        color: ${colors.darkRed};
        position: absolute;
        z-index: 10;
        margin-right: 1em;
        width: 1rem;
        left: 0;
        top: 0;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
    `

    export const Item = styled.div`
        animation: ${scaleIn} 0.5s ease;
        transform-origin: center top;
        border-bottom: 1px solid ${colors.lightGrey};
        position: relative;
    `

    export const Link = styled.a`
        padding: 10px 0;
        transition: all 0.35s ease-in;
        display: flex;
        flex-flow: row wrap;
        align-items: center;
        line-height: 2;

        svg {
            margin-right: 1em;
        }

        ${Edit} svg {
            margin-right: 0;
        }

        &:hover {
            opacity: 0.8;

            ${Buttons} {
                transform: translateX(0%);
                opacity: 1;
            }
        }
    `

    export const List = styled.div`
        margin-bottom: 20px;
    `
}
