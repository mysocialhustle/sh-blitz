import { Styled } from './ReportList.styles'
import Icon from 'app/core/components/Icon/Icon'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import { CampaignReport } from '../../types'

export default function Report({ link, name, index, campaign }) {
    const { mode } = useContext(Context)
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const deleteReport = async () => {
        let copy: CampaignReport[] = [...campaign.reports]
        copy.splice(index, 1)
        if (window.confirm('Are you sure you want to delete this databox?')) {
            const updated = await updateCampaignMutation({
                id: campaign.id,
                reports: copy,
            })
            if (updated) {
                invalidateQuery(getCampaign)
            }
        }
    }

    return (
        <Styled.Item>
            {mode === 'edit' && (
                <Styled.Edit
                    title="Delete Report"
                    onClick={() => deleteReport()}
                >
                    <Icon name="exCircle" />
                </Styled.Edit>
            )}

            <Styled.Link
                href={link}
                target="_blank"
                rel="noreferrer"
                style={{ paddingLeft: mode === 'edit' ? `2rem` : 0 }}
            >
                <Icon name="download" />
                <span>{name}</span>
            </Styled.Link>
        </Styled.Item>
    )
}
