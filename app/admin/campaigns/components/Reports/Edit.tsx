import { Context } from 'app/admin/contexts'
import { useContext, useState } from 'react'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import { Campaign } from 'db'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import { CampaignReport } from '../../types'
import { Row, Column } from 'app/styles/grid'
import FileInput from 'app/core/components/Form/File/File'
import hasValue from 'app/core/utils/hasValue'
import AddDeleteButtons from 'app/admin/components/AddDeleteButtons/AddDeleteButtons'

interface Props {
    campaign: Campaign
}

export default function Edit({ campaign }: Props) {
    const { mode } = useContext(Context)
    const [boxCount, setBoxCount] = useState(1)
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const reports = campaign.reports as unknown as CampaignReport[]

    return mode === 'edit' ? (
        <EditBox>
            <h3>Upload a Report</h3>
            <p>
                <em>
                    <strong>NOTE:</strong>{' '}
                    {`You must upload at least one file before clicking "Update Reports".`}
                </em>
            </p>
            <Form
                submitText="Submit"
                successMessage={null}
                buttonProps={{
                    style: {
                        marginBottom: 20,
                    },
                }}
                onSubmit={async (values: { reports: CampaignReport[] }) => {
                    /**console.log(
                        values?.reports.filter(
                            (report) =>
                                hasValue(report.name) && hasValue(report.link)
                        )
                    )**/
                    const nonEmpty = values?.reports.filter(
                        (report) =>
                            hasValue(report.name) && hasValue(report.link)
                    )
                    const currentValues =
                        reports && Array.isArray(reports) ? reports : []
                    try {
                        const updated = await updateCampaignMutation({
                            id: campaign.id,
                            reports: [...currentValues, ...nonEmpty],
                        })
                        if (updated) {
                            invalidateQuery(getCampaign)
                            setBoxCount(1)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            >
                {Array.from(Array(boxCount).keys()).map((index) => (
                    <Row key={index}>
                        <Column sm={75}>
                            <Input
                                label={`Name Your File (e.g. "February Report")`}
                                name={`reports[${index}][name]`}
                                required
                            />
                        </Column>
                        <Column sm={25}>
                            <FileInput
                                name={`reports[${index}][link]`}
                                uploadPath={`campaigns/${campaign.id}/reports`}
                            >
                                Upload Report
                            </FileInput>
                        </Column>
                    </Row>
                ))}
                <AddDeleteButtons
                    add={() => setBoxCount(boxCount + 1)}
                    remove={() => {
                        const num = boxCount <= 1 ? 1 : boxCount - 1
                        setBoxCount(num)
                    }}
                />
            </Form>
        </EditBox>
    ) : null
}
