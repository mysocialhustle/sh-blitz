import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import Form from 'app/core/components/Form/Form/Form'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'

export default function LoginsWrapper({ children, campaign, ...rest }) {
    const { mode, setMode } = useContext(Context)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    const saveLogins = async (vals) => {
        const updated = await updateCampaignMutation({
            id: campaign.id,
            logins: vals.logins,
        })
        if (updated) {
            invalidateQuery(getCampaign)
            setMode('view')
        }
    }

    return mode === 'edit' ? (
        <Form
            onSubmit={saveLogins}
            submitText="Save Logins"
            successMessage={null}
            buttonProps={{
                style: {
                    marginBottom: 50,
                },
            }}
            {...rest}
        >
            {children}
        </Form>
    ) : (
        <>{children}</>
    )
}
