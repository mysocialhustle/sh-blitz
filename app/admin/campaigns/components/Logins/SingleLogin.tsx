//import Edit from './Edit'
import { CampaignLogin } from '../../types'
import Section from 'app/core/components/Section/Section'
import * as Styled from 'app/styles/tables'
import Edit from './Edit'
import hasValue from 'app/core/utils/hasValue'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import Icon from 'app/core/components/Icon/Icon'
import Input from 'app/core/components/Form/Input/Input'
import Password from 'app/core/components/Form/Password/Password'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'

export default function SingleLogin({
    campaign,
    showWebsite,
    website = '',
    username,
    password,
    index,
}) {
    const { mode } = useContext(Context)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    const deleteLogin = async () => {
        let copy: CampaignLogin[] = [...campaign.logins]
        copy.splice(index, 1)
        //console.log(copy, campaign.logins)
        if (window.confirm('Are you sure you want to delete this login?')) {
            const updated = await updateCampaignMutation({
                id: campaign.id,
                logins: copy,
            })
            if (updated) {
                invalidateQuery(getCampaign)
                //setMode('view')
            }
        }
    }

    return (
        <tr>
            {mode === 'edit' && (
                <Styled.TableCell style={{ textAlign: `center` }}>
                    <span
                        style={{ cursor: `pointer` }}
                        onClick={() => deleteLogin()}
                    >
                        <Icon name="exCircle" />
                    </span>
                </Styled.TableCell>
            )}
            {showWebsite && (
                <Styled.TableCell>
                    {mode === 'edit' ? (
                        <>
                            {' '}
                            <Input
                                label={`(Optional) Website`}
                                name={`logins[${index}][website]`}
                                defaultValue={website}
                            />
                        </>
                    ) : (
                        <>{website || ''}</>
                    )}
                </Styled.TableCell>
            )}

            <Styled.TableCell>
                {mode === 'edit' ? (
                    <Input
                        label={`Username`}
                        name={`logins[${index}][username]`}
                        required
                        defaultValue={username}
                    />
                ) : (
                    <>{username}</>
                )}
            </Styled.TableCell>
            <Styled.TableCell>
                {mode === 'edit' ? (
                    <Password
                        label={`Password`}
                        name={`logins[${index}][password]`}
                        defaultValue={password}
                        required
                    />
                ) : (
                    <>{password}</>
                )}
            </Styled.TableCell>
        </tr>
    )
}
