import { CampaignLogin } from '../../types'
import Section from 'app/core/components/Section/Section'
import * as Styled from 'app/styles/tables'
import Edit from './Edit'
import hasValue from 'app/core/utils/hasValue'
import SingleLogin from './SingleLogin'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import LoginsWrapper from './LoginsWrapper'

export default function Logins({ campaign }) {
    const { mode } = useContext(Context)
    const logins = campaign?.logins as unknown as CampaignLogin[]
    const hasWebsiteColumn = logins
        ? logins.filter((el) => hasValue(el.website)).length > 0
        : false
    const showWebsite = hasWebsiteColumn || mode === 'edit'

    return (
        <>
            {logins && Array.isArray(logins) && logins?.length > 0 && (
                <LoginsWrapper campaign={campaign}>
                    <h3>Logins</h3>
                    <Styled.Table>
                        <thead>
                            <tr>
                                {mode === 'edit' && <Styled.TableHeading />}
                                {showWebsite && (
                                    <Styled.TableHeading>
                                        Website
                                    </Styled.TableHeading>
                                )}
                                <Styled.TableHeading>
                                    Username
                                </Styled.TableHeading>
                                <Styled.TableHeading>
                                    Password
                                </Styled.TableHeading>
                            </tr>
                        </thead>
                        <tbody>
                            {logins.map((login, index) => {
                                return (
                                    <SingleLogin
                                        key={index}
                                        campaign={campaign}
                                        showWebsite={showWebsite}
                                        index={index}
                                        {...login}
                                    />
                                )
                            })}
                        </tbody>
                    </Styled.Table>
                </LoginsWrapper>
            )}

            <Edit campaign={campaign} />
        </>
    )
}
