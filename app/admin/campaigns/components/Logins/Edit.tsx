import { Context } from 'app/admin/contexts'
import { useContext, useState } from 'react'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import { Campaign } from 'db'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import { CampaignLogin } from '../../types'
import { Row, Column } from 'app/styles/grid'
import hasValue from 'app/core/utils/hasValue'
import AddDeleteButtons from 'app/admin/components/AddDeleteButtons/AddDeleteButtons'
import Password from 'app/core/components/Form/Password/Password'

interface Props {
    campaign: Campaign
}

export default function Edit({ campaign }: Props) {
    const { mode } = useContext(Context)
    const [boxCount, setBoxCount] = useState(1)
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const logins = campaign.logins as unknown as CampaignLogin[]

    return mode === 'edit' ? (
        <EditBox>
            <h3>Add a Login</h3>
            <Form
                submitText="Submit"
                successMessage={null}
                buttonProps={{
                    style: {
                        marginBottom: 20,
                    },
                }}
                onSubmit={async (values: { logins: CampaignLogin[] }) => {
                    const nonEmpty = values?.logins.filter(
                        (login) =>
                            hasValue(login.username) && hasValue(login.password)
                    )
                    const currentValues =
                        logins && Array.isArray(logins) ? logins : []
                    //console.log(currentValues, nonEmpty)
                    try {
                        const updated = await updateCampaignMutation({
                            id: campaign.id,
                            logins: [...currentValues, ...nonEmpty],
                        })
                        if (updated) {
                            invalidateQuery(getCampaign)
                            setBoxCount(1)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            >
                {Array.from(Array(boxCount).keys()).map((index) => (
                    <Row key={index}>
                        <Column sm={33}>
                            <Input
                                label={`(Optional) Website`}
                                name={`logins[${index}][website]`}
                            />
                        </Column>
                        <Column sm={33}>
                            <Input
                                label={`Username`}
                                name={`logins[${index}][username]`}
                                required
                            />
                        </Column>
                        <Column sm={33}>
                            <Password
                                label={`Password`}
                                name={`logins[${index}][password]`}
                                required
                            />
                        </Column>
                    </Row>
                ))}
                <AddDeleteButtons
                    add={() => setBoxCount(boxCount + 1)}
                    remove={() => {
                        const num = boxCount <= 1 ? 1 : boxCount - 1
                        setBoxCount(num)
                    }}
                />
            </Form>
        </EditBox>
    ) : null
}
