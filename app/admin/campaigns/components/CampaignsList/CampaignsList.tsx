import { Row, Column } from 'app/styles/grid'
import { Link, usePaginatedQuery, useRouter, Routes } from 'blitz'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import Box from 'app/core/components/Box/Box'
import Icon from 'app/core/components/Icon/Icon'
import { campaigns as campaignObjects } from 'app/data/campaigns'

export default function CampaignsList({ pagination }) {
    const router = useRouter()
    const page = Number(router.query.page) || 0
    const [{ campaigns, hasMore }] = usePaginatedQuery(getCampaigns, {
        orderBy: { id: 'asc' },
        skip: pagination * page,
        take: pagination,
    })

    const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
    const goToNextPage = () => router.push({ query: { page: page + 1 } })

    return (
        <>
            {campaigns.length > 0 && (
                <Row>
                    {campaigns.map((campaign, index) => {
                        const reference = campaignObjects[campaign.type]
                        return (
                            <Column sm={33} key={index}>
                                <Box
                                    href={Routes.CampaignOverviewPage({
                                        campaignId: campaign.id,
                                    })}
                                >
                                    <p>
                                        <Icon name={reference?.icon?.filled} />{' '}
                                        {reference?.name}
                                    </p>
                                    <h3>{campaign.name}</h3>
                                </Box>
                            </Column>
                        )
                    })}
                </Row>
            )}

            {campaigns.length > pagination && (
                <>
                    <button disabled={page === 0} onClick={goToPreviousPage}>
                        Previous
                    </button>
                    <button disabled={!hasMore} onClick={goToNextPage}>
                        Next
                    </button>
                </>
            )}
        </>
    )
}
