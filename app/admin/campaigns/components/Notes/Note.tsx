import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import Form from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { EditBox } from 'app/admin/styles'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import CloseButton from 'app/admin/components/CloseButton/CloseButton'
import { CampaignNote } from '../../types'

export default function Note({ label, value, index, campaign }) {
    const { mode } = useContext(Context)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    const deleteBox = async () => {
        let copy: CampaignNote[] = [...campaign.notes]
        copy.splice(index, 1)
        //console.log(copy, campaign.notes)
        if (window.confirm('Are you sure you want to delete this note?')) {
            const updated = await updateCampaignMutation({
                id: campaign.id,
                notes: copy,
            })
            if (updated) {
                invalidateQuery(getCampaign)
                //setMode('view')
            }
        }
    }

    return mode === 'edit' ? (
        <EditBox>
            <CloseButton
                style={{ top: -10, left: -10 }}
                onClick={() => deleteBox()}
            />
            <Input
                label="Note Label"
                name={`notes[${index}][label]`}
                defaultValue={label}
                required={true}
            />
            <Input
                element="textarea"
                rows={6}
                label="Note"
                name={`notes[${index}][value]`}
                defaultValue={value}
                required={true}
            />
        </EditBox>
    ) : (
        <>
            <p>
                <strong>{label}</strong>
            </p>
            <p>{value}</p>
        </>
    )
}
