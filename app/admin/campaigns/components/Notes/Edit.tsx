import { Context } from 'app/admin/contexts'
import { useContext, useState } from 'react'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import { Campaign } from 'db'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import { CampaignNote } from '../../types'
import hasValue from 'app/core/utils/hasValue'
import AddDeleteButtons from 'app/admin/components/AddDeleteButtons/AddDeleteButtons'
import { Row, Column } from 'app/styles/grid'

interface Props {
    campaign: Campaign
}

export default function Edit({ campaign }: Props) {
    const { mode } = useContext(Context)
    const [boxCount, setBoxCount] = useState(1)
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const notes = campaign.notes as unknown as CampaignNote[]

    return mode === 'edit' ? (
        <EditBox>
            <h3>Add a Note</h3>
            <Form
                submitText="Submit"
                successMessage={null}
                onSubmit={async (values: { notes: CampaignNote[] }) => {
                    const nonEmpty = values?.notes.filter(
                        (report) =>
                            hasValue(report.label) && hasValue(report.value)
                    )
                    const currentValues =
                        notes && Array.isArray(notes) ? notes : []
                    try {
                        const updated = await updateCampaignMutation({
                            id: campaign.id,
                            notes: [...currentValues, ...nonEmpty],
                        })
                        if (updated) {
                            invalidateQuery(getCampaign)
                            setBoxCount(1)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            >
                {Array.from(Array(boxCount).keys()).map((index) => (
                    <Row key={index}>
                        <Column sm={35}>
                            <Input
                                label="Note Label"
                                name={`notes[${index}][label]`}
                                required={true}
                            />
                        </Column>
                        <Column sm={65}>
                            <Input
                                element="textarea"
                                rows={4}
                                label="Note"
                                name={`notes[${index}][value]`}
                                required={true}
                            />
                        </Column>
                    </Row>
                ))}
                <AddDeleteButtons
                    add={() => setBoxCount(boxCount + 1)}
                    remove={() => {
                        const num = boxCount <= 1 ? 1 : boxCount - 1
                        setBoxCount(num)
                    }}
                />
            </Form>
        </EditBox>
    ) : null
}
