import Section from 'app/core/components/Section/Section'
import { Row, Column } from 'app/styles/grid'
import Note from './Note'
import NotesWrapper from './NotesWrapper'
import Edit from './Edit'

export default function Notes({ campaign }) {
    const { notes } = campaign

    return (
        <>
            {notes && Array.isArray(notes) && notes?.length > 0 && (
                <>
                    <h3>Campaign Notes</h3>
                    <NotesWrapper campaign={campaign}>
                        <Row>
                            {notes.map((note, index) => {
                                return (
                                    <Column key={index} sm={50}>
                                        <Note
                                            index={index}
                                            campaign={campaign}
                                            {...note}
                                        />
                                    </Column>
                                )
                            })}
                        </Row>
                    </NotesWrapper>
                </>
            )}
            <Edit campaign={campaign} />
        </>
    )
}
