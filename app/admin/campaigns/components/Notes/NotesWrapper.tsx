import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import Form from 'app/core/components/Form/Form/Form'
import { CampaignNote } from '../../types'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { useMutation, invalidateQuery } from 'blitz'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'

export default function NotesWrapper({ children, campaign, ...rest }) {
    const { mode, setMode } = useContext(Context)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    const saveNotes = async (vals) => {
        const updated = await updateCampaignMutation({
            id: campaign.id,
            notes: vals.notes,
        })
        if (updated) {
            invalidateQuery(getCampaign)
            setMode('view')
        }
    }

    return mode === 'edit' ? (
        <Form
            onSubmit={saveNotes}
            submitText="Save Notes"
            successMessage={null}
            buttonProps={{
                style: {
                    marginBottom: 50,
                },
            }}
            {...rest}
        >
            {children}
        </Form>
    ) : (
        <>{children}</>
    )
}
