import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { z } from 'zod'
import { CampaignType } from 'db'
import Select from 'app/core/components/Form/Select/Select'
import camelCaseToString from 'app/core/utils/camelCaseToString'
import {
    Head,
    Link,
    useRouter,
    useQuery,
    useMutation,
    useParam,
    BlitzPage,
    Routes,
} from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import colors from 'app/core/constants/colors'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { Row, Column } from 'app/styles/grid'

export default function UpdateCampaign({ campaign, setQueryData, ...props }) {
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const router = useRouter()

    return (
        <Form
            submitText="Update Campaign"
            // TODO use a zod schema for form validation
            //  - Tip: extract mutation's schema into a shared `validations.ts` file and
            //         then import and use it here
            // schema={UpdateCampaign}
            onSubmit={async (values) => {
                try {
                    const updated = await updateCampaignMutation({
                        id: campaign.id,
                        ...values,
                    })
                    await setQueryData(updated)
                    router.push(
                        Routes.CampaignOverviewPage({
                            campaignId: updated.id,
                        })
                    )
                } catch (error: any) {
                    console.error(error)
                    return {
                        [FORM_ERROR]: error.toString(),
                    }
                }
            }}
            {...props}
        >
            <Input
                name="name"
                label="Campaign Name"
                required
                defaultValue={campaign?.name}
            />
            <Input
                name="budget"
                label="Budget"
                defaultValue={campaign?.budget}
            />
            <Input
                name="goals"
                label="Campaign Goals"
                defaultValue={campaign?.goals}
                element="textarea"
                rows={6}
            />
            {campaign?.notes?.length > 0 && (
                <>
                    <h4>Notes</h4>
                    {campaign?.notes?.map((note, index) => (
                        <div key={index}>
                            {Object.entries(note).map(
                                ([key, val]: [string, string], index) => (
                                    <Input
                                        key={index}
                                        name={`notes[${key}]`}
                                        label={key}
                                        defaultValue={val}
                                        element="textarea"
                                    />
                                )
                            )}
                        </div>
                    ))}
                </>
            )}
        </Form>
    )
}
