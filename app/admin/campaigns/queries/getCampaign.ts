import { resolver, NotFoundError } from 'blitz'
import db from 'db'
import { z } from 'zod'

const GetCampaign = z.object({
    // This accepts type of undefined, but is required at runtime
    id: z.number().optional().refine(Boolean, 'Required'),
})

export default resolver.pipe(
    resolver.zod(GetCampaign),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const campaign = await db.campaign.findFirst({ where: { id } })

        if (!campaign) throw new NotFoundError()

        return campaign
    }
)
