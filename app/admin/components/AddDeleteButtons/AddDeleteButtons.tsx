import Icon from 'app/core/components/Icon/Icon'
import { Styled } from './AddDeleteButtons.styles'

export default function AddDeleteButtons({ add, remove }) {
    return (
        <Styled.Buttons>
            <Styled.Button
                title="Remove"
                onClick={() => remove()}
                style={{ borderRight: `1px solid rgba(0, 0, 0, 0.25)` }}
            >
                <Icon name="minus" />
            </Styled.Button>
            <Styled.Button title="Add Another" onClick={() => add()}>
                <Icon name="plus" />
            </Styled.Button>
        </Styled.Buttons>
    )
}
