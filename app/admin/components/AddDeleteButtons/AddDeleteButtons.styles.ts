import styled, { keyframes } from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Buttons = styled.div`
        border: 1px solid rgba(0, 0, 0, 0.25);
        display: inline-block;
        margin-bottom: 20px;
    `

    export const Button = styled.div`
        background-color: ${colors.lightGrey};
        transition: all 0.3s ease;
        display: inline-block;
        cursor: pointer;
        transform-origin: center top;
        padding: 10px;
        text-align: center;
        width: 3em;
        font-size: 14px;

        &:hover {
            opacity: 0.6;
        }
    `
}
