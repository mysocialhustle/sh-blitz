import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import { hexToRGBA } from 'app/core/utils/colors'

export namespace Styled {
    export const SessionManager = styled.div`
        width: 100%;
        background-color: ${colors.offWhite};
        font-size: 12px;
        color: ${hexToRGBA(colors.darkGrey, 0.75)};
        text-transform: uppercase;
        padding-top: 5px;
        padding-bottom: 5px;
    `

    export const Select = styled.div`
        max-width: 350px;
        margin-left: auto;

        .react-select-container {
            margin-bottom: 0px !important;
        }

        .react-select__placeholder {
            color: ${hexToRGBA(colors.darkGrey, 0.75)} !important;
            max-width: 100%;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        .react-select__control {
            background-color: transparent;
            border-color: transparent;

            &:hover,
            &.react-select__control--is-focused {
                border-color: transparent !important;
            }
        }

        .react-select__value-container {
            padding: 5px;
        }
    `

    export const Logout = styled.span`
        color: ${colors.darkGrey};
        font-weight: bold;
        opacity: 0.85;
        cursor: pointer;
    `
}
