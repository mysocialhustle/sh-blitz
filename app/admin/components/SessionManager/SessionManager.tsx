import { Styled } from './SessionManager.styles'
import { Container, Column, Row } from 'app/styles/grid'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import CompanySelect from './CompanySelect'
import { Link, useRouter, useMutation } from 'blitz'
import logout from 'app/auth/mutations/logout'
import { Divider } from 'app/styles/utilities'

export default function SessionManager() {
    const user = useCurrentUser()
    const [logoutMutation] = useMutation(logout)

    return (
        <Styled.SessionManager>
            <Container fluid>
                <Row alignY="center">
                    <Column sm={50}>
                        {user?.email && (
                            <>
                                {user.email}
                                <Divider />
                                <Styled.Logout
                                    onClick={async () => {
                                        await logoutMutation()
                                    }}
                                >
                                    Logout
                                </Styled.Logout>
                            </>
                        )}
                    </Column>
                    <Column sm={50}>
                        <Styled.Select>
                            <CompanySelect />
                        </Styled.Select>
                    </Column>
                </Row>
            </Container>
        </Styled.SessionManager>
    )
}
