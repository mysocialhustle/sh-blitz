import { useState } from 'react'
import { SelectBase } from 'app/core/components/Form/Select/Select'
import { useQuery, useSession, setPublicDataForUser, useMutation } from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import useCurrentUserCompanies from 'app/core/hooks/useCurrentUserCompanies'
import updateSessionContext from 'app/admin/users/mutations/updateSessionContext'

export default function CompanySelect() {
    const session = useSession()
    const user = useCurrentUser()
    const [updateSessionCompany] = useMutation(updateSessionContext)

    const selectProps = session.company
        ? {
              label: `Currently Managing ${session?.company?.name}`,
          }
        : {
              label: 'Select a Company to Manage',
          }

    const companies = useCurrentUserCompanies()

    return (
        <SelectBase
            name="company"
            options={companies.map((el) => ({
                value: {
                    id: el.id,
                    name: el.name,
                },
                label: el.name,
            }))}
            onChange={async ({ value }) => {
                //console.log(input)
                try {
                    await updateSessionCompany({
                        company: value,
                    })
                } catch (error) {
                    alert('Error setting company')
                }
            }}
            {...selectProps}
        />
    )
}
