import { Link } from 'blitz'
import ArrowLeft from 'app/svg/arrow-left.inline.svg'
import { Styled } from './BackButton.styles'

export default function BackButton({ href, label = 'BACK' }) {
    return (
        <Styled.Button>
            <Link href={href} passHref>
                <a>
                    <ArrowLeft />
                    <Styled.Inner>{label}</Styled.Inner>
                </a>
            </Link>
        </Styled.Button>
    )
}
