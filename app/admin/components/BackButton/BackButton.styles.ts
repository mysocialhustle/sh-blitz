import styled from 'styled-components'

export namespace Styled {
    export const Button = styled.div`
        font-size: 1.25rem;
        margin-bottom: 20px;
        display: inline-block;

        svg {
            height: 1rem;
            width: auto;
            stroke: currentColor;
        }

        a {
            display: flex;
            align-items: center;
        }
    `
    export const Inner = styled.span`
        display: inline-block;
        margin-left: 10px;
    `
}
