import Icon from 'app/core/components/Icon/Icon'
import { Styled } from './DeleteButton.styles'

export default function DeleteButton({ children, onClick, ...rest }) {
    return (
        <Styled.Button onClick={() => onClick()} {...rest}>
            <Icon name="delete" /> {children}
        </Styled.Button>
    )
}
