import styled from 'styled-components'

export namespace Styled {
    export const Button = styled.span`
        font-size: 0.9rem;
        opacity: 0.8;
        cursor: pointer;
        user-select: none;
    `
}
