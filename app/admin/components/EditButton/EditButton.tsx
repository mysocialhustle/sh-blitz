import Icon from 'app/core/components/Icon/Icon'
import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import { Styled } from './EditButton.styles'

export default function EditButton({ children }) {
    const { mode, setMode } = useContext(Context)
    return mode === 'view' ? (
        <Styled.Button onClick={() => setMode('edit')}>
            <Icon name="edit" /> {children}
        </Styled.Button>
    ) : (
        <Styled.Button onClick={() => setMode('view')}>
            <Icon name="close" /> Exit editing mode
        </Styled.Button>
    )
}
