import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Nav = styled.ul`
        margin-bottom: 20px;
        font-size: 14px;
        letter-spacing: 1px;
        font-weight: 300;
        text-transform: uppercase;
    `

    export const Item = styled.li`
        display: inline-block;

        a {
            display: inline-block;
            padding-bottom: 2px;
            position: relative;

            &:hover {
                &:after {
                    transform: scaleX(1);
                }
            }

            &: after {
                content: '';
                width: 100%;
                height: 1px;
                bottom: 0px;
                left: 0;
                position: absolute;
                background-color: currentColor;
                transform-origin: left center;
                transform: scaleX(0);
                transition: transform 0.3s linear;
            }
        }
    `

    export const Divider = styled.span`
        margin: 0px 10px;
        display: inline-block;
    `

    export const Active = styled.span`
        color: ${colors.red};
        opacity: 0.75;
        font-weight: 800;
        cursor: default;
    `
}
