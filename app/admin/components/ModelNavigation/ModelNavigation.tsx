import { Styled } from './ModelNavigation.styles'
import { useRouter, Link } from 'blitz'
import Icon from 'app/core/components/Icon/Icon'

export default function ModelNavigation({ items }) {
    const router = useRouter()

    return (
        <Styled.Nav>
            {items.map((item, index) => {
                const { link, name, icon } = item
                const isActive = router.pathname === link.pathname
                const ItemIcon = () => (icon ? <Icon name={icon} /> : null)

                return (
                    <Styled.Item key={index}>
                        {isActive ? (
                            <Styled.Active>
                                <span>
                                    <ItemIcon /> {name}
                                </span>
                            </Styled.Active>
                        ) : (
                            <Link href={link} passHref>
                                <a>
                                    <ItemIcon /> {name}
                                </a>
                            </Link>
                        )}
                        {index < items.length - 1 && (
                            <Styled.Divider>|</Styled.Divider>
                        )}
                    </Styled.Item>
                )
            })}
        </Styled.Nav>
    )
}
