import { Styled } from './CloseButton.styles'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import Icon from 'app/core/components/Icon/Icon'

export default function CloseButton({ onClick, ...rest }) {
    const { mode } = useContext(Context)
    return mode === 'edit' ? (
        <Styled.Close onClick={() => onClick()} {...rest}>
            <Icon name="close" />
        </Styled.Close>
    ) : null
}
