import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Close = styled.span`
        display: inline-block;
        position: absolute;
        z-index: 1;
        background: white;
        line-height: 0;
        padding: 5px;
        border-radius: 50%;
        top: -5px;
        left: 0px;
        filter: drop-shadow(2px 4px 6px rgba(0, 0, 0, 0.25));
        transition: background-color 0.25s ease;
        cursor: pointer;
        font-size: 0.75em;

        &:hover {
            background-color: ${colors.red};
        }
    `
}
