import { useQuery, Routes } from 'blitz'
import getCompany from 'app/admin/companies/queries/getCompany'
import Button from 'app/core/components/Button/Button'

export default function RelatedCompany({ model }) {
    const [company] = useQuery(getCompany, { id: model.companyId })
    return (
        <p>
            <Button
                style={{ textAlign: `center`, maxWidth: 350 }}
                href={Routes.CompanyOverviewPage({ companyId: company.id })}
            >
                {`Go to ${company.name}'s Company Page`}
            </Button>
        </p>
    )
}
