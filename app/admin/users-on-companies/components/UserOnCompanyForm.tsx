import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import Select from 'app/core/components/Form/Select/Select'
import { Row, Column } from 'app/styles/grid'
import { UserRole } from 'db'
import getUsers from 'app/admin/users/queries/getUsers'
import { useQuery, useMutation, invalidateQuery } from 'blitz'
import createUserOnCompany from '../mutations/createUserOnCompany'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import getUsersOnCompanies from '../queries/getUsersOnCompanies'

export default function UserOnCompanyForm({ company, ...props }) {
    const { mode } = useContext(Context)
    const [{ users }] = useQuery(getUsers, {})
    const [addUserMutation] = useMutation(createUserOnCompany)
    return mode === 'edit' ? (
        <>
            <h3>{`Add Users to ${company.name}`}</h3>
            <Form
                submitText={`Add User to ${company.name}`}
                // TODO use a zod schema for form validation
                //  - Tip: extract mutation's schema into a shared `validations.ts` file and
                //         then import and use it here
                successMessage={`User successfully added to ${company.name}`}
                onSubmit={async (values) => {
                    //console.log({ ...values, companyId })
                    try {
                        const updated = await addUserMutation({
                            ...values,
                            companyId: company.id,
                        })
                        if (updated) {
                            invalidateQuery(getUsersOnCompanies)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
                {...props}
            >
                <Select
                    name="userId"
                    isSearchable={true}
                    label="User"
                    options={users.map((user) => {
                        return {
                            label: user.email,
                            value: user.id,
                        }
                    })}
                />
            </Form>
        </>
    ) : null
}
