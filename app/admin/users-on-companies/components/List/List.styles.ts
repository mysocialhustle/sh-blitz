import colors from 'app/core/constants/colors'
import Headings from 'app/styles/headings'
import styled from 'styled-components'

export namespace Styled {
    export const Item = styled.li`
        display: block;
        padding: 10px 0px;
        border-bottom: 1px solid ${colors.lightGrey};
        position: relative;
    `
    export const Edit = styled.div`
        cursor: pointer;
        color: ${colors.darkRed};
        position: absolute;
        z-index: 10;
        margin-right: 1em;
        width: 1rem;
        left: 0;
        top: 0;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
    `

    export const ItemInner = styled.div`
        position: relative;
        padding-left: 2em;
        line-height: 2;

        &:before {
            background: url('/images/chevron.svg') scroll no-repeat center
                center/contain;
            position: absolute;
            left: 0;
            top: 0.5em;
            height: 1em;
            width: 1em;
            content: '';
        }
    `
    export const List = styled.ul`
        list-style: outside none none;
        margin-bottom: 20px;
    `
}
