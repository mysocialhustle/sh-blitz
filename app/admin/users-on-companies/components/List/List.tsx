import { useQuery, Routes, Link } from 'blitz'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import Icon from 'app/core/components/Icon/Icon'
import { Styled } from './List.styles'
import Section from 'app/core/components/Section/Section'
import Item from './Item'

export default function List({ users, company }) {
    return users.length > 0 ? (
        <Section theme="transparentLight" size="tiny">
            <Styled.List>
                {users.map((user) => {
                    return <Item user={user} key={user.id} company={company} />
                })}
            </Styled.List>
        </Section>
    ) : null
}
