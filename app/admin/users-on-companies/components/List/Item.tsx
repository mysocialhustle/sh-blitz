import getUser from 'app/admin/users/queries/getUser'
import { Link, useQuery, Routes, invalidateQuery, useMutation } from 'blitz'
import { Context } from 'app/admin/contexts'
import { useContext } from 'react'
import Icon from 'app/core/components/Icon/Icon'
import { Styled } from './List.styles'
import getUsersOnCompanies from 'app/admin/users-on-companies/queries/getUsersOnCompanies'
import deleteUserOnCompany from '../../mutations/deleteUserOnCompany'

export default function Item({ company, user }) {
    const { mode } = useContext(Context)
    const [deleteUserOnCompanyMutation] = useMutation(deleteUserOnCompany)

    return (
        <Styled.Item style={{ paddingLeft: mode === 'edit' ? `2rem` : 0 }}>
            {mode === 'edit' && (
                <Styled.Edit
                    title="Delete User from Company"
                    onClick={async () => {
                        if (
                            window.confirm(
                                `Are you sure you want to delete this user from ${company.name}? This CANNOT be undone.`
                            )
                        ) {
                            const updated = await deleteUserOnCompanyMutation({
                                userId: user.id,
                                companyId: company.id,
                            })
                            if (updated) {
                                invalidateQuery(getUsersOnCompanies)
                            }
                        }
                    }}
                >
                    <Icon name="exCircle" />
                </Styled.Edit>
            )}
            <Styled.ItemInner>
                <Link href={Routes.UserOverviewPage({ userId: user.id })}>
                    {user.email}
                </Link>
            </Styled.ItemInner>
        </Styled.Item>
    )
}
