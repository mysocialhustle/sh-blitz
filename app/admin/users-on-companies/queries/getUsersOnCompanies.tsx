import { paginate, resolver } from 'blitz'
import db, { Prisma } from 'db'

interface GetUsersOnCompaniesInput
    extends Pick<
        Prisma.UsersOnCompaniesFindManyArgs,
        'where' | 'orderBy' | 'skip' | 'take'
    > {}

export default resolver.pipe(
    resolver.authorize(),
    async ({
        where,
        orderBy,
        skip = 0,
        take = 100,
    }: GetUsersOnCompaniesInput) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const {
            items: usersOnCompanies,
            hasMore,
            nextPage,
            count,
        } = await paginate({
            skip,
            take,
            count: () => db.usersOnCompanies.count({ where }),
            query: (paginateArgs) =>
                db.usersOnCompanies.findMany({
                    ...paginateArgs,
                    where,
                    orderBy,
                }),
        })

        return {
            usersOnCompanies,
            nextPage,
            hasMore,
            count,
        }
    }
)
