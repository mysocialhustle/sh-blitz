import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const DeleteUserOnCompany = z.object({
    userId: z.number(),
    companyId: z.number(),
})

export default resolver.pipe(
    resolver.zod(DeleteUserOnCompany),
    resolver.authorize(),
    async ({ userId, companyId }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const usersOnCompanies = await db.usersOnCompanies.deleteMany({
            where: { userId, companyId },
        })

        return usersOnCompanies
    }
)
