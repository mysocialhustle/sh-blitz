import React from 'react'

export type ModeType = 'view' | 'edit'

export interface AdminContext {
    mode: ModeType
    setMode: React.Dispatch<React.SetStateAction<ModeType>>
}

export const Context = React.createContext<AdminContext>({
    mode: 'view',
    setMode: () => {},
})
