import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export const EditBox = styled.div`
    border: 1px solid ${colors.lightGrey};
    padding: 20px;
    background-color: white;
    transform-origin: center top;
    margin-bottom: 20px;
    position: relative;
`
