import { useState, useCallback, useReducer } from 'react'
import { usePaginatedQuery, useRouter } from 'blitz'
import getUsers from 'app/admin/users/queries/getUsers'
import OrderIcons from 'app/core/blocks/OrderIcons/OrderIcons'
import * as Styled from 'app/styles/tables'
import List from './List'
import Section from 'app/core/components/Section/Section'
import Filters from './Filters'

const orderBy = {
    top: 'asc',
    bottom: 'desc',
}

export default function UsersList({ pagination }) {
    const router = useRouter()
    const page = Number(router.query.page) || 0

    const initialState = {
        orderBy: { id: 'asc' },
        skip: pagination * page,
        take: pagination,
    }
    const [state, dispatch] = useReducer(reducer, initialState)
    function reducer(state, action) {
        let newState
        switch (action.type) {
            case 'orderByEmail':
                newState = { ...state, orderBy: { email: action.direction } }
                break
            case 'orderByFirstName':
                newState = {
                    ...state,
                    orderBy: { firstName: action.direction },
                }
                break
            case 'orderByLastName':
                newState = { ...state, orderBy: { lastName: action.direction } }
                break
            case 'filterBy':
                newState = { ...state, where: { ...action.filters } }
                break
            default:
                throw new Error()
        }
        return newState
    }
    const [{ users, hasMore }] = usePaginatedQuery(getUsers, { ...state })

    const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
    const goToNextPage = () => router.push({ query: { page: page + 1 } })

    return (
        <>
            <Filters
                setColumns={(e) => console.log(e)}
                setFilters={(e) =>
                    dispatch({
                        type: 'filterBy',
                        filters: e,
                    })
                }
            />
            <Section theme="transparentLight" size="tiny">
                <Styled.Table>
                    <thead>
                        <tr>
                            <Styled.TableHeading>Role</Styled.TableHeading>
                            <Styled.TableHeading>
                                <Styled.HeadingInner>
                                    <span>First Name</span>
                                    <OrderIcons
                                        callback={(e) =>
                                            dispatch({
                                                type: 'orderByFirstName',
                                                direction: orderBy[e],
                                            })
                                        }
                                    />
                                </Styled.HeadingInner>
                            </Styled.TableHeading>
                            <Styled.TableHeading>
                                <Styled.HeadingInner>
                                    <span>Last Name</span>
                                    <OrderIcons
                                        callback={(e) =>
                                            dispatch({
                                                type: 'orderByLastName',
                                                direction: orderBy[e],
                                            })
                                        }
                                    />
                                </Styled.HeadingInner>
                            </Styled.TableHeading>
                            <Styled.TableHeading>
                                <Styled.HeadingInner>
                                    <span>Email</span>
                                    <OrderIcons
                                        callback={(e) =>
                                            dispatch({
                                                type: 'orderByEmail',
                                                direction: orderBy[e],
                                            })
                                        }
                                    />
                                </Styled.HeadingInner>
                            </Styled.TableHeading>
                            <Styled.TableHeading>
                                Confirmed?
                            </Styled.TableHeading>
                        </tr>
                    </thead>
                    <List users={users} />
                </Styled.Table>

                {/**<button disabled={page === 0} onClick={goToPreviousPage}>
                Previous
            </button>
            <button disabled={!hasMore} onClick={goToNextPage}>
                Next
                </button>**/}
            </Section>
        </>
    )
}
