import * as Styled from 'app/styles/tables'
import UserLink from './UserLink'
import Icon from 'app/core/components/Icon/Icon'
import colors from 'app/core/constants/colors'

export default function List({ users }) {
    return (
        <tbody>
            {users.map((user) => {
                const { id, confirmed, role, firstName, lastName, email } = user
                return (
                    <tr key={id}>
                        <Styled.TableCell>
                            <UserLink id={id} />
                            {role}
                        </Styled.TableCell>
                        <Styled.TableCell>
                            <UserLink id={id} />
                            {firstName || ``}
                        </Styled.TableCell>
                        <Styled.TableCell>
                            <UserLink id={id} />
                            {lastName || ``}
                        </Styled.TableCell>
                        <Styled.TableCell>
                            <UserLink id={id} />
                            {email}
                        </Styled.TableCell>
                        <Styled.TableCell>
                            <div
                                style={{
                                    textAlign: `center`,
                                    color: confirmed
                                        ? colors.green
                                        : colors.darkRed,
                                }}
                            >
                                <Icon
                                    name={
                                        confirmed ? 'checkCircle' : 'exCircle'
                                    }
                                />
                            </div>
                        </Styled.TableCell>
                    </tr>
                )
            })}
        </tbody>
    )
}
