import { useState } from 'react'
import Section from 'app/core/components/Section/Section'
import Checkboxes from 'app/core/components/Form/Checkboxes/Checkboxes'
import { EditBox } from 'app/admin/styles'
import Accordion from 'app/core/components/Accordion/Accordion'
import { Row, Column } from 'app/styles/grid'
import { SelectBase } from 'app/core/components/Form/Select/Select'
import { UserRole } from 'db'

export default function Filters({ setColumns, setFilters }) {
    return (
        <Section
            theme="transparentLight"
            size="tiny"
            style={{ paddingBottom: 0 }}
        >
            <Accordion title="Search Settings">
                <EditBox>
                    {/**<p style={{ marginBottom: 10 }}>
                            <strong>Show Columns:</strong>
                        </p>
                        <Checkboxes
                            name="columns"
                            inputs={[
                                'Role',
                                'First Name',
                                'Last Name',
                                'Confirmation Status',
                            ]}
                        />**/}
                    <Row>
                        <Column sm={50}>
                            <p style={{ marginBottom: 10 }}>
                                <strong>Filter By Role:</strong>
                            </p>
                            <SelectBase
                                options={Object.keys(UserRole)}
                                name="filters[role]"
                                label="All Roles"
                                onChange={(option) => {
                                    setFilters({ role: option?.value })
                                }}
                            />
                        </Column>
                        <Column sm={50}>
                            <p style={{ marginBottom: 10 }}>
                                <strong>Filter By Confirmation Status:</strong>
                            </p>
                            <SelectBase
                                options={[
                                    { label: 'CONFIRMED', value: true },
                                    { label: 'UNCONFIRMED', value: false },
                                ]}
                                name="filters[confirmed]"
                                label="Any Confirmation Status"
                                onChange={(option) => {
                                    setFilters({ confirmed: option?.value })
                                }}
                            />
                        </Column>
                    </Row>
                </EditBox>
            </Accordion>
        </Section>
    )
}
