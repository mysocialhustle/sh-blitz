import { Link, Routes } from 'blitz'

export default function UserLink({ id }) {
    return (
        <Link
            href={Routes.UserOverviewPage({
                userId: id,
            })}
        >
            <a />
        </Link>
    )
}
