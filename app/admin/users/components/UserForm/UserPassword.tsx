import { useState, useEffect, useReducer } from 'react'
import Checkbox from 'app/core/components/Form/Checkboxes/Checkbox'
import Input from 'app/core/components/Form/Input/Input'
import { useFormContext } from 'react-hook-form'
import generatePassword from 'app/core/utils/generatePassword'

interface State {
    password: string
    auto: boolean
}

export default function UserPassword({}) {
    const initialState: State = {
        password: generatePassword(),
        auto: true,
    }
    const [state, dispatch] = useReducer(reducer, initialState)

    const { setValue } = useFormContext()

    function reducer(state: typeof initialState, action) {
        switch (action) {
            case 'enable':
                return { ...state, ...initialState }
            case 'disable':
                return { ...state, auto: false, password: '' }
            default:
                throw new Error()
        }
    }

    const changeInput = () => {
        if (state.auto) {
            dispatch('disable')
        } else {
            dispatch('enable')
        }
    }

    useEffect(() => {
        setValue('password', state.password)
    }, [state, setValue])

    return (
        <>
            <Checkbox
                name="autoPass"
                label="Automatically generate password?"
                value="true"
                onChange={() => changeInput()}
                checked={state.auto}
            />
            <Input
                required
                label="Password"
                name="password"
                defaultValue={state.password}
            />
        </>
    )
}
