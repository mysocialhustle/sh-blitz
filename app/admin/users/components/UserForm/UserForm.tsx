import Form from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import Select from 'app/core/components/Form/Select/Select'
import { Row, Column } from 'app/styles/grid'
import { UserRole } from 'db'
import UserPassword from './UserPassword'

export { FORM_ERROR } from 'app/core/components/Form/Form/Form'

/**
 * TO-DO: Password field
 */

export function UserForm(props) {
    return (
        <Form {...props}>
            <Row>
                <Column sm={50}>
                    <Input name="firstName" label="First Name" />
                </Column>
                <Column sm={50}>
                    <Input name="lastName" label="Last Name" />
                </Column>
                <Column sm={50}>
                    <Input name="email" label="Email" type="email" />
                </Column>
                <Column sm={50}>
                    <Input name="phone" label="Phone Number" type="tel" />
                </Column>
            </Row>
            <UserPassword />
            <Select
                name="role"
                isSearchable={false}
                label="Role"
                options={Object.keys(UserRole)}
            />
        </Form>
    )
}
