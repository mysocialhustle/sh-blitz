import { Ctx, resolver } from 'blitz'
import * as z from 'zod'

const CompanyObject = z.object({
    company: z.object({
        id: z.number(),
        name: z.string(),
    }),
})

export default resolver.pipe(
    resolver.zod(CompanyObject),
    async ({ company }, ctx) => {
        // This merges the input data with whatever is already in current publicData
        return await ctx.session.$setPublicData({ company })
    }
)
