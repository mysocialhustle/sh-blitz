import { resolver, SecurePassword } from 'blitz'
import db, { UserRole } from 'db'
import { z } from 'zod'
import { password, email } from 'app/auth/validations'
import sendUserInvitation from 'app/core/utils/sendUserInvitation'

const CreateUser = z.object({
    email: email,
    password: password,
    role: z.nativeEnum(UserRole).optional(),
    firstName: z.string().optional(),
    lastName: z.string().optional(),
    phone: z.string().optional(),
})

export default resolver.pipe(
    resolver.zod(CreateUser),
    resolver.authorize(),
    async ({ email, password, role, ...rest }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const hashedPassword = await SecurePassword.hash(password.trim())
        const user = await db.user.create({
            data: {
                email: email.toLowerCase().trim(),
                hashedPassword,
                role: role ? role : UserRole.CLIENT,
                ...rest,
            },
        })
        if (user) {
            const [fetcher, response] = await sendUserInvitation(
                email,
                password
            )
            return {
                user,
                fetcher,
                response,
            }
        }
    }
)
