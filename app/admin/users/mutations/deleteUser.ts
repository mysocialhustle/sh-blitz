import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const DeleteUser = z.object({
    id: z.number(),
})

export default resolver.pipe(
    resolver.zod(DeleteUser),
    resolver.authorize(),
    async ({ id }) => {
        try {
            // TODO: in multi-tenant app, you must add validation to ensure correct tenant
            await db.usersOnCompanies.deleteMany({
                where: { userId: id },
            })
            await db.user.deleteMany({ where: { id } })
        } catch (error) {
            return error.toString()
        }
    }
)
