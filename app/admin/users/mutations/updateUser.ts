import { resolver } from 'blitz'
import db, { UserRole } from 'db'
import { z } from 'zod'
import { email } from 'app/auth/validations'

const UpdateUser = z.object({
    id: z.number(),
    firstName: z.string().optional(),
    lastName: z.string().optional(),
    email: email,
    phone: z.string().optional(),
    role: z.nativeEnum(UserRole),
})

export default resolver.pipe(
    resolver.zod(UpdateUser),
    resolver.authorize(),
    async ({ id, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const user = await db.user.update({ where: { id }, data })

        return user
    }
)
