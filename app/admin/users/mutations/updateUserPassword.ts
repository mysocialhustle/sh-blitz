import { NotFoundError, SecurePassword, resolver } from 'blitz'
import db from 'db'
import { password } from 'app/auth/validations'
import { z } from 'zod'

const UpdatePassword = z.object({
    id: z.number(),
    password: password,
})

export default resolver.pipe(
    resolver.zod(UpdatePassword),
    resolver.authorize(),
    async ({ id, password }) => {
        const hashedPassword = await SecurePassword.hash(password.trim())

        const user = await db.user.update({
            where: { id },
            data: { hashedPassword },
        })

        return user
    }
)
