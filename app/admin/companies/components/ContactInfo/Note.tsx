import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import Form from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { EditBox } from 'app/admin/styles'
import updateCompany from '../../mutations/updateCompany'
import { useMutation, invalidateQuery } from 'blitz'
import getCompany from '../../queries/getCompany'
import CloseButton from 'app/admin/components/CloseButton/CloseButton'
import { Notes } from 'types'

export default function Note({ label, value, index, company }) {
    const { mode } = useContext(Context)
    const [updateCompanyMutation] = useMutation(updateCompany)

    const deleteBox = async () => {
        let copy: Notes = [...company.contactInfo]
        copy.splice(index, 1)
        //console.log(copy, campaign.notes)
        if (
            window.confirm('Are you sure you want to delete this contact info?')
        ) {
            const updated = await updateCompanyMutation({
                id: company.id,
                contactInfo: copy,
            })
            if (updated) {
                invalidateQuery(getCompany)
                //setMode('view')
            }
        }
    }

    return mode === 'edit' ? (
        <EditBox>
            <CloseButton
                style={{ top: -10, left: -10 }}
                onClick={() => deleteBox()}
            />
            <Input
                label="Label"
                name={`contactInfo[${index}][label]`}
                defaultValue={label}
                required={true}
            />
            <Input
                element="textarea"
                rows={6}
                label="Info"
                name={`contactInfo[${index}][value]`}
                defaultValue={value}
                required={true}
            />
        </EditBox>
    ) : (
        <>
            <p>
                <strong>{label}</strong>
            </p>
            <p>{value}</p>
        </>
    )
}
