import { useContext } from 'react'
import { Context } from 'app/admin/contexts'
import Form from 'app/core/components/Form/Form/Form'
import updateCompany from '../../mutations/updateCompany'
import { useMutation, invalidateQuery } from 'blitz'
import getCompany from '../../queries/getCompany'
import { Notes } from 'types'

interface FormVals {
    id: number
    contactInfo: Notes
}

export default function NotesWrapper({ children, company, ...rest }) {
    const { mode, setMode } = useContext(Context)
    const [updateCompanyMutation] = useMutation(updateCompany)

    const saveNotes = async (vals: FormVals) => {
        const updated = await updateCompanyMutation({
            id: company.id,
            contactInfo: vals.contactInfo,
        })
        if (updated) {
            invalidateQuery(getCompany)
            setMode('view')
        }
    }

    return mode === 'edit' ? (
        <Form
            onSubmit={saveNotes}
            submitText="Save Contact Info"
            successMessage={null}
            buttonProps={{
                style: {
                    marginBottom: 50,
                },
            }}
            {...rest}
        >
            {children}
        </Form>
    ) : (
        <>{children}</>
    )
}
