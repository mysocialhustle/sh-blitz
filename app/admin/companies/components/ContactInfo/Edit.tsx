import { Context } from 'app/admin/contexts'
import { useContext, useState } from 'react'
import { EditBox } from 'app/admin/styles'
import Input from 'app/core/components/Form/Input/Input'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import { Company } from 'db'
import updateCompany from '../../mutations/updateCompany'
import { useMutation, invalidateQuery } from 'blitz'
import getCompany from '../../queries/getCompany'
import hasValue from 'app/core/utils/hasValue'
import AddDeleteButtons from 'app/admin/components/AddDeleteButtons/AddDeleteButtons'
import { Row, Column } from 'app/styles/grid'
import { Notes } from 'types'

interface Props {
    company: Company
}

export default function Edit({ company }: Props) {
    const { mode } = useContext(Context)
    const [boxCount, setBoxCount] = useState(1)
    const [updateCompanyMutation] = useMutation(updateCompany)
    const contactInfo = company.contactInfo as unknown as Notes

    return mode === 'edit' ? (
        <EditBox>
            <h3>Add Contact Info</h3>
            <Form
                submitText="Submit"
                successMessage={null}
                onSubmit={async (values: { contactInfo: Notes }) => {
                    const nonEmpty = values?.contactInfo.filter(
                        (report) =>
                            hasValue(report.label) && hasValue(report.value)
                    )
                    const currentValues =
                        contactInfo && Array.isArray(contactInfo)
                            ? contactInfo
                            : []
                    try {
                        const updated = await updateCompanyMutation({
                            id: company.id,
                            contactInfo: [...currentValues, ...nonEmpty],
                        })
                        if (updated) {
                            invalidateQuery(getCompany)
                            setBoxCount(1)
                        }
                    } catch (error: any) {
                        console.error(error)
                        return {
                            [FORM_ERROR]: error.toString(),
                        }
                    }
                }}
            >
                {Array.from(Array(boxCount).keys()).map((index) => (
                    <Row key={index}>
                        <Column sm={35}>
                            <Input
                                label="Label"
                                name={`contactInfo[${index}][label]`}
                                required={true}
                            />
                        </Column>
                        <Column sm={65}>
                            <Input
                                element="textarea"
                                rows={4}
                                label="Info"
                                name={`contactInfo[${index}][value]`}
                                required={true}
                            />
                        </Column>
                    </Row>
                ))}
                <AddDeleteButtons
                    add={() => setBoxCount(boxCount + 1)}
                    remove={() => {
                        const num = boxCount <= 1 ? 1 : boxCount - 1
                        setBoxCount(num)
                    }}
                />
            </Form>
        </EditBox>
    ) : null
}
