import { Row, Column } from 'app/styles/grid'
import Note from './Note'
import NotesWrapper from './NotesWrapper'
import Edit from './Edit'

export default function ContactInfo({ company }) {
    const { contactInfo } = company

    return (
        <>
            {contactInfo &&
                Array.isArray(contactInfo) &&
                contactInfo?.length > 0 && (
                    <>
                        <h3>Contact Info</h3>
                        <NotesWrapper company={company}>
                            <Row>
                                {contactInfo.map((note, index) => {
                                    return (
                                        <Column key={index} sm={50}>
                                            <Note
                                                index={index}
                                                company={company}
                                                {...note}
                                            />
                                        </Column>
                                    )
                                })}
                            </Row>
                        </NotesWrapper>
                    </>
                )}
            <Edit company={company} />
        </>
    )
}
