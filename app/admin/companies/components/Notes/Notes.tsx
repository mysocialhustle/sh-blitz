import { Row, Column } from 'app/styles/grid'
import Note from './Note'
import NotesWrapper from './NotesWrapper'
import Edit from './Edit'

export default function Notes({ company }) {
    const { notes } = company

    return (
        <>
            {notes && Array.isArray(notes) && notes?.length > 0 && (
                <>
                    <h3>Notes</h3>
                    <NotesWrapper company={company}>
                        <Row>
                            {notes.map((note, index) => {
                                return (
                                    <Column key={index} sm={50}>
                                        <Note
                                            index={index}
                                            company={company}
                                            {...note}
                                        />
                                    </Column>
                                )
                            })}
                        </Row>
                    </NotesWrapper>
                </>
            )}
            <Edit company={company} />
        </>
    )
}
