import { useQuery, Routes, Link } from 'blitz'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import Icon from 'app/core/components/Icon/Icon'
import { Styled } from './RelatedModels.styles'
import Section from 'app/core/components/Section/Section'

interface Props {
    items: any[]
    modelType: 'campaigns' | 'websites' | 'contents'
}

export default function RelatedModels({ items, modelType }: Props) {
    const modelLink = (id) => {
        let link

        switch (modelType) {
            case 'campaigns':
                link = Routes.CampaignOverviewPage({ campaignId: id })
                break

            case 'websites':
                link = Routes.WebsiteOverviewPage({ websiteId: id })
                break

            case 'contents':
                link = Routes.ContentOverviewPage({ contentId: id })
                break

            default:
                throw new Error()
        }

        return link
    }
    return items.length > 0 ? (
        <Section theme="transparentLight" size="tiny">
            <Styled.List>
                {items.map(({ name, id }) => {
                    return (
                        <Styled.Item key={id}>
                            <Styled.ItemInner>
                                <Link href={modelLink(id)}>{name}</Link>
                            </Styled.ItemInner>
                        </Styled.Item>
                    )
                })}
            </Styled.List>
        </Section>
    ) : null
}
