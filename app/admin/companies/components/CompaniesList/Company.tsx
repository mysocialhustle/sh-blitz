import { Head, Link, useQuery, useRouter, BlitzPage, Routes } from 'blitz'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import Account from 'app/core/layouts/Account/Account'
import Button from 'app/core/components/Button/Button'
import Section from 'app/core/components/Section/Section'
import { Styled } from './CompaniesList.styles'
import Icon from 'app/core/components/Icon/Icon'
import { Divider } from 'app/styles/utilities'

export default function Company({ id, name }) {
    const [{ count: campaignCount }] = useQuery(getCampaigns, {
        where: { companyId: id },
    })
    const [{ count: websiteCount }] = useQuery(getWebsites, {
        where: { companyId: id },
    })
    const [{ count: contentCount }] = useQuery(getContents, {
        where: { companyId: id },
    })

    const models = [
        { name: 'Campaign', count: campaignCount, icon: 'chart' },
        { name: 'Website', count: websiteCount, icon: 'website' },
        { name: 'Content', count: contentCount, icon: 'images' },
    ].filter((el) => el.count > 0)

    return (
        <Styled.Item key={id}>
            <Styled.Inner>
                <Styled.Title>{name}</Styled.Title>
                <Button
                    href={Routes.CompanyOverviewPage({
                        companyId: id,
                    })}
                >
                    View Company
                </Button>
            </Styled.Inner>
            <Styled.Models>
                {models.map(({ count, name, icon }, index) => {
                    const plural = count > 1 ? `s` : ``
                    return (
                        <li key={index}>
                            <Icon name={icon} />
                            <span>
                                {count} {name}
                                {plural}
                            </span>
                            {index < models.length - 1 && <Divider />}
                        </li>
                    )
                })}
            </Styled.Models>
        </Styled.Item>
    )
}
