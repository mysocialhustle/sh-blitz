import colors from 'app/core/constants/colors'
import Headings from 'app/styles/headings'
import styled from 'styled-components'

export namespace Styled {
    export const List = styled.ul`
        list-style: inside none none;
    `
    export const Item = styled.li`
        padding: 20px 0;
        border-bottom: 1px solid ${colors.lightGrey};
    `

    export const Inner = styled.div`
        display: flex;
        justify-content: space-between;
        align-items: center;
    `

    export const Title = styled(Headings.H4).attrs({ as: `h3` })`
        letter-spacing: 0.25px;
        font-weight: 600;
        margin-bottom: 0;
        text-transform: uppercase;
    `

    export const Models = styled.ul`
        list-style: inside none none;
        margin-top: 10px;

        li {
            display: inline-block;

            svg {
                margin-right: 10px;
            }
        }
    `
}
