import {
    Head,
    Link,
    usePaginatedQuery,
    useRouter,
    BlitzPage,
    Routes,
} from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import Account from 'app/core/layouts/Account/Account'
import Button from 'app/core/components/Button/Button'
import Section from 'app/core/components/Section/Section'
import { Styled } from './CompaniesList.styles'
import Company from './Company'

export default function CompaniesList({ pagination }) {
    const router = useRouter()
    const page = Number(router.query.page) || 0
    const [{ companies, hasMore }] = usePaginatedQuery(getCompanies, {
        orderBy: { id: 'asc' },
        skip: pagination * page,
        take: pagination,
    })

    const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
    const goToNextPage = () => router.push({ query: { page: page + 1 } })

    return (
        <Section theme="transparentLight" size="tiny">
            <Styled.List>
                {companies.map((company) => (
                    <Company key={company.id} {...company} />
                ))}
            </Styled.List>

            {/**<button disabled={page === 0} onClick={goToPreviousPage}>
                Previous
            </button>
            <button disabled={!hasMore} onClick={goToNextPage}>
                Next
                        </button>**/}
        </Section>
    )
}
