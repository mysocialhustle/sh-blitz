import ModelNavigation from 'app/admin/components/ModelNavigation/ModelNavigation'
import { useQuery, useParam, Routes } from 'blitz'
import getCompany from '../queries/getCompany'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import Icon from 'app/core/components/Icon/Icon'

export default function CompanyNavigation() {
    const companyId = useParam('companyId', 'number')
    const [company] = useQuery(getCompany, { id: companyId })
    const [campaigns] = useQuery(getCampaigns, {
        where: { companyId: companyId },
    })
    const [websites] = useQuery(getWebsites, {
        where: { companyId: companyId },
    })
    const [contents] = useQuery(getContents, {
        where: { companyId: companyId },
    })

    const models = [
        {
            name: 'Campaign',
            count: campaigns.count,
            icon: 'chart',
            link: Routes.CompanyCampaigns({
                companyId: company.id,
            }),
        },
        {
            name: 'Website',
            count: websites.count,
            icon: 'website',
            link: Routes.CompanyWebsites({
                companyId: company.id,
            }),
        },
        {
            name: 'Content',
            count: contents.count,
            icon: 'images',
            link: Routes.CompanyContents({
                companyId: company.id,
            }),
        },
    ]
        .filter((el) => el?.count > 0)
        .map((el) => {
            const plural = el.count > 1 ? `s` : ``
            return {
                ...el,
                name: `${el.count} ${el.name}${plural}`,
            }
        })

    const baseNavigation = [
        {
            name: 'Company Overview',
            link: Routes.CompanyOverviewPage({
                companyId: company.id,
            }),
        },
        {
            name: 'Users',
            link: Routes.CompanyUsers({
                companyId: company.id,
            }),
        },
    ]

    const items = [...baseNavigation, ...models]

    return <ModelNavigation items={items} />
}
