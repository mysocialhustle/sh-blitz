import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const CreateCompany = z.object({
    name: z.string(),
})

export default resolver.pipe(
    resolver.zod(CreateCompany),
    resolver.authorize(),
    async (input) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const company = await db.company.create({ data: input })

        return company
    }
)
