import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const UpdateCompany = z.object({
    id: z.number(),
    notes: z
        .array(
            z.object({
                label: z.string(),
                value: z.string(),
            })
        )
        .optional(),
    contactInfo: z
        .array(
            z.object({
                label: z.string(),
                value: z.string(),
            })
        )
        .optional(),
})

export default resolver.pipe(
    resolver.zod(UpdateCompany),
    resolver.authorize(),
    async ({ id, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const company = await db.company.update({ where: { id }, data })

        return company
    }
)
