import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const DeleteCompany = z.object({
    id: z.number(),
})

export default resolver.pipe(
    resolver.zod(DeleteCompany),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const company = await db.company.deleteMany({ where: { id } })

        return company
    }
)
