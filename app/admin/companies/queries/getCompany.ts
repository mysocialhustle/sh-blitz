import { resolver, NotFoundError } from 'blitz'
import db from 'db'
import { z } from 'zod'

const GetCompany = z.object({
    // This accepts type of undefined, but is required at runtime
    id: z.number().optional().refine(Boolean, 'Required'),
})

export default resolver.pipe(
    resolver.zod(GetCompany),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const company = await db.company.findFirst({ where: { id } })

        if (!company) throw new NotFoundError()

        return company
    }
)
