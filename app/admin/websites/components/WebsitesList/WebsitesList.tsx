import { Link, usePaginatedQuery, useRouter, Routes } from 'blitz'
import getWebsites from 'app/admin/websites/queries/getWebsites'

export default function WebsitesList({ pagination }) {
    const router = useRouter()
    const page = Number(router.query.page) || 0
    const [{ websites, hasMore }] = usePaginatedQuery(getWebsites, {
        orderBy: { id: 'asc' },
        skip: pagination * page,
        take: pagination,
    })

    const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
    const goToNextPage = () => router.push({ query: { page: page + 1 } })

    return (
        <div>
            <ul>
                {websites.map((website) => (
                    <li key={website.id}>
                        <Link
                            href={Routes.WebsiteOverviewPage({
                                websiteId: website.id,
                            })}
                        >
                            <a>{website.name}</a>
                        </Link>
                    </li>
                ))}
            </ul>

            {/**<button disabled={page === 0} onClick={goToPreviousPage}>
                Previous
            </button>
            <button disabled={!hasMore} onClick={goToNextPage}>
                Next
                        </button>**/}
        </div>
    )
}
