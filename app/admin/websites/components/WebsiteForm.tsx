import Form from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import Select from 'app/core/components/Form/Select/Select'
import { Row, Column } from 'app/styles/grid'
import { websiteTypes } from 'app/data/websites'

export { FORM_ERROR } from 'app/core/components/Form/Form/Form'

export function WebsiteForm(props) {
    return (
        <Form {...props}>
            <Row>
                <Column md={33}>
                    <Input name="name" label="Name" required />
                </Column>
                <Column md={33}>
                    <Input name="url" label="URL (optional)" type="url" />
                </Column>
                <Column md={33}>
                    <Select
                        name="type"
                        isSearchable={false}
                        label="Type of Website"
                        options={Object.entries(websiteTypes).map(
                            ([key, val]) => {
                                return {
                                    value: key,
                                    label: val,
                                }
                            }
                        )}
                    />
                </Column>
            </Row>
        </Form>
    )
}
