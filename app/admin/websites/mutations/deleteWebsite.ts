import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const DeleteWebsite = z.object({
    id: z.number(),
})

export default resolver.pipe(
    resolver.zod(DeleteWebsite),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const website = await db.website.deleteMany({ where: { id } })

        return website
    }
)
