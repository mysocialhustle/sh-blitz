import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const UpdateWebsite = z.object({
    id: z.number(),
    name: z.string(),
})

export default resolver.pipe(
    resolver.zod(UpdateWebsite),
    resolver.authorize(),
    async ({ id, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const website = await db.website.update({ where: { id }, data })

        return website
    }
)
