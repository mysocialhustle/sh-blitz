import { resolver, NotFoundError } from 'blitz'
import db from 'db'
import { z } from 'zod'

const GetWebsite = z.object({
    // This accepts type of undefined, but is required at runtime
    id: z.number().optional().refine(Boolean, 'Required'),
})

export default resolver.pipe(
    resolver.zod(GetWebsite),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const website = await db.website.findFirst({ where: { id } })

        if (!website) throw new NotFoundError()

        return website
    }
)
