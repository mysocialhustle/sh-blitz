import { paginate, resolver } from 'blitz'
import db, { Prisma } from 'db'

interface GetWebsitesInput
    extends Pick<
        Prisma.WebsiteFindManyArgs,
        'where' | 'orderBy' | 'skip' | 'take'
    > {}

export default resolver.pipe(
    resolver.authorize(),
    async ({ where, orderBy, skip = 0, take = 100 }: GetWebsitesInput) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const {
            items: websites,
            hasMore,
            nextPage,
            count,
        } = await paginate({
            skip,
            take,
            count: () => db.website.count({ where }),
            query: (paginateArgs) =>
                db.website.findMany({ ...paginateArgs, where, orderBy }),
        })

        return {
            websites,
            nextPage,
            hasMore,
            count,
        }
    }
)
