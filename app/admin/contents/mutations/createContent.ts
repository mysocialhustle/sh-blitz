import { resolver } from 'blitz'
import db, { ContentType } from 'db'
import { z } from 'zod'

const CreateContent = z.object({
    name: z.string(),
    type: z.nativeEnum(ContentType),
    companyId: z.number(),
})

export default resolver.pipe(
    resolver.zod(CreateContent),
    resolver.authorize(),
    async (input) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const content = await db.content.create({ data: input })

        return content
    }
)
