import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const UpdateContent = z.object({
    id: z.number(),
    name: z.string(),
})

export default resolver.pipe(
    resolver.zod(UpdateContent),
    resolver.authorize(),
    async ({ id, ...data }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const content = await db.content.update({ where: { id }, data })

        return content
    }
)
