import { resolver } from 'blitz'
import db from 'db'
import { z } from 'zod'

const DeleteContent = z.object({
    id: z.number(),
})

export default resolver.pipe(
    resolver.zod(DeleteContent),
    resolver.authorize(),
    async ({ id }) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const content = await db.content.deleteMany({ where: { id } })

        return content
    }
)
