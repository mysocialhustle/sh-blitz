import { paginate, resolver } from 'blitz'
import db, { Prisma } from 'db'

interface GetContentsInput
    extends Pick<
        Prisma.ContentFindManyArgs,
        'where' | 'orderBy' | 'skip' | 'take'
    > {}

export default resolver.pipe(
    resolver.authorize(),
    async ({ where, orderBy, skip = 0, take = 100 }: GetContentsInput) => {
        // TODO: in multi-tenant app, you must add validation to ensure correct tenant
        const {
            items: contents,
            hasMore,
            nextPage,
            count,
        } = await paginate({
            skip,
            take,
            count: () => db.content.count({ where }),
            query: (paginateArgs) =>
                db.content.findMany({ ...paginateArgs, where, orderBy }),
        })

        return {
            contents,
            nextPage,
            hasMore,
            count,
        }
    }
)
