import {
    Head,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import OnboardingLayout from 'app/onboarding/layouts/Onboarding'
import getContent from 'app/admin/contents/queries/getContent'
import getCompany from 'app/admin/companies/queries/getCompany'

export default function ContentOnboardingPage() {
    const contentId = useParam('contentId', 'number')
    const [content] = useQuery(getContent, { id: contentId })
    const [company] = useQuery(getCompany, { id: content.companyId })

    return (
        <>
            <Head>
                <title>{content.name} Onboarding</title>
            </Head>
            <div>{JSON.stringify(content)}</div>
        </>
    )
}

ContentOnboardingPage.authenticate = { redirectTo: '/login' }
ContentOnboardingPage.getLayout = (page) => (
    <OnboardingLayout>{page}</OnboardingLayout>
)
