import {
    Head,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import OnboardingLayout from 'app/onboarding/layouts/Onboarding'
import getWebsite from 'app/admin/websites/queries/getWebsite'
import getCompany from 'app/admin/companies/queries/getCompany'

export default function WebsiteOnboardingPage() {
    const websiteId = useParam('websiteId', 'number')
    const [website] = useQuery(getWebsite, { id: websiteId })
    const [company] = useQuery(getCompany, { id: website.companyId })

    return (
        <>
            <Head>
                <title>{website.name} Onboarding</title>
            </Head>
            <div>{JSON.stringify(website)}</div>
        </>
    )
}

WebsiteOnboardingPage.authenticate = { redirectTo: '/login' }
WebsiteOnboardingPage.getLayout = (page) => (
    <OnboardingLayout>{page}</OnboardingLayout>
)
