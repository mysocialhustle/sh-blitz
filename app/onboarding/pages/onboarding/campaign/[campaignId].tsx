import {
    Head,
    useRouter,
    useQuery,
    useParam,
    BlitzPage,
    useMutation,
    Routes,
} from 'blitz'
import OnboardingLayout from 'app/onboarding/layouts/Onboarding'
import getCampaign from 'app/admin/campaigns/queries/getCampaign'
import getCompany from 'app/admin/companies/queries/getCompany'
import CompanyContact from 'app/onboarding/components/CompanyContact'
import MarketingInfo from 'app/onboarding/components/MarkertingInfo'
import TikTok from 'app/onboarding/components/TikTok'
import { useContext } from 'react'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import { motion, AnimatePresence } from 'framer-motion'
import Confirmation from 'app/onboarding/components/Confirmation'
import { Container } from 'app/styles/grid'
import { OnboardingContext } from 'app/onboarding/context'
import GoogleAds from 'app/onboarding/components/GoogleAds'
import { CampaignType } from 'db'
import SnapchatAds from 'app/onboarding/components/SnapchatAds'
import FacebookAds from 'app/onboarding/components/FacebookAds'

type ComponentType = Partial<Record<CampaignType, any>>

export default function CampaignOnboardingPage() {
    const campaignId = useParam('campaignId', 'number')
    const [campaign] = useQuery(getCampaign, { id: campaignId })
    const [company] = useQuery(getCompany, { id: campaign.companyId })
    const [updateCampaignMutation] = useMutation(updateCampaign)
    const { page, setPage } = useContext(OnboardingContext)

    const campaignComponents: ComponentType = {
        tiktokAds: TikTok,
        googleAdwords: GoogleAds,
        snapchatAds: SnapchatAds,
        facebookAds: FacebookAds,
    }

    const components = [
        {
            Component: CompanyContact,
            props: {
                company,
                callback: () => {
                    setPage(1)
                },
            },
        },
        {
            Component: MarketingInfo,
            props: {
                campaign,
                callback: () => {
                    setPage(2)
                },
            },
        },
        {
            Component: campaignComponents[campaign.type],
            props: {
                campaign,
                callback: async () => {
                    setPage(3)
                    await updateCampaignMutation({
                        id: campaign.id,
                        onboarded: true,
                    })
                },
            },
        },
        {
            Component: Confirmation,
            props: {
                modelType: 'campaign',
            },
        },
    ]

    return (
        <>
            <Head>
                <title>{campaign.name} Onboarding</title>
            </Head>
            {/**<CompanyContact
                company={company}
                callback={(e) => console.log(e)}
    />
            <MarketingInfo
                campaign={campaign}
                callback={(e) => console.log(e)}
/>
            <TikTok campaign={campaign} callback={(e) => console.log(e)} />**/}
            <AnimatePresence exitBeforeEnter>
                {components.map(({ Component, props }, index) => {
                    return page === index ? (
                        <motion.div
                            key={page}
                            initial={{
                                opacity: 0,
                                y: `100%`,
                            }}
                            animate={{
                                opacity: 1,
                                y: 0,
                            }}
                            exit={{
                                opacity: 0,
                                y: `-100%`,
                            }}
                            transition={{
                                ease: 'easeInOut',
                                duration: 0.75,
                            }}
                        >
                            <Component key={index} {...props} />
                        </motion.div>
                    ) : null
                })}
            </AnimatePresence>
        </>
    )
}

CampaignOnboardingPage.authenticate = { redirectTo: '/login' }
CampaignOnboardingPage.getLayout = (page) => (
    <OnboardingLayout maxPages={4}>{page}</OnboardingLayout>
)
