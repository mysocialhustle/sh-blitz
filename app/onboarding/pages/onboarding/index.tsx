import OnboardingLayout from 'app/onboarding/layouts/Onboarding'
import { Container, Row, Column } from 'app/styles/grid'
import { useSession } from 'blitz'
import Selection from 'app/onboarding/components/Selection'
import CompanySelect from 'app/admin/components/SessionManager/CompanySelect'
import useSessionCompany from 'app/core/hooks/useSessionCompany'

export default function OnboardingPage() {
    const company = useSessionCompany()

    return (
        <>
            <h2 style={{ textAlign: 'center' }}>
                Welcome to the Social Hustle Family!
            </h2>
            <Row alignX="center">
                <Column sm={70}>
                    <p style={{ textAlign: 'center' }}>
                        Our team is ecstatic to begin working with you to help
                        you grow your business. Before our team can get to work,
                        we need just a bit of information from you.
                    </p>
                </Column>
            </Row>
            {company ? (
                <Selection companyId={company?.id} />
            ) : (
                <>
                    <h4
                        style={{
                            textAlign: `center`,
                            fontWeight: `bold`,
                            letterSpacing: 0,
                        }}
                    >
                        Select the Company You Want to Onboard:
                    </h4>
                    <Row alignX="center">
                        <Column sm={50} md={40}>
                            <CompanySelect />
                        </Column>
                    </Row>
                </>
            )}
        </>
    )
}

OnboardingPage.authenticate = { redirectTo: '/login' }
OnboardingPage.getLayout = (page) => (
    <OnboardingLayout title="Onboarding">{page}</OnboardingLayout>
)
