import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import { useMutation } from 'blitz'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import hasValue from 'app/core/utils/hasValue'

export default function MarketingInfo({ campaign, callback }) {
    const [updateCampaignMutation] = useMutation(updateCampaign)

    return (
        <Form
            submitText="NEXT"
            onSubmit={async (values) => {
                const map = Object.entries(values)?.map(([key, val]) => {
                    return {
                        label: key,
                        value: val,
                    }
                })
                const nonEmpty = map?.filter(({ value }) => hasValue(value))
                const currentValues =
                    campaign?.notes && Array.isArray(campaign?.notes)
                        ? campaign?.notes
                        : []
                try {
                    const updated = await updateCampaignMutation({
                        id: campaign.id,
                        notes: [...currentValues, ...nonEmpty],
                    })
                    if (updated) {
                        callback(updated)
                    }
                } catch (error: any) {
                    console.error(error)
                    return {
                        [FORM_ERROR]: error.toString(),
                    }
                }
            }}
            /**initialValues={contactInfo}**/
            buttonProps={{
                centered: true,
            }}
            successMessage={null}
        >
            <h3 style={{ textAlign: 'center' }}>
                {`First, let's go over your target market.`}
            </h3>
            <Row>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What are your target countries?"
                        label="What are your target countries?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What states do you want to target?"
                        label="What states do you want to target?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What are your target cities or zip codes?"
                        label="And your target cities or zip codes?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="Excluded Locations"
                        label="Any excluded locations?"
                    />
                </Column>
            </Row>
            <Input
                element="textarea"
                rows={5}
                name="Keywords"
                label="List the main keywords you want us to focus on"
            />

            <h3 style={{ textAlign: 'center' }}>
                Next, we will get an overview of your company and its goals!
            </h3>
            <Row>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What does your ideal customer look like?"
                        label="What does your ideal customer look like?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="Why would a potential customer use you over the competition?"
                        label="Why would a potential customer use you over the competition?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="Who are your primary competitors?"
                        label="Who are your primary competitors?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What are the most important or unique aspects of your company?"
                        label="What are the most important or unique aspects of your company?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What are your primary products and/or services?"
                        label="What are your primary products and/or services?"
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        rows={5}
                        name="What is the average lifetime value of your customer?"
                        label="What is the average lifetime value of your customer?"
                    />
                </Column>
            </Row>
            <Input
                element="textarea"
                rows={5}
                name="Where would you like to receive reporting and Paid Media Related communication?"
                label="Where would you like to receive reporting and Paid Media Related communication?"
            />
        </Form>
    )
}
