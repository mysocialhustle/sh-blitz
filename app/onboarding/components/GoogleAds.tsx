import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import { useMutation } from 'blitz'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import hasValue from 'app/core/utils/hasValue'
import { useState } from 'react'
import Checkboxes from './Checkbox/Checkboxes'
import Conditional from './Conditional'
import Select from 'app/core/components/Form/Select/Select'
import Password from 'app/core/components/Form/Password/Password'

export default function GoogleAds({ campaign, callback }) {
    const [hasAccount, setHasAccount] = useState(null)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    return (
        <Form
            submitText="NEXT"
            onSubmit={async ({ notes, logins }) => {
                const notesMap = Object.entries(notes)?.map(([key, val]) => {
                    return {
                        label: key,
                        value: val,
                    }
                })
                const nonEmptyNotes = notesMap?.filter(({ value }) =>
                    hasValue(value)
                )
                const currentNotes =
                    campaign?.notes && Array.isArray(campaign?.notes)
                        ? campaign?.notes
                        : []
                const loginValues = logins
                    ? [
                          {
                              website: `Google Ads Account #`,
                              ...logins,
                          },
                      ]
                    : []
                const currentLogins =
                    campaign?.logins && Array.isArray(campaign?.logins)
                        ? campaign?.logins
                        : []
                try {
                    const updated = await updateCampaignMutation({
                        id: campaign.id,
                        notes: [...currentNotes, ...nonEmptyNotes],
                        logins: [...currentLogins, ...loginValues],
                    })
                    if (updated) {
                        callback(updated)
                    }
                } catch (error: any) {
                    console.error(error)
                    return {
                        [FORM_ERROR]: error.toString(),
                    }
                }
            }}
            /**initialValues={contactInfo}**/
            buttonProps={{
                centered: true,
                disabled: !hasAccount,
            }}
            successMessage={null}
        >
            <h3 style={{ textAlign: `center` }}>
                Do you have a Google Ads account?
            </h3>
            <Checkboxes
                name="Do you have a Google Ads account?"
                onChange={(e) => setHasAccount(e)}
            />
            <Conditional active={hasAccount}>
                <Conditional active={hasAccount === 'Yes'} speed={0.5}>
                    <Input
                        name="logins[username]"
                        required
                        label="What Is Your Google Ads Account Number?"
                    />
                </Conditional>
                <Input
                    element="textarea"
                    rows={5}
                    label="What is your primary objective within Google Ads?"
                    name="notes[What is your primary objective within Google Ads?]"
                />
            </Conditional>
        </Form>
    )
}
