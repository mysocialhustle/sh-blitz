import { useQuery, useParam, Routes } from 'blitz'
import getCampaigns from 'app/admin/campaigns/queries/getCampaigns'
import getWebsites from 'app/admin/websites/queries/getWebsites'
import getContents from 'app/admin/contents/queries/getContents'
import { Container, Row, Column } from 'app/styles/grid'
import Box from 'app/core/components/Box/Box'

export default function Selection({ companyId }) {
    const [{ campaigns }] = useQuery(getCampaigns, {
        where: { companyId: companyId },
    })
    const [{ websites }] = useQuery(getWebsites, {
        where: { companyId: companyId },
    })
    const [{ contents }] = useQuery(getContents, {
        where: { companyId: companyId },
    })

    const selections = [
        ...campaigns
            .filter((campaign) => campaign.onboarded === false)
            .map((project) => {
                return {
                    ...project,
                    projectType: 'campaign',
                    link: Routes.CampaignOnboardingPage({
                        campaignId: project.id,
                    }),
                }
            }),
        ...websites
            .filter((website) => website.onboarded === false)
            .map((project) => {
                return {
                    ...project,
                    projectType: 'website',
                    link: Routes.WebsiteOnboardingPage({
                        websiteId: project.id,
                    }),
                }
            }),
        ...contents
            .filter((content) => content.onboarded === false)
            .map((project) => {
                return {
                    ...project,
                    projectType: 'content',
                    link: Routes.ContentOnboardingPage({
                        contentId: project.id,
                    }),
                }
            }),
    ]

    return selections.length > 0 ? (
        <>
            <h4
                style={{
                    textAlign: `center`,
                    fontWeight: `bold`,
                    letterSpacing: 0,
                }}
            >
                Select The Project You Want to Onboard:
            </h4>
            <Row alignX="center">
                {selections.map((project, index) => {
                    const { link, name } = project
                    console.log(project)
                    return (
                        <Column sm={33} key={index}>
                            <Box href={link}>
                                <h3>{name}</h3>
                            </Box>
                        </Column>
                    )
                })}
            </Row>
        </>
    ) : null
}
