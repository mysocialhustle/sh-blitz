import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import { Row, Column } from 'app/styles/grid'
import { StyledProps } from 'types'

export namespace Styled {
    export const Label = styled.p`
        font-size: 1.25rem;
        text-transform: uppercase;
        font-weight: 600;
        letter-spacing: 0.5px;
    `

    export const Wrapper = styled.label`
        display: block;
        width: 100%;
        cursor: pointer;

        svg {
            transition: all 0.25s ease-in-out;
            font-size: 5rem;
            margin-bottom: 30px;
        }

        &:hover {
            svg {
                transform: scale(1.25);
            }
        }
    `

    export const Checkbox = styled.input`
        position: absolute;
        visibility: hidden;

        &:checked + svg {
            transform: scale(1.1);
        }
    `

    export const Group = styled(Row)`
        margin-bottom: 30px;
        text-align: center;
    `
}
