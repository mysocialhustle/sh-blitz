import { Styled } from './Checkbox.styles'
import { CheckboxInput } from 'types'

export default function Checkbox({
    label,
    value,
    onChange,
    ...rest
}: CheckboxInput) {
    return (
        <Styled.Label>
            <Styled.Checkbox
                type="radio"
                value={value}
                onChange={(e) => {
                    if (onChange) onChange(e.target)
                }}
                {...rest}
            />
            {label}
        </Styled.Label>
    )
}
