import { useState, useCallback } from 'react'
import { useFormContext, useController } from 'react-hook-form'
import Checkbox from './Checkbox'
import { Row, Column } from 'app/styles/grid'
import { Styled } from './Checkbox.styles'
import Icon from 'app/core/components/Icon/Icon'
import colors from 'app/core/constants/colors'

export default function Checkboxes({ name, onChange }) {
    const { control } = useFormContext()
    const { field } = useController({ control, name })
    const [value, setValue] = useState<'Yes' | 'No' | null>(field.value || null)

    const change = useCallback(
        (val) => {
            // https://codesandbox.io/s/usecontroller-checkboxes-99ld4?file=/src/App.js:1048-1055
            field.onChange(val)
            setValue(val)

            if (onChange) {
                onChange(val)
            }
        },
        [field, onChange]
    )

    return (
        <Styled.Group alignX="center">
            <Column sm={25}>
                <Styled.Wrapper>
                    <Styled.Checkbox
                        name={name}
                        value="Yes"
                        type="radio"
                        onChange={() => change('Yes')}
                        checked={value === 'Yes'}
                    />
                    <Icon
                        name="checkCircle"
                        style={{
                            color: value === 'Yes' ? colors.green : `black`,
                        }}
                    />
                    <Styled.Label>Yes</Styled.Label>
                </Styled.Wrapper>
            </Column>
            <Column sm={25}>
                <Styled.Wrapper>
                    <Styled.Checkbox
                        name={name}
                        value="No"
                        type="radio"
                        onChange={() => change('No')}
                        checked={value === 'No'}
                    />
                    <Icon
                        name="exCircle"
                        style={{
                            color: value === 'No' ? colors.red : `black`,
                        }}
                    />
                    <Styled.Label>No</Styled.Label>
                </Styled.Wrapper>
            </Column>
        </Styled.Group>
    )
}
