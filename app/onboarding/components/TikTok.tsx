import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import { useMutation } from 'blitz'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import hasValue from 'app/core/utils/hasValue'
import { useState } from 'react'
import Checkboxes from './Checkbox/Checkboxes'
import Conditional from './Conditional'
import Select from 'app/core/components/Form/Select/Select'
import Password from 'app/core/components/Form/Password/Password'

export default function TikTok({ campaign, callback }) {
    const [hasAccount, setHasAccount] = useState(null)
    const [updateCampaignMutation] = useMutation(updateCampaign)

    return (
        <Form
            submitText="NEXT"
            onSubmit={async ({ notes, logins }) => {
                console.log(logins)
                const notesMap = Object.entries(notes)?.map(([key, val]) => {
                    return {
                        label: key,
                        value: val,
                    }
                })
                const nonEmptyNotes = notesMap?.filter(({ value }) =>
                    hasValue(value)
                )
                const currentNotes =
                    campaign?.notes && Array.isArray(campaign?.notes)
                        ? campaign?.notes
                        : []
                const loginValues = logins
                    ? [
                          {
                              website: `TikTok Login`,
                              ...logins,
                          },
                      ]
                    : []
                const currentLogins =
                    campaign?.logins && Array.isArray(campaign?.logins)
                        ? campaign?.logins
                        : []
                try {
                    const updated = await updateCampaignMutation({
                        id: campaign.id,
                        notes: [...currentNotes, ...nonEmptyNotes],
                        logins: [...currentLogins, ...loginValues],
                    })
                    if (updated) {
                        callback(updated)
                    }
                } catch (error: any) {
                    console.error(error)
                    return {
                        [FORM_ERROR]: error.toString(),
                    }
                }
            }}
            /**initialValues={contactInfo}**/
            buttonProps={{
                centered: true,
                disabled: !hasAccount,
            }}
            successMessage={null}
        >
            <h3 style={{ textAlign: `center` }}>
                Do you have a Tik Tok account?
            </h3>
            <Checkboxes
                name="Do you have a TikTok account?"
                onChange={(e) => setHasAccount(e)}
            />
            <Conditional active={hasAccount}>
                <Conditional active={hasAccount === 'Yes'} speed={0.5}>
                    <Row>
                        <Column sm={50}>
                            <Input
                                name="logins[username]"
                                required
                                label="TikTok Username"
                            />
                        </Column>
                        <Column sm={50}>
                            <Password
                                name="logins[password]"
                                required
                                label="TikTok Password"
                            />
                        </Column>
                    </Row>
                </Conditional>

                <Row>
                    <Column sm={50}>
                        <Select
                            label="Are you advertising on TikTok now?"
                            options={['Yes', 'No']}
                            name="notes[Are you advertising on TikTok now?]"
                        />
                    </Column>
                    <Column sm={50}>
                        <Select
                            label="Do you have an organic TikTok business page?"
                            options={['Yes', 'No']}
                            name="notes[Do you have an organic TikTok business page?]"
                        />
                    </Column>
                </Row>
                <Input
                    element="textarea"
                    rows={5}
                    label="Who is your perfect customer?"
                    name="notes[Who is your perfect customer?]"
                />
                <Input
                    element="textarea"
                    rows={5}
                    label="How would you like your customer to feel, think, and interact in relation to your company?"
                    name="notes[How would you like your customer to feel/think/interact in relation to your company?]"
                />
                <Input
                    element="textarea"
                    rows={5}
                    label="Who is the Point of Contact that our team will be working with? (ex. Mary Smith - CMO)"
                    name="notes[Who is the Point of Contact that our team will be working with?]"
                />
                <Input
                    label="What is the email address for the Point of Contact? (ex. mary@smith.com)"
                    name="notes[What is the email address for the Point of Contact?]"
                    type="email"
                />
                <Input
                    element="textarea"
                    rows={5}
                    label="If you could give hashtags that relate with your business and brand what would they be?"
                    name="notes[If you could give hashtags that relate with your business and brand what would they be?]"
                />
                <Input
                    element="textarea"
                    rows={5}
                    label="Share with us your brand statement; One to two sentences that summarize what you do, how you do it, and what makes you unique."
                    name="notes[Brand statement]"
                />
                <Row>
                    <Column sm={50}>
                        <Input
                            label="What is your Facebook handle?"
                            name="notes[What is your Facebook handle?]"
                        />
                    </Column>
                    <Column sm={50}>
                        <Input
                            label="What is your Instagram handle?"
                            name="notes[What is your Instagram handle?]"
                        />
                    </Column>
                </Row>

                <Select
                    label="Do you utilize a Post-Purchase Survey on your website?"
                    options={['Yes', 'No']}
                    name="notes[Do you utilize a Post-Purchase Survey on your website?]"
                />
                <Input
                    element="textarea"
                    rows={5}
                    label="What is your primary objective within Tik Tok Ads?"
                    name="notes[What is your primary objective within Tik Tok Ads?]"
                />
            </Conditional>
        </Form>
    )
}
