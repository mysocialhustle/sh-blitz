import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import { useMutation } from 'blitz'
import updateCampaign from 'app/admin/campaigns/mutations/updateCampaign'
import hasValue from 'app/core/utils/hasValue'
import { useState } from 'react'
import Checkboxes from './Checkbox/Checkboxes'
import Conditional from './Conditional'
import Select from 'app/core/components/Form/Select/Select'
import Password from 'app/core/components/Form/Password/Password'
import updateWebsite from 'app/admin/websites/mutations/updateWebsite'

export default function WebDevelopment({ website, callback }) {
    const [hasWebsite, setHasWebsite] = useState(null)
    const [updateWebsiteMutation] = useMutation(updateWebsite)

    return (
        <Form
            submitText="NEXT"
            onSubmit={async ({ notes, logins }) => {
                /**const notesMap = Object.entries(notes)?.map(([key, val]) => {
                    return {
                        label: key,
                        value: val,
                    }
                })
                const nonEmptyNotes = notesMap?.filter(({ value }) =>
                    hasValue(value)
                )
                const currentNotes =
                    campaign?.notes && Array.isArray(campaign?.notes)
                        ? campaign?.notes
                        : []
                const loginValues = logins
                    ? [
                          {
                              website: `Snapchat`,
                              ...logins,
                          },
                      ]
                    : []
                const currentLogins =
                    campaign?.logins && Array.isArray(campaign?.logins)
                        ? campaign?.logins
                        : []
                try {
                    const updated = await updateCampaignMutation({
                        id: campaign.id,
                        notes: [...currentNotes, ...nonEmptyNotes],
                        logins: [...currentLogins, ...loginValues],
                    })
                    if (updated) {
                        callback(updated)
                    }
                } catch (error: any) {
                    console.error(error)
                    return {
                        [FORM_ERROR]: error.toString(),
                    }
                }**/
            }}
            /**initialValues={contactInfo}**/
            buttonProps={{
                centered: true,
                disabled: !hasWebsite,
            }}
            successMessage={null}
        >
            <h3 style={{ textAlign: `center` }}>Do you have a website?</h3>
            <Checkboxes
                name="Do you have a website?"
                onChange={(e) => setHasWebsite(e)}
            />
            <Conditional active={hasWebsite === 'Yes'} speed={0.5}>
                <Input
                    label="What is the URL for your website? (Including https://)"
                    name="url"
                    required
                    type="url"
                />
                <Input
                    label="Where is your domain registered?"
                    name="notes[Domain Registrar]"
                />
                <Row>
                    <Column sm={50}>
                        <Input
                            name="logins[username]"
                            required
                            label="Snapchat Username"
                        />
                    </Column>
                    <Column sm={50}>
                        <Password
                            name="logins[password]"
                            required
                            label="Snapchat Password"
                        />
                    </Column>
                </Row>
                <Input
                    element="textarea"
                    rows={5}
                    label="What is your primary objective within Snapchat Ads?"
                    name="notes[What is your primary objective within Snapchat Ads?]"
                />
            </Conditional>
        </Form>
    )
}
