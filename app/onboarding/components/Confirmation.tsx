import HubspotMeeting from 'integrations/Hubspot/HubspotMeeting'
import { Row, Column } from 'app/styles/grid'
import Button from 'app/core/components/Button/Button'
import colors from 'app/core/constants/colors'
import { Routes } from 'blitz'

export default function Confirmation({ modelType }) {
    const hubspotMeetings = {
        campaign: `https://meetings.hubspot.com/ashlie-green`,
        website: `https://meetings.hubspot.com/christian213`,
        content: `https://meetings.hubspot.com/christian213`,
    }

    return (
        <Row alignX="center">
            <Column sm={75} style={{ textAlign: 'center' }}>
                <h2>Done!</h2>
                <p>
                    {`Thanks so much for answering all of our questions!
                            Now that we have all the information we need, let's
                            schedule your kickoff call.`}
                </p>
                <HubspotMeeting url={hubspotMeetings[modelType]} />
                {/**<h4 style={{ marginTop: 30 }}>
                    {`Thanks for scheduling your call, our team can't wait
                            to meet with you!`}
                </h4>
                <Button
                    href={Routes.AccountPage()}
                    backgroundColor={colors.red}
                    color="#fff"
                    hoverBackgroundColor="#000"
                    hoverColor="#fff"
                    centered
                >
                    Click Here to Access Your Account
    </Button>**/}
            </Column>
        </Row>
    )
}
