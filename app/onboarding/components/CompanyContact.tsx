import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import { ContactInfo } from '../validation'
import { useMutation } from 'blitz'
import updateCompany from 'app/admin/companies/mutations/updateCompany'

interface Contact {
    Email: string
    Address: string
    Phone: string
}

export default function CompanyContact({ company, callback }) {
    const [updateCompanyMutation] = useMutation(updateCompany)

    let contactInfo: Contact = {
        Email: company.contactInfo?.find(({ label }) => label === 'Email')
            ?.value,
        Phone: company.contactInfo?.find(({ label }) => label === 'Phone')
            ?.value,
        Address: company.contactInfo?.find(({ label }) => label === 'Address')
            ?.value,
    }

    return (
        <Row alignX="center">
            <Column md={75}>
                <h3
                    style={{ textAlign: `center` }}
                >{`Let's Get Started With ${company.name}'s Contact Information:`}</h3>
                <Form
                    submitText="NEXT"
                    onSubmit={async (values) => {
                        const formatted = Object.entries(values).map(
                            ([key, val]) => {
                                return {
                                    label: key,
                                    value: val,
                                }
                            }
                        )
                        try {
                            const updated = await updateCompanyMutation({
                                id: company.id,
                                contactInfo: formatted,
                            })
                            if (updated) {
                                callback(updated)
                            }
                        } catch (error: any) {
                            console.error(error)
                            return {
                                [FORM_ERROR]: error.toString(),
                            }
                        }
                    }}
                    initialValues={contactInfo}
                    buttonProps={{
                        centered: true,
                    }}
                    successMessage={null}
                    schema={ContactInfo}
                >
                    <Row>
                        <Column sm={50}>
                            <Input
                                name="Email"
                                label="Email Address"
                                type="email"
                                required
                            />
                        </Column>
                        <Column sm={50}>
                            <Input
                                name="Phone"
                                label="Phone Number"
                                type="tel"
                                required
                            />
                        </Column>
                    </Row>
                    <Input
                        element="textarea"
                        name="Address"
                        label="Address"
                        rows={6}
                        required
                    />
                </Form>
            </Column>
        </Row>
    )
}
