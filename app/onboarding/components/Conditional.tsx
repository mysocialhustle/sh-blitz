import { motion, AnimatePresence } from 'framer-motion'
import { useId } from 'react'

export default function Conditional({ active, children, speed = 0.75 }) {
    const id = useId()
    return (
        <AnimatePresence exitBeforeEnter>
            {active && (
                <motion.div
                    layout
                    key={id}
                    initial={{
                        opacity: 0,
                        y: `50%`,
                    }}
                    animate={{
                        opacity: 1,
                        y: 0,
                    }}
                    exit={{
                        opacity: 0,
                        y: `50%`,
                    }}
                    transition={{
                        ease: 'easeInOut',
                        duration: speed,
                    }}
                >
                    {children}
                </motion.div>
            )}
        </AnimatePresence>
    )
}
