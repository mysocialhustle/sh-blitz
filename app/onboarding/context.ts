import { createContext } from 'react'

interface Context {
    page: number
    setPage: React.Dispatch<React.SetStateAction<number>>
}

export const OnboardingContext = createContext<Context>({
    page: 0,
    setPage: () => {},
})
