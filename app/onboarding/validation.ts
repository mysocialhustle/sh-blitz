import { z } from 'zod'

export const email = z
    .string()
    .email()
    .transform((str) => str.toLowerCase().trim())

export const ContactInfo = z.object({
    Email: email,
    Phone: z.string(),
    Address: z.string(),
})
