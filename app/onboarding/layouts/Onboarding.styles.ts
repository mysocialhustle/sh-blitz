import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Wrapper = styled.div`
        min-height: 100vh;
        display: grid;
        grid-template-rows: max-content minmax(0, 1fr);
    `
    export const Header = styled.aside`
        padding: 15px 30px;
        position: relative;
        border-bottom: 4px solid ${colors.lightGrey};
        background-color: white;
        position: relative;
        z-index: 10;
    `
    export const Form = styled.div`
        align-self: center;
        padding-top: 75px;
        padding-bottom: 75px;
    `
    export const Progress = styled.div`
        transform-origin: left center;
        background-color: ${colors.red};
        position: absolute;
        bottom: -4px;
        left: 0;
        height: 4px;
        width: 100%;
        transition: transform 0.65s cubic-bezier(0.8, 0, 0.2, 1);
    `
    export const Nav = styled.div`
        position: absolute;
        top: 0;
        height: 100%;
        left: 0;
        display: flex;
        flex-flow: row wrap;
        justify-content: space-between;
        align-items: center;
        padding: 15px 30px;
        width: 100%;
    `
    export const NavButton = styled.button`
        -webkit-appearance: none;
        background: none;
        border: none;
        display: block;
        opacity: ${(props) => (props.disabled ? '0.15' : '1')};
        line-height: 1;
        padding: 10px;
        border: 1px solid currentColor;

        svg {
            fill: currentColor;
            height: 1em;
            width: 1em;
        }
    `
}
