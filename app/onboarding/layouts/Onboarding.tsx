import { Suspense, useState, useMemo, useContext } from 'react'
import { Head, BlitzLayout } from 'blitz'
import { Styled } from './Onboarding.styles'
import { ThemeType } from 'types'
import LogoDark from 'app/svg/logo-dark.inline.svg'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { Container } from 'app/styles/grid'
import { OnboardingContext } from '../context'

const ContextWrapper = ({ children }) => {
    const [page, setPage] = useState(0)

    const value = useMemo(
        () => ({
            page,
            setPage,
        }),
        [page]
    )

    return (
        <Styled.Wrapper>
            <OnboardingContext.Provider value={value}>
                {children}
            </OnboardingContext.Provider>
        </Styled.Wrapper>
    )
}

const Wrapper = ({ children, maxPages }) => {
    const { page } = useContext(OnboardingContext)

    return (
        <Styled.Wrapper>
            <Styled.Header>
                <LogoDark
                    style={{
                        width: 200,
                        marginLeft: 'auto',
                        marginRight: 'auto',
                        display: 'block',
                    }}
                />
                <Styled.Progress
                    style={{ transform: `scaleX(${page / maxPages})` }}
                />
            </Styled.Header>
            <Styled.Form>
                <Container>{children}</Container>
            </Styled.Form>
        </Styled.Wrapper>
    )
}

const OnboardingLayout: BlitzLayout<{
    title?: string
    children?: React.ReactNode
    maxPages: number
}> = ({ title, children, maxPages }) => {
    return (
        <>
            <Head>
                <title>{title || 'social-hustle-blitz'}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Suspense fallback={<Skeleton />}>
                <ContextWrapper>
                    <Wrapper maxPages={maxPages}>{children}</Wrapper>
                </ContextWrapper>
            </Suspense>
        </>
    )
}

export default OnboardingLayout
