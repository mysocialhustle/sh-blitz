import { WebsiteType } from 'db'

export const websiteTypes: Record<WebsiteType, string> = {
    wordpress: 'Wordpress',
    ecommerce: 'E-Commerce',
    custom: 'Custom',
}
