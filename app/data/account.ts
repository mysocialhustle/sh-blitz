import { UserRole } from 'db'
import { UserPrivileges, UserPrivilegeLabel } from 'types'

interface NavItem {
    label: string
    path: string
    icon: string
    privileges?: UserPrivilegeLabel
}

export const userPrivileges: UserPrivileges = {
    editAdmins: ['ADMIN'],
    deleteUsers: ['ADMIN'],
    editUsers: ['ADMIN', 'TEAMMEMBER'],
    editCompanies: ['ADMIN', 'TEAMMEMBER'],
    editCampaigns: ['ADMIN', 'TEAMMEMBER'],
    editWebsites: ['ADMIN', 'TEAMMEMBER'],
    editContents: ['ADMIN', 'TEAMMEMBER'],
}

export const accountNav: NavItem[] = [
    {
        label: 'Dashboard',
        path: '',
        icon: 'home',
    },
    {
        label: 'Account Details',
        path: '/settings',
        icon: 'tools',
    },
    {
        label: 'Users',
        path: '/users',
        icon: 'user',
        privileges: 'editUsers',
    },
    {
        label: 'Sales Order',
        path: '/sales-order-form',
        icon: 'invoice',
        privileges: 'editCompanies',
    },
    {
        label: 'Companies',
        path: '/companies',
        icon: 'company',
    },
    {
        label: 'Campaigns',
        path: '/campaigns',
        icon: 'chart',
    },
    {
        label: 'Websites',
        path: '/websites',
        icon: 'website',
    },
    {
        label: 'Assets',
        path: '/content',
        icon: 'images',
    },
    {
        label: 'Book a Meeting',
        path: '/book-a-meeting',
        icon: 'calendar',
    },
]
