import { ServiceItem } from 'types'

const GraphicDesign: ServiceItem = {
    title: 'Graphic Design',
    slug: 'graphic-design',
    category: 'Creative',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/graphicdesign.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/graphicdesign.png',
    },
    hero: {
        type: 'image',
        theme: 'dark',
    },
}

export default GraphicDesign
