import { ServiceItem } from 'types'

const SEO: ServiceItem = {
    title: 'Enterprise SEO',
    slug: 'search-engine-optimization',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/SEO-mockup.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/SEO-Mockup.png',
    },
    hero: {
        type: 'image',
        imgHeight: 1320,
    },
    counters: {
        counters: [
            {
                digit: 100,
                textAfter: 'M+',
                text: 'ORGANIC SESSIONS GENERATED',
            },
            {
                digit: 100,
                textAfter: 'M+',
                text: 'KEYWORDS RANKED ON GOOGLE',
            },
            {
                digit: 388,
                textAfter: 'K+',
                text: 'PHONE CALLS PRODUCED',
            },
        ],
    },
    icons: {
        headline: ``,
        icons: [
            {
                title: 'IN DEPTH KEYWORD RESEARCH',
                text: 'Our team puts their proprietary methodology to use and puts together the keywords that are most likely to be searched.',
                icon: 'keyword',
            },
            {
                title: 'ON-SITE GAP ANALYSIS',
                text: 'We look at the overall site structure to evaluate, among others, page response codes, the sitemap, page speed, metadata and HTML source code of the pages',
                icon: 'remarketing',
            },
            {
                title: 'USER EXPERIENCE GAP ANALYSIS',
                text: 'Our SEO experts will audit the content on your website to ensure it provides value and is optimized to fulfill the intent of the searcher.',
                icon: 'listRetargeting',
            },
            {
                title: 'WEB PRESENCE GAP ANALYSIS',
                text: `Our experts will perform a holistic overview of your client's entire website to ensure that all necessary performance components are in excellent health.`,
                icon: 'mail',
            },
            {
                title: 'BRAND SIGNAL ANALYSIS',
                text: `Our team of SEO experts take a deep dive into your brand's sentiment and online reputation.`,
                icon: 'social',
            },
            {
                title: 'BENCHMARK REPORT',
                text: 'As the SEO team works on your campaign, this benchmark becomes the measuring stick for progress and is included in a monthly report.',
                icon: 'newsletter',
            },
        ],
    },
}

export default SEO
