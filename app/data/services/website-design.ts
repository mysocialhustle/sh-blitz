import { ServiceItem } from 'types'

const WebsiteDesign: ServiceItem = {
    title: 'Website Design',
    slug: 'website-design',
    category: 'Creative',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/webdesign-hero2.jpeg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/web-design.png',
    },
    hero: {
        type: 'image',
        theme: 'dark',
        imgHeight: 1500,
    },
}

export default WebsiteDesign
