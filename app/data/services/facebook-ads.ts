import { ServiceItem } from 'types'

const FacebookAds: ServiceItem = {
    title: 'Facebook Ads',
    slug: 'facebook-ads',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/Facebook-Mobile-Ad.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/Facebook-Mobile-Ad.png',
    },
    hero: {
        type: 'image',
        theme: 'light',
        imgHeight: 1320,
    },
    counters: {
        counters: [
            {
                digit: 2.8,
                round: 1,
                textAfter: ' Billion',
                text: 'ACTIVE MONTHLY USERS',
            },
            {
                start: 100,
                digit: 200,
                textAfter: ' Million',
                text: 'BUSINESS PAGES',
            },
            {
                digit: 19,
                textAfter: ' Hours',
                text: 'PER MONTH SPENT ON APP',
            },
        ],
    },
    icons: {
        headline: `Facebook has a massive amount of user data, which we can use to effectively target those using their platform with relevant ads.`,
        icons: [
            {
                title: 'DEMOGRAPHIC',
                text: 'Reach your target audience based on information like their age, gender, language, and HHI.',
                icon: 'demographics',
            },
            {
                title: 'BEHAVIORS & INTERESTS',
                text: 'Reach your target audience based on their online behaviors and interests',
                icon: 'conversion',
            },
            {
                title: 'CUSTOM AUDIENCES',
                text: 'Target users familiar with your brand by uploading CRM files or retargeting engagement, activity, and site traffic.',
                icon: 'communication',
            },
            {
                title: 'LOOKALIKE AUDIENCES',
                text: 'Leverage lookalike audiences to reach new people similar to your existing customer',
                icon: 'eye',
            },
            {
                title: 'AD SPECS',
                text: 'Facebook can run single image ads, video ads, carousels, collection ads, and event ads',
                icon: 'advertise',
            },
            {
                title: 'Trust Signals',
                text: 'A popular Facebook profile with consistent content shows trust and credibility to researchers',
                icon: 'social',
            },
        ],
    },
    portfolio: {
        title: `Our Facebook Advertising Success Stories`,
    },
}

export default FacebookAds
