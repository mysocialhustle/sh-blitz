import { ServiceItem } from 'types'

const GoogleAds: ServiceItem = {
    title: 'Google Ads',
    slug: 'ppc-management',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/GLD-Google-Ads.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/GLD-Google-Ads-transparent.png',
    },
    hero: {
        type: 'image',
        theme: 'light',
        imgHeight: 1320,
    },
    counters: {
        counters: [
            {
                digit: 10,
                textBefore: '$',
                textAfter: ' Million+',
                text: 'AD SPEND MANAGED',
            },
            {
                digit: 100,
                textBefore: '$',
                textAfter: ' Million+',
                text: 'REVENUE GENERATED',
            },
            {
                digit: 2,
                textAfter: ' Billion+',
                text: 'IMPRESSIONS CREATED',
            },
        ],
    },
    icons: {
        theme: 'light',
        headline: `Increase Your <span>Revenue</span> & <span>ROI</span> Through <span>Proven Digital Marketing Methodologies</span>`,
        icons: [
            {
                title: 'LOCAL SERVICE ADS',
                text: 'Only pay for leads when utilizing a local service ads campaign.',
                icon: 'localService',
            },
            {
                title: 'CUSTOMER LIST RETARGETING',
                text: 'Retarget users that were previous customers of your business.',
                icon: 'listRetargeting',
            },
            {
                title: 'CONVERSION RATE OPTIMIZATION',
                text: 'We analyze & optimize the full conversion funnel from first click to final conversion.',
                icon: 'cro',
            },
            {
                title: 'GOOGLE MAP ADS',
                text: 'We link your GMB to your ads, allowing you to run ads in the local three-pack as well as the top of Google.',
                icon: 'googleMaps',
            },
            {
                title: 'REMARKETING',
                text: 'Remarket to users that have visited your site & are searching on Google and Bing.',
                icon: 'remarketing',
            },
        ],
    },
}

export default GoogleAds
