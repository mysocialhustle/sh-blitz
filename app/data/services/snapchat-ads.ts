import { ServiceItem } from 'types'

const SnapchatAds: ServiceItem = {
    title: 'Snapchat Ads',
    slug: 'snapchat-ads',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/Snapchat-ads.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/Snapchat-Ads.png',
    },
    hero: {
        type: 'svg',
        theme: 'dark',
        inlineImg: true,
    },
    counters: {
        theme: 'darkGrey',
        counters: [
            {
                start: 300,
                digit: 433,
                textAfter: ' Million',
                text: 'ACTIVE MONTHLY USERS',
            },
            {
                digit: 78,
                textAfter: '%',
                text: 'OF USERS UNDER AGE 35',
            },
            {
                digit: 63,
                textAfter: '%',
                text: 'OF USERS ARE ACTIVE DAILY',
            },
        ],
    },
    icons: {
        theme: 'light',
        headline: `Snapchat has an extremely young audience that is there to consume content that is short and concise.`,
        icons: [
            {
                title: 'DEMOGRAPHIC',
                text: 'Reach your target audience based on information like their age, gender, language, and HHI.',
                icon: 'demographics',
            },
            {
                title: 'BEHAVIORS & INTERESTS',
                text: 'Target users based on their interests, behaviors, and previous locations visited when applicable.',
                icon: 'conversion',
            },
            {
                title: 'PLACEMENT TARGETING',
                text: 'Run your ads within content of a specific category to reach users while their watching content relevant to your business.',
                icon: 'remarketing',
            },
            {
                title: 'CUSTOM AUDIENCES',
                text: 'Target users familiar with your brand by uploading CRM files or retargeting engagement, activity, and site traffic.',
                icon: 'communication',
            },
            {
                title: 'LOOKALIKE AUDIENCES',
                text: 'Leverage lookalike audiences to reach new people similar to your existing customer.',
                icon: 'eye',
            },
            {
                title: 'AD SPECS',
                text: 'Snapchat can run single image ads, video ads, collection ads, commercials, AR lenses, filters, and stories ads.',
                icon: 'advertise',
            },
        ],
    },
    portfolio: {
        title: 'Our Snapchat Advertising Success Stories',
        theme: 'grey',
    },
}

export default SnapchatAds
