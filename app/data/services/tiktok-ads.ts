import { ServiceItem } from 'types'

const TikTokAds: ServiceItem = {
    title: 'TikTok Ads',
    slug: 'tiktok-ads',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/Tik-Tok-Ads-SH.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/TikTok-Ad.png',
    },
    hero: {
        type: 'svg',
        theme: 'dark',
        inlineImg: true,
    },
    counters: {
        theme: 'light',
        counters: [
            {
                digit: 1,
                textAfter: '+ Billion',
                text: 'ACTIVE USERS',
                round: 1,
            },
            {
                digit: 100,
                textAfter: '%',
                text: 'DOWNLOADS UP YOY',
            },
            {
                digit: 5,
                textAfter: '+ Million',
                text: 'AD SPEND MANAGED',
            },
        ],
    },
    icons: {
        theme: 'dark',
        headline: `Invest in <span>trends</span>. TikTok is <span>what all the cool kids are doing.</span>`,
        icons: [
            {
                title: 'IDENTIFY GOALS',
                text: 'Discovering our clients goals is the foundation we build our strategy on.',
                icon: 'newsletter',
            },
            {
                title: 'COLLABORATE',
                text: 'We need involvement on both sides of the phone to create a strategy with the best chance of success.',
                icon: 'communication',
            },
            {
                title: 'STRUCTURE CONTENT',
                text: 'TikTok allows for a completely creative, sound on, advertising experience that needs a steady flow of fresh content.',
                icon: 'blog',
            },
            {
                title: 'ITERATIVE TESTING',
                text: 'Based on your goals we will drive tests that will supply us with ample data and we can start testing.',
                icon: 'research',
            },
            {
                title: 'REPORTING',
                text: 'Our team provides weekly, monthly and quarterly reports and offers a communication channel that is open at all times.',
                icon: 'listRetargeting',
            },
            {
                title: 'BIG WINS',
                text: 'Steps #1 - Steps #5 ensure Step #6 - BIG WINS.',
                icon: 'cro',
            },
        ],
    },
    testimonials: {
        theme: 'light',
    },
    portfolio: {
        theme: 'dark',
        title: 'Our TikTok Advertising Success Stories',
    },
}

export default TikTokAds
