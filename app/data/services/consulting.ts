import { ServiceItem } from 'types'

const Consultation: ServiceItem = {
    title: 'Marketing Consultation',
    slug: 'consulting',
    category: 'Marketing',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/SH-Consulting.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/SH-Consulting.png',
    },
    hero: {
        type: 'image',
        imgHeight: 1320,
        theme: 'dark',
    },
    counters: {
        counters: [
            {
                digit: 35,
                textBefore: '$',
                textAfter: ' Million+',
                text: 'IN PRODUCT SOLD',
            },
            {
                digit: 25,
                textAfter: '+',
                text: 'INDUSTRIES WORKED IN',
            },
            {
                digit: 20,
                textAfter: '+',
                text: 'YEARS OF EXPERIENCE',
            },
        ],
    },
    icons: {
        theme: 'light',
        headline: `Increase Your <span>Revenue &amp; ROI</span> Through Thoughtful <span>Planning &amp; Strategies</span>`,
        icons: [
            {
                title: 'SITUATIONAL/OPERATIONAL ANALYSIS',
                text: 'A complete understanding of how things are currently operating and performing',
                icon: 'newsletter',
            },
            {
                title: 'ONLINE PRESENCE EVALUATION/PR',
                text: 'Identifying your presence in the market, online real estate, and recognition',
                icon: 'ecommerce',
            },
            {
                title: 'TOOLS & SOFTWARE',
                text: 'Evaluating tools & software for efficiency and capability',
                icon: 'creativity',
            },
            {
                title: 'ORGANIZATIONAL STRUCTURE',
                text: 'Understanding the current structure of internal teams, external teams, partners, and agencies',
                icon: 'communication',
            },
            {
                title: 'IDENTIFY KPIS',
                text: 'Benchmarking the most important metrics and setting goals and milestones',
                icon: 'cro',
            },
            {
                title: 'BUDGETS',
                text: 'A holistic look at budgteing across the entire organization',
                icon: 'budget',
            },
        ],
    },
}

export default Consultation
