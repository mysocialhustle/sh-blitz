import { ServiceItem } from 'types'

const ContentCreation: ServiceItem = {
    title: 'Content Creation',
    slug: 'content-creation',
    category: 'Creative',
    images: {
        featured:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/content-creation.jpg',
        transparent:
            'https://socialhustle-blitz-static.s3.amazonaws.com/services/content-creation.png',
    },
    hero: {
        type: 'image',
        theme: 'dark',
    },
}

export default ContentCreation
