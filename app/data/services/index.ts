import { ServiceItem } from 'types'
import GoogleAds from './google-ads'
import WebsiteDesign from './website-design'
import ContentCreation from './content-creation'
import FacebookAds from './facebook-ads'
import TikTokAds from './tiktok-ads'
import SEO from './search-engine-optimization'
import Consultation from './consulting'
import SnapchatAds from './snapchat-ads'
import GraphicDesign from './graphic-design'

const services: ServiceItem[] = [
    GoogleAds,
    WebsiteDesign,
    ContentCreation,
    FacebookAds,
    GraphicDesign,
    TikTokAds,
    SEO,
    Consultation,
    SnapchatAds,
]

export default services
