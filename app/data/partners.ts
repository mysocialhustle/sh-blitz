interface Partner {
    name: string
    link: string
    imageSrc: string
    tagline: string
}

const partners: Partner[] = [
    {
        name: 'HUBSPOT',
        link: 'https://www.hubspot.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/2560px-HubSpot_Logo.svg.png',
        tagline: 'Customer Relation Manager Solutions',
    },
    {
        name: 'GOOGLE SKILLSHOP',
        link: 'https://skillshop.withgoogle.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/skillshop.png',
        tagline: 'Google Advertising Training',
    },
    {
        name: 'WP ENGINE',
        link: 'https://wpengine.com/platform/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/wpengine-logo.png',
        tagline: 'Cutting-Edge Hosting Solutions',
    },
    {
        name: 'SPROUT SOCIAL',
        link: 'https://sproutsocial.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/sprout-social-logo-1.png',
        tagline: 'Social Media Marketing Automation',
    },
    {
        name: 'SHOPIFY',
        link: 'https://www.shopify.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Shopify-Logo.wine_.png',
        tagline: 'E-commerce Website Development',
    },
    {
        name: 'GODADDY',
        link: 'https://godaddy.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/1280px-GoDaddy_logo.svg.png',
        tagline: 'Website Domain Curation',
    },
    {
        name: 'SKG TEXAS',
        link: 'https://skgtexas.com/services',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/skg-logo-red-2.png',
        tagline: 'Office Space Design',
    },
    {
        name: 'THE REDWOOD AGENCY',
        link: 'https://redwoodagencygroup.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Redwood-Small.png',
        tagline: 'Business Insurance',
    },
    {
        name: 'KEITHCITY',
        link: 'https://www.keithcity.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Keithcity_logo_.png',
        tagline: 'Creative Design Partner',
    },
    {
        name: 'WASATCH PREMIER HOLDINGS',
        link: 'https://fundwph.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Wasatch.png',
        tagline: 'Small Business Capital Funding',
    },
    {
        name: 'FEEDONOMICS',
        link: 'https://feedonomics.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/feedologo.png',
        tagline: 'Product Feed Optimization',
    },
    {
        name: 'DATABOX',
        link: 'https://databox.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/databox.png',
        tagline: 'Custom Business Analytics',
    },
    {
        name: 'GATSBY',
        link: 'https://www.gatsbyjs.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Gatsby_Logo.png',
        tagline: 'Blazingly Fast Static Site Generation',
    },
    {
        name: 'AMAZON WEB SERVICES',
        link: 'https://aws.amazon.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/aws.png',
        tagline: 'Scalable Cloud Computing',
    },
    {
        name: 'ATLASSIAN',
        link: 'https://atlassian.net/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/Atlassian-logo.png',
        tagline: 'Agile Application Development & Deployment',
    },
    {
        name: 'MONDAY.COM',
        link: 'https://monday.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/monday-logo.png',
        tagline: 'Project Management',
    },
    {
        name: 'JOHNSON VISUALS',
        link: 'https://johnsonvisuals.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/johnson-visuals-logo.png',
        tagline: 'Photography & Videography',
    },
    {
        name: 'VISCAP MEDIA',
        link: 'https://viscapmedia.com/',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/partners/viscap-logo.png',
        tagline: 'Conversion-Focused Content',
    },
]

export default partners
