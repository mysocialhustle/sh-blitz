interface ClientLogo {
    name: string
    type: 'image' | 'svg'
    main: string
    hover: string
}

const featuredLogos: ClientLogo[] = [
    {
        name: 'SKG',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/skg.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/skg-red.png',
    },
    {
        name: 'Ball Ventures',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/ball-ventures.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/ball-ventures-red.png',
    },
    {
        name: '6Connex',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/6connex.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/6connex-red.png',
    },
    {
        name: 'Keller Williams',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/keller-williams.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/keller-williams-red.png',
    },
    {
        name: 'GLD',
        type: 'svg',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/gld.svg',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/gld-red.svg',
    },
    {
        name: 'Taylor Gang',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/taylor-gang.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/taylor-gang-red.png',
    },
    {
        name: 'Solar Energy Partners',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/sep.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/sep-red.png',
    },
    {
        name: 'Aurate',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/aurate.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/aurate-red.png',
    },
    {
        name: 'Switchback Motorspots',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/switchback.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/switchback-red.png',
    },
    {
        name: 'Super T Transportation',
        type: 'image',
        main: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/super-t.png',
        hover: 'https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/super-t-red.png',
    },
]

export default featuredLogos
