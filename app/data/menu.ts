import { Menu } from 'types'
import Facebook from 'app/svg/facebook.inline.svg'
import PartnerNetwork from 'app/svg/partner-network.inline.svg'
import BusinessAnalysis from 'app/svg/business-analysis.inline.svg'
import Seo from 'app/svg/enterprise-seo.inline.svg'
import LocalSeo from 'app/svg/local-seo.inline.svg'
import Snapchat from 'app/svg/snapchat.inline.svg'
import TikTok from 'app/svg/tiktok.inline.svg'
import Adwords from 'app/svg/google-adwords.inline.svg'
import WebDesign from 'app/svg/web-design.inline.svg'
import WebHosting from 'app/svg/hosting.inline.svg'
import GraphicDesign from 'app/svg/graphic-design.inline.svg'
import Photography from 'app/svg/photography.inline.svg'
import Videography from 'app/svg/videography.inline.svg'
import SeoAudit from 'app/svg/seo-audit.inline.svg'
import Bing from 'app/svg/bing.inline.svg'
import BusinessServices from 'app/svg/business-services.inline.svg'
import Contact from 'app/svg/contact.inline.svg'
import Awards from 'app/svg/awards.inline.svg'
import OurCompany from 'app/svg/our-company.inline.svg'
import Swag from 'app/svg/swag.inline.svg'

const menu: Menu = [
    {
        name: `Marketing`,
        type: `mega`,
        submenu: [
            {
                name: `Paid Media`,
                submenu: [
                    {
                        name: `Google Ads`,
                        link: `/ppc-management/`,
                        icon: Adwords,
                    },
                    {
                        name: `Tik Tok Ads`,
                        link: `/tiktok-ads/`,
                        icon: TikTok,
                    },
                    {
                        name: `Facebook Ads`,
                        link: `/facebook-ads/`,
                        icon: Facebook,
                    },
                    {
                        name: `Bing Ads`,
                        link: `/ppc-management/#bing`,
                        icon: Bing,
                    },
                    {
                        name: `Snapchat Ads`,
                        link: `/snapchat-ads/`,
                        icon: Snapchat,
                    },
                ],
            },
            {
                name: `Search Engine Optimization`,
                submenu: [
                    {
                        name: `Audit`,
                        link: `/seo-audit/`,
                        icon: SeoAudit,
                    },
                    {
                        name: `Enterprise SEO`,
                        link: `/search-engine-optimization/`,
                        icon: Seo,
                    },
                    {
                        name: `Local SEO`,
                        link: `/search-engine-optimization/#local`,
                        icon: LocalSeo,
                    },
                ],
            },
            {
                name: `Marketing Consultation`,
                submenu: [
                    {
                        name: `Business Analysis`,
                        link: `/consulting/`,
                        icon: BusinessAnalysis,
                    },
                    {
                        name: `Partner Network`,
                        link: `/partners/`,
                        icon: PartnerNetwork,
                    },
                ],
            },
        ],
    },
    {
        name: `Creative`,
        type: `mega`,
        submenu: [
            {
                name: `Web`,
                submenu: [
                    {
                        name: `Website Design`,
                        link: `/website-design/`,
                        icon: WebDesign,
                    },
                    {
                        name: `Web Hosting`,
                        link: `/web-hosting/`,
                        icon: WebHosting,
                    },
                ],
            },
            {
                name: `Content`,
                submenu: [
                    {
                        name: `Videography`,
                        link: `/content-creation/`,
                        icon: Videography,
                    },
                    {
                        name: `Photography`,
                        link: `/content-creation/`,
                        icon: Photography,
                    },
                    {
                        name: `Graphic Design`,
                        link: `/graphic-design/`,
                        icon: GraphicDesign,
                    },
                ],
            },
        ],
    },
    {
        name: `Portfolio`,
        type: `external`,
        link: `https://portfolio.mysocialhustle.com`,
        submenu: [],
    },
    {
        name: `About Us`,
        link: null,
        submenu: [
            {
                name: `Our Company`,
                link: `/our-company/`,
                icon: OurCompany,
            },
            {
                name: `Awards & Certifications`,
                link: `/award-certifications/`,
                icon: Awards,
            },
            {
                name: `Contact Us`,
                link: `/get-in-touch/`,
                icon: Contact,
            },
        ],
    },
    {
        name: `Shop`,
        link: null,
        submenu: [
            {
                name: `Swag`,
                link: `/products/collections/swag/`,
                icon: Swag,
            },
            {
                name: `Business Services`,
                link: `/products/collections/business-services/`,
                icon: BusinessServices,
            },
            {
                name: `Web Hosting`,
                link: `/web-hosting/`,
                icon: WebHosting,
            },
        ],
    },
]

export default menu
