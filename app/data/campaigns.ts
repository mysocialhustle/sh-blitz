import { CampaignType } from 'db'

interface Campaign {
    name: string
    icon: {
        filled: string
        outline: string
    }
}

export const campaigns: Record<CampaignType, Campaign> = {
    bingAds: {
        name: 'Bing Ads',
        icon: {
            outline: 'bingOutline',
            filled: 'bing',
        },
    },
    facebookAds: {
        name: 'Facebook Ads',
        icon: {
            outline: 'facebookOutline',
            filled: 'facebook',
        },
    },
    googleAdwords: {
        name: 'Google Adwords',
        icon: {
            filled: 'adwords',
            outline: 'adwordsOutline',
        },
    },
    instagramAds: {
        name: 'Instagram Ads',
        icon: {
            filled: 'instagram',
            outline: 'instagram',
        },
    },
    linkedin: {
        name: 'LinkedIn Ads',
        icon: {
            filled: 'linkedin',
            outline: 'linkedinOutline',
        },
    },
    seo: {
        name: 'SEO',
        icon: {
            filled: 'link',
            outline: 'link',
        },
    },
    seoAudit: {
        name: 'SEO Audit',
        icon: {
            filled: 'seoOutline',
            outline: 'seoOutline',
        },
    },
    snapchatAds: {
        name: 'Snapchat Ads',
        icon: {
            filled: 'snapchat',
            outline: 'snapchatOutline',
        },
    },
    tiktokAds: {
        name: 'TikTok Ads',
        icon: {
            filled: 'tiktok',
            outline: 'tiktokOutline',
        },
    },
    youtubeAds: {
        name: 'YouTube Ads',
        icon: {
            filled: 'youtube',
            outline: 'youtubeOutline',
        },
    },
}
