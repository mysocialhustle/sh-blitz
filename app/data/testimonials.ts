import { TestimonialItem } from 'types'

const testimonials: TestimonialItem[] = [
    {
        title: `Arena Solutions`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/arena-logo.png`,
        },
        categories: [`social-media-marketing`],
        author: `William Coles, Digital Marketing Manager at Arena Solutions`,
        body: `I had the pleasure of working with both Chris Parrett and Dalin Bernard on ABM. They managed our Terminus and LinkedIn accounts and found them to be very thorough in their analysis and recommendations. In addition, they managed our budget exceptionally well. I highly recommend them.`,
    },
    {
        title: `Calc XML`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/calcxml-logo.png`,
        },
        categories: [`consulting`, `search-engine-optimization`],
        author: `Kaylee Sutherin, Social Media Manager at CalcXML`,
        body: `I’ve been so impressed by the knowledge and initiative that Dalin &#x26; Chris provide. I’ve worked closely with them in multiple business initiatives and the value they added to our company was incredible— we saw growth and profit come directly from their efforts. I can’t recommend their team enough.`,
    },
    {
        title: `Dakri Bernard`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/rize.png`,
        },
        categories: [`social-media-marketing`],
        author: `Dakri Bernard, Founder at Dakri Bernard Realty`,
        body: 'Where do I start? Social Hustle has digital marketing on lock down. They`re always bringing new, fresh ideas to the table and encouraging us to try different approaches to meet our goals. They provide a personal touch and it`s very clear that our success is their top priority. I can wholeheartedly recommend them to anyone needing marketing expertise.',
    },
    {
        title: `EZ Detailing`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/ezlogo.png`,
        },
        categories: [`consulting`, `search-engine-optimization`],
        author: `Isahias Galvan, EZ Detailing`,
        body: `Chris and his team are awesome to work with. He designed my car detailing page. Very professional. He listened to everything I wanted. Super impressed with his work. I highly recommend him.`,
    },
    {
        title: `FB (Unknown Author)`,
        categories: [`facebook-ads`],
        body: 'Chris and The Social Hustle team are AMAZING! Honestly, the best I have worked with. They truly are part of YOUR team. They are always on, available, and ready to strategize and optimize your business. No matter what the ask, how big or small, the team graciously accommodates in the most professional manner. Highly Recommend! Personally, I couldn`t imagine our team without them.',
    },
    {
        title: `G10 Academy`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/g10.png`,
        },
        categories: [`search-engine-optimization`, `consulting`],
        author: `David Granson, Owner at G10 Academy`,
        body: `Entire staff is very knowledgeable, accommodating and communication is clear, often and on point. They made all the right recommendations for my business needs and we love our new website!`,
    },
    {
        title: `GLD`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/gld.png`,
        },
        categories: [
            `homepage`,
            `ppc-management`,
            `content-creation`,
            `social-media-marketing`,
            `tiktok-ads`,
            `facebook-ads`,
            `snapchat-ads`,
        ],
        author: `Mark Seremet, COO at The GLD Shop`,
        body: `Social Hustle has been instrumental in transforming our entire business. They worked hard to help us find new audiences and helped coordinate better returns across all our marketing efforts.`,
    },
    {
        title: `Mike The Trainer Taylor`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/mike-taylor.png`,
        },
        categories: [`website-design`],
        author: `Mike The Trainer Taylor`,
        body: `The guys at Social Hustle, (Chris, Dalin, Christian) do an outstanding, professional job of defining my business needs and making a clear and concise path of how to get there by supporting with marketing and superior web development!  They have an eye for consumer pleasing presentations and a finger on the pulse of forward marketing!  I would choose them again and again to grow my business and my wealth.`,
    },
    {
        title: `Miracle Silver`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/miracle-silver.png`,
        },
        categories: [`ppc-management`],
        author: `Antony Frolov, CEO at Miracle Silver`,
        body: `Social Hustle is an amazing marketing company with some of the best staff. They are extremely professional, responsive and go to measures beyond what most marketing companies would ever do. They are trustworthy and make you feel safe working with them and are always ready to share the latest updates and progress with your project. Highly skilled, knowledgeable, professional and passionate - what more can you ask for?`,
    },
    {
        title: `Super T`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/super-t.png`,
        },
        categories: [`ppc-management`, `website-design`],
        author: `Sydney Boothroyd, Manager at Super T Transportation`,
        body: 'Social Hustle has been amazing to work with. They took our companies marketing game to a new level. They also helped us execute a killer website. Social Hustle has some of the best marketing minds and they are always looking for new ways to help us grow our brand and leverage our social media and Google presence to increase conversions. I would recommend their services for anyone wanting to implement a marketing strategy. If you don`t even know where to start with a marketing strategy then you definitely  need to start with a call to SocialHustle.',
    },
    {
        title: `TBR`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/tbr.png`,
        },
        categories: [`ppc-management`, `search-engine-optimization`],
        author: `Matthew Rabe, Owner at TBR`,
        body: `Quality work and quality service. These guys know exactly what they're doing and they'll take care of you and your company.`,
    },
    {
        title: `Wilcox`,
        images: {
            featured: `https://socialhustle-blitz-static.s3.amazonaws.com/client-logos/wilcox.png`,
        },
        categories: [`website-design`, `content-creation`],
        author: `Tavann Young, Wilcox Fresh`,
        body: `Social Hustle is a team made up of very talented people whose goal is to make you stand out. They'll work hard for your business and give you a product you’ll be proud of. Dalin and Christian helped us to get our business off the ground and customer-ready on a short time line with stellar results! Looking forward to future successes with them.`,
    },
]

export default testimonials
