import { PortfolioItem } from 'types'

const portfolio: PortfolioItem[] = [
    {
        slug: 'advanced-heating-cooling',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/advanced-heating.jpg',
        },
        bgColor: '#0a0a0a',
        category: 'Marketing',
        title: 'Advanced Heating & Cooling',
        placement: [
            'ppc-management',
            'tiktok-ads',
            'snapchat-ads',
            'facebook-ads',
            'search-engine-optimization',
        ],
        tags: ['Google Ads - Search'],
    },
    {
        slug: 'ball-ventures',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/Ball-Ventures-Ipad-Mockup.jpg',
        },
        bgColor: '#ffffff',
        category: 'Creative',
        title: 'Ball Ventures',
        placement: ['homepage', 'website-design', 'content-creation'],
        tags: ['Photography', 'Video Production', 'Website Development'],
        tagline: 'Digital Experience Creation',
    },
    {
        slug: 'gld-creative',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/gld.jpg',
        },
        bgColor: '#161616',
        category: 'Creative',
        title: 'GLD',
        placement: ['content-creation', 'website-design'],
        tags: ['Photography', 'Video Production', 'Website Development'],
    },
    {
        slug: 'aurate',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/aurate2.jpg',
        },
        bgColor: '#ece6e2',
        category: 'Marketing',
        title: 'Aurate',
        placement: [
            'homepage',
            'ppc-management',
            'tiktok-ads',
            'snapchat-ads',
            'facebook-ads',
            'search-engine-optimization',
        ],
        tags: ['Google Ads - Shopping'],
        tagline: '28x purchases in 4 months',
    },
    {
        slug: 'gld',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/GLD-TikTok-Mockup.jpg',
        },
        bgColor: '#161616',
        category: 'Marketing',
        title: 'GLD',
        placement: [
            'homepage',
            'ppc-management',
            'tiktok-ads',
            'snapchat-ads',
            'facebook-ads',
            'search-engine-optimization',
        ],
        tags: ['Google Ads', 'Linked In', 'Youtube', 'Tiktok', 'SEO'],
        tagline: '80x Revenue In 6 Years',
    },
    {
        slug: 'palisades-creek-trailhead',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/palisades.jpg',
        },
        bgColor: '#F6F3EA',
        category: 'Creative',
        title: 'Palisades Creek Trailhead',
        placement: ['content-creation', 'website-design'],
        tags: ['Photography', 'Graphic Design', 'Website Development'],
    },
    {
        slug: 'preppd',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/preppd1.jpeg',
        },
        bgColor: '#edf1ed',
        category: 'Creative',
        title: "Prepp'd",
        placement: ['content-creation', 'website-design'],
        tags: ['Photography', 'Graphic Design', 'Website Development'],
    },
    {
        slug: 'rings-by-liv',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/ringsbyliv.jpg',
        },
        bgColor: '#E9DCC8',
        category: 'Creative',
        title: 'Rings By Liv',
        placement: ['content-creation'],
        tags: ['Photography', 'Video Production'],
    },
    {
        slug: 'super-t-transportation',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/super-t.jpg',
        },
        bgColor: '#ffffff',
        category: 'Marketing',
        title: 'Super T Transportation',
        placement: [
            'homepage',
            'tiktok-ads',
            'snapchat-ads',
            'facebook-ads',
            'search-engine-optimization',
        ],
        tags: ['Google Ads'],
        tagline: '8x CVR in 6 months',
    },
    {
        slug: 'teton-workshop',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/teton.jpeg',
        },
        bgColor: '#212121',
        category: 'Creative',
        title: 'Teton Workshop',
        placement: ['website-design', 'content-creation'],
        tags: ['Photography', 'Product Photography'],
    },
    {
        slug: 'wilcox-fresh',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/meat-potatoes.jpeg',
        },
        bgColor: '#FDFDF7',
        category: 'Creative',
        title: 'Wilcox Meat & Potatoes',
        placement: ['homepage', 'website-design', 'content-creation'],
        tags: ['Photography', 'Product Photography'],
        tagline: 'Creating A Brand',
    },
    {
        slug: 'yellowstone-bear-world',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/yellowstone.jpeg',
        },
        bgColor: '#282828',
        category: 'Creative',
        title: 'Yellowstone Bear World',
        placement: ['content-creation'],
        tags: ['Photography', 'Video Production'],
    },
    {
        slug: 'nicks-demo-hauling',
        images: {
            featured:
                'https://socialhustle-blitz-static.s3.amazonaws.com/portfolio/nicks-demo.jpg',
        },
        bgColor: '#FFECE6',
        category: 'Marketing',
        title: 'Nicks Demo & Hauling',
        placement: [
            'ppc-management',
            'tiktok-ads',
            'snapchat-ads',
            'facebook-ads',
            'search-engine-optimization',
        ],
        tags: ['Google Ads - Search'],
    },
]

export default portfolio
