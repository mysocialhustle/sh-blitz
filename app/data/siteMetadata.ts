const siteMetadata = {
  siteUrl: `https://mysocialhustle.com`,
  description: `Social Hustle is an award winning digital marketing, website development and creative design agency.`,
  title: `Social Hustle`,
  titleTemplate: "%s | Award-Winning Design & Marketing",
  image: "/images/main-img.jpg",
  socials: {
    instagram: "https://www.instagram.com/mysocialhustle/",
    facebook: "https://www.facebook.com/mysocialhustle",
    linkedin: "https://www.linkedin.com/company/social-hustle",
    youtube: "https://www.youtube.com/channel/UCCGV8tK3JN4InG8UMuRoQ4A",
  },
  contact: {
    phone: "(208) 656-1903",
    email: "theplug@mysocialhustle.com",
    address: {
      street: "3725 Woodking Dr.",
      city: "Idaho Falls",
      state: "ID",
      zip: "83404",
    },
  },
}

export default siteMetadata
