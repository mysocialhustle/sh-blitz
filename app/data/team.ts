import { TeamMember } from 'types'

const team: TeamMember[] = [
    {
        name: 'Dalin Bernard',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/dalin.jpg',
        position: 'Founder',
    },
    {
        name: 'Chris Parrett',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/chris.jpg',
        position: 'CEO',
    },
    {
        name: 'Christian Parrett',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/christian.jpg',
        position: 'Head of Creative Design',
    },
    {
        name: 'Ashlie Green',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/ashlie.jpg',
        position: 'Head of Paid Media and Marketing',
    },
    {
        name: 'John Myers',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/john.jpg',
        position: 'Senior Account Manager',
    },
    {
        name: 'Chase Salazar',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/chase.jpg',
        position: 'Customer Success Manager',
    },
    {
        name: 'Jaxon Chantrill',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/jackson.jpg',
        position: 'Creative Specialist',
    },
    {
        name: 'Thai Cao',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/thai.jpg',
        position: 'Paid Media Specialist',
    },
    {
        name: 'Bree Williams',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/bree.jpg',
        position: 'Paid Media Specialist',
    },
    {
        name: 'Laura Trevino',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/laura.jpg',
        position: 'Office Administrator',
    },
    {
        name: 'Deja Bingham',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/deja.jpg',
        position: 'Social Media Specialist',
    },
    {
        name: 'Kaden Miller',
        image: 'https://socialhustle-blitz-static.s3.amazonaws.com/team/kaden.jpg',
        position: 'Sales Development Representative',
    },
]

export default team
