import { Certification } from 'types'

const certifications: Certification[] = [
    {
        name: 'Google Premier Partner',
        title: 'Premier Partner',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/google-logo.svg',
        homepage: true,
    },
    {
        name: 'YouTube Marketing Partner',
        title: 'Marketing Partner',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/YouTube-logo.svg',
        homepage: true,
    },
    {
        name: 'TikTok Marketing Partner',
        title: 'Marketing Partner',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/TikTok.svg',
        homepage: true,
    },
    {
        name: 'Facebook Business Partner',
        title: 'Business Partner',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/facebook.svg',
        homepage: true,
    },
    {
        name: 'Adobe Creative Partner',
        title: 'Creative Partner',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/adobe-logo.svg',
        homepage: true,
    },
    {
        name: 'Designrush Accredited Agency',
        title: 'Accredited Agency',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/designrush-logo.svg',
        homepage: false,
    },
    {
        name: 'SEMRush Certified Marketer',
        title: 'Certified Marketer',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/SEMrush-logo.svg',
        homepage: false,
    },
    {
        name: 'Hootsuite Certified Agency',
        title: 'Certified Agency',
        category: 'Certifications',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/hootsuite-logo.svg',
        homepage: false,
    },
    {
        name: 'Madcon Top 100 of 2020 Marketing & Advertising Leaders',
        title: '2020 Top 100 of Marketing & Advertising Leaders',
        category: 'Awards',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/madcon-logo.jpg',
        homepage: false,
    },
    {
        name: 'MAR Summit 2021 Top 100 Marketing & Advertising Leaders',
        title: '2021 Top 100 Marketing & Advertising Leaders',
        category: 'Awards',
        type: 'svg',
        imageSrc:
            'https://socialhustle-blitz-static.s3.amazonaws.com/certifications/MARsum-logo.svg',
        homepage: false,
    },
]

export default certifications
