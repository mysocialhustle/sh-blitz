import { Styled } from './Checkboxes.styles'
import { CheckboxInput } from 'types'

export default function Checkbox({ label, value, ...rest }: CheckboxInput) {
    return (
        <Styled.Label>
            <Styled.Checkbox value={value} {...rest} />
            <span>{label}</span>
        </Styled.Label>
    )
}
