import styled from 'styled-components'

export namespace Styled {
    export const Checkbox = styled.input.attrs({ type: 'checkbox' })`
        display: inline-block;
        margin-right: 5px;
    `

    export const Label = styled.label`
        display: inline-block;
        margin-right: 10px;
        margin-bottom: 20px;
    `
}
