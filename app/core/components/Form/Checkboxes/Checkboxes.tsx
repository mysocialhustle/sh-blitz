import { useState, useCallback } from 'react'
import { useFormContext, useController } from 'react-hook-form'
import { CheckboxProps } from 'types'
import Checkbox from './Checkbox'

export default function Checkboxes({ inputs, name, onChange }: CheckboxProps) {
    const { control } = useFormContext()
    const { field } = useController({ control, name })
    const [values, setValues] = useState(field.value || [])

    const change = useCallback(
        (target) => {
            // https://codesandbox.io/s/usecontroller-checkboxes-99ld4?file=/src/App.js:1048-1055
            const { checked, value } = target
            const valuesCopy = [...values]
            if (checked) {
                valuesCopy.push(value)
            } else {
                valuesCopy.splice(valuesCopy.indexOf(value), 1)
            }
            field.onChange(valuesCopy)
            setValues(valuesCopy)

            if (onChange) {
                onChange(valuesCopy)
            }
        },
        [field, values, onChange]
    )

    const inputMap = inputs.map((input) => {
        return typeof input === 'string'
            ? {
                  label: input,
                  value: input,
              }
            : input
    })

    return (
        <>
            {inputMap?.map((input, index) => {
                return (
                    <Checkbox
                        {...input}
                        key={index}
                        onChange={(e) => change(e.target)}
                    />
                )
            })}
        </>
    )
}
