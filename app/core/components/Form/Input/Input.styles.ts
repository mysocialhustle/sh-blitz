import styled from 'styled-components'

export namespace Styled {
    export const Wrapper = styled.div`
        cursor: pointer;
        position: relative;
        z-index: 1;
        margin-bottom: 20px;
        background-color: white;
        &:after {
            content: '';
            display: block;
            clear: both;
        }
    `

    export const Input = styled.input`
        position: relative;
        display: block;
        padding: 20px 15px 10px;
        border-radius: 0;
        font-weight: 400;
        -webkit-appearance: none;
        width: 100%;
        background-color: rgba(239, 239, 239, 0.66);
        border: none;
        transition: background-color 0.3s;
        outline: none;
        line-height: 1;

        &:focus,
        &.input-filled {
            background-color: transparent;
        }
        &:focus + label,
        &.input-filled + label {
            color: #a6a6a6;
            span {
                transform: translate3d(15px, 10px, 0) scale(0.6);
                opacity: 0.9;
                letter-spacing: 1px;
            }
            svg {
                stroke-dashoffset: 0 !important;
            }
        }
    `

    export const Label = styled.label`
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        z-index: -1;
        color: #000000;

        span {
            width: 100%;
            text-align: left;
            position: absolute;
            top: 0;
            left: 0;
            pointer-events: none;
            transform: translate3d(15px, 15px, 0) scale(1);
            transition: all 0.25s ease-in-out 0.15s;
            user-select: none;
            line-height: 1;
            transform-origin: top left;
        }

        svg {
            position: absolute;
            top: 0;
            left: 0;
            fill: none;
            transition: stroke-dashoffset 0.4s ease-in-out;
            pointer-events: none;
            stroke: #000;
        }
    `
}
