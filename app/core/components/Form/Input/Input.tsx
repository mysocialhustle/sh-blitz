import { forwardRef } from 'react'
import { useFormContext } from 'react-hook-form'
import { InputOrTextareaProps } from 'types'
import { Styled } from './Input.styles'
import InputBorder from './Border'

const Input = forwardRef<HTMLInputElement, InputOrTextareaProps>(
    (
        {
            label,
            outerProps,
            labelProps,
            name,
            required = false,
            element = 'input',
            type = 'text',
            ...props
        },
        ref
    ) => {
        const {
            register,
            formState: { isSubmitting, errors },
            getValues,
        } = useFormContext()
        const error = Array.isArray(errors[name])
            ? errors[name].join(', ')
            : errors[name]?.message || errors[name]
        const initialValues = getValues()

        const setFilled = (e) => {
            const { target } = e
            if (target.value.length > 0) {
                target.classList.add('input-filled')
            } else {
                target.classList.remove('input-filled')
            }
        }

        return (
            <Styled.Wrapper className="input-wrapper" {...outerProps}>
                <Styled.Input
                    as={element}
                    disabled={isSubmitting}
                    required={required}
                    type={type}
                    className={
                        props?.defaultValue
                            ? `input-filled`
                            : initialValues[name]
                            ? `input-filled`
                            : ``
                    }
                    {...register(name, {
                        required: required,
                        onChange: (e) => {
                            setFilled(e)
                        },
                    })}
                    {...props}
                />
                <Styled.Label {...labelProps}>
                    <span>{label}</span>
                    <InputBorder />
                </Styled.Label>

                {/**error && (
                    <div role="alert" style={{ color: 'red' }}>
                        {error}
                    </div>
                )**/}
            </Styled.Wrapper>
        )
    }
)

export default Input
