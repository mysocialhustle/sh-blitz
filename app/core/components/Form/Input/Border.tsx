import React from 'react'
import useDimensions from 'app/core/hooks/useDimensions'

export default function InputBorder() {
    const [trackRef, trackDimensions] = useDimensions()
    const height = trackDimensions?.height ? trackDimensions.height : 0
    const width = trackDimensions?.width ? trackDimensions.width : 0
    const strokeVars = (width + height) * 2

    return (
        <svg
            ref={trackRef}
            width="100%"
            height="100%"
            viewBox="0 0 404 77"
            preserveAspectRatio="none"
            style={{
                strokeDasharray: strokeVars,
                strokeDashoffset: strokeVars,
            }}
        >
            <path
                stroke="1"
                vectorEffect="non-scaling-stroke"
                d="m0,0l404,0l0,77l-404,0l0,-77z"
            />
        </svg>
    )
}
