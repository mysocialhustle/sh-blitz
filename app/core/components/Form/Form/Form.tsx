import { useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from 'zod'
import Button from '../../Button/Button'
import { FormProps } from 'types'
import Message from '../Messages/Messages'
import { ErrorMessage } from '@hookform/error-message'
import Loader from '../../Loader/Loader'

export const FORM_ERROR = 'FORM_ERROR'

export function Form<S extends z.ZodType<any, any>, T extends unknown>({
    children,
    submitText,
    schema,
    initialValues,
    onSubmit,
    buttonProps,
    successMessage = 'Thank you for reaching out. Your message has been sent.',
    resetOnSubmit = true,
    ...props
}: FormProps<S, T>) {
    const ctx = useForm<z.infer<S>>({
        resolver: schema ? zodResolver(schema) : undefined,
        defaultValues: initialValues,
        criteriaMode: 'all',
    })
    const [formError, setFormError] = useState<string | null>(null)

    const { formState, reset } = ctx

    const filteredErrors = Object.entries(formState?.errors)
        ?.filter(([name, err]) => err.type !== 'submit')
        ?.map(([name, err]) => {
            return {
                name: name,
                ...err,
            }
        })

    return (
        <FormProvider {...ctx}>
            <form
                onSubmit={ctx.handleSubmit(async (values) => {
                    const result = (await onSubmit(values)) || {}
                    for (const [key, value] of Object.entries(result)) {
                        if (key === FORM_ERROR) {
                            setFormError(value)
                        }
                    }
                    if (resetOnSubmit && formState.isSubmitSuccessful) reset()
                })}
                {...props}
            >
                {/* Form fields supplied as children are rendered here */}
                {children}

                {formError && <Message type="error">{formError}</Message>}

                {filteredErrors.map(({ name, message }, index) => (
                    <ErrorMessage
                        name={name}
                        key={index}
                        render={({ message }) => (
                            <Message type="error">{message}</Message>
                        )}
                    />
                ))}

                {formState.isSubmitting && <Loader />}

                {formState.isSubmitSuccessful && successMessage && (
                    <Message type="success">{successMessage}</Message>
                )}

                <Button
                    element="button"
                    props={{
                        type: 'submit',
                        disabled: ctx.formState.isSubmitting,
                    }}
                    {...buttonProps}
                >
                    {submitText ? submitText : `SUBMIT`}
                </Button>
            </form>
        </FormProvider>
    )
}

export default Form
