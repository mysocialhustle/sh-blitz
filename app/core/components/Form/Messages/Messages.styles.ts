import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'

const MessageBox = styled(motion.div)`
    padding: 10px 20px;
    color: white;
    margin: 20px 0;
    transform-origin: center top;
`

export const ErrorBox = styled(MessageBox)`
    background-color: ${colors.darkRed};
`

export const SuccessBox = styled(MessageBox)`
    background-color: ${colors.green};
`
