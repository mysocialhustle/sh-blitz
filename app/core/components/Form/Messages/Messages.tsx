import { ReactNode } from 'react'
import * as Styled from './Messages.styles'
import { AnimatePresence } from 'framer-motion'

interface Props {
    children: ReactNode
    type: 'success' | 'error'
}

export default function Message({ type = 'error', children }: Props) {
    const Component = type === 'success' ? Styled.SuccessBox : Styled.ErrorBox

    return (
        <AnimatePresence exitBeforeEnter>
            <Component
                key={type}
                initial={{
                    scaleY: 0,
                    opacity: 0,
                }}
                animate={{
                    scaleY: 1,
                    opacity: 1,
                }}
                exit={{
                    scaleY: 0,
                    opacity: 0,
                }}
                transition={{
                    duration: 0.35,
                }}
            >
                {children}
            </Component>
        </AnimatePresence>
    )
}
