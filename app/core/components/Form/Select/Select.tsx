import { Styled } from './Select.styles'
import SelectComponent, { ActionMeta } from 'react-select'
import CreatableSelect from 'react-select/creatable'
import { useFormContext, useController } from 'react-hook-form'

interface Option {
    label: string
    value: any
}
interface Props {
    onChange?: (
        option: Option | Option['value'] | null,
        actionMeta?: ActionMeta<Option>
    ) => void
    creatable?: boolean
    options: any[]
    label: string
    name: string
    defaultValue?: Option
    isSearchable?: boolean
    isClearable?: boolean
}

export function SelectBase(props: Props) {
    const Component = props.creatable ? CreatableSelect : SelectComponent
    const { options, isClearable = true, ...rest } = props

    return (
        <Styled.Wrap>
            <Component
                isClearable={isClearable}
                className="react-select-container"
                classNamePrefix="react-select"
                createOptionPosition="first"
                placeholder={props.label}
                controlShouldRenderValue={true}
                options={options.map((option) => {
                    return typeof option === 'string'
                        ? {
                              label: option,
                              value: option,
                          }
                        : option
                })}
                {...rest}
            />
        </Styled.Wrap>
    )
}

export default function Select(props) {
    const { control, getValues } = useFormContext()

    const { field } = useController({ control, name: props.name })

    const { onChange, ...rest } = props

    const initialValues = getValues()

    return (
        <Styled.Wrap>
            <SelectBase
                onChange={(input, action) => {
                    field.onChange(input?.value)
                    if (onChange) {
                        onChange(input, action)
                    }
                }}
                defaultValue={
                    initialValues[props.name]
                        ? {
                              label: initialValues[props.name],
                              value: initialValues[props.name],
                          }
                        : null
                }
                {...rest}
            />
        </Styled.Wrap>
    )
}
