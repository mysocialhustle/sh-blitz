import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Wrap = styled.div`
        position: relative;

        .react-select-container {
            margin-bottom: 20px;
        }

        .react-select__menu {
            z-index: 98;
        }

        .react-select__control {
            border-radius: 0;
            background-color: #f4f4f4;
            border-color: transparent;
            outline: none;
            box-shadow: none;
            z-index: 0;

            &.react-select__control--is-focused {
                border-color: ${colors.lightGrey};
                z-index: 100;
            }
        }

        .react-select__placeholder {
            color: #9d9d9d;
            margin: 0px;
        }

        .react-select__value-container {
            padding: 11px 15px;
        }

        .react-select__input-container {
            margin: 0;
            padding: 0;
        }
    `
}
