import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Eye = styled.span`
        position: absolute;
        top: 0;
        right: 0;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        padding: 5px 10px;
        cursor: pointer;
        z-index: 2;
        font-size: 0.9rem;
        color: ${colors.darkGrey};
    `

    export const Wrapper = styled.div`
        position: relative;
    `
}
