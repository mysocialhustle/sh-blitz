import Input from '../Input/Input'
import { Styled } from './Password.styles'
import { useState } from 'react'
import Icon from '../../Icon/Icon'

export default function Password(props) {
    const [visibleInput, setVisibleInput] = useState(false)

    return (
        <Styled.Wrapper>
            <Styled.Eye
                onClick={() => setVisibleInput(!visibleInput)}
                title={visibleInput ? 'Hide password' : 'View password'}
            >
                <Icon name={visibleInput ? 'eyeClosed' : 'eyeOpen'} />
            </Styled.Eye>
            <Input type={visibleInput ? 'text' : 'password'} {...props} />
        </Styled.Wrapper>
    )
}
