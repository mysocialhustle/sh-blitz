import React, { useId, useReducer } from 'react'
import { Styled } from './File.styles'
import { useFormContext, useController } from 'react-hook-form'
import Icon from 'app/core/components/Icon/Icon'
import colors from 'app/core/constants/colors'
import Loader from '../../Loader/Loader'
import Message from '../Messages/Messages'
import FileUploadIcon from 'app/svg/file-upload.svg'
import { FileInputProps, FileInputState, S3SignedPost } from 'types'

const initialState: FileInputState = {
    file: null,
    stage: 'empty',
    fileLink: null,
    disabled: false,
    message: null,
    presignedPost: null,
}

export default function FileInput({
    label,
    onChange,
    children,
    name,
    uploadPath = '',
}: FileInputProps) {
    const id = useId()
    const [state, dispatch] = useReducer(reducer, initialState)
    const { control } = useFormContext()

    const { field } = useController({ control, name })

    function reducer(state, action) {
        let newState
        switch (action.type) {
            case 'presign':
                newState = {
                    ...state,
                    disabled: true,
                    stage: 'pending',
                }
                break
            case 'stageUpload':
                newState = {
                    ...state,
                    file: action.payload.file,
                    presignedPost: action.payload.presignedPost,
                    stage: 'signed',
                    disabled: true,
                }
                break
            case 'cancelUpload':
                newState = {
                    ...state,
                    stage: 'empty',
                    file: null,
                    presignedPost: null,
                    disabled: false,
                }
                break
            case 'confirmUpload':
                newState = {
                    ...state,
                    stage: 'complete',
                    fileLink: action.payload.fileLink,
                    file: null,
                    message: action.payload.message,
                    presignedPost: null,
                    disabled: false,
                }
                break
            case 'abortOnError':
                newState = {
                    ...state,
                    stage: 'error',
                    fileLink: null,
                    message: action.payload,
                    presignedPost: null,
                    disabled: false,
                }
                break
            default:
                throw new Error()
        }
        return newState
    }

    const setError = () => {
        dispatch({
            type: 'abortOnError',
            payload: 'Unable to upload file.',
        })
    }

    const handleFileInput = async (e) => {
        const file = e.target.files[0]
        dispatch({
            type: 'presign',
        })
        await fetch('/api/presignS3Upload', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: `${uploadPath}/${file.name}`,
                type: file.type,
            }),
        })
            .then((response) => response.json())
            .then((res) => {
                console.log(res)
                if (res?.fields && res?.url) {
                    setTimeout(function () {
                        dispatch({
                            type: 'stageUpload',
                            payload: {
                                file: file,
                                presignedPost: res,
                            },
                        })
                        if (onChange) {
                            onChange(e)
                        }
                    }, 1000)
                } else {
                    setError()
                }
            })
    }

    const upload = async () => {
        const {
            presignedPost,
            file,
        }: { presignedPost: S3SignedPost; file: any } = state

        const fileLink = `https://socialhustle-blitz-static.s3.amazonaws.com/${presignedPost.fields.key}`

        console.log(fileLink)
        const formData = new FormData()

        Object.entries(presignedPost.fields).forEach(([k, v]) => {
            formData.append(k, v)
        })

        formData.append('file', file)
        await fetch(presignedPost.url, {
            method: 'POST',
            body: formData,
        }).then((response) => {
            // Expect no response
            if (response.status === 204) {
                dispatch({
                    type: 'confirmUpload',
                    payload: {
                        fileLink: fileLink,
                        message: 'Successfully uploaded file.',
                    },
                })
                field.onChange(fileLink)
            } else {
                setError()
            }
        })
    }

    return (
        <>
            <Styled.Wrapper>
                {label && <Styled.Label>{label}</Styled.Label>}
                <Styled.ControlWrap>
                    <input
                        type="file"
                        name={name}
                        size={40}
                        id={id}
                        aria-invalid="false"
                        onChange={handleFileInput}
                        disabled={state.disabled}
                    />
                </Styled.ControlWrap>
                <Styled.IconWrapper htmlFor={id} active={!state.disabled}>
                    <FileUploadIcon />
                    <span>{children}</span>
                </Styled.IconWrapper>
            </Styled.Wrapper>
            {state.stage === 'pending' && <Loader />}
            {state.stage === 'error' && (
                <Message type="error">{state.message}</Message>
            )}
            {state.stage === 'complete' && (
                <Message type="success">{state.message}</Message>
            )}
            {state?.file?.name && (
                <Styled.ConfirmationMessage>
                    Upload {state?.file?.name}?
                    <Styled.ConfirmationButton
                        style={{ color: colors.darkRed }}
                        title="Cancel Upload"
                        onClick={() =>
                            dispatch({
                                type: 'cancelUpload',
                            })
                        }
                    >
                        <Icon name="exCircle" />
                    </Styled.ConfirmationButton>
                    <Styled.ConfirmationButton
                        style={{ color: colors.green }}
                        title="Confirm Upload"
                        onClick={() => upload()}
                    >
                        <Icon name="checkCircle" />
                    </Styled.ConfirmationButton>
                </Styled.ConfirmationMessage>
            )}
        </>
    )
}
