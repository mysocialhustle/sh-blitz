import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import { StyledProps } from 'types'

export namespace Styled {
    export const Wrapper = styled.div`
        margin-bottom: 20px;
        position: relative;
        line-height: 0;
        max-width: 300px;
    `

    export const Label = styled.label`
        display: block;
        margin-bottom: 10px;
    `

    export const ControlWrap = styled.div`
        display: inline-block;
        position: relative;

        input {
            width: 0.1px;
            height: 0.1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
            cursor: pointer;
        }
    `

    export const IconWrapper = styled.label<StyledProps>`
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
        display: inline-block;
        overflow: hidden;
        padding: 15px;
        background-color: ${colors.darkGrey};
        color: white;
        text-transform: uppercase;
        transition: all 0.45s;
        line-height: 1;
        width: 100%;
        pointer-events: ${(props) => (props.active ? `all` : `none`)};
        opacity: ${(props) => (props.active ? 1 : 0.5)};
        text-align: center;

        &:hover {
            background-color: ${colors.red};
        }

        svg {
            display: inline-block;
            margin-right: 1em;
            fill: currentColor;
        }
    `

    export const ConfirmationButton = styled.span`
        display: inline-block;
        font-size: 1.25rem;
        margin-left: 10px;
        cursor: pointer;
    `

    export const ConfirmationMessage = styled.p`
        font-weight: 500;
        font-size: 14px;
    `
}
