import React, { useState, useRef, ReactNode, useEffect } from 'react'
import {
    motion,
    useViewportScroll,
    useTransform,
    useSpring,
    useReducedMotion,
} from 'framer-motion'
import useWindowSize from 'app/core/hooks/useWindowSize'
import useDimensions from 'app/core/hooks/useDimensions'

type ParallaxProps = {
    children: ReactNode
    offset?: number
}

export default function Parallax({ children }: ParallaxProps): JSX.Element {
    const prefersReducedMotion = useReducedMotion()
    const { height: windowHeight } = useWindowSize()
    const [ref, dimensions] = useDimensions()

    const { scrollY } = useViewportScroll()

    const input = [dimensions.y, dimensions.y + dimensions.height]

    const output = [0, windowHeight - dimensions.y - dimensions.height]

    const yRange = useTransform(scrollY, input, output)
    const y = useSpring(yRange, { stiffness: 400, damping: 90 })

    // Don't parallax if the user has "reduced motion" enabled
    if (prefersReducedMotion) {
        return <>{children}</>
    }

    return (
        <motion.div className="parallax" ref={ref} style={{ y }}>
            {children}
        </motion.div>
    )
}
