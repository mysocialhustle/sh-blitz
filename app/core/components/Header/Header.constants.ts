export const subMenuPadding = 20
export const mainFontSize = 18
export const megaFontSize = 18
export const megaHeadingMarginBottom = 20
export const mainLineHeight = 2.5
export const megaLineHeight = 2.5

export const getHeight = (
    numberItems: number,
    fontSize: number,
    lineHeight: number,
    padding: number
) => numberItems * fontSize * lineHeight + padding

export const popoverHeight = 150
