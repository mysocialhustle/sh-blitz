import React, { useEffect, useState } from 'react'
import NavBar from './NavBar/NavBar'
import Widgets from './Widgets/Widgets'
import Logo from './Logo/Logo'
import MobileMenu from './MobileMenu/MobileMenu'
import menu from 'app/data/menu'
import * as Styled from './Header.styles'
import { ThemeProvider } from 'styled-components'
import themes from 'app/core/constants/themes'

export default function Header({ theme = 'light' }) {
    const [navOpen, setNavOpen] = useState(false)
    const [scrolled, setScrolled] = useState(false)

    useEffect(() => {
        function onScroll() {
            let scheduledAnimationFrame
            // Store the scroll value for laterz.
            const lastScrollY = window.scrollY
            function cb() {
                if (lastScrollY === 0) {
                    setScrolled(false)
                } else {
                    setScrolled(true)
                }
            }

            // Prevent multiple rAF callbacks.
            if (scheduledAnimationFrame) return

            scheduledAnimationFrame = true
            requestAnimationFrame(cb)
        }

        onScroll()

        window.addEventListener('scroll', onScroll, false)

        return () => {
            window.removeEventListener('scroll', onScroll, false)
        }
    }, [])

    return (
        <ThemeProvider theme={themes[theme]}>
            <Styled.Header
                style={{
                    backgroundColor: scrolled
                        ? themes[theme].background
                        : 'rgba(0,0,0,0)',
                }}
            >
                <Styled.HeaderContainer fluid>
                    <a>
                        <Logo />
                    </a>
                    <NavBar menu={menu} />
                    <Widgets setOpen={(e) => setNavOpen(e)} navOpen={navOpen} />
                </Styled.HeaderContainer>
            </Styled.Header>
            {
                <MobileMenu
                    isOpen={navOpen}
                    menu={menu}
                    setOpen={(e) => setNavOpen(e)}
                />
            }
        </ThemeProvider>
    )
}
