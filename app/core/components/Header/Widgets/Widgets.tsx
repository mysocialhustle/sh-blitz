import React from 'react'
import Hamburger from '../Hamburger/Hamburger'
import { Link } from 'blitz'
import * as Styled from './Widgets.styled'
import CartIcon from 'app/svg/cart-icon.inline.svg'
import UserIcon from 'app/svg/user.inline.svg'

const Widgets = ({ setOpen, navOpen }) => {
    return (
        <>
            <Styled.Widgets>
                <Styled.Widget>
                    <Styled.WidgetInner>
                        <Link href="/account/" passHref>
                            <a>
                                <UserIcon className="icon" />
                            </a>
                        </Link>
                    </Styled.WidgetInner>
                </Styled.Widget>

                <Styled.Widget>
                    <Styled.CartInner>
                        <CartIcon className="icon" />
                        <Styled.CartCount>{0}</Styled.CartCount>
                    </Styled.CartInner>
                    <Styled.CartDrop>
                        <Styled.CartDropInner>
                            <p>No products in the cart.</p>
                        </Styled.CartDropInner>
                    </Styled.CartDrop>
                </Styled.Widget>

                <Styled.Hamburger>
                    <Hamburger setOpen={(e) => setOpen(e)} isOpen={navOpen} />
                </Styled.Hamburger>
            </Styled.Widgets>
        </>
    )
}

export default Widgets
