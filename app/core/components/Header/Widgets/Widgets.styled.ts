import styled from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'

export const Widgets = styled.div`
    display: flex;
    height: 100%;
    align-items: center;
`

export const Widget = styled.div`
    color: ${(props) => props.theme.color};
    position: relative;
    margin-left: 30px;

    .icon {
        height: 1.3em;
    }
`

export const WidgetInner = styled.div`
    display: inline-block;
`

export const Hamburger = styled(Widget)`
    ${mediaQueries.md} {
        display: none;
    }
`

export const CartInner = styled(WidgetInner)`
    padding-right: 10px;
`

export const CartDrop = styled.div`
    left: auto;
    opacity: 0;
    position: absolute;
    right: 0;
    text-align: left;
    top: 100%;
    transition: opacity 0.25s ease, visibility 0.25s ease;
    visibility: hidden;

    ${Widget}:hover & {
        visibility: visible;
        opacity: 1;
    }
`

export const CartDropInner = styled.div`
    background-color: ${(props) => props.theme.color};
    border: 1px solid transparent;
    display: inline-block;
    padding: 25px;
    position: relative;
    vertical-align: top;
    width: 300px;
    color: ${(props) => props.theme.background};
`

export const CartCount = styled.span`
    font-size: 19px;
    font-weight: 600;
    position: absolute;
    right: 0px;
    top: -10px;
`
