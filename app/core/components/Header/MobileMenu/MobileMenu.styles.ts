import styled from 'styled-components'
import headerHeight from 'app/core/constants/headerHeight'
import { gutter } from 'app/core/constants/grid'
import zIndices from 'app/core/constants/zIndices'
import { motion } from 'framer-motion'

export const HamburgerWrap = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    padding-right: ${gutter + 15}px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: ${headerHeight}px;
`

export const Logo = styled(motion.nav)`
    margin-bottom: 30px;
    opacity: 0;
`

export const SideArea = styled(motion.nav)`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    font-size: 1.25rem;
    padding: ${headerHeight}px 75px 30px;
    z-index: ${zIndices.mobileMenu};
    color: #fff;
    clip-path: circle(0px at 100% 0%);
`

export const NavList = styled(motion.ul)`
    list-style: outside none none;
    text-align: center;
`

export const NavItem = styled(motion.li)`
    margin: 1.5em 0;
    text-transform: uppercase;
    font-weight: 700;
    letter-spacing: -0.5px;
    opacity: 0;
    transform: scale(0);
`
