import React from 'react'
import { Link } from 'blitz'
import SubMenu from '../SubMenu/SubMenu'

export default function NavItem({
    index,
    link,
    name,
    submenu,
    active,
    setActive,
    type,
}) {
    const isActive = index === active

    return link != null ? (
        <Link href={link}>{name}</Link>
    ) : (
        <div
            style={{ cursor: 'pointer', userSelect: 'none' }}
            onClick={() => setActive(isActive ? false : index)}
        >
            {name}
            <SubMenu items={submenu} type={type} active={isActive} />
        </div>
    )
    //return null
}
