const variants = {
    sideArea: {
        initial: {
            clipPath: `circle(0px at 100% 0%)`,
        },
        animate: (height) => ({
            clipPath: `circle(${height * 2}px at 100% 0%)`,
            transition: {
                type: 'spring',
                stiffness: 20,
            },
        }),
        exit: {
            clipPath: `circle(0px at 100% 0%)`,
            transition: {
                delay: 0.5,
                type: 'spring',
                stiffness: 300,
                damping: 40,
            },
        },
    },
    logo: {
        initial: {
            opacity: 0,
        },
        animate: {
            opacity: 1,
            transition: {
                duration: 1.5,
                delay: 0.5,
                ease: [0.22, 1, 0.36, 1],
            },
        },
        exit: {
            opacity: 0,
            transition: {
                duration: 0.3,
                ease: [0.64, 0, 0.78, 0],
            },
        },
    },
    list: {
        animate: {
            transition: { staggerChildren: 0.07, delayChildren: 0.2 },
        },
        exit: {
            transition: { staggerChildren: 0.05, staggerDirection: -1 },
        },
    },
    item: {
        initial: {
            scale: 0,
            opacity: 0,
        },
        animate: {
            scale: 1,
            opacity: 1,
            transition: {
                scale: {
                    type: 'spring',
                    velocity: 0,
                },
                opacity: {
                    duration: 0.5,
                },
            },
        },
        exit: {
            scale: 0,
            opacity: 0,
            transition: {
                scale: {
                    type: 'spring',
                },
                opacity: {
                    duration: 0.5,
                },
            },
        },
    },
}

export default variants
