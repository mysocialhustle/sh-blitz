import React, { useState } from 'react'
import Logo from 'app/svg/logo-light.inline.svg'
import Hamburger from '../Hamburger/Hamburger'
import * as Styled from './MobileMenu.styles'
import { Link } from 'blitz'
import NavItem from './NavItem/NavItem'
import useWindowSize from 'app/core/hooks/useWindowSize'
import { AnimatePresence } from 'framer-motion'
import variants from './MobileNav.variants'
import { ThemeProvider } from 'styled-components'
import themes from 'app/core/constants/themes'

const MobileNav = ({ isOpen, menu, setOpen }) => {
    const [active, setActive] = useState(false)
    const { height } = useWindowSize()

    return (
        <ThemeProvider theme={themes.light}>
            <AnimatePresence exitBeforeEnter>
                {isOpen && (
                    <Styled.SideArea
                        key="sideArea"
                        variants={variants.sideArea}
                        initial="initial"
                        animate="animate"
                        exit="exit"
                        custom={height}
                    >
                        <Styled.HamburgerWrap>
                            <Hamburger
                                setOpen={(e) => setOpen(e)}
                                isOpen={isOpen}
                            />
                        </Styled.HamburgerWrap>
                        <Styled.Logo variants={variants.logo}>
                            <Logo />
                        </Styled.Logo>
                        <Styled.NavList variants={variants.list}>
                            {menu.map((item, index) =>
                                item.type === 'external' ? (
                                    <Styled.NavItem
                                        variants={variants.item}
                                        key={index}
                                    >
                                        <a
                                            href={item.link}
                                            target="_blank"
                                            rel="noreferrer"
                                        >
                                            {item.name}
                                        </a>
                                    </Styled.NavItem>
                                ) : (
                                    <Styled.NavItem
                                        variants={variants.item}
                                        key={index}
                                    >
                                        <NavItem
                                            index={index}
                                            active={active}
                                            setActive={setActive}
                                            {...item}
                                        />
                                    </Styled.NavItem>
                                )
                            )}
                        </Styled.NavList>
                    </Styled.SideArea>
                )}
            </AnimatePresence>
        </ThemeProvider>
    )
}

export default MobileNav
