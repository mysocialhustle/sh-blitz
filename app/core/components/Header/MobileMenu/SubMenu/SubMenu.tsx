import React from 'react'
import * as Styled from './SubMenu.styles'
import { Link } from 'blitz'
import variants from './SubMenu.variants'
import { AnimatePresence } from 'framer-motion'

export default function SubMenu({ items, type, active }) {
    const itemDimensions = {
        fontSize: 16,
        lineHeight: 2.5,
    }

    const allItems = items
        .map((subItem) => {
            const { submenu } = subItem
            if (submenu && submenu.length > 0) {
                const allSubs = submenu.map((submenuItems) => {
                    return submenuItems
                })
                return allSubs
            } else {
                return subItem
            }
        })
        .flat()

    const height =
        itemDimensions.fontSize * itemDimensions.lineHeight * allItems.length

    return (
        <AnimatePresence>
            {active && (
                <Styled.SubNavList
                    key="itemList"
                    variants={variants.list}
                    initial="initial"
                    animate="animate"
                    exit="exit"
                    custom={height}
                >
                    {/**items.length > 0 &&
                        items.map((item, index) =>
                            type === 'mega' ? (
                                item.submenu.map((submenuItem, subIndex) => (
                                    <Styled.SubNavItem
                                        key={subIndex}
                                        variants={variants.item}
                                    >
                                        <Link href={submenuItem.link}>
                                            {submenuItem.name}
                                        </Link>
                                    </Styled.SubNavItem>
                                ))
                            ) : (
                                <Styled.SubNavItem
                                    key={index}
                                    variants={variants.item}
                                >
                                    <Link href={item.link}>{item.name}</Link>
                                </Styled.SubNavItem>
                            )
                            )**/}
                    {allItems.map((item, index) => {
                        return (
                            <Styled.SubNavItem
                                key={index}
                                variants={variants.item}
                            >
                                <Link href={item.link}>{item.name}</Link>
                            </Styled.SubNavItem>
                        )
                    })}
                </Styled.SubNavList>
            )}
        </AnimatePresence>
    )
}
