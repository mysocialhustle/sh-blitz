import styled from 'styled-components'
import { motion } from 'framer-motion'

export const SubNavList = styled(motion.ul)`
    list-style: outside none none;
    text-align: center;
    transform-origin: center top;
    overflow: hidden;
`

export const SubNavItem = styled(motion.li)`
    text-transform: capitalize;
    font-size: 1rem;
    font-weight: 400;
    line-height: 2.5;
`
