const variants = {
    list: {
        initial: {
            height: 0,
            opacity: 0,
        },
        animate: (height) => ({
            height: height,
            opacity: 1,
            transition: {
                duration: 1,
                ease: [0.04, 0.62, 0.23, 0.98],
            },
        }),
        exit: {
            height: 0,
            opacity: 0,
            transition: {
                duration: 1,
                ease: [0.04, 0.62, 0.23, 0.98],
            },
        },
    },
    item: {
        initial: {
            scale: 0.95,
        },
        animate: {
            scale: 1,
            transition: {
                duration: 1,
            },
        },
    },
}

export default variants
