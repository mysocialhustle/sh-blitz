import styled from 'styled-components'
import headerHeight from 'app/core/constants/headerHeight'
import { Container } from 'app/styles/grid'
import zIndices from 'app/core/constants/zIndices'

export const Header = styled.header`
    position: fixed;
    display: inline-block;
    width: 100%;
    vertical-align: top;
    height: ${headerHeight}px;
    z-index: ${zIndices.header};
    padding-left: 15px;
    padding-right: 15px;
    left: 0;
    top: 0;
    background-color: rgba(0, 0, 0, 0);
    color: ${(props) => props.theme.color};
    transition: background-color 0.3s ease-out;
`

export const HeaderContainer = styled(Container)`
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
    height: 100%;
`
