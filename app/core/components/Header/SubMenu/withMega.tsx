import React from 'react'
import * as Styled from './SubMenu.styles'
import * as layout from '../Header.constants'

export default function withMega(Component, props) {
    const { items, type, index, active } = props
    const length = items.length
    //console.log(index, active)
    const subItems = items.map(({ submenu }) => (submenu ? submenu.length : 0))
    const maxItems = Math.max(...subItems)
    const padding = layout.subMenuPadding * 2
    const headingHeight =
        layout.megaFontSize * layout.mainLineHeight +
        layout.megaHeadingMarginBottom

    return type === `mega` ? (
        <Styled.MegaMenu
            id={`submenu-${index}`}
            style={{ width: length * 275 }}
            className={`submenu ${active ? `active` : ``}`}
            data-height={
                headingHeight +
                layout.getHeight(
                    maxItems,
                    layout.megaLineHeight,
                    layout.megaFontSize,
                    padding
                )
            }
        >
            {items.map((item, index) => (
                <li key={index}>
                    <Styled.MegaHeading>{item.name}</Styled.MegaHeading>
                    <Component
                        items={item.submenu}
                        height={layout.getHeight(
                            maxItems,
                            layout.mainLineHeight,
                            layout.mainFontSize,
                            padding
                        )}
                    />
                </li>
            ))}
        </Styled.MegaMenu>
    ) : (
        <Component
            {...props}
            active={active}
            height={layout.getHeight(
                length,
                layout.mainLineHeight,
                layout.mainFontSize,
                padding
            )}
        />
    )
    //return null
}
