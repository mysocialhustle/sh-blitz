import styled from 'styled-components'
import {
    mainLineHeight,
    subMenuPadding,
    megaFontSize,
    megaLineHeight,
    megaHeadingMarginBottom,
} from '../Header.constants'
import { StyledProps } from 'types'

export const SubMenu = styled.ul`
    color: ${(props) => props.theme.background};
    font-weight: 500;
    text-transform: capitalize;
    line-height: ${mainLineHeight};
    position: absolute;
    overflow: hidden;
    padding: ${subMenuPadding}px;

    &.submenu {
        opacity: 0;
        transition: all 0.3s;
        z-index: -1;

        &.active {
            opacity: 1;
            z-index: 1;
        }
    }

    svg {
        height: 1.5em;
        width: 1.5em;
        margin-right: 10px;
        fill: currentColor;
        display: inline-block;
        vertical-align: middle;
    }
`

export const MegaMenu = styled(SubMenu)<StyledProps>`
    display: flex;
    flex-flow: row wrap;
    font-size: ${megaFontSize}px;

    li {
        flex-grow: 1;
        padding-right: 20px;

        &:last-child {
            padding-right: 0;
        }

        ${SubMenu} {
            opacity: 1;
            position: static;
            padding: 0;
            line-height: ${megaLineHeight};
        }
    }
`

export const MegaHeading = styled.div`
    text-transform: uppercase;
    margin-bottom: ${megaHeadingMarginBottom}px;
`
