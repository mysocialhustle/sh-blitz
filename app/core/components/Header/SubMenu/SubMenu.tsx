import React from 'react'
import * as Styled from './SubMenu.styles'
import { Link } from 'blitz'
import withMega from './withMega'

export default function SubMenu(props) {
    const MenuItems = (props) => {
        const attrs = props.index
            ? {
                  id: `submenu-${props.index}`,
                  className: `submenu ${props.active ? `active` : ``}`,
              }
            : null
        return (
            <Styled.SubMenu data-height={props.height} {...attrs}>
                {props?.items?.map(({ link, name, icon: Icon }, index) => (
                    <li key={index}>
                        {Icon && <Icon />}
                        <Link href={link}>{name}</Link>
                    </li>
                ))}
            </Styled.SubMenu>
        )
    }

    return props?.items?.length > 0 ? withMega(MenuItems, props) : null
}
