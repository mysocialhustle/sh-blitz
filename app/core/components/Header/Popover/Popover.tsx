import React, { useMemo, useCallback } from 'react'
import * as Styled from './Popover.styles'
import SubMenu from '../SubMenu/SubMenu'
import { Menu, ActiveMenuItem, DimensionObject } from 'types'
import usePrevious from 'app/core/hooks/usePrevious'
import variants from './Popover.variants'
import hasValue from 'app/core/utils/hasValue'
import { AnimatePresence } from 'framer-motion'
import { popoverHeight } from '../Header.constants'
import defaultDimensions from 'app/core/constants/defaultDimensions'

interface Props {
    menu: Menu
    activeItem: ActiveMenuItem | null
    navDimensions: DimensionObject
}

export default function Popover({ menu, activeItem, navDimensions }: Props) {
    const prevActiveItem = usePrevious(activeItem)

    const activeChildDimensions = useMemo(() => {
        if (typeof document !== 'undefined') {
            const { index } = activeItem
                ? activeItem
                : prevActiveItem
                ? prevActiveItem
                : { index: 0 }

            const node = document.getElementById(`submenu-${index}`)
            if (node) {
                return {
                    height: node.dataset.height
                        ? parseInt(node.dataset.height)
                        : 0,
                    width: node.getBoundingClientRect().width,
                }
            }
        }
        return defaultDimensions
    }, [activeItem, prevActiveItem])

    const animateValues = useMemo(() => {
        if (navDimensions) {
            const baseDimensions = {
                height: popoverHeight,
                width: navDimensions?.width || 0,
            }
            if (activeItem || prevActiveItem) {
                const { x, width } = activeItem
                    ? activeItem
                    : prevActiveItem
                    ? prevActiveItem
                    : defaultDimensions
                const { x: navX, width: navWidth } = navDimensions
                const { height: childHeight, width: childWidth } =
                    activeChildDimensions

                if (
                    x &&
                    width &&
                    navX &&
                    navWidth &&
                    activeChildDimensions &&
                    childHeight &&
                    childWidth
                ) {
                    const diff = childWidth - navWidth
                    const offset = Math.max(diff / 2, 0)

                    return {
                        arrowX: x + width / 2 - navX,
                        boxX: x - navX - offset,
                        scaleX: childWidth / baseDimensions.width,
                        scaleY: childHeight / baseDimensions.height,
                        height: childHeight,
                        width: childWidth,
                    }
                }
            }
        }
        return {
            arrowX: 0,
            boxX: 0,
            scaleX: 0,
            scaleY: 0,
            height: 0,
            width: 0,
        }
    }, [activeItem, navDimensions, prevActiveItem, activeChildDimensions])

    return (
        <Styled.PopOver
            key="popover"
            custom={activeItem ? true : false}
            variants={variants.popover}
            animate="animate"
            initial="initial"
            exit="initial"
            transition={{
                duration: 0.3,
                ease: [0.25, 0.1, 0.25, 1],
            }}
        >
            <Styled.PopOverContent
                id="popover-content"
                variants={variants.content}
                custom={[
                    animateValues.boxX,
                    animateValues.width,
                    animateValues.height,
                ]}
                animate="animate"
                initial={false}
                transition={{
                    duration: 0.3,
                    ease: 'linear',
                }}
            >
                {menu.map((item, index) => {
                    return (
                        <SubMenu
                            index={index}
                            key={`submenu-${index}`}
                            items={item.submenu}
                            type={item.type}
                            active={activeItem && activeItem.index === index}
                        />
                    )
                })}
            </Styled.PopOverContent>
            <Styled.PopOverBackground
                id="popover-background"
                variants={variants.background}
                custom={[
                    animateValues.boxX,
                    animateValues.scaleX,
                    animateValues.scaleY,
                ]}
                animate="animate"
                initial="initial"
                exit="initial"
                transition={{
                    duration: 0.3,
                    ease: 'linear',
                }}
            />
            <Styled.PopOverArrow
                id="popover-arrow"
                variants={variants.arrow}
                custom={animateValues.arrowX}
                animate="animate"
                initial="initial"
                exit="initial"
                transition={{
                    duration: 0.3,
                    ease: 'linear',
                }}
            />
        </Styled.PopOver>
    )
}
