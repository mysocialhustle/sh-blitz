const variants = {
    popover: {
        initial: {
            zIndex: -1,
            rotateX: -15,
            opacity: 0,
        },
        animate: (active) => ({
            zIndex: active ? 100 : -1,
            rotateX: active ? 0 : -15,
            opacity: active ? 1 : 0,
        }),
    },
    arrow: {
        initial: (x) => ({
            rotate: 45,
            x: x,
        }),
        animate: (x) => ({
            rotate: -45,
            x: x,
        }),
    },
    background: {
        initial: ([x, scaleX, scaleY]) => ({
            scaleX: 1,
            scaleY: 1,
            x: x,
        }),
        animate: ([x, scaleX, scaleY]) => ({
            scaleX: scaleX,
            scaleY: scaleY,
            x: x,
        }),
    },
    content: {
        animate: ([x, width, height]) => ({
            width: width,
            height: height,
            x: x,
        }),
    },
}

export default variants
