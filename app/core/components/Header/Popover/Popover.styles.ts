import styled from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import fonts from 'app/core/constants/fonts'
import { mainFontSize } from '../Header.constants'
import { motion } from 'framer-motion'
import { popoverHeight } from '../Header.constants'

export const PopOver = styled(motion.div)`
    position: absolute;
    left: 0;
    right: 0;
    transform-origin: center 0px;
    display: inline-block;
    margin-top: 1.5em;
    z-index: -1;
`

export const PopOverContent = styled(motion.div)`
    position: absolute;
    overflow: hidden;
    z-index: 1;
    top: 5px;
    left: 0;
`

export const PopOverBackground = styled(motion.div)`
    position: absolute;
    top: 5px;
    left: 0;
    background: ${(props) => props.theme.color};
    border-radius: 6px;
    box-shadow: 0 50px 100px -20px rgba(50, 50, 93, 0.25),
        0 30px 60px -30px rgba(0, 0, 0, 0.3);
    transform-origin: 0 0;
    width: 100%;
    height: ${popoverHeight}px;
`
export const PopOverArrow = styled(motion.div)`
    position: absolute;
    top: 0px;
    left: -6px;
    width: 12px;
    height: 12px;
    margin-left: -6px;
    background: ${(props) => props.theme.color};
    box-shadow: -3px -3px 5px rgba(80, 90, 120, 0.05);
    border-radius: 4px 0 0 0;
    will-change: transform;
`
