import React from 'react'
import * as Styled from './Hamburger.styles'

const Hamburger = ({ isOpen = false, setOpen }) => {
    return (
        <Styled.Hamburger onClick={() => setOpen(!isOpen)}>
            <Styled.Bar active={isOpen} />
        </Styled.Hamburger>
    )
}

export default Hamburger
