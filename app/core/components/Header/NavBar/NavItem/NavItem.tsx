import React, { useEffect, useCallback, useRef } from 'react'
import * as Styled from './NavItem.styles'
import { Link } from 'blitz'
import { MenuItem } from 'types'
import useDimensions from 'app/core/hooks/useDimensions'
import { ActiveMenuItem } from 'types'
import { motion } from 'framer-motion'

interface Props extends MenuItem {
    index: number
    setActive: (val: ActiveMenuItem | null) => void
}

export default function NavItem({
    link = null,
    name,
    type,
    index,
    setActive,
}: Props) {
    const [ref, dimensions] = useDimensions()

    if (link != null) {
        if (type === 'external') {
            return (
                <Styled.NavItem
                    as={motion.a}
                    href={link}
                    target="_blank"
                    style={{ cursor: 'pointer' }}
                    onHoverStart={() => setActive(null)}
                >
                    {name}
                </Styled.NavItem>
            )
        } else {
            return (
                <Styled.NavItem
                    ref={ref}
                    onHoverStart={() =>
                        setActive({ ...dimensions, index: index })
                    }
                >
                    <Link href={link}>{name}</Link>
                </Styled.NavItem>
            )
        }
    } else {
        return (
            <Styled.NavItem
                ref={ref}
                onHoverStart={() => setActive({ ...dimensions, index: index })}
            >
                {name}
            </Styled.NavItem>
        )
    }
}
