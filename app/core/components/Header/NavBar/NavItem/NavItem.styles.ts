import styled from 'styled-components'
import { motion } from 'framer-motion'

export const NavItem = styled(motion.span)`
    align-items: center;
    display: flex;
    height: 100%;
    position: relative;
    padding: 5px 15px;
    cursor: default;
`
