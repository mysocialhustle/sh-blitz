import React, { useState } from 'react'
import * as Styled from './NavBar.styles'
import NavItem from './NavItem/NavItem'
import { Menu, ActiveMenuItem } from 'types'
import useDimensions from 'app/core/hooks/useDimensions'
import Popover from '../Popover/Popover'

interface Props {
    menu: Menu
}

export default function NavBar({ menu }: Props) {
    const [activeItem, setActiveItem] = useState<ActiveMenuItem | null>(null)
    const [ref, dimensions] = useDimensions()

    const baseDimensions = {
        height: 150,
        width: dimensions.width,
    }

    return (
        <Styled.Nav
            ref={ref}
            role="navigation"
            aria-label="Top Menu"
            id="mainNav"
            onHoverEnd={() => setActiveItem(null)}
        >
            {menu.map((item, index) => {
                return (
                    <NavItem
                        index={index}
                        key={`navitem-${index}`}
                        setActive={(e) => setActiveItem(e)}
                        {...item}
                    />
                )
            })}
            <Popover
                activeItem={activeItem}
                menu={menu}
                navDimensions={dimensions}
            />
        </Styled.Nav>
    )
}
