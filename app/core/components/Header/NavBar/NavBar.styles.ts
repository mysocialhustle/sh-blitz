import styled from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import fonts from 'app/core/constants/fonts'
import { mainFontSize } from '../Header.constants'
import { motion } from 'framer-motion'

export const Nav = styled(motion.nav)`
    align-items: center;
    display: none;
    height: 100%;
    position: relative;
    white-space: nowrap;
    perspective: 2000px;
    font-family: ${fonts.headings};
    font-size: ${mainFontSize}px;
    font-weight: 700;
    letter-spacing: 0.025em;
    text-transform: uppercase;
    color: ${(props) => props.theme.main};
    line-height: 1.1em;

    ${mediaQueries.md} {
        display: flex;
    }
`

export const PopOver = styled.div`
    position: absolute;
    left: 0;
    right: 0;
    transform-origin: center 0px;
    display: inline-block;
    margin-top: 1.5em;
    opacity: 0;
    transform: rotateX(-15deg);
    transition: transform 0.3s, opacity 0.3s;
    z-index: 0;
    visibility: hidden;

    &.open {
        opacity: 1;
        transform: rotateX(0);
        z-index: 100;
        visibility: visible;
    }
`

export const PopOverContent = styled(motion.div)`
    position: absolute;
    overflow: hidden;
    z-index: 1;
    top: 5px;
    left: 0;
`

export const PopOverBackground = styled(motion.div)`
    position: absolute;
    top: 5px;
    left: 0;
    background: ${(props) => props.theme.main};
    border-radius: 6px;
    box-shadow: 0 50px 100px -20px rgba(50, 50, 93, 0.25),
        0 30px 60px -30px rgba(0, 0, 0, 0.3);
    transform-origin: 0 0;
    width: 100%;
    height: 150px;
`
export const PopOverArrow = styled(motion.div)`
    position: absolute;
    top: 0px;
    left: -6px;
    width: 12px;
    height: 12px;
    margin-left: -6px;
    background: ${(props) => props.theme.main};
    box-shadow: -3px -3px 5px rgba(80, 90, 120, 0.05);
    border-radius: 4px 0 0 0;
    transform: rotate(45deg);
    will-change: transform;
`
