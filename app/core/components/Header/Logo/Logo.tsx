import React from 'react'
import LogoSvg from 'app/svg/logo.svg'
import { Link } from 'blitz'
import * as Styled from './Logo.styles'

export default function Logo() {
    return (
        <Link href="/" passHref>
            <Styled.Logo>
                <LogoSvg />
            </Styled.Logo>
        </Link>
    )
}
