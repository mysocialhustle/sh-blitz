import styled from 'styled-components'

export const Logo = styled.a`
    display: inline-block;
    svg {
        fill: ${(props) => props.theme.color};
        height: 70px;
        width: auto;
    }
`
