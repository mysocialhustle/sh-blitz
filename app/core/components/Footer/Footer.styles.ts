import styled from 'styled-components'
import { StyledProps } from 'types'
import { Column as GridColumn } from 'app/styles/grid'

export const Footer = styled.footer`
    position: relative;
    display: inline-block;
    width: 100%;
    vertical-align: top;
    color: #ffffff;
    font-size: 13px;
    padding: 50px 0 0;
    background-color: #000;
`

export const ResponsiveIframe = styled.div`
    position: relative;
    margin-bottom: 10px;
`

export const SocialButton = styled.a.attrs({
    target: '_blank',
})<StyledProps>`
    display: inline-block;
    vertical-align: top;
    zoom: 1;
    padding: 7px 14px;
    margin: 5px auto 0 auto;
    background: ${(props) => props.backgroundColor};
    color: #fff;
    text-decoration: none;
    font-size: 13px;
    line-height: 1.5;
    border-radius: 4px;
    transition: opacity 0.2s ease;
    margin-top: 20px;

    svg {
        margin-right: 10px;
    }

    &:hover {
        opacity: 0.8;
    }
`

export const Column = styled(GridColumn)`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-bottom: 50px;
`

export const Contacts = styled.ul`
    font-size: 14px;
    line-height: 2;

    li {
        margin-bottom: 10px;
    }
`
