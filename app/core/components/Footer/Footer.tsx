import React from 'react'
import * as Styled from './Footer.styles'
import { Container, Row, Column } from 'app/styles/grid'
import siteMetadata from 'app/data/siteMetadata'
import Icon from '../Icon/Icon'
import Headings from 'app/styles/headings'
import Videos from 'app/data-feeds/components/YouTube/Videos/Videos'
import {
    PlaceholderGrid,
    Grid,
    Feed,
} from 'app/data-feeds/components/Instagram'
import SocialIcons from 'app/core/blocks/SocialIcons/SocialIcons'
import Newsletter from 'app/core/blocks/Forms/Newsletter'

export default function Footer() {
    const { contact, socials } = siteMetadata
    const phoneLink = `tel:${contact.phone}`
    const mailLink = `mailto:${contact.email}`

    return (
        <Styled.Footer>
            <Container>
                <Row alignY="stretch">
                    <Column sm={25} md={22}>
                        <Headings.H4>Get In Touch</Headings.H4>
                        <Styled.Contacts>
                            <li>
                                {contact.address.street}
                                <br />
                                {contact.address.city}, {contact.address.state}{' '}
                                {contact.address.zip}
                            </li>
                            <li>
                                <a href={phoneLink}>{contact.phone}</a>
                            </li>
                            <li>
                                <a href={mailLink}>{contact.email}</a>
                            </li>
                            <SocialIcons el="li" />
                        </Styled.Contacts>
                    </Column>
                    <Styled.Column sm={25} md={26}>
                        <Headings.H4>SH YouTube</Headings.H4>
                        <Videos />
                        <Styled.SocialButton
                            backgroundColor="#ff0000"
                            href={socials.youtube}
                            title="Subscribe to Social Hustle on YouTube"
                        >
                            <Icon name="youtube" />
                            <span>Subscribe on YouTube</span>
                        </Styled.SocialButton>
                    </Styled.Column>
                    <Styled.Column sm={25} md={26}>
                        <Headings.H4>SH Instagram</Headings.H4>
                        <Feed
                            Component={Grid}
                            Placeholder={PlaceholderGrid}
                            props={{ xs: 50, md: 33, count: 6 }}
                        />
                        <Styled.SocialButton
                            backgroundColor="linear-gradient(45deg, #c32aa3, #f46f30)"
                            href={socials.instagram}
                            title="Follow Social Hustle on Instagram"
                        >
                            <Icon name="instagram" />
                            <span>Follow on Instagram</span>
                        </Styled.SocialButton>
                    </Styled.Column>
                    <Column sm={25} md={26}>
                        <Headings.H4>Newsletter</Headings.H4>
                        <p>
                            Enter your email to recieve the Social Hustle
                            newsletter once per month. This newsletter provides
                            free business and marketing resources, changes in
                            industries trends, content from around the world,
                            and even the occasional giveaway.
                        </p>
                        <Newsletter />
                    </Column>
                </Row>
            </Container>
        </Styled.Footer>
    )
}
