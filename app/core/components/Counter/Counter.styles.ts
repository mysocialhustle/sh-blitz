import styled from 'styled-components'
import { StyledProps } from 'types'
import { motion } from 'framer-motion'

export const Digit = styled(motion.div)<StyledProps>`
    font-weight: 900;
    line-height: 1.1;
    word-wrap: break-word;
    font-size: ${(props) => props.size}em;
    line-height: 1;
    margin: 0;
`
