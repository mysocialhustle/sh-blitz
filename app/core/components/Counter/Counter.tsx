import React, { useEffect, useRef, useState } from 'react'
import * as Styled from './Counter.styles'
import Digit from './Digit'
import { CounterSize, Counter as CounterProps } from 'types/counters'

// https://stackoverflow.com/questions/60205544/in-framer-motion-how-to-animate-pure-data-like-a-number-from-0-to-100
export default function Counter({
    start = 0,
    digit,
    size = 'medium',
    textBefore = '',
    textAfter = '',
    style,
    round = 0,
    duration = 1.5,
}: CounterProps) {
    const [inView, setInView] = useState(false)
    return (
        <Styled.Digit
            onViewportEnter={() => setInView(true)}
            size={CounterSize[size]}
            style={style}
        >
            <span>{textBefore}</span>
            <Digit
                from={start}
                to={digit}
                inView={inView}
                duration={duration}
                round={round}
            />
            <span>{textAfter}</span>
        </Styled.Digit>
    )
}
