import React, { useEffect, useRef } from 'react'
import { animate } from 'framer-motion'

export default function Digit({ from, to, inView, duration, round }) {
    const ref = useRef<any>(null)

    useEffect(() => {
        if (ref && ref.current) {
            const node = ref.current

            if (inView) {
                const controls = animate(from, to, {
                    duration: duration,
                    onUpdate(value) {
                        node.textContent = value.toFixed(round)
                    },
                })

                return () => controls.stop()
            }
        }
    }, [from, to, inView, ref, duration, round])

    return <span ref={ref} />
}
