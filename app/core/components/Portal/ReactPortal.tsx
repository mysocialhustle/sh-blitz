import { useState, useEffect } from 'react'
import { createPortal } from 'react-dom'

// Src: https://blog.logrocket.com/build-modal-with-react-portals/

function createWrapperAndAppendToBody(target, wrapperId) {
    const wrapperElement = document.createElement('div')
    wrapperElement.setAttribute('id', wrapperId)
    target.prepend(wrapperElement)
    return wrapperElement
}

export default function ReactPortal({
    children,
    wrapperId = 'react-portal-wrapper',
    targetEl = null,
}) {
    const [wrapperElement, setWrapperElement] = useState<HTMLElement | null>(
        null
    )

    useEffect(() => {
        let element = document.getElementById(wrapperId)
        let systemCreated = false
        // if element is not found with wrapperId or wrapperId is not provided,
        // create and append to body
        if (typeof document !== 'undefined') {
            const target = targetEl ? targetEl : document.body
            if (!element) {
                systemCreated = true
                element = createWrapperAndAppendToBody(target, wrapperId)
            }
            setWrapperElement(element)

            return () => {
                // delete the programatically created element
                if (systemCreated && element?.parentNode) {
                    element.parentNode.removeChild(element)
                }
            }
        }
    }, [wrapperId, targetEl])

    if (wrapperElement) {
        return createPortal(children, wrapperElement)
    }

    return null
}
