import styled from 'styled-components'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Box = styled.div`
        border: 1px solid ${colors.lightGrey};
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
        padding: 20px;
        overflow: hidden;
        transition: all 0.4s ease-in;
        background-color: white;
        display: flex;
        position: relative;
        height: 100%;
        flex-direction: column;
        justify-content: space-between;

        a {
            position: absolute;
            inset: 0;
        }

        .arrow {
            stroke: currentColor;
            height: 1.5em;
            display: block;
            width: auto;
            margin-left: auto;
            transition: stroke 0.4s ease-in;
        }

        &:hover {
            border-color: ${colors.red};
            color: white;
            background-color: black;

            .arrow {
                stroke: ${colors.red};
            }
        }
    `
}
