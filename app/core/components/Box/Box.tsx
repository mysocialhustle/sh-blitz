import { Styled } from './Box.styles'
import { Link } from 'blitz'
import Arrow from 'app/svg/arrow-right.inline.svg'

export default function Box({ children, href }) {
    return (
        <Styled.Box>
            <Link href={href}>
                <a />
            </Link>
            {children}
            <Arrow className="arrow" />
        </Styled.Box>
    )
}
