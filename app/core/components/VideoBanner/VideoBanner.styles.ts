import styled, { keyframes } from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import headerHeight from 'app/core/constants/headerHeight'
import { gutter, maxWidth } from 'app/core/constants/grid'
import { StyledProps } from 'types'

const slideUp1 = keyframes`
  from {
    transform: translateY(50%);
  }

  to {
    transform: translateY(0%);
  }
`

const slideUp2 = keyframes`
  from {
    transform: translateY(100%);
  }

  to {
    transform: translateY(0%);
  }
`
const slideUp3 = keyframes`
  from {
    transform: translateY(-100%);
  }

  to {
    transform: translateY(0%);
  }
`

export const Slider = styled.div`
    width: 100%;
    overflow: hidden;
    position: relative;
    margin-top: ${headerHeight}px;
    padding: 0px ${gutter}px;

    ${mediaQueries.sm} {
        padding: 0px;
        height: calc(75vw - ${headerHeight}px);
    }

    ${mediaQueries.md} {
        height: calc(100vh - ${headerHeight}px);
    }
`

export const Slide = styled.div<StyledProps>`
    position: relative;
    padding-bottom: 56.25%;
    width: 100%;
    background: url(${(props) => props.backgroundImage}) scroll no-repeat center
        center/cover;

    ${mediaQueries.sm} {
        padding-bottom: 42.1875vw;
        width: 75vw;
        position: absolute;
        top: 50%;
        left: 25vw;
        z-index: 1;
        transform: translateY(-50%);
    }

    &:after {
        background-color: rgba(0, 0, 0, 0.35);
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1;
        content: '';
    }
`

export const Player = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;

    video {
        position: absolute;
        top: 0px;
        left: 0px;
        object-fit: cover;
        object-position: center center;
        width: 100%;
        height: 100%;
    }
`

export const Heading = styled.h1`
    font-weight: 800;
    letter-spacing: -0.05em;
    font-family: 'Inter', sans-serif;
    line-height: 1.1em;
    word-wrap: break-word;
    font-size: 4rem;
    margin: 40px 0;
    animation: ${slideUp1} 1.2s cubic-bezier(0.4, 0.1, 0.355, 1) 0.5s;
    animation-fill-mode: both;

    ${mediaQueries.sm} {
        font-size: 6rem;
        margin: 0px;
    }

    ${mediaQueries.md} {
        font-size: 8rem;
    }

    .animation__mask {
        animation: ${slideUp2} 1s cubic-bezier(0.54, 0.005, 0.155, 0.99) 0.5s;
        animation-fill-mode: both;
        overflow: hidden;
    }

    .animation__el {
        animation: ${slideUp3} 1s cubic-bezier(0.54, 0.005, 0.155, 0.99) 0.5s;
        animation-fill-mode: both;
    }
`

export const Content = styled.div`
    overflow: hidden;

    ${mediaQueries.sm} {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        padding: 0px ${gutter}px;
        user-select: none;
    }

    @media (min-width: ${maxWidth}px) {
        padding: 0 calc((100vw - ${maxWidth}px) / 2 + ${gutter}px);
    }
`

export const TextOverlay = styled(Heading).attrs({ as: 'div' })`
    clip-path: inset(0px 0px 0px calc(25vw - ${gutter}px));
    color: white;
    display: none;
    margin: 0px;

    ${mediaQueries.sm} {
        display: block;
    }

    @media (min-width: ${maxWidth}px) {
        clip-path: inset(
            0px 0px 0px calc(25vw - (100vw - ${maxWidth}px) / 2 - ${gutter}px)
        );
    }
`
