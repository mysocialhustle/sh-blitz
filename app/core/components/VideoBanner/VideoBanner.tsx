import React from 'react'
import * as Styled from './VideoBanner.styles'

interface Props {
    mp4: string
    webm?: string | null
    children: React.ReactNode
    poster: string
}

export default function VideoBanner({
    mp4,
    webm = null,
    children,
    poster,
}: Props) {
    return (
        <Styled.Slider>
            <Styled.Content style={{ zIndex: 2 }}>
                <Styled.TextOverlay>{children}</Styled.TextOverlay>
            </Styled.Content>
            <Styled.Content>
                <Styled.Heading>{children}</Styled.Heading>
            </Styled.Content>
            <Styled.Slide backgroundImage={poster}>
                <Styled.Player>
                    <video
                        muted
                        autoPlay={true}
                        poster={poster}
                        loop
                        playsInline
                        preload="none"
                    >
                        <source src={mp4} type="video/mp4" />
                        {webm && <source src={webm} type="video/webm" />}
                    </video>
                </Styled.Player>
            </Styled.Slide>
        </Styled.Slider>
    )
}
