const variants = {
    main: {
        initial: {
            y: 0,
        },
        animate: {
            y: 0,
        },
    },
    wrapper: {
        initial: {
            y: `50%`,
        },
        animate: (delay) => ({
            y: 0,
            transition: {
                delay: delay,
                duration: 1.2,
                ease: [0.4, 0.1, 0.355, 1],
            },
        }),
    },
    mask: {
        initial: {
            y: `100%`,
        },
        animate: (delay) => ({
            y: 0,
            transition: {
                delay: delay,
                duration: 1,
                ease: [0.54, 0.005, 0.155, 0.99],
            },
        }),
    },
    element: {
        initial: {
            y: `-100%`,
        },
        animate: (delay) => ({
            y: 0,
            transition: {
                delay: delay,
                duration: 1,
                ease: [0.54, 0.005, 0.155, 0.99],
            },
        }),
    },
}

export default variants
