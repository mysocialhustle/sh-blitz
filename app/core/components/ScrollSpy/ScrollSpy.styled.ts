import styled from 'styled-components'
import { motion } from 'framer-motion'

export const ScrollSpy = styled(motion.div)`
    position: relative;
`

export const Wrapper = styled(motion.div)`
    transform: translateY(50%);
`

export const Mask = styled(motion.div)`
    overflow: hidden;
    transform: translateY(100%);
`

export const Element = styled(motion.div)`
    transform: translateY(-100%);
`
