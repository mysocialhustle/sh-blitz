import React from 'react'
import * as Styled from './ScrollSpy.styled'
import variants from './ScrollSpy.variants'

export default function ScrollSpy({ children, delay = 0 }) {
    return (
        <Styled.ScrollSpy
            variants={variants.main}
            initial="initial"
            whileInView="animate"
            viewport={{ once: true }}
        >
            <Styled.Wrapper variants={variants.wrapper} custom={delay}>
                <Styled.Mask variants={variants.mask} custom={delay}>
                    <Styled.Element variants={variants.element} custom={delay}>
                        {children}
                    </Styled.Element>
                </Styled.Mask>
            </Styled.Wrapper>
        </Styled.ScrollSpy>
    )
}
