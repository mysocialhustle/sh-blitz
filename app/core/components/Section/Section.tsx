import React from 'react'
import { ThemeProvider } from 'styled-components'
import themes from 'app/core/constants/themes'
import * as Styled from './Section.styles'
import { ThemeType } from 'types'

enum Size {
    min = 0,
    tiny = 50,
    small = 100,
    medium = 150,
    large = 200,
}

interface Props {
    size?: string
    children: React.ReactNode
    theme?: ThemeType
    style?: React.CSSProperties
    id?: string
}

export default function Section({
    size = 'medium',
    theme = 'light',
    children,
    ...rest
}: Props) {
    const providedTheme = themes[theme]

    return (
        <ThemeProvider theme={providedTheme}>
            <Styled.Section size={Size[size]} {...rest}>
                {children}
            </Styled.Section>
        </ThemeProvider>
    )
}
