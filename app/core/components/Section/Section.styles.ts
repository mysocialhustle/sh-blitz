import styled from 'styled-components'
import { StyledProps } from 'types'

export const Section = styled.section<StyledProps>`
    position: relative;
    padding-top: ${(props) => props.size}px;
    padding-bottom: ${(props) => props.size}px;
    background-color: ${(props) => props.theme.background};
    color: ${(props) => props.theme.color};
`
