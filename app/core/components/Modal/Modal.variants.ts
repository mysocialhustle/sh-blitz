const variants = {
    wrapper: {
        initial: {
            opacity: 0,
            scale: 0.4,
        },
        animate: {
            opacity: 1,
            scale: 1,
        },
        exit: {
            opacity: 0,
            scale: 0.4,
        },
    },
    overlay: {
        initial: {
            opacity: 0,
        },
        animate: {
            opacity: 0.9,
        },
        exit: {
            opacity: 0,
        },
    },
}

export default variants
