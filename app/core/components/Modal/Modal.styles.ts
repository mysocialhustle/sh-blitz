import styled from 'styled-components'
import { maxWidth } from 'app/core/constants/grid'
import { motion } from 'framer-motion'
import colors from 'app/core/constants/colors'
import zIndices from 'app/core/constants/zIndices'

export const ModalWrapper = styled(motion.div)`
    position: fixed;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    transform-origin: center center;
    z-index: ${zIndices.modalWrapper};
`

export const Overlay = styled(motion.div)`
    background: rgba(0, 0, 0, 0.6);
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 0px;
    z-index: ${zIndices.modal};
`

export const Modal = styled.div`
    margin: 0 auto;
    width: 100%;
    max-width: ${maxWidth}px;
    position: relative;
    z-index: 2;
`
