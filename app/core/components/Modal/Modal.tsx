import { useEffect } from 'react'
import ReactPortal from '../Portal/ReactPortal'
import { AnimatePresence } from 'framer-motion'
import variants from './Modal.variants'
import * as Styled from './Modal.styles'

export default function Modal({ children, isOpen, handleClose }) {
    useEffect(() => {
        const closeOnEscapeKey = (e) =>
            e.key === 'Escape' ? handleClose() : null
        document.body.addEventListener('keydown', closeOnEscapeKey)
        return () => {
            document.body.removeEventListener('keydown', closeOnEscapeKey)
        }
    }, [handleClose])

    if (typeof document !== 'undefined') {
        return (
            <ReactPortal>
                <AnimatePresence>
                    {isOpen && (
                        <>
                            <Styled.Overlay
                                key="overlay"
                                variants={variants.overlay}
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                transition={{
                                    duration: 0.6,
                                    delay: 0.5,
                                    ease: 'easeInOut',
                                }}
                            />
                            <Styled.ModalWrapper
                                key="modal"
                                variants={variants.wrapper}
                                initial="initial"
                                animate="animate"
                                exit="exit"
                                transition={{
                                    duration: 0.6,
                                    ease: 'easeInOut',
                                }}
                            >
                                <Styled.Modal>{children}</Styled.Modal>
                            </Styled.ModalWrapper>
                        </>
                    )}
                </AnimatePresence>
            </ReactPortal>
        )
    }
    return null
}
