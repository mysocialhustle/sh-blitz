import { Styled } from './Banner.styles'
import { Image } from 'blitz'
import { ThemeType } from 'types'
import { ThemeProvider } from 'styled-components'
import themes from 'app/core/constants/themes'

interface Props {
    title: string
    image: {
        src: string
        alt?: string | null
    }
    theme?: ThemeType
}

export default function Banner({ title, image, theme = 'dark' }: Props) {
    return (
        <ThemeProvider theme={themes[theme]}>
            <Styled.Banner>
                <Styled.Title>{title}</Styled.Title>
                <Image
                    src={image.src}
                    alt={image.alt ? image.alt : title}
                    layout="fill"
                    objectFit="cover"
                    objectPosition={`center center`}
                />
            </Styled.Banner>
        </ThemeProvider>
    )
}
