import styled from 'styled-components'
import Headings from 'app/styles/headings'

export namespace Styled {
    export const Banner = styled.div`
        padding: min(200px, 20vh) 0;
        overflow: hidden;
        position: relative;
        text-align: center;
        color: ${(props) => props.theme.color};
        background-color: ${(props) => props.theme.background};

        img {
            opacity: 0.5;
        }
    `

    export const Title = styled(Headings.H1)`
        margin-bottom: 0px;
        position: relative;
        z-index: 1;
    `
}
