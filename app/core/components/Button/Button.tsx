import React from 'react'
import * as Styled from './Button.styles'
import { Link } from 'blitz'
import colors from '../../constants/colors'
import { ButtonProps } from 'types'

export default function Button<T extends unknown>({
    children,
    href,
    element = 'link',
    color = '#ffffff',
    backgroundColor = '#000000',
    hoverColor = '#000000',
    hoverBackgroundColor = colors.red,
    centered = false,
    onClick,
    style,
    ...props
}: ButtonProps<T>) {
    const allProps = {
        color: color,
        backgroundColor: backgroundColor,
        hoverColor: hoverColor,
        hoverBackgroundColor: hoverBackgroundColor,
        centered: centered,
        onClick,
        style,
        ...props,
    }
    return href ? (
        <Link href={href} passHref>
            <Styled.Button as="a" {...allProps}>
                <span>{children}</span>
            </Styled.Button>
        </Link>
    ) : (
        <Styled.Button as={element} {...allProps}>
            <span>{children}</span>
        </Styled.Button>
    )
}
