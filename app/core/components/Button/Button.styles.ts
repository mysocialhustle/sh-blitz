import styled from 'styled-components'
import { StyledProps } from 'types'

export const Button = styled.a<StyledProps>`
    display: flex;
    position: relative;
    align-items: center;
    justify-content: center;
    max-width: 300px;
    min-width: 100px;
    padding: 18px 6px;
    border: 0;
    background-color: ${(props) => props.backgroundColor};
    transition: all 0.3s;
    overflow: hidden;
    cursor: pointer;
    color: ${(props) => props.color};
    width: 100%;

    ${(props) => props.centered && `margin-left: auto; margin-right: auto;`}

    &:disabled {
        opacity: 0.75;
        filter: grayscale(1);
        pointer-events: none;
    }

    span {
        padding-left: 1%;
        padding-right: 1%;
        background-image: ${(props) =>
            `linear-gradient(90deg,${props.color} 50%,${props.hoverColor} 0)`};
        background-size: 200% 100%;
        transition: background-position 0.2s;
        -webkit-background-clip: text;
        background-clip: text;
        -webkit-text-fill-color: transparent;
        text-fill-color: transparent;
        background-position: 200% 0;
        position: relative;
        z-index: 1;
        font-weight: 700;
        font-size: 0.625rem;
        letter-spacing: 2.5px;
        text-transform: uppercase;
    }

    &:before,
    &:after {
        content: '';
        width: 100%;
        height: auto;
        background-color: ${(props) => props.hoverBackgroundColor};
        position: absolute;
        top: 0;
        display: block;
        transition: transform 0.3s linear;
        bottom: 0;
        left: 0;
        z-index: 0;
    }

    &:before {
        visibility: hidden;
        transform: translateX(-100%);
    }

    &:after {
        visibility: visible;
        transition: transform 0.15s linear 0.2s;
        transform: translateX(100%);
    }

    &:hover {
        &:before {
            transform: translateX(0);
            visibility: visible;
        }

        &:after {
            transition: transform 0.5s;
            transform: translateX(0);
            visibility: hidden;
        }

        span {
            background-position: 100% 0;
            transition-delay: 0.3s;
        }
    }
`
