import { Styled } from './Accordion.styles'
import { useState } from 'react'
import { AnimatePresence, motion } from 'framer-motion'
import ChevronDown from 'app/svg/chevron-down.inline.svg'
import colors from 'app/core/constants/colors'

export default function Accordion({ title, children }) {
    const [expanded, setExpanded] = useState(false)

    // By using `AnimatePresence` to mount and unmount the contents, we can animate
    // them in and out while also only rendering the contents of open accordions
    return (
        <>
            <Styled.Header
                initial={false}
                animate={{ color: expanded ? colors.red : '#000000' }}
                onClick={() => setExpanded(!expanded)}
            >
                {title}
                <motion.div
                    animate={{
                        rotate: expanded ? '-180deg' : '0deg',
                    }}
                    transition={{ duration: 0.6, ease: 'linear' }}
                >
                    <ChevronDown className="icon" />
                </motion.div>
            </Styled.Header>
            <AnimatePresence initial={false}>
                {expanded && (
                    <Styled.Body
                        key="content"
                        initial="collapsed"
                        animate="open"
                        exit="collapsed"
                        layout
                        variants={{
                            open: {
                                opacity: 1,
                                height: 'auto',
                                overflow: `visible`,
                            },
                            collapsed: {
                                opacity: 0,
                                height: 0,
                                overflow: `hidden`,
                            },
                        }}
                        transition={{
                            duration: 0.8,
                            ease: [0.04, 0.62, 0.23, 0.98],
                        }}
                    >
                        <Styled.Content
                            variants={{
                                collapsed: { scale: 0.95 },
                                open: { scale: 1 },
                            }}
                            transition={{ duration: 0.8 }}
                        >
                            {children}
                        </Styled.Content>
                    </Styled.Body>
                )}
            </AnimatePresence>
        </>
    )
}
