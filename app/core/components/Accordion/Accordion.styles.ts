import styled from 'styled-components'
import { motion } from 'framer-motion'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Header = styled(motion.header)`
        display: flex;
        justify-content: space-between;
        padding: 20px 0;
        cursor: pointer;
        border-bottom: 1px solid ${colors.lightGrey};
        font-size: 1.125rem;
        text-transform: uppercase;
        font-weight: 900;
        user-select: none;
    `

    export const Body = styled(motion.div)``

    export const Content = styled(motion.div)`
        padding: 20px 0;
        transform-origin: top center;
    `
}
