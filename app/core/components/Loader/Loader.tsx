import colors from 'app/core/constants/colors'

export default function Loader({ color = colors.lightGrey, ...props }) {
    return (
        <svg
            height="50px"
            width="60px"
            x="0px"
            y="0px"
            viewBox="0 0 60 20"
            enableBackground="new 0 0 0 0"
            {...props}
        >
            <circle fill={color} stroke="none" cx="6" cy="10" r="6">
                <animateTransform
                    attributeName="transform"
                    dur="1s"
                    type="translate"
                    values="0 15 ; 0 -15; 0 15"
                    repeatCount="indefinite"
                    begin="0.1"
                ></animateTransform>
            </circle>
            <circle fill={color} stroke="none" cx="30" cy="10" r="6">
                <animateTransform
                    attributeName="transform"
                    dur="1s"
                    type="translate"
                    values="0 10 ; 0 -10; 0 10"
                    repeatCount="indefinite"
                    begin="0.2"
                ></animateTransform>
            </circle>
            <circle fill={color} stroke="none" cx="54" cy="10" r="6">
                <animateTransform
                    attributeName="transform"
                    dur="1s"
                    type="translate"
                    values="0 5 ; 0 -5; 0 5"
                    repeatCount="indefinite"
                    begin="0.3"
                ></animateTransform>
            </circle>
        </svg>
    )
}
