import React from 'react'
import icons from 'app/core/constants/icons'

export default function Icon({ name, ...rest }) {
    const FetchedIcon = icons[name]

    return <FetchedIcon className="icon" {...rest} />
}
