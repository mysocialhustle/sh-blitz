const zIndices = {
    header: 99,
    modal: 100,
    modalWrapper: 101,
    mobileMenu: 999,
}

export default zIndices
