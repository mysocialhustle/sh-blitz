import breakpoints from "./breakpoints"
import { BreakpointKey } from "types"

//https://willmendesneto.com/posts/easy-and-reusable-media-query-breakpoints-for-your-css-in-js-components/

let mediaQueries: { [key: BreakpointKey | string]: any } = Object.assign({}, breakpoints)

Object.entries(mediaQueries).forEach(([key, val]) => {
  mediaQueries[key] = `@media (min-width: ${val}px)`
})

export default mediaQueries
