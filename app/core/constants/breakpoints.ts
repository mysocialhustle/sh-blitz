import { ResponsiveProp } from "types"

const breakpoints: ResponsiveProp = {
  sm: 768,
  md: 960,
  lg: 1280,
}

export default breakpoints
