import InstagramIcon from 'app/svg/instagram.inline.svg'
import FacebookIcon from 'app/svg/facebook.inline.svg'
import LinkedinIcon from 'app/svg/linkedin.inline.svg'
import YoutubeIcon from 'app/svg/youtube.inline.svg'
import VideoIcon from 'app/svg/video.inline.svg'
import CarouselAlbumIcon from 'app/svg/carousel_album.inline.svg'
import CommentIcon from 'app/svg/comment.inline.svg'
import HeartIcon from 'app/svg/heart.inline.svg'
import GooglePlusIcon from 'app/svg/googleplus.inline.svg'
import HomeIcon from 'app/svg/home-icon.inline.svg'
import ToolsIcon from 'app/svg/tools-icon.inline.svg'
import UserIcon from 'app/svg/account-user.inline.svg'
import GraphIcon from 'app/svg/graph-icon.inline.svg'
import LogoutIcon from 'app/svg/logout-icon.inline.svg'
import ImagesIcon from 'app/svg/images-icon.inline.svg'
import PaperclipIcon from 'app/svg/paperclip-icon.inline.svg'
import WalletIcon from 'app/svg/wallet-icon.inline.svg'
import InvoiceIcon from 'app/svg/invoice-icon.inline.svg'
import CartIcon from 'app/svg/cart-icon.inline.svg'
import CalendarIcon from 'app/svg/calendar-icon.inline.svg'
import WebsiteIcon from 'app/svg/website.inline.svg'
import CompanyIcon from 'app/svg/corporate.inline.svg'
import ChartIcon from 'app/svg/chart.inline.svg'
import EyeOpenIcon from 'app/svg/eye-uncrossed.inline.svg'
import EyeClosedIcon from 'app/svg/eye-crossed.inline.svg'
import TiktokOutlineIcon from 'app/svg/tiktok-outline.inline.svg'
import GraphicDesignOutlineIcon from 'app/svg/graphicdesign-outline.inline.svg'
import FacebookOutlineIcon from 'app/svg/facebook-outline.inline.svg'
import AdwordsOutlineIcon from 'app/svg/adwords-outline.inline.svg'
import SnapchatOutline from 'app/svg/snapchat-outline.inline.svg'
import SeoOutlineIcon from 'app/svg/seo-outline.inline.svg'
import LinkIcon from 'app/svg/link.inline.svg'
import CodeOutlineIcon from 'app/svg/code-outline.inline.svg'
import CameraOutlineIcon from 'app/svg/camera-outline.inline.svg'
import CheckCircleIcon from 'app/svg/check-circle.inline.svg'
import ExCircleIcon from 'app/svg/ex-circle.inline.svg'
import EditIcon from 'app/svg/edit.inline.svg'
import PlusIcon from 'app/svg/plus.inline.svg'
import CloseIcon from 'app/svg/close.inline.svg'
import MinusIcon from 'app/svg/minus.svg'
import DownloadIcon from 'app/svg/download.svg'
import BingOutlineIcon from 'app/svg/bing-outline.inline.svg'
import YoutubeOutlineIcon from 'app/svg/youtube-outline.inline.svg'
import LinkedinOutlineIcon from 'app/svg/linkedin-outline.inline.svg'
import DeleteIcon from 'app/svg/delete.svg'
import BingIcon from 'app/svg/bing.inline.svg'
import SnapchatIcon from 'app/svg/snapchat.inline.svg'
import TikTokIcon from 'app/svg/tiktok.inline.svg'
import AdwordsIcon from 'app/svg/google-adwords.inline.svg'

const icons = {
    cameraOutline: CameraOutlineIcon,
    graphicDesignOutline: GraphicDesignOutlineIcon,
    codeOutline: CodeOutlineIcon,
    seoOutline: SeoOutlineIcon,
    link: LinkIcon,
    facebookOutline: FacebookOutlineIcon,
    snapchatOutline: SnapchatOutline,
    tiktokOutline: TiktokOutlineIcon,
    adwordsOutline: AdwordsOutlineIcon,
    instagram: InstagramIcon,
    facebook: FacebookIcon,
    linkedin: LinkedinIcon,
    youtube: YoutubeIcon,
    video: VideoIcon,
    carousel_album: CarouselAlbumIcon,
    comment: CommentIcon,
    heart: HeartIcon,
    googlePlus: GooglePlusIcon,
    home: HomeIcon,
    tools: ToolsIcon,
    user: UserIcon,
    graph: GraphIcon,
    logout: LogoutIcon,
    images: ImagesIcon,
    paperclip: PaperclipIcon,
    wallet: WalletIcon,
    invoice: InvoiceIcon,
    cart: CartIcon,
    calendar: CalendarIcon,
    website: WebsiteIcon,
    company: CompanyIcon,
    chart: ChartIcon,
    eyeOpen: EyeOpenIcon,
    eyeClosed: EyeClosedIcon,
    checkCircle: CheckCircleIcon,
    exCircle: ExCircleIcon,
    edit: EditIcon,
    plus: PlusIcon,
    close: CloseIcon,
    minus: MinusIcon,
    download: DownloadIcon,
    bingOutline: BingOutlineIcon,
    youtubeOutline: YoutubeOutlineIcon,
    linkedinOutline: LinkedinOutlineIcon,
    delete: DeleteIcon,
    bing: BingIcon,
    adwords: AdwordsIcon,
    snapchat: SnapchatIcon,
    tiktok: TikTokIcon,
}

export default icons
