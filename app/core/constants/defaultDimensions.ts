import { DimensionObject } from 'types'

const defaultDimensions: DimensionObject = {
    width: 0,
    height: 0,
    top: 0,
    left: 0,
    x: 0,
    y: 0,
    right: 0,
    bottom: 0,
}

export default defaultDimensions
