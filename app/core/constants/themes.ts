import colors from './colors'
import { Themes } from 'types'

const themes: Themes = {
    dark: {
        color: '#ffffff',
        background: '#000000',
    },
    light: {
        color: '#000000',
        background: '#ffffff',
    },
    grey: {
        color: '#000000',
        background: colors.offWhite,
    },
    darkGrey: {
        color: '#ffffff',
        background: colors.darkGrey,
    },
    transparentLight: {
        color: '#000000',
        background: 'transparent',
    },
    transparentDark: {
        color: '#ffffff',
        background: 'transparent',
    },
    none: {
        color: 'inherit',
        background: 'transparent',
    },
}

export default themes
