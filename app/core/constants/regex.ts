export const hasLowercase = new RegExp(/(?=.*[a-z])/)

export const hasUppercase = new RegExp(/(?=.*[A-Z])/)

export const hasNumber = new RegExp(/(?=.*[0-9])/)

export const hasSpecialChar = new RegExp(/(?=.*[!@#$%^&*])/)
