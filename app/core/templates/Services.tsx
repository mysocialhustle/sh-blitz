import { dynamic } from 'blitz'
import Hero from 'app/core/sections/ServicePages/Hero/Hero'
import testimonials from 'app/data/testimonials'
import portfolio from 'app/data/portfolio'
import Form from 'app/core/sections/ServicePages/Form/Form'

const Counters = dynamic(
    () => import('app/core/sections/ServicePages/Counters/Counters')
)
const Icons = dynamic(
    () => import('app/core/sections/ServicePages/Icons/Icons')
)
const Testimonials = dynamic(
    () => import('app/core/sections/Testimonials/Testimonials')
)
const Portfolio = dynamic(() => import('app/core/sections/Portfolio/Portfolio'))

export default function ServiceTemplate({
    page,
    CustomComponent = null,
}: {
    page: any
    CustomComponent?: any
}) {
    const relatedTestimonials = testimonials.filter((item) =>
        item.categories.includes(page?.slug)
    )
    const relatedPortfolio = portfolio.filter((item) =>
        item.placement.includes(page?.slug)
    )

    return (
        <>
            {page?.hero && page?.images?.featured && (
                <Hero
                    imageSrc={page?.images?.featured}
                    title={page?.title}
                    slug={page?.slug}
                    inlineImgsrc={
                        page?.hero?.inlineImg && page?.images?.transparent
                            ? page?.images?.transparent
                            : false
                    }
                    {...page?.hero}
                />
            )}
            {page?.counters?.counters && <Counters {...page?.counters} />}
            {page?.icons && <Icons {...page?.icons} />}
            {relatedTestimonials.length > 0 && (
                <Testimonials
                    nodes={relatedTestimonials}
                    {...page?.testimonials}
                />
            )}
            {relatedPortfolio.length > 0 && (
                <Portfolio
                    nodes={relatedPortfolio}
                    theme={page?.portfolio?.theme}
                    title={page?.portfolio?.title}
                />
            )}
            {CustomComponent && <CustomComponent />}
            <Form
                formId={page?.formId}
                title={page?.form?.title}
                theme={page?.form?.theme}
            />
        </>
    )
}
