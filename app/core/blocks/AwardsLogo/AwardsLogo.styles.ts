import styled from 'styled-components'
import mediaQueries from 'app/core/constants/mediaQueries'
import colors from 'app/core/constants/colors'
import { gutter } from 'app/core/constants/grid'

export namespace Styled {
    export const Column = styled.div`
        height: 100%;
        display: flex;
        flex-direction: column;
        user-select: none;
        margin-bottom: 50px;
    `

    export const Img = styled.div`
        flex-grow: 2;
        display: flex;
        align-items: center;
        padding-left: ${gutter}px;
        padding-right: ${gutter}px;
        margin-bottom: 5px;
        width: 100%;
    `

    export const ImgWrap = styled.div`
        position: relative;
        padding-bottom: 40%;
        width: 100%;
    `

    export const Title = styled.div`
        font-weight: 300;
        color: ${colors.red};
        text-transform: uppercase;
        text-align: center;
        margin-bottom: 50px;

        ${mediaQueries.md} {
            font-size: 1.15rem;
        }
    `
}
