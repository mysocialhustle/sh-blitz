import { Styled } from './AwardsLogo.styles'
import { Certification } from 'types'
import { Image } from 'blitz'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'

export default function AwardsLogo({ name, title, imageSrc }: Certification) {
    return (
        <ScrollSpy>
            <Styled.Column>
                <Styled.Img>
                    <Styled.ImgWrap>
                        <Image
                            src={imageSrc}
                            alt={name}
                            layout="fill"
                            objectFit="contain"
                            objectPosition="center center"
                        />
                    </Styled.ImgWrap>
                </Styled.Img>
                <Styled.Title>{title}</Styled.Title>
            </Styled.Column>
        </ScrollSpy>
    )
}
