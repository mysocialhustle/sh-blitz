import colors from 'app/core/constants/colors'
import styled from 'styled-components'

export const SocialIcons = styled.p`
    display: flex;
    align-items: center;
`
export const Icon = styled.span`
    display: inline-block;
    margin-right: 20px;

    a {
        transition: color 0.3s ease;

        &:hover {
            color: ${colors.red};
        }
    }
`
