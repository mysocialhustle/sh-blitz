import React from 'react'
import siteMetadata from 'app/data/siteMetadata'
import Icon from 'app/core/components/Icon/Icon'
import * as Styled from './SocialIcons.styles'

interface Props {
    el?: any
}

export default function SocialIcons({ el = 'p' }: Props) {
    const { socials } = siteMetadata

    return (
        <Styled.SocialIcons as={el}>
            {Object.entries(socials).map(([name, link], index) => {
                return (
                    <Styled.Icon key={index}>
                        <a href={link} target="_blank" rel="noreferrer">
                            <Icon name={name} />
                        </a>
                    </Styled.Icon>
                )
            })}
        </Styled.SocialIcons>
    )
}
