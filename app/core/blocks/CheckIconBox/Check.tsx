import { Styled } from './CheckIconBox.styles'
import { motion, useMotionValue, useTransform } from 'framer-motion'
import colors from 'app/core/constants/colors'
import variants from './CheckIconBox.variants'

export default function Check({ isChecked }) {
    const pathLength = useMotionValue(0)
    const opacity = useTransform(pathLength, [0.05, 0.15], [0, 1])

    return (
        <Styled.Checkbox
            initial={false}
            animate={isChecked ? 'checked' : 'unchecked'}
            width="20"
            height="20"
            viewBox="0 0 440 440"
        >
            <motion.path
                d="M 72 136 C 72 100.654 100.654 72 136 72 L 304 72 C 339.346 72 368 100.654 368 136 L 368 304 C 368 339.346 339.346 368 304 368 L 136 368 C 100.654 368 72 339.346 72 304 Z"
                fill="transparent"
                strokeWidth="35"
                stroke="#000000"
                variants={variants.checkBox}
            />
            <motion.path
                d="M 0 128.666 L 128.658 257.373 L 341.808 0"
                transform="translate(54.917 88.332) rotate(-4 170.904 128.687)"
                fill="transparent"
                strokeWidth="65"
                stroke={colors.darkGrey}
                strokeLinecap="round"
                strokeLinejoin="round"
                variants={variants.tick}
                style={{ pathLength, opacity }}
            />
            <motion.path
                d="M 0 128.666 L 128.658 257.373 L 341.808 0"
                transform="translate(54.917 68.947) rotate(-4 170.904 128.687)"
                fill="transparent"
                strokeWidth="65"
                stroke={colors.red}
                strokeLinecap="round"
                strokeLinejoin="round"
                variants={variants.tick}
                style={{ pathLength, opacity }}
            />
        </Styled.Checkbox>
    )
}
