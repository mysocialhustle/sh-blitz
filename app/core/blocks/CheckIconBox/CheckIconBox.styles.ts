import styled from 'styled-components'
import { motion } from 'framer-motion'
import { StyledProps } from 'types'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Box = styled(motion.label)`
        border: 2px solid;
        cursor: pointer;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
        text-align: center;
        padding: 10px;
        position: relative;

        input {
            position: absolute;
            visibility: hidden;
        }
    `

    export const Icon = styled.span<StyledProps>`
        font-size: 2rem;
        color: ${(props) => (props.active ? colors.red : '#000')};
        transition: color 0.25s ease;
    `

    export const Label = styled.p`
        font-size: 0.8rem;
        margin: 10px 0;
        text-transform: uppercase;
        user-select: none;
    `

    export const Checkbox = styled(motion.svg)`
        position: absolute;
        top: 5px;
        right: 5px;
    `
}
