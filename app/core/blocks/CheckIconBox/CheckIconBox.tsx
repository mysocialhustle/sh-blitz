import { Styled } from './CheckIconBox.styles'
import { motion, useMotionValue, useTransform } from 'framer-motion'
import Check from './Check'
import variants from './CheckIconBox.variants'
import Icon from 'app/core/components/Icon/Icon'

export default function CheckIconBox({ value, item, onChange }) {
    const { label, icon, name } = item

    const active = value.indexOf(name) >= 0

    return (
        <Styled.Box
            layout
            variants={variants.box}
            initial={false}
            animate={active ? 'active' : 'inactive'}
            whileHover="hover"
            transition={{ duration: 0.25, ease: 'easeIn' }}
        >
            <input
                onChange={(e) => onChange(e.target)}
                type="checkbox"
                value={name}
            />
            <Check isChecked={active} />
            <Styled.Icon active={active}>
                <Icon name={icon} />
            </Styled.Icon>
            <Styled.Label>{label}</Styled.Label>
        </Styled.Box>
    )
}
