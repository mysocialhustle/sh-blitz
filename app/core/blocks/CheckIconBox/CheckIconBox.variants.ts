import colors from 'app/core/constants/colors'

const variants = {
    box: {
        inactive: {
            borderColor: colors.lightGrey,
            boxShadow: `0px 0px 0px rgba(0,0,0,0)`,
            backgroundColor: '#ffffff',
            color: '#000000',
        },
        active: {
            borderColor: colors.red,
            boxShadow: `0px 0px 25px rgba(0,0,0,.2)`,
            backgroundColor: colors.darkGrey,
            color: '#ffffff',
        },
        hover: {
            boxShadow: `0px 0px 25px rgba(0,0,0,.2)`,
        },
    },
    tick: {
        checked: {
            pathLength: 1,
        },
        unchecked: {
            pathLength: 0,
        },
    },
    checkBox: {
        checked: {
            stroke: '#ffffff',
        },
        unchecked: {
            stroke: colors.lightGrey,
        },
    },
}

export default variants
