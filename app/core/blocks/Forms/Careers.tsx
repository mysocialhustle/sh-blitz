import { Form } from 'app/core/components/Form/Form/Form'
import { z } from 'zod'
import Input from 'app/core/components/Form/Input/Input'

export const schema = z.object({
    firstname: z.string(),
    lastname: z.string(),
    email: z.string().email(),
    phone: z.string().optional(),
    why_do_you_want_to_join_social_hustle_: z.string().optional(),
})

export default function CareersForm(props) {
    return (
        <Form schema={schema} {...props}>
            <Input name="firstname" label="First Name" required />

            <Input name="lastname" label="Last Name" required />

            <Input name="email" label="Email Address" type="email" required />

            <Input name="phone" label="Phone Number" type="tel" required />
            <Input
                name="why_do_you_want_to_join_social_hustle_"
                label="Why do you want to join Social Hustle?"
                element="textarea"
                rows={5}
            />
        </Form>
    )
}
