import { Form } from 'app/core/components/Form/Form/Form'
import { z } from 'zod'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'

export const schema = z.object({
    firstname: z.string(),
    lastname: z.string(),
    email: z.string().email(),
    message: z.string().optional(),
})

export default function FeedbackForm(props) {
    return (
        <Form schema={schema} {...props}>
            <Row>
                <Column sm={50}>
                    <Input name="firstname" label="First Name" required />
                </Column>
                <Column sm={50}>
                    <Input name="lastname" label="Last Name" required />
                </Column>
                <Column sm={50}></Column>
            </Row>
            <Input name="email" label="Email Address" type="email" required />
            <Input
                name="message"
                label="Your Message"
                element="textarea"
                rows={5}
            />
        </Form>
    )
}
