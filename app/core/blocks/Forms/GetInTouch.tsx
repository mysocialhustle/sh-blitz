import { Form } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { z } from 'zod'
import { Row, Column } from 'app/styles/grid'

export const schema = z.object({
    firstname: z.string(),
    lastname: z.string(),
    email: z.string().email(),
    subject: z.string().optional(),
    message: z.string().optional(),
})

export default function GetInTouch(props) {
    return (
        <Form schema={schema} {...props}>
            <Row>
                <Column sm={50}>
                    <Input name="firstname" label="First Name" required />
                </Column>
                <Column sm={50}>
                    <Input name="lastname" label="Last Name" required />
                </Column>
                <Column sm={50}>
                    <Input
                        name="email"
                        label="Email Address"
                        type="email"
                        required
                    />
                </Column>
                <Column sm={50}>
                    <Input name="subject" label="Subject" />
                </Column>
            </Row>
            <Input name="message" label="Message" element="textarea" rows={5} />
        </Form>
    )
}
