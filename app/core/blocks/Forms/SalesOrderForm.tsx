import { useState, useMemo } from 'react'
import Form, { FORM_ERROR } from 'app/core/components/Form/Form/Form'
import Company from 'app/core/sections/SalesOrderForm/Company'
import { SOFContext } from 'app/core/sections/SalesOrderForm/context'
import Services from 'app/core/sections/SalesOrderForm/Services'
import Advertising from 'app/core/sections/SalesOrderForm/Advertising'
import TikTokAds from 'app/core/sections/SalesOrderForm/TikTokAds'
import SEO from 'app/core/sections/SalesOrderForm/SEO'
import WebsiteDevelopment from 'app/core/sections/SalesOrderForm/WebsiteDevelopment'
import GraphicDesign from 'app/core/sections/SalesOrderForm/GraphicDesign'
import ContentCreation from 'app/core/sections/SalesOrderForm/ContentCreation'

export default function SalesOrderForm() {
    const [departments, setDepartments] = useState([])
    const [services, setServices] = useState([])

    const value = useMemo(
        () => ({
            departments,
            setDepartments,
            services,
            setServices,
        }),
        [departments, services]
    )

    return (
        <Form onSubmit={async (e) => console.log(e)}>
            <SOFContext.Provider value={value}>
                <Company />
                <Services />
                <Advertising
                    title="Google AdWords"
                    name="googleAdwords"
                    inputs={[
                        'budget',
                        'location',
                        'website',
                        'goals',
                        'existingAccount',
                    ]}
                />
                <TikTokAds />
                <Advertising
                    title="Snapchat Ads"
                    name="snapchatAds"
                    inputs={[
                        'budget',
                        'location',
                        'website',
                        'goals',
                        'existingAccount',
                        'contentSolution',
                    ]}
                />
                <Advertising
                    title="Facebook Ads"
                    name="facebookAds"
                    inputs={[
                        'budget',
                        'location',
                        'website',
                        'goals',
                        'existingAccount',
                    ]}
                />
                <SEO
                    title="SEO Campaign"
                    name="seo"
                    inputs={['budget', 'localOrNational', 'ranking']}
                />
                <SEO
                    title="SEO Audit"
                    name="seoAudit"
                    inputs={['ranking', 'website', 'pastSEO']}
                />
                <WebsiteDevelopment />
                <GraphicDesign />
                <ContentCreation />
            </SOFContext.Provider>
        </Form>
    )
}
