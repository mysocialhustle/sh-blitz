import { Form } from 'app/core/components/Form/Form/Form'
import Input from 'app/core/components/Form/Input/Input'
import { z } from 'zod'
import HubspotForm from 'integrations/Hubspot/HubspotForm'
import colors from 'app/core/constants/colors'

export const schema = z.object({
    email: z.string().email(),
})

function NewsletterForm(props) {
    return (
        <Form
            schema={schema}
            submitText="Subscribe"
            buttonProps={{
                backgroundColor: colors.red,
                hoverColor: '#000000',
                hoverBackgroundColor: '#ffffff',
            }}
            {...props}
        >
            <Input name="email" label="Email Address" type="email" required />
        </Form>
    )
}

export default function Newsletter(props) {
    return (
        <HubspotForm
            Component={NewsletterForm}
            id="8202de1c-3abd-414f-a6d0-60a5de3bd110"
            hotLead={false}
            {...props}
        />
    )
}
