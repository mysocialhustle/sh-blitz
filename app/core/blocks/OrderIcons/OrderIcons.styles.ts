import styled from 'styled-components'
import { StyledProps } from 'types'

export namespace Styled {
    export const Icons = styled.span`
        position: relative;
        height: 1.2em;
        width: 1.2em;
    `

    export const Icon = styled.span<StyledProps>`
        position: absolute;
        height: 45%;
        width: 100%;
        display: flex;
        flex-direction: column;
        left: 0;

        ${(props) =>
            props.position === `top` &&
            `
            top: 0;
            justify-content: flex-start;
        `}

        ${(props) =>
            props.position === `bottom` &&
            `
            bottom: 0;
            justify-content: flext-end;
        `}

        svg {
            height: 0.4em;
            width: auto;
            fill: currentColor;
            cursor: pointer;
        }
    `
}
