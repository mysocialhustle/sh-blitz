import CaretUp from 'app/svg/caret-up.svg'
import CaretDown from 'app/svg/caret-down.svg'
import { Styled } from './OrderIcons.styles'

interface Props {
    callback: (position: 'top' | 'bottom') => void
}

export default function OrderIcons({ callback }: Props) {
    return (
        <Styled.Icons>
            <Styled.Icon position="top">
                <CaretUp onClick={() => callback('top')} />
            </Styled.Icon>
            <Styled.Icon position="bottom">
                <CaretDown onClick={() => callback('bottom')} />
            </Styled.Icon>
        </Styled.Icons>
    )
}
