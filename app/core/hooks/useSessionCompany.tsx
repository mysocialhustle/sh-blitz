import { useSession } from 'blitz'

export default function useSessionCompany() {
    const session = useSession()

    return session?.company ? session.company : null
}
