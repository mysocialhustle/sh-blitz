import { userPrivileges } from 'app/data/account'
import { UserPrivilegeLabel } from 'types'
import useCurrentUser from './useCurrentUser'

export default function useCurrentPrivileges() {
    const user = useCurrentUser()

    if (user) {
        const userRole = user.role

        const arr = Object.entries(userPrivileges)
            .filter(([key, val]) => val.includes(userRole))
            .map(([label]) => {
                return label
            })
        return arr
    }
    return []
}
