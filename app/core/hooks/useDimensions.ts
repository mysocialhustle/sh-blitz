import { useState, useCallback, useEffect } from 'react'
import { DimensionObject } from 'types'

type UseDimensionsHook = [
    (node: HTMLElement | SVGSVGElement | null) => void,
    DimensionObject,
    HTMLElement | null
]

function getDimensionObject(node: HTMLElement): DimensionObject {
    const rect = node.getBoundingClientRect()

    return {
        width: rect.width,
        height: rect.height,
        top: 'y' in rect ? rect.y : rect.top,
        left: 'x' in rect ? rect.x : rect.left,
        x: 'x' in rect ? rect.x : rect.left,
        y: 'y' in rect ? rect.y : rect.top,
        right: rect.right,
        bottom: rect.bottom,
    }
}

export default function useDimensions(): UseDimensionsHook {
    const defaults: DimensionObject = {
        width: 0,
        height: 0,
        top: 0,
        left: 0,
        x: 0,
        y: 0,
        right: 0,
        bottom: 0,
    }
    const [dimensions, setDimensions] = useState(() => defaults)
    const [node, setNode] = useState(null)

    const ref = useCallback((node) => {
        setNode(node)
    }, [])

    useEffect(() => {
        if (node) {
            const measure = () =>
                window.requestAnimationFrame(() =>
                    setDimensions(getDimensionObject(node))
                )
            measure()

            window.addEventListener('resize', measure)

            return () => {
                window.removeEventListener('resize', measure)
            }
        }
    }, [node])

    return [ref, dimensions, node]
}
