import getUsersOnCompanies from 'app/admin/users-on-companies/queries/getUsersOnCompanies'
import useCurrentUser from './useCurrentUser'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import { useQuery } from 'blitz'

export default function useCurrentUserCompanies() {
    const user = useCurrentUser()
    const [{ usersOnCompanies }] = useQuery(getUsersOnCompanies, {
        where: { userId: user?.id },
    })
    const map = usersOnCompanies.map(({ companyId }) => {
        return companyId
    })

    const params =
        user?.role === 'ADMIN' || user?.role === 'TEAMMEMBER'
            ? {}
            : {
                  where: { id: { in: [...map] } },
              }

    const [{ companies }] = useQuery(getCompanies, params)

    return companies
}
