import useSWR from 'swr'
import fetcher from '../utils/fetcher'

export default function useAPI(url, options = {}) {
    const { data, error } = useSWR(url, fetcher, {
        keepPreviousData: true,
        errorRetryInterval: 10000,
        focusThrottleInterval: 10000,
        refreshInterval: 3600000,
        errorRetryCount: 5,
        revalidateOnFocus: false,
        revalidateOnMount: false,
        ...options,
    })
    return [data, error]
}
