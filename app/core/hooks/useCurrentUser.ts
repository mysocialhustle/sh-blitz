import { useQuery } from 'blitz'
import getCurrentUser from 'app/admin/users/queries/getCurrentUser'

const useCurrentUser = () => {
    const [user] = useQuery(getCurrentUser, null)
    return user
}

export default useCurrentUser
