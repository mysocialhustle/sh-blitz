import { useRouter } from 'blitz'

export default function useCurrentUrl(fallback) {
    const { pathname } = useRouter()
    if (typeof window !== 'undefined') {
        const url = `${window.location.protocol}//${window.location.hostname}/${pathname}`
        return url
    } else {
        return fallback
    }
}
