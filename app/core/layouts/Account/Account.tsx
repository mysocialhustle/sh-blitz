import { Suspense, useState, useMemo } from 'react'
import { Head, BlitzLayout } from 'blitz'
import Header from '../../components/Header/Header'
import { Styled } from './Account.styles'
import Nav from './Nav/Nav'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'
import { Context, ModeType } from 'app/admin/contexts'
import SessionManager from 'app/admin/components/SessionManager/SessionManager'

const Wrapper = ({ children }) => {
    const [mode, setMode] = useState<ModeType>('view')

    const value = useMemo(
        () => ({
            mode,
            setMode,
        }),
        [mode]
    )

    return (
        <Styled.Wrapper>
            <Context.Provider value={value}>{children}</Context.Provider>
        </Styled.Wrapper>
    )
}

const Account: BlitzLayout<{
    title?: string
    children?: React.ReactNode
}> = ({ title, children }) => {
    return (
        <>
            <Head>
                <title>{title || 'social-hustle-blitz'}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Header theme="dark" />
            <Wrapper>
                <Suspense fallback={<Skeleton />}>
                    <Nav />
                    <Styled.Page>
                        <SessionManager />
                        <Styled.Main>{children}</Styled.Main>
                    </Styled.Page>
                </Suspense>
            </Wrapper>
        </>
    )
}

export default Account
