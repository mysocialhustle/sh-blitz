import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import mediaQueries from 'app/core/constants/mediaQueries'
import headerHeight from 'app/core/constants/headerHeight'
import { Container } from 'app/styles/grid'
import { NavWidth } from './Nav/Nav.constants'

export namespace Styled {
    export const Wrapper = styled.div`
        min-height: 100vh;
        padding-top: ${headerHeight}px;
        position: relative;

        ${mediaQueries.sm} {
            display: flex;
            flex-flow: row wrap;
        }

        &:before {
            height: ${headerHeight}px;
            background: black;
            content: '';
            display: block;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
        }
    `

    export const Page = styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
        min-height: 100%;
        ${mediaQueries.sm} {
            flex: 1 0 calc(100% - ${NavWidth});
        }
    `

    export const Main = styled.div`
        padding-top: 100px;
        padding-bottom: 100px;
        width: 100%;

        & > * {
            flex: 1;
        }
    `
}
