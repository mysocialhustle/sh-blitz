import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import mediaQueries from 'app/core/constants/mediaQueries'
import { NavWidth } from './Nav.constants'
import { StyledProps } from 'types'

export namespace Styled {
    export const Sidebar = styled.aside`
        background-color: ${colors.darkGrey};
        position: relative;
        display: inline-block;
        vertical-align: top;
        padding-top: 20px;
        padding-bottom: 10px;
        color: white;
        font-weight: 600;
        letter-spacing: -0.5px;
        font-size: 1.05em;
        width: 100%;

        ${mediaQueries.sm} {
            width: ${NavWidth};
        }
    `

    export const Nav = styled.ul`
        margin-bottom: 0px;
        padding: 0;
        list-style: none;
        max-height: 0px;
        transform-origin: center top;
        transition: max-height 0.8s cubic-bezier(0.04, 0.62, 0.23, 0.98);
        overflow: hidden;

        &.shown {
            max-height: 1000px;
            overflow: visible;
            transition-duration: 1.5s;
        }

        ${mediaQueries.sm} {
            max-height: unset;
            overflow: visible;
            position: fixed;
            width: ${NavWidth};
        }
    `

    export const NavItem = styled.li<StyledProps>`
        padding-left: 0;
        line-height: 2;
        position: relative;

        &:before {
            content: none;
        }

        .navLink {
            display: block;
            padding: 20px 10px;
            color: white;
            transition: all 0.4s cubic-bezier(0.165, 0.84, 0.44, 1);
            line-height: 1;
            cursor: pointer;

            &:hover {
                color: ${colors.red};
                background-color: #000000;
            }

            ${(props) =>
                props.active &&
                `
            color: ${colors.red};
            background-color: #000000;

            &:before {
                content: '';
                width: 3px;
                background: ${colors.red};
                position: absolute;
                right: -3px;
                top: 0;
                height: 100%;
            }

            `}
        }
    `

    export const Label = styled.div`
        display: flex;
        align-items: center;

        svg {
            margin-right: 1em;
        }
    `
}
