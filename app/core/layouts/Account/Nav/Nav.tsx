import { useState } from 'react'
import { Styled } from './Nav.styles'
import { accountNav } from 'app/data/account'
import { Link, useRouter, useMutation } from 'blitz'
import Icon from 'app/core/components/Icon/Icon'
import logout from 'app/auth/mutations/logout'
import useCurrentUser from 'app/core/hooks/useCurrentUser'
import useCurrentPrivileges from 'app/core/hooks/useCurrentPrivileges'

export default function Nav() {
    const [active, setActive] = useState(false)
    const { pathname } = useRouter()
    const [logoutMutation] = useMutation(logout)
    const currentUser = useCurrentUser()
    const currentPrivileges = useCurrentPrivileges()

    return (
        <Styled.Sidebar>
            <Styled.Nav>
                {accountNav.map((item, index) => {
                    const { label, path, privileges, icon } = item
                    const fullPath = `/account${path}`

                    let hasAccess = true
                    if (
                        typeof privileges !== 'undefined' &&
                        !currentPrivileges.includes(privileges)
                    ) {
                        hasAccess = false
                    }
                    return hasAccess ? (
                        <Styled.NavItem
                            key={index}
                            active={pathname === fullPath}
                        >
                            <Link href={fullPath} passHref>
                                <a className="navLink">
                                    <Styled.Label>
                                        <Icon name={icon} />
                                        <span>{label}</span>
                                    </Styled.Label>
                                </a>
                            </Link>
                        </Styled.NavItem>
                    ) : null
                })}
                <Styled.NavItem>
                    <span
                        className="navLink"
                        onClick={async () => {
                            await logoutMutation()
                        }}
                    >
                        <Styled.Label>
                            <Icon name="logout" />
                            <span>Logout</span>
                        </Styled.Label>
                    </span>
                </Styled.NavItem>
            </Styled.Nav>
        </Styled.Sidebar>
    )
}
