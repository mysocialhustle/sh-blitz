import { Head, BlitzLayout } from 'blitz'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import * as Styled from './Layout.styles'
import { ThemeType } from 'types'

const Layout: BlitzLayout<{
    title?: string
    children?: React.ReactNode
    theme?: ThemeType
}> = ({ title, children, theme = 'light' }) => {
    return (
        <>
            <Head>
                <title>{title || 'social-hustle-blitz'}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Header theme={theme} />
            <Styled.Wrapper>
                <Styled.Main>{children}</Styled.Main>
                <Footer />
            </Styled.Wrapper>
        </>
    )
}

export default Layout
