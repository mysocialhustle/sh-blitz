import styled from 'styled-components'

export const Main = styled.main`
    background-color: white;

    ul {
        list-style: outside none none;
        margin-bottom: 20px;

        li {
            line-height: 2;
            padding-left: 2em;
            position: relative;

            &:before {
                background: url('/images/chevron.svg') scroll no-repeat center
                    center/contain;
                position: absolute;
                left: 0;
                top: 0.5em;
                height: 1em;
                width: 1em;
                content: '';
            }
        }
    }
`

export const Wrapper = styled.div`
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    justify-content: space-between;
    overflow: hidden;
`
