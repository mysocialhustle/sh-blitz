const random = (length) => Math.floor(Math.random() * length)

const randomChar = (string) => {
    const randomNumber = random(string.length)
    return string.substring(randomNumber, randomNumber + 1)
}

export default function generatePassword() {
    const length = 8
    const specialChars = `!@#$%^*`
    const uppercase = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`
    const lowercase = `abcdefghijklmnopqrstuvwxyz`
    const digits = `0123456789`

    const chars = lowercase + uppercase + digits + specialChars

    let result = ''
    let containsSpecial = false
    let containsUppercase = false
    let containsLowercase = false
    let containsDigit = false

    for (let i = 0; i < length; i++) {
        const char = randomChar(chars)

        if (specialChars.includes(char)) containsSpecial = true
        if (uppercase.includes(char)) containsUppercase = true
        if (lowercase.includes(char)) containsLowercase = true
        if (digits.includes(char)) containsDigit = true

        result += char
    }

    if (!containsSpecial) result += randomChar(specialChars)
    if (!containsUppercase) result += randomChar(uppercase)
    if (!containsLowercase) result += randomChar(lowercase)
    if (!containsDigit) result += randomChar(digits)

    return result
}
