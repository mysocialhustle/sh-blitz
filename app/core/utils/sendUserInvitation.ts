export default async function sendUserInvitation(email, password) {
    const fetcher = await fetch(
        `${process.env.BLITZ_PUBLIC_USER_INVITE_ZAPIER_ENDPOINT}`,
        {
            method: 'POST',
            body: JSON.stringify({
                username: email,
                password: password,
            }),
        }
    )
    const response = await fetcher.json()
    return [fetcher, response]
}
