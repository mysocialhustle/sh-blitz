const hexRegEx = /^#(?:[0-9a-fA-F]{3}){1,2}$/

export function hexToRGB(h: String) {
    if (h.match(hexRegEx)) {
        let r = '0',
            g = '0',
            b = '0'

        // 3 digits
        if (h.length == 4) {
            r = '0x' + h[1] + h[1]
            g = '0x' + h[2] + h[2]
            b = '0x' + h[3] + h[3]

            // 6 digits
        } else if (h.length == 7) {
            r = '0x' + h[1] + h[2]
            g = '0x' + h[3] + h[4]
            b = '0x' + h[5] + h[6]
        }

        //return "rgb("+ +r + "," + +g + "," + +b + ")";
        return [+r, +g, +b]
    } else throw new Error('Provide either a 3 or 6 digit hex preceded by a #')
}

export function hexToRGBA(h: string, a: number) {
    const rgb = hexToRGB(h)
    //console.log(rgb)
    if (typeof rgb !== 'undefined' && rgb.length === 3) {
        const array = [...rgb, a]
        return `rgba(${array.join(',')})`
    } else throw new Error('There has been an error converting the hex value')
}
