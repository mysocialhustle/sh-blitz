export const arrayContainsAll = (haystack: [], needles: []) =>
    needles.every((element) => {
        return Array.isArray(haystack) ? haystack.includes(element) : false
    })

export const arrayContainsOne = (haystack: [], needles: []) =>
    needles.some((element) => {
        return Array.isArray(haystack) ? haystack.includes(element) : false
    })

export const mutableArrayMerge = (array1, array2) => {
    if (Array.isArray(array1) && Array.isArray(array2)) {
        const newArray = array1.concat(array2)
        const filtered = newArray.filter((item, index) => {
            return newArray.indexOf(item) == index
        })
        return filtered
    } else return []
}
