import { CampaignType } from 'db'
import { AdvertisingField, SEOField } from './types'

export const services = [
    {
        label: 'Google AdWords',
        icon: 'adwordsOutline',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.googleAdwords,
    },
    {
        label: 'TikTok Ads',
        icon: 'tiktokOutline',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.tiktokAds,
    },
    {
        label: 'Snapchat Ads',
        icon: 'snapchatOutline',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.snapchatAds,
    },
    {
        label: 'Facebook Ads',
        icon: 'facebookOutline',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.facebookAds,
    },
    {
        label: 'Search Engine Optimization',
        icon: 'link',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.seo,
    },
    {
        label: 'SEO Audit',
        icon: 'seoOutline',
        type: 'marketing',
        deliverable: 'campaign',
        name: CampaignType.seoAudit,
    },
    {
        label: 'Website Development',
        icon: 'codeOutline',
        type: 'creative',
        deliverable: 'website',
        name: 'websiteDevelopment',
    },
    {
        label: 'Graphic Design',
        icon: 'graphicDesignOutline',
        type: 'creative',
        deliverable: 'content',
        name: 'graphicDesign',
    },
    {
        label: 'Content Creation',
        icon: 'cameraOutline',
        type: 'creative',
        deliverable: 'content',
        name: 'contentCreation',
    },
]

export const advertisingFields: AdvertisingField[] = [
    {
        label: () => 'Adspend Budget',
        name: 'Budget',
        component: 'input',
        props: { type: 'text' },
        size: 0.5,
        inputKey: 'budget',
    },
    {
        label: () => 'Location Targeting',
        name: 'Location Targeting',
        component: 'input',
        props: { type: 'text' },
        size: 0.5,
        inputKey: 'location',
    },
    {
        label: () => 'Website URL (Including http:// or https://)',
        name: 'Website URL',
        component: 'input',
        props: { type: 'url' },
        inputKey: 'website',
    },
    {
        label: () => 'Campaign Goals',
        name: 'Goals',
        props: { element: 'textarea', rows: 5 },
        inputKey: 'goals',
    },
    {
        label: (name) => `Do they have an existing ${name} account?`,
        name: 'Do they have an existing account?',
        component: 'select',
        props: { options: ['Yes', 'No'] },
        inputKey: 'existingAccount',
    },
    {
        label: () =>
            'Do they have a content solution or do we need to propose one?',
        name: 'Do they have a content solution?',
        component: 'select',
        props: {
            options: ['Yes - They Have One', 'No - We Need to Propose One'],
        },
        inputKey: 'contentSolution',
    },
]

export const SEOFields: SEOField[] = [
    {
        label: 'Budget',
        name: 'Budget',
        component: 'input',
        props: { type: 'text' },
        inputKey: 'budget',
    },
    {
        label: 'Local or National?',
        name: 'Local or National',
        component: 'select',
        props: { options: ['Local', 'National'] },
        inputKey: 'localOrNational',
    },
    {
        label: 'What do they want to rank for?',
        name: 'What do they want to rank for',
        props: { element: 'textarea', rows: 5 },
        inputKey: 'ranking',
    },
    {
        label: 'Website URL (Including http:// or https://)',
        name: 'Website URL',
        component: 'input',
        props: { type: 'url' },
        inputKey: 'website',
    },
    {
        label: 'Have they done SEO in the past? And if so was it white or black hat?',
        name: 'Have they done SEO in the past?',
        props: { element: 'textarea', rows: 5 },
        inputKey: 'pastSEO',
    },
]
