import Section from 'app/core/components/Section/Section'
import { useContext } from 'react'
import { SOFContext } from './context'
import { motion, AnimatePresence } from 'framer-motion'

export default function FormSection({ children, context, ...rest }) {
    const { services } = useContext(SOFContext)
    return (
        <AnimatePresence exitBeforeEnter>
            {services.includes(context) && (
                <motion.div
                    initial={{
                        opacity: 0,
                        y: `-10vh`,
                    }}
                    animate={{
                        opacity: 1,
                        y: 0,
                    }}
                    exit={{
                        opacity: 0,
                        y: `10vh`,
                    }}
                    style={{
                        transformOrigin: 'center top',
                    }}
                    transition={{
                        duration: 0.65,
                        ease: 'easeInOut',
                    }}
                    layout
                >
                    <Section theme="transparentLight" size="tiny" {...rest}>
                        {children}
                    </Section>
                </motion.div>
            )}
        </AnimatePresence>
    )
}
