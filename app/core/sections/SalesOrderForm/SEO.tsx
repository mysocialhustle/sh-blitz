import { SEOFields } from './constants'
import { SEOInput } from './types'
import FormSection from './FormSection'
import UniversalQuestions from './UniversalQuestions'
import Select from 'app/core/components/Form/Select/Select'
import Input from 'app/core/components/Form/Input/Input'

interface Props {
    name: string
    title: string
    inputs: SEOInput[]
}

const components = {
    select: Select,
    input: Input,
}

export default function SEO({ name, title, inputs }: Props) {
    // Filter by the inputs listed in the prop
    const appliedInputs = SEOFields.filter((field) =>
        inputs.includes(field.inputKey)
    )

    return (
        <FormSection context={name}>
            <h3>{title}</h3>
            <UniversalQuestions context={name} />
            {appliedInputs.map((input, index) => {
                const {
                    label,
                    name: inputName,
                    component = 'input',
                    props,
                } = input
                const Component = components[component]
                return (
                    <Component
                        key={index}
                        name={`${name}[${inputName}]`}
                        label={label}
                        {...props}
                    />
                )
            })}
        </FormSection>
    )
}
