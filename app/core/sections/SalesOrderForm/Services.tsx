import { useContext, useMemo, useState, useCallback } from 'react'
import { services } from './constants'
import { Row, Column } from 'app/styles/grid'
import CheckIconBox from 'app/core/blocks/CheckIconBox/CheckIconBox'
import { SOFContext } from './context'
import { useFormContext, useController } from 'react-hook-form'

export default function Services() {
    const { departments, setServices } = useContext(SOFContext)
    const filteredServices = useMemo(
        () => services.filter(({ type }) => departments.includes(type)),
        [departments]
    )
    const { control } = useFormContext()
    const { field } = useController({ control, name: 'services' })
    const [values, setValues] = useState(field.value || [])

    const change = useCallback(
        (target) => {
            // https://codesandbox.io/s/usecontroller-checkboxes-99ld4?file=/src/App.js:1048-1055
            const { checked, value } = target
            const valuesCopy = [...values]
            if (checked) {
                valuesCopy.push(value)
            } else {
                valuesCopy.splice(valuesCopy.indexOf(value), 1)
            }
            field.onChange(valuesCopy)
            setValues(valuesCopy)
            setServices(valuesCopy)
        },
        [field, values, setServices]
    )

    return (
        <Row alignY="normal">
            {filteredServices.map((service, index) => {
                return (
                    <Column
                        sm={25}
                        md={20}
                        key={index}
                        style={{ marginBottom: 20 }}
                    >
                        <CheckIconBox
                            value={values}
                            onChange={(e) => change(e)}
                            item={service}
                        />
                    </Column>
                )
            })}
        </Row>
    )
}
