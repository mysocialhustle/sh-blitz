import Input from 'app/core/components/Form/Input/Input'
import Select from 'app/core/components/Form/Select/Select'
import UniversalQuestions from './UniversalQuestions'
import FormSection from './FormSection'

export default function ContentCreation() {
    return (
        <FormSection context="contentCreation">
            <h3>Content Creation</h3>
            <UniversalQuestions context="contentCreation" />
            <Select
                label="What content services will they be recieving?"
                name="contentCreation[notes][What content services will they be recieving?]"
                options={[
                    'Photography',
                    'Videography',
                    'Both Photography & Videography',
                ]}
            />
            <Input
                element="textarea"
                label="What type of content will it be? e.g. Headshots"
                name="contentCreation[notes][What type of content will it be?]"
                rows={5}
            />
            <Select
                label="What is this content meant for?"
                name="contentCreation[notes][What is this content meant for?]"
                options={['General Branding', 'Marketing', 'Website', 'Other']}
            />
            <Select
                label="Where will the shoot be located?"
                name="contentCreation[notes][Where will the shoot be located?]"
                options={[
                    'At the SH Studio',
                    'At the clients office',
                    'At a venue',
                    'Other',
                ]}
            />
        </FormSection>
    )
}
