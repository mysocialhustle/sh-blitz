import { Row, Column } from 'app/styles/grid'
import Input from 'app/core/components/Form/Input/Input'
import { useState } from 'react'
import Select from 'app/core/components/Form/Select/Select'
import UniversalQuestions from './UniversalQuestions'
import FormSection from './FormSection'
import { WebsiteType } from 'db'

interface Type {
    value: WebsiteType
    label: string
}

const websiteTypes: Type[] = [
    { value: WebsiteType.wordpress, label: 'Standard WordPress' },
    { value: WebsiteType.ecommerce, label: 'E-Commerce' },
    { value: WebsiteType.custom, label: 'Full Custom' },
]

export default function WebsiteDevelopment() {
    const [hasURL, setHasURL] = useState('No')

    return (
        <FormSection context="websiteDevelopment">
            <h3>Website Development</h3>
            <UniversalQuestions context="websiteDevelopment" />
            <Select
                label="What type of website project is this?"
                name="websiteDevelopment[type]"
                options={websiteTypes}
            />
            <Select
                label="Do they have an existing website?"
                name="websiteDevelopment[notes][Do they have an existing website?]"
                options={['Yes', 'No']}
                onChange={(option) => {
                    if (option) setHasURL(option.value)
                }}
            />
            {hasURL === 'Yes' && (
                <Input
                    label="Website URL (Including http:// or https://)"
                    type="url"
                    name="websiteDevelopment[Website URL]"
                />
            )}
            <Row>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        label="What is the primary objective with the new website?"
                        name="websiteDevelopment[notes][Primary objective]"
                        rows={5}
                    />
                </Column>
                <Column sm={50}>
                    <Input
                        element="textarea"
                        label="What unique aspects/functionality were discussed?"
                        name="websiteDevelopment[notes][Unique aspects and functionality]"
                        rows={5}
                    />
                </Column>
            </Row>
            <Select
                label="Are there additional services being performed as part of the website project?"
                name="websiteDevelopment[notes][Additional services being performed as part of the project]"
                options={[
                    'No',
                    'Graphic Design',
                    'Content Creation',
                    'Both Graphic Design & Content Creation',
                ]}
            />
        </FormSection>
    )
}
