import React from 'react'

type value = any[]

export interface SOFProps {
    departments: value
    services: value
    setDepartments: React.Dispatch<React.SetStateAction<value>>
    setServices: React.Dispatch<React.SetStateAction<value>>
}

export const SOFDefaults: SOFProps = {
    departments: [],
    services: [],
    setDepartments: () => {},
    setServices: () => {},
}

export const SOFContext = React.createContext<SOFProps>(SOFDefaults)
