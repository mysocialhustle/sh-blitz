import { useContext } from 'react'
import Select from 'app/core/components/Form/Select/Select'
import { useQuery } from 'blitz'
import getCompanies from 'app/admin/companies/queries/getCompanies'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'
import Checkboxes from 'app/core/components/Form/Checkboxes/Checkboxes'
import { SOFContext } from './context'

export default function Company() {
    const { setDepartments } = useContext(SOFContext)
    const [companies] = useQuery(getCompanies, {})

    return (
        <>
            <h5>Select Company or Create a New One</h5>
            <Select
                name="company"
                options={companies.companies.map((el) => ({
                    ...el,
                    value: el.id,
                    label: el.name,
                }))}
                creatable={true}
                label="Start typing a company name"
                isClearable={true}
            />
            <Row>
                <Column sm={50}>
                    <Input name="firstname" label="First Name" required />
                </Column>
                <Column sm={50}>
                    <Input name="lastname" label="Last Name" required />
                </Column>
                <Column sm={50}>
                    <Input name="email" label="Email Address" type="email" />
                </Column>
                <Column sm={50}>
                    <Input name="phone" label="Telephone" type="tel" />
                </Column>
            </Row>
            <Input
                name="notes"
                label="Any Notes About the Company?"
                element="textarea"
                rows={5}
            />
            <h5>Which department(s) will they be working with?</h5>
            <Checkboxes
                name="departments"
                inputs={[
                    {
                        label: 'Marketing',
                        value: 'marketing',
                    },
                    {
                        label: 'Creative',
                        value: 'creative',
                    },
                ]}
                onChange={(e) => setDepartments(e)}
            />
        </>
    )
}
