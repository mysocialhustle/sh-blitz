import { advertisingFields } from './constants'
import { Row, Column } from 'app/styles/grid'
import UniversalQuestions from './UniversalQuestions'
import Select from 'app/core/components/Form/Select/Select'
import Input from 'app/core/components/Form/Input/Input'
import { CampaignType } from 'db'
import { columns } from 'app/core/constants/grid'
import { AdvertisingInput } from './types'
import FormSection from './FormSection'

const components = {
    select: Select,
    input: Input,
}
interface Props {
    name: CampaignType
    title: string
    inputs: AdvertisingInput[]
}

export default function Advertising({ name, title, inputs }: Props) {
    // Filter by the inputs listed in the prop
    const appliedInputs = advertisingFields.filter((field) =>
        inputs.includes(field.inputKey)
    )

    return (
        <FormSection context={name}>
            <h3>{title}</h3>
            <UniversalQuestions context={name} />
            <Row>
                {appliedInputs.map((input, index) => {
                    const {
                        label,
                        name: inputName,
                        component = 'input',
                        props,
                        size = 1,
                    } = input
                    const Component = components[component]
                    const width = size * columns
                    return (
                        <Column sm={width} key={index}>
                            <Component
                                name={`${name}[${inputName}]`}
                                label={label(title)}
                                {...props}
                            />
                        </Column>
                    )
                })}
            </Row>
        </FormSection>
    )
}
