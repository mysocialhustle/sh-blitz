import Checkboxes from 'app/core/components/Form/Checkboxes/Checkboxes'
import Input from 'app/core/components/Form/Input/Input'
import UniversalQuestions from './UniversalQuestions'
import FormSection from './FormSection'

export default function GraphicDesign() {
    return (
        <FormSection context="graphicDesign">
            <h3>Graphic Design</h3>
            <UniversalQuestions context="graphicDesign" />
            <Checkboxes
                name="graphicDesign[notes][What kind of design services will they be recieving?]"
                inputs={[
                    'Logo Creation',
                    'Brand Design',
                    'Packaging Design',
                    'Print Material',
                    'Design',
                    'Other',
                ]}
            />
            <Input
                element="textarea"
                name="graphicDesign[notes][Links to inspiration]"
                label="Links to inspiration (if provided by client)"
                rows={7}
            />
        </FormSection>
    )
}
