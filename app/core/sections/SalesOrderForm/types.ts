export type AdvertisingInput =
    | 'budget'
    | 'location'
    | 'website'
    | 'goals'
    | 'existingAccount'
    | 'contentSolution'

export interface AdvertisingField {
    label: (name?: string) => void
    name: string
    component?: 'input' | 'select'
    props: any
    size?: number
    inputKey: AdvertisingInput
}

export type SEOInput =
    | 'budget'
    | 'localOrNational'
    | 'website'
    | 'ranking'
    | 'pastSEO'

export interface SEOField {
    label: string
    name: string
    component?: 'input' | 'select'
    props: any
    inputKey: SEOInput
}
