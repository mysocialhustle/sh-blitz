import UniversalQuestions from './UniversalQuestions'
import FormSection from './FormSection'
import { Row, Column } from 'app/styles/grid'
import Input from 'app/core/components/Form/Input/Input'
import { useState } from 'react'
import Select from 'app/core/components/Form/Select/Select'
import Checkboxes from 'app/core/components/Form/Checkboxes/Checkboxes'

export default function TikTokAds() {
    const [hasAccount, setHasAccount] = useState('No')

    return (
        <FormSection context="tiktokAds">
            <h3>TikTok Ads</h3>
            <UniversalQuestions context="tiktokAds" />
            <Row>
                <Column sm={50}>
                    <Input label="Adspend Budget" name="tiktokAds[Budget]" />
                </Column>
                <Column sm={50}>
                    <Input
                        label="Location Targeting"
                        name="tiktokAds[Location Targeting]"
                    />
                </Column>
            </Row>
            <Select
                label="Do they have an existing TikTok account?"
                name="tiktokAds[Do they have an existing account?]"
                options={['Yes', 'No']}
                onChange={(option) => {
                    if (option) setHasAccount(option.value)
                }}
            />
            {hasAccount === 'Yes' && (
                <>
                    <Input
                        label="What is the username?"
                        name="tiktokAds[TikTok Username]"
                    />
                </>
            )}
            <Input
                element="textarea"
                label="What is the client niche?"
                name="tiktokAds[What is the client niche?]"
                rows={5}
            />
            <Select
                label="Is the client e-commerce?"
                name="tiktokAds[Is the client e-commerce?]"
                options={['Yes', 'No']}
            />
            <p>What budget does the client have? (Select all that apply)</p>
            <Checkboxes
                name="tiktokAds[What budget does the client have?]"
                inputs={[
                    {
                        label: 'Ad Spend Budget',
                        value: 'Ad Spend Budget',
                    },
                    {
                        label: 'Content Creation Budget',
                        value: 'Content Creation Budget',
                    },
                    {
                        label: 'Management Budget',
                        value: 'Management Budget',
                    },
                ]}
            />
            <p>{`What is the client's goal on TikTok? (Select all that apply)`}</p>
            <Checkboxes
                name="tiktokAds[What is the client's goal on TikTok?]"
                inputs={[
                    {
                        label: 'Traffic',
                        value: 'Traffic',
                    },
                    {
                        label: 'Conversions',
                        value: 'Conversions',
                    },
                    {
                        label: 'App Installs',
                        value: 'App Installs',
                    },
                    {
                        label: 'Catalog Sales',
                        value: 'Catalog Sales',
                    },
                    {
                        label: 'Video Views',
                        value: 'Video Views',
                    },
                    {
                        label: 'Lead Generation',
                        value: 'Lead Generation',
                    },
                    {
                        label: 'Community Interaction',
                        value: 'Community Interaction',
                    },
                ]}
            />
            <Input
                label="Website URL (Including http:// or https://)"
                type="url"
                name="tiktokAds[Website URL]"
            />
            <Select
                label="Do they have a content solution or do we need to propose one?"
                name="tiktokAds[Do they have a content solution?]"
                options={['Yes - They Have One', 'No - We Need to Propose One']}
            />
        </FormSection>
    )
}
