import Select from 'app/core/components/Form/Select/Select'
import Input from 'app/core/components/Form/Input/Input'
import { Row, Column } from 'app/styles/grid'

export default function UniversalQuestions({ context }) {
    return (
        <>
            <Select
                name={`${context}[Timeline]`}
                label="Timeline"
                options={[
                    'Immediate Start',
                    '1-2 Weeks',
                    '2-4 Weeks',
                    '1+ Month',
                    'After Completetion of Other Service',
                ]}
            />
            <Row>
                <Column sm={50}>
                    <Input name={`${context}[Sale Price]`} label="Sale Price" />
                </Column>
                <Column sm={50}>
                    <Input
                        name={`${context}[Expected Cost]`}
                        label="Expected Cost"
                    />
                </Column>
            </Row>
        </>
    )
}
