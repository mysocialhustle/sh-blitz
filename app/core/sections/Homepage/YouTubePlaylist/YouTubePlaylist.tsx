import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import Playlist from 'app/data-feeds/components/YouTube/Playlist/Playlist'
import SocialHustleTube from 'app/svg/SocialHustleTube.inline.svg'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'

const playlists = [
    {
        name: 'Google Ads Academy',
        key: 'googleAdsAcademyPlaylist',
    },
    {
        name: 'Promotional Videos',
        key: 'promotionalVideosPlaylist',
    },
]

export default function YouTubePlaylists() {
    return (
        <Section theme="dark" size="large">
            <Container>
                <ScrollSpy>
                    <SocialHustleTube
                        style={{
                            maxWidth: 500,
                            margin: '0px auto 60px',
                            display: 'block',
                        }}
                    />
                </ScrollSpy>
                <ScrollSpy>
                    <Row>
                        {playlists.map((list, index) => {
                            return (
                                <Column sm={50} key={index}>
                                    <h3>{list.name}</h3>
                                    <Playlist playlistKey={list.key} />
                                </Column>
                            )
                        })}
                    </Row>
                </ScrollSpy>
            </Container>
        </Section>
    )
}
