import React from 'react'
import * as Styled from './HomepageAbout.styles'
import { Image } from 'blitz'
import { Container, Row, Column } from 'app/styles/grid'
import Section from 'app/core/components/Section/Section'
import Button from 'app/core/components/Button/Button'
import Headings from 'app/styles/headings'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'

export default function HomepageAbout() {
    return (
        <Section theme="grey" style={{ fontSize: '1.15rem' }}>
            <Container>
                <Row alignY="center" alignX="space-between">
                    <Column sm={45}>
                        <ScrollSpy>
                            <Headings.H2>Welcome to Social Hustle</Headings.H2>

                            <p>
                                Social Hustle is an award-winning digital
                                marketing, website development, and creative
                                design agency. Our team is devoted to helping
                                you create the infrastructure your business
                                needs to thrive in a digital world, from
                                functional and aesthetic websites and content to
                                highly performing marketing campaigns.
                            </p>

                            <Button href="/get-in-touch/">
                                Schedule a Consultation
                            </Button>
                        </ScrollSpy>
                    </Column>
                    <Column sm={45}>
                        <Styled.BrandImages
                            initial={{ scale: 0 }}
                            whileInView={{ scale: 1 }}
                            transition={{
                                duration: 1,
                                ease: [0.5, 0, 0.75, 0],
                            }}
                            viewport={{ once: true }}
                        >
                            <Styled.BrandImg1>
                                <Image
                                    src="https://socialhustle-blitz-static.s3.amazonaws.com/homepage/CPR50012.jpg"
                                    alt="Social Hustle Brand Image"
                                    width={570}
                                    height={380}
                                />
                            </Styled.BrandImg1>
                            <Styled.BrandImg2>
                                <Image
                                    src="https://socialhustle-blitz-static.s3.amazonaws.com/homepage/CPR58171.jpg"
                                    alt="Social Hustle Brand Image"
                                    width={570}
                                    height={380}
                                />
                            </Styled.BrandImg2>
                        </Styled.BrandImages>
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
