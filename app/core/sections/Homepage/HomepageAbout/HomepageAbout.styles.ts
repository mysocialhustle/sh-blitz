import styled from 'styled-components'
import { motion } from 'framer-motion'

export const BrandImg1 = styled.div`
    line-height: 0;
    transform: translate3d(-10%, -10%, 0);
`

export const BrandImg2 = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 100%;
    line-height: 0;
    transform: translate3d(10%, 10%, 0);
`

export const BrandImages = styled(motion.div)`
    width: 100%;
`
