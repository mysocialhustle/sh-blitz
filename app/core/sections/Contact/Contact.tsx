import React from 'react'
import { Container, Row, Column } from 'app/styles/grid'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import ContactInfo from './ContactInfo'
import Headings from 'app/styles/headings'
import GetInTouch from 'app/core/blocks/Forms/GetInTouch'
import HubspotForm from 'integrations/Hubspot/HubspotForm'

export default function Contact() {
    return (
        <Section size="large">
            <Container>
                <Row alignX="space-between" alignY="center">
                    <Column sm={60}>
                        <Headings.H2>{`Don't Be Shy, Say Hello!`}</Headings.H2>
                        <ScrollSpy>
                            <HubspotForm
                                Component={GetInTouch}
                                id="8202de1c-3abd-414f-a6d0-60a5de3bd110"
                                hotLead={false}
                            />
                        </ScrollSpy>
                    </Column>
                    <Column sm={30}>
                        <ContactInfo />
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
