import React from 'react'
import siteMetadata from 'app/data/siteMetadata'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Contact } from 'types'
import Headings from 'app/styles/headings'
import SocialIcons from 'app/core/blocks/SocialIcons/SocialIcons'

export default function ContactInfo() {
    const {
        contact: { address, phone, email },
    }: { contact: Contact } = siteMetadata

    return (
        <>
            <ScrollSpy>
                <Headings.H4>Visit Us</Headings.H4>
                <p>
                    {address.street}
                    <br />
                    {address.city}, {address.state} {address.zip}
                </p>
            </ScrollSpy>
            {phone && (
                <ScrollSpy>
                    <Headings.H4>Ring Us</Headings.H4>
                    <p>
                        <a href={`tel:${phone!.match(/\d+/g)!.join('')}`}>
                            {phone}
                        </a>
                    </p>
                </ScrollSpy>
            )}
            {email && (
                <ScrollSpy>
                    <Headings.H4>Email Us</Headings.H4>
                    <p>
                        <a href={`mailto:${email}`}>{email}</a>
                    </p>
                </ScrollSpy>
            )}
            <ScrollSpy>
                <Headings.H4>Follow Us</Headings.H4>
                <SocialIcons />
            </ScrollSpy>
        </>
    )
}
