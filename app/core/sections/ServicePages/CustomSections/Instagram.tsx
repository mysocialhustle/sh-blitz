import Button from 'app/core/components/Button/Button'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Section from 'app/core/components/Section/Section'
import { Column, Container, Row } from 'app/styles/grid'
import InstagramLogo from 'app/svg/instagram-lettermark.svg'
import colors from 'app/core/constants/colors'

export default function Instagram() {
    return (
        <Section theme="dark">
            <Container>
                <Row alignX="space-between">
                    <Column sm={50}>
                        <ScrollSpy>
                            <InstagramLogo
                                style={{
                                    width: '100%',
                                    height: 'auto',
                                    fill: 'currentColor',
                                    maxWidth: 400,
                                    marginBottom: 40,
                                }}
                            />
                        </ScrollSpy>
                        <ScrollSpy>
                            <p>
                                Becuase Instagram and Facebook are both owned by
                                Meta, Instagram advertising is run through the
                                Facebook busiiness center while offering a few
                                added benefits and differences:
                            </p>
                        </ScrollSpy>
                    </Column>
                    <Column
                        sm={50}
                        md={40}
                        style={{
                            lineHeight: 2,
                            fontSize: '1.05em',
                            fontWeight: 'bold',
                        }}
                    >
                        <ScrollSpy>
                            <ul>
                                <li>1 Billion Active Monthly Users</li>
                                <li>81% of Users Use Instagram to Research</li>
                                <li>30 Minutes Spent on Instagram / Day</li>
                                <li>Accepts Similiar Content to Facebook</li>
                                <li>Multiple Bid Strategies</li>
                                <li>Additonal Revenue Channel</li>
                            </ul>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Button
                                href="#start"
                                backgroundColor={colors.red}
                                hoverBackgroundColor="white"
                            >
                                Request a Consultation
                            </Button>
                        </ScrollSpy>
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
