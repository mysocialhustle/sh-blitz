import BingLogo from 'app/svg/bingads.svg'
import Button from 'app/core/components/Button/Button'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Section from 'app/core/components/Section/Section'
import { Column, Container, Row } from 'app/styles/grid'
import colors from 'app/core/constants/colors'
import Headings from 'app/styles/headings'

export default function BingAds() {
    return (
        <Section theme="dark" id="bing">
            <Container>
                <Row alignX="space-between">
                    <Column sm={40}>
                        <ScrollSpy>
                            <BingLogo
                                style={{
                                    width: '100%',
                                    height: 'auto',
                                    fill: 'currentColor',
                                    maxWidth: 400,
                                    marginBottom: 40,
                                }}
                            />
                        </ScrollSpy>
                        <ScrollSpy>
                            <p>
                                {`Our Bing Ads team consistently finds low hanging fruit, and quick wins within the Bing platform. Bing is still a channel that can make you money, and that's all we care about.`}
                            </p>
                        </ScrollSpy>
                    </Column>
                    <Column
                        sm={50}
                        style={{
                            lineHeight: 2,
                            fontSize: '1.05em',
                            fontWeight: 'bold',
                        }}
                    >
                        <ScrollSpy>
                            <Headings.H4 as="h3">{`Bing is still a search engine that for most companies, shouldn't be ignored. Within the platform and its ad network you should expect to find:`}</Headings.H4>
                        </ScrollSpy>
                        <ScrollSpy>
                            <ul>
                                <li>An Older, Less Technical Audience</li>
                                <li>Less Expensive Traffic</li>
                                <li>Easier To Dominate</li>
                                <li>Affordable Management Costs</li>
                                <li>Predictable Traffic &amp; Projections</li>
                                <li>Limited Market</li>
                            </ul>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Button
                                href="#start"
                                backgroundColor={colors.red}
                                hoverBackgroundColor="white"
                            >
                                Request a Consultation
                            </Button>
                        </ScrollSpy>
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
