import BingLogo from 'app/svg/bingads.svg'
import Button from 'app/core/components/Button/Button'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Section from 'app/core/components/Section/Section'
import { Column, Container, Row } from 'app/styles/grid'
import colors from 'app/core/constants/colors'
import Headings from 'app/styles/headings'
import { Image } from 'blitz'

export default function LocalSEO() {
    return (
        <Section id="local" theme="dark">
            <Container>
                <Row alignX="space-between">
                    <Column sm={45}>
                        <ScrollSpy>
                            <h2>{`Local SEO`}</h2>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Image
                                src={`https://socialhustle-blitz-static.s3.amazonaws.com/local-seo.jpg`}
                                alt="Local SEO"
                                height={656}
                                width={1200}
                            />
                        </ScrollSpy>
                    </Column>
                    <Column sm={45}>
                        <ScrollSpy>
                            <p>{`Local SEO includes many of the same factors as Enterprise SEO, but hyper-focused to benefit a geogrpahical area or region. Local SEO can usually provide a faster ROI due to less competiton.`}</p>
                        </ScrollSpy>
                        <ScrollSpy>
                            <ul
                                style={{
                                    lineHeight: 2,
                                    fontSize: '1.05em',
                                    fontWeight: 'bold',
                                }}
                            >
                                <li>Local Citation Management</li>
                                <li>GMB Listing Optimization</li>
                                <li>Review Stratgey Analysis and Monitoring</li>
                                <li>Localized Content Creation</li>
                                <li>Social Signal Review</li>
                                <li>Localized On-page optimization</li>
                            </ul>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Button
                                href="#start"
                                backgroundColor={colors.red}
                                hoverBackgroundColor="white"
                            >
                                Request a Consultation
                            </Button>
                        </ScrollSpy>
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
