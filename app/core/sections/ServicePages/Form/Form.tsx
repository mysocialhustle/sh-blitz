import HubspotForm from 'integrations/Hubspot/HubspotForm'
import ServiceForm from 'app/core/blocks/Forms/ServiceForm'
import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import { ThemeType } from 'types'

interface Props {
    formId: string
    theme?: ThemeType
    title?: string
}

export default function Form({
    formId,
    theme = 'light',
    title = 'Start the Conversation',
}: Props) {
    return (
        <Section id="#start" theme={theme}>
            <Container>
                <h2 style={{ textAlign: `center` }}>{title}</h2>
                <Row alignX="center">
                    <Column sm={80}>
                        <HubspotForm
                            Component={ServiceForm}
                            id={formId}
                            hotLead={true}
                        />
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
