const variants = {
    div: {
        initial: {
            opacity: 0,
        },
        animate: {
            opacity: 1,
            transition: {
                duration: 0.1,
                ease: 'linear',
            },
        },
    },
    vector: {
        initial: {
            pathLength: 0,
        },
        animate: {
            pathLength: 1,
            transition: {
                duration: 1.5,
                ease: [0.37, 0, 0.63, 1],
            },
        },
    },
}

export default variants
