import variants from './Icons.variants'
import { dynamic } from 'blitz'
import { Styled } from './Icons.styles'

const icons = {
    advertise: dynamic(() => import('app/svg/drawPath/Advertise')),
    blog: dynamic(() => import('app/svg/drawPath/Blog')),
    budget: dynamic(() => import('app/svg/drawPath/Budget')),
    communication: dynamic(() => import('app/svg/drawPath/Communication')),
    conversion: dynamic(() => import('app/svg/drawPath/Conversion')),
    creativity: dynamic(() => import('app/svg/drawPath/Creativity')),
    cro: dynamic(() => import('app/svg/drawPath/CRO')),
    demographics: dynamic(() => import('app/svg/drawPath/Demographics')),
    ecommerce: dynamic(() => import('app/svg/drawPath/Ecommerce')),
    eye: dynamic(() => import('app/svg/drawPath/Eye')),
    googleMaps: dynamic(() => import('app/svg/drawPath/GoogleMaps')),
    keyword: dynamic(() => import('app/svg/drawPath/Keyword')),
    listRetargeting: dynamic(() => import('app/svg/drawPath/ListRetargeting')),
    localService: dynamic(() => import('app/svg/drawPath/LocalService')),
    mail: dynamic(() => import('app/svg/drawPath/Mail')),
    newsletter: dynamic(() => import('app/svg/drawPath/Newsletter')),
    remarketing: dynamic(() => import('app/svg/drawPath/Remarketing')),
    research: dynamic(() => import('app/svg/drawPath/Research')),
    social: dynamic(() => import('app/svg/drawPath/Social')),
}

export default function Icon({ name }) {
    const Component = icons[name]
    return (
        <Styled.Icon
            variants={variants.div}
            initial="initial"
            whileInView="animate"
            viewport={{ once: true }}
        >
            <Component variants={variants.vector} />
        </Styled.Icon>
    )
}
