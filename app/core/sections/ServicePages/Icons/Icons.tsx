import Section from 'app/core/components/Section/Section'
import { Styled } from './Icons.styles'
import { ServiceIcons } from 'types'
import { Column, Container, Row } from 'app/styles/grid'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Icon from './Icon'
import Button from 'app/core/components/Button/Button'
import themes from 'app/core/constants/themes'
import colors from 'app/core/constants/colors'

export default function Icons({
    theme = 'light',
    headline,
    icons,
}: ServiceIcons) {
    return (
        <Section theme={theme}>
            <Container>
                {headline && (
                    <Styled.Headline
                        dangerouslySetInnerHTML={{ __html: headline }}
                    />
                )}
                <Row>
                    {icons.map(({ icon, title, text }, index) => {
                        return (
                            <Styled.Column key={index} sm={50}>
                                <Row alignX="space-between">
                                    <Styled.IconColumn sm={17}>
                                        <Icon name={icon} />
                                    </Styled.IconColumn>
                                    <Column sm={83}>
                                        <ScrollSpy>
                                            <Styled.Title>{title}</Styled.Title>
                                        </ScrollSpy>
                                        <ScrollSpy>
                                            <p>{text}</p>
                                        </ScrollSpy>
                                    </Column>
                                </Row>
                            </Styled.Column>
                        )
                    })}
                </Row>
                <Button
                    href="#start"
                    backgroundColor={colors.red}
                    color="#ffffff"
                    hoverBackgroundColor={themes[theme]?.color}
                    hoverColor={themes[theme]?.background}
                >
                    Start the Conversation
                </Button>
            </Container>
        </Section>
    )
}
