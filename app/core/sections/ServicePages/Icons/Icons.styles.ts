import styled from 'styled-components'
import { Column as GridColumn } from 'app/styles/grid'
import Headings from 'app/styles/headings'
import colors from 'app/core/constants/colors'
import { motion } from 'framer-motion'
import mediaQueries from 'app/core/constants/mediaQueries'

export namespace Styled {
    export const Headline = styled(Headings.H2)`
        ${mediaQueries.md} {
            font-size: 3rem;
        }
        span {
            color: ${colors.red};
        }
    `

    export const Icon = styled(motion.div)`
        opacity: 0;
        svg {
            max-width: 100%;
            height: auto;
            stroke: currentColor;
            max-width: 100px;
        }
    `

    export const IconColumn = styled(GridColumn)`
        text-align: center;
        margin-bottom: 50px;
    `

    export const Column = styled(GridColumn)`
        margin-bottom: 40px;
    `

    export const Title = styled(Headings.H4).attrs({
        as: 'h3',
    })`
        text-transform: uppercase;
        letter-spacing: 0.01em;
        font-size: 1.25rem;
        margin-bottom: 10px;
        font-weight: 700;
    `
}
