import Section from 'app/core/components/Section/Section'
import { Column, Container, Row } from 'app/styles/grid'
import { columns } from 'app/core/constants/grid'
import { ServiceCounters } from 'types'
import Counter from 'app/core/components/Counter/Counter'
import Button from 'app/core/components/Button/Button'
import colors from 'app/core/constants/colors'
import themes from 'app/core/constants/themes'
import * as Styled from './Counters.styles'

export default function Counters({
    theme = 'dark',
    counters,
}: ServiceCounters) {
    return (
        <Section theme={theme}>
            <Container>
                <Styled.Row>
                    {counters.map((counter, index) => (
                        <Column key={index} sm={columns / counters.length}>
                            <Counter {...counter} />
                            <p>{counter?.text}</p>
                        </Column>
                    ))}
                </Styled.Row>
                <Button
                    href="#start"
                    centered
                    backgroundColor={colors.red}
                    color="#ffffff"
                    hoverBackgroundColor={themes[theme]?.color}
                    hoverColor={themes[theme]?.background}
                >
                    START THE CONVERSATION
                </Button>
            </Container>
        </Section>
    )
}
