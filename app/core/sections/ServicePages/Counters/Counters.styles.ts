import styled from 'styled-components'
import { Row as GridRow } from 'app/styles/grid'

export const Row = styled(GridRow)`
    margin-bottom: 50px;
    text-align: center;
`
