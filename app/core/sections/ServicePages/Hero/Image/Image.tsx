import { Styled } from './Image.styles'
import { Image } from 'blitz'
import Parallax from 'app/core/components/Parallax/Parallax'
import { ServiceHero } from 'types'

type Props = Partial<ServiceHero> & {
    title: string
    imageSrc: string
}

export default function ImageHero({
    imageSrc,
    title,
    imgHeight = 1280,
    imgWidth = 1920,
}: Props) {
    const aspectRatio = (imgWidth / imgHeight) * 100

    return (
        <Styled.Image aspectRatio={aspectRatio}>
            <Parallax>
                <Image
                    src={imageSrc}
                    alt={title}
                    layout="fill"
                    objectFit="cover"
                    objectPosition={`center center`}
                />
            </Parallax>
        </Styled.Image>
    )
}
