import styled from 'styled-components'
import Headings from 'app/styles/headings'
import mediaQueries from 'app/core/constants/mediaQueries'
import { StyledProps } from 'types'
import headerHeight from 'app/core/constants/headerHeight'

export namespace Styled {
    export const Image = styled.div<StyledProps>`
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 500px;
        min-height: 100%;

        ${mediaQueries.md} {
            height: ${(props) =>
                props.aspectRatio ? props.aspectRatio : 100}%;
        }

        .parallax {
            will-change: transform;
            -webkit-backface-visibility: hidden;
            -webkit-transform-style: preserve-3d;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
        }
    `
}
