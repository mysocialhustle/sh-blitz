import styled from 'styled-components'
import Headings from 'app/styles/headings'
import mediaQueries from 'app/core/constants/mediaQueries'
import { StyledProps } from 'types'
import headerHeight from 'app/core/constants/headerHeight'
import { motion } from 'framer-motion'

export namespace Styled {
    export const Wrapper = styled.div<StyledProps>`
        position: absolute;
        bottom: 0;
        height: 100vh;
        width: ${(props) => props.aspectRatio}vh;
        min-width: 100vw;
        min-height: ${(props) =>
            props.aspectRatio ? 1 / props.aspectRatio : 50}vw;
    `

    export const Background = styled(motion.div)`
        transition: all 1s linear;
        &,
        svg {
            position: absolute;
            bottom: 0;
            height: 100%;
            width: 100%;
            left: 0;
        }
    `
}
