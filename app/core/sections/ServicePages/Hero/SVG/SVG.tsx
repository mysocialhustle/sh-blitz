import { useEffect } from 'react'
import { Styled } from './SVG.styles'
import { ServiceHero } from 'types'
import TikTokBG from 'app/svg/TikTokBG.inline.svg'
import RedTikTokBG from 'app/svg/RedTikTok-BG.inline.svg'
import SnapchatBG from 'app/svg/Snapchat-ads-bg.inline.svg'
import RedSnapchatBG from 'app/svg/Snapchat-ads-bg-red.inline.svg'
import useDimensions from 'app/core/hooks/useDimensions'
import {
    useViewportScroll,
    useTransform,
    useMotionTemplate,
    useMotionValue,
} from 'framer-motion'

type Props = Partial<ServiceHero> & {
    slug: string
}

const foreground = {
    ['tiktok-ads']: TikTokBG,
    ['snapchat-ads']: SnapchatBG,
}

const background = {
    ['tiktok-ads']: RedTikTokBG,
    ['snapchat-ads']: RedSnapchatBG,
}

export default function SVGHero({
    imgHeight = 1500,
    imgWidth = 3000,
    slug,
}: Props) {
    const [ref, dimensions] = useDimensions()
    const aspectRatio = (imgWidth / imgHeight) * 100
    const Foreground = foreground[slug]
    const Background = background[slug]

    const { scrollY } = useViewportScroll()
    const range = [dimensions.y, dimensions.y + dimensions.height]

    const val = useTransform(scrollY, range, range)

    const clipBottom = useMotionValue(0)
    const clipPath = useMotionTemplate`inset(0px 0px ${clipBottom}px 0px)`

    useEffect(() => {
        function update() {
            clipBottom.set(val.get())
        }

        const unsubscribe = val.onChange(update)

        return () => {
            unsubscribe()
        }
    }, [val, clipBottom])

    return Foreground && Background ? (
        <Styled.Wrapper ref={ref} aspectRatio={aspectRatio}>
            <Styled.Background>
                <Background />
            </Styled.Background>
            <Styled.Background style={{ clipPath }}>
                <Foreground />
            </Styled.Background>
        </Styled.Wrapper>
    ) : null
}
