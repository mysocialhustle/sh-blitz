import { ThemeProvider } from 'styled-components'
import themes from 'app/core/constants/themes'
import { Styled } from './Hero.styles'
import { Container } from 'app/styles/grid'
import { ServiceHero } from 'types'
import ImageHero from './Image/Image'
import SVGHero from './SVG/SVG'
import { Image } from 'blitz'

interface Props extends Partial<ServiceHero> {
    title: string
    imageSrc: string
    slug: string
    inlineImgsrc: string | boolean
}

export default function Hero({
    title,
    theme = 'light',
    type,
    inlineImgsrc = false,
    ...rest
}: Props) {
    const titleWords = title.split(' ')
    const titleWordsHalf = Math.ceil(titleWords.length / 2)
    const titleSolid = titleWords.splice(0, titleWordsHalf).join(' ')
    const titleOutline = titleWords.join(' ')

    const Background = type === 'svg' ? SVGHero : ImageHero

    return (
        <ThemeProvider theme={themes[theme]}>
            <Styled.Hero>
                <Background title={title} {...rest} />
                <Styled.Inner>
                    <Container>
                        <Styled.Title>
                            <span>{titleSolid}</span>
                            <br />
                            <Styled.OutlineText>
                                {titleOutline}
                            </Styled.OutlineText>
                        </Styled.Title>
                    </Container>
                    {inlineImgsrc && typeof inlineImgsrc === 'string' && (
                        <Styled.InlineImg>
                            <Image
                                src={inlineImgsrc}
                                alt={title}
                                layout="fill"
                                objectFit="contain"
                            />
                        </Styled.InlineImg>
                    )}
                </Styled.Inner>
            </Styled.Hero>
        </ThemeProvider>
    )
}
