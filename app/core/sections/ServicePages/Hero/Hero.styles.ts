import styled from 'styled-components'
import Headings from 'app/styles/headings'
import mediaQueries from 'app/core/constants/mediaQueries'
import { StyledProps } from 'types'
import headerHeight from 'app/core/constants/headerHeight'

export namespace Styled {
    export const Title = styled(Headings.H1)`
        text-transform: uppercase;
        font-weight: 900;
        word-spacing: 0.15em;
        color: ${(props) => props.theme.color};
    `

    export const OutlineText = styled.span`
        color: ${(props) => props.theme.color}00;
        -webkit-text-stroke-width: 1px;
        -webkit-text-stroke-color: ${(props) => props.theme.color};
        -webkit-text-fill-color: transparent;
        letter-spacing: calc(1px - 0.05em);

        ${mediaQueries.sm} {
            -webkit-text-stroke-width: 2px;
            letter-spacing: calc(2px - 0.05em);
        }
    `

    export const Hero = styled.div`
        padding-top: ${headerHeight}px;
        height: 500px;
        position: relative;
        overflow: hidden;
        display: flex;
        align-items: center;

        ${mediaQueries.md} {
            height: 100vh;
            padding-top: 0;
        }
    `

    export const Inner = styled.div`
        width: 100%;
        flex: 1;
        position: relative;
        z-index: 1;
        height: 100%;
        display: flex;
        align-items: center;
    `

    export const InlineImg = styled.div`
        position: absolute;
        right: -50px;
        top: 0;
        height: 100%;
        width: 300px;
        z-index: -1;
        user-select: none;

        ${mediaQueries.sm} {
            width: 400px;
            right: 10px;
        }

        ${mediaQueries.md} {
            width: 600px;
        }
    `
}
