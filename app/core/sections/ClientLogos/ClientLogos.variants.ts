const variants = {
    grid: {
        initial: {
            opacity: 1,
        },
        inView: {
            transition: {
                staggerChildren: 0.2,
            },
        },
    },
    item: {
        initial: {
            y: '50%',
            opacity: 0,
        },
        inView: {
            y: 0,
            opacity: 1,
            transition: {
                ease: [0.4, 0.1, 0.355, 1],
                duration: 0.75,
            },
        },
    },
}

export default variants
