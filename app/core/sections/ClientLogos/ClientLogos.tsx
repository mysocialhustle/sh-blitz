/* eslint-disable @next/next/no-img-element */
import { Styled } from './ClientLogos.styles'
import variants from './ClientLogos.variants'
import { Image } from 'blitz'
import Section from 'app/core/components/Section/Section'
import { Container } from 'app/styles/grid'
import featuredLogos from 'app/data/featuredLogos'

export default function ClientLogos() {
    return (
        <Section theme="dark">
            <Container>
                <Styled.Grid
                    variants={variants.grid}
                    initial="initial"
                    whileInView="inView"
                    viewport={{ once: true }}
                >
                    {featuredLogos.map((item, index) => {
                        const { name, type, main, hover } = item
                        return (
                            <Styled.ImageWrap
                                key={index}
                                variants={variants.item}
                            >
                                <Image
                                    src={hover}
                                    className="redImg"
                                    alt={name}
                                    layout="fill"
                                    objectFit="contain"
                                    objectPosition="center"
                                />
                                <Image
                                    src={main}
                                    className="whiteImg"
                                    alt={name}
                                    layout="fill"
                                    objectFit="contain"
                                    objectPosition="center"
                                />
                            </Styled.ImageWrap>
                        )
                    })}
                </Styled.Grid>
            </Container>
        </Section>
    )
}
