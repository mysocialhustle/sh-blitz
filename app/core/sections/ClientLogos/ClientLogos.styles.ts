import styled from 'styled-components'
import { gutter } from 'app/core/constants/grid'
import mediaQueries from 'app/core/constants/mediaQueries'
import { motion } from 'framer-motion'

export namespace Styled {
    export const Grid = styled(motion.div)`
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-column-gap: ${gutter}px;
        grid-row-gap: 50px;
        grid-auto-rows: 1fr;

        ${mediaQueries.sm} {
            grid-template-columns: repeat(5, 1fr);
        }
    `

    export const ImageWrap = styled(motion.div)`
        position: relative;
        transform-origin: center bottom;
        opacity: 0;
        transform: translateY(50%);
        height: 50px;

        img {
            transition: opacity 0.3s linear;
            height: 100%;
            width: 100%;
            object-fit: contain;
            object-position: center;
        }

        .redImg {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
        }

        &:hover {
            .whiteImg {
                opacity: 0;
                transition-delay: 0.05s;
            }
            .redImg {
                opacity: 1;
            }
        }
    `
}
