import styled from 'styled-components'
import { Container as GridContainer } from 'app/styles/grid'

export const Background = styled.div`
    position: absolute;
    top: 0;
    height: 100%;
    width: 100%;
    left: 0;
`

export const Container = styled(GridContainer)`
    padding-top: 90px;
    padding-bottom: 90px;
    min-height: 100vh;
    display: flex;
`
