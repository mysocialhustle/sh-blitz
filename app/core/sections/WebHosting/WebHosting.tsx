import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Button from 'app/core/components/Button/Button'
import { Container, Row, Column } from 'app/styles/grid'
import { Image } from 'blitz'
import * as Styled from './WebHosting.styles'
import colors from 'app/core/constants/colors'

export default function WebHosting({ theme }) {
    return (
        <Section theme={theme} size="min">
            <Styled.Background>
                <Image
                    src="https://socialhustle-blitz-static.s3.amazonaws.com/web-hosting/hostingbg.svg"
                    alt="Web Hosting"
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center"
                />
            </Styled.Background>
            <Styled.Container>
                <Row alignY="center">
                    <Column sm={50}>
                        <ScrollSpy>
                            <h2>Social Hustle Hosting Center</h2>
                            <p>
                                The hosting power you need, from the marketing
                                company you know. Cutting edge servers and
                                cloud-based software ensures that your website
                                stays at the front of the digital race.
                            </p>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Button
                                href="/web-hosting/"
                                color="#000"
                                backgroundColor="#fff"
                                hoverColor="#fff"
                            >
                                CHECK OUT OUR PLANS
                            </Button>
                        </ScrollSpy>
                    </Column>
                </Row>
            </Styled.Container>
        </Section>
    )
}
