import styled from 'styled-components'
import { Row } from 'app/styles/grid'
import mediaQueries from 'app/core/constants/mediaQueries'
import colors from 'app/core/constants/colors'
import { gutter } from 'app/core/constants/grid'

export namespace Styled {
    export const Image = styled.div`
        padding-bottom: 50%;
        position: relative;
        margin-left: ${gutter}px;
        margin-right: ${gutter}px;

        ${mediaQueries.sm} {
            position: absolute;
            top: 0;
            right: 0;
            height: 100%;
            width: 60%;
            padding: 0;
            margin: 0;
        }
        ${mediaQueries.md} {
            width: 50%;
        }
    `

    export const List = styled(Row).attrs({ as: 'ul' })`
        color: ${colors.darkGrey};
        letter-spacing: 0.2em;
        font-weight: 300;
        text-transform: uppercase;

        ${mediaQueries.md} {
            font-size: 1.1em;
        }

        li {
            margin-bottom: 20px;
        }
    `
}
