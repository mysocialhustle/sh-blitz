import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import { Styled } from './Values.styles'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Image } from 'blitz'

const traits = [`Data-Driven`, `Dedicated`, `Agile`, `Tenacious`]
const values = [`Communication`, `Relationships`, `Transparency`, `Hard Work`]

export default function Values() {
    return (
        <Section size="tiny">
            <Container>
                <Row alignY="center">
                    <Column sm={40} md={50}>
                        <h3>We Are:</h3>
                        <Styled.List style={{ marginBottom: 50 }}>
                            {traits.map((item, index) => (
                                <Column md={50} key={index}>
                                    <ScrollSpy>
                                        <li>{item}</li>
                                    </ScrollSpy>
                                </Column>
                            ))}
                        </Styled.List>
                        <h3>We Value:</h3>
                        <Styled.List>
                            {values.map((item, index) => (
                                <Column md={50} key={index}>
                                    <ScrollSpy>
                                        <li>{item}</li>
                                    </ScrollSpy>
                                </Column>
                            ))}
                        </Styled.List>
                    </Column>
                </Row>
            </Container>
            <Styled.Image>
                <Image
                    src="https://socialhustle-blitz-static.s3.amazonaws.com/our-company/sh-office-meeting-room.jpg"
                    alt="Social Hustle Office"
                    layout="fill"
                    objectFit="cover"
                    objectPosition="center center"
                />
            </Styled.Image>
        </Section>
    )
}
