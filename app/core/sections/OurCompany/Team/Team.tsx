import Section from 'app/core/components/Section/Section'
import { Container, Row } from 'app/styles/grid'
import team from 'app/data/team'
import { Styled } from './Team.styles'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Image } from 'blitz'

export default function Team() {
    return (
        <Section style={{ paddingBottom: 0 }}>
            <Container>
                <ScrollSpy>
                    <h2>Our Team</h2>
                </ScrollSpy>
                <Row alignX="space-between">
                    {team.map((el, index) => {
                        const { image, name, position } = el
                        return (
                            <Styled.Column key={index} sm={50} md={33}>
                                <ScrollSpy>
                                    <Styled.Image>
                                        <Image
                                            src={image}
                                            alt={`${name} - ${position}`}
                                            width={500}
                                            height={750}
                                            layout="responsive"
                                            lazyBoundary="500px"
                                        />
                                    </Styled.Image>
                                    <Styled.Name>{name}</Styled.Name>
                                    <Styled.Title>{position}</Styled.Title>
                                </ScrollSpy>
                            </Styled.Column>
                        )
                    })}
                </Row>
            </Container>
        </Section>
    )
}
