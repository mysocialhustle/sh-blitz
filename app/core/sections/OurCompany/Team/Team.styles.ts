import styled from 'styled-components'
import { Column as GridColumn } from 'app/styles/grid'
import Headings from 'app/styles/headings'

export namespace Styled {
    export const Column = styled(GridColumn)`
        margin-bottom: 50px;
    `

    export const Name = styled(Headings.H3)`
        font-weight: 300;
        margin-bottom: 10px;
    `

    export const Title = styled(Headings.H4)`
        text-transform: uppercase;
        font-weight: 800;
        letter-spacing: -0.025em;
    `

    export const Image = styled.div`
        margin-bottom: 20px;
    `
}
