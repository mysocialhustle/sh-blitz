import Button from 'app/core/components/Button/Button'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Section from 'app/core/components/Section/Section'
import { Column, Container, Row } from 'app/styles/grid'
import { Image } from 'blitz'
import { Styled } from './Intro.styles'

export default function Intro() {
    return (
        <Section size="small">
            <Container>
                <Row>
                    <Column md={60}>
                        <ScrollSpy>
                            <Styled.Title>Our Agency</Styled.Title>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Styled.Description>
                                Social Hustle is an award-winning digital
                                marketing and creative design agency.
                            </Styled.Description>
                        </ScrollSpy>
                        <ScrollSpy>
                            <Button href="/consulting">
                                Schedule a Consultation
                            </Button>
                        </ScrollSpy>
                    </Column>
                </Row>
                <ScrollSpy>
                    <Styled.Image>
                        <Image
                            src="https://socialhustle-blitz-static.s3.amazonaws.com/our-company/sh-office-front.jpg"
                            alt="Social Hustle Office"
                            layout="fill"
                        />
                    </Styled.Image>
                </ScrollSpy>
            </Container>
        </Section>
    )
}
