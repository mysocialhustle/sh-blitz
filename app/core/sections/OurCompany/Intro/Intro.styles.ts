import styled from 'styled-components'
import Headings from 'app/styles/headings'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Title = styled(Headings.H4).attrs({ as: 'h1' })`
        text-transform: uppercase;
        color: ${colors.red};
    `
    export const Description = styled(Headings.H3).attrs({ as: 'h2' })`
        font-weight: 600;
        font-size: 3rem;
    `

    export const Image = styled.div`
        position: relative;
        width: 100%;
        padding-bottom: 50%;
        margin-top: 100px;
        margin-bottom: 50px;
    `
}
