import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Image } from 'blitz'
import { Styled } from './Story.styles'

export default function Story() {
    return (
        <>
            <Section size="small">
                <Container>
                    <h2>Our Story</h2>
                    <Row alignX="space-between">
                        <Column sm={33}>
                            <ScrollSpy>
                                <h3>Founded In 2014</h3>
                                <p>
                                    In 2014, our Founder and President Dalin
                                    Bernard reached out to Christopher Parrett
                                    after acquiring Social Hustle’s first large
                                    account that paid purely on a commission
                                    basis.
                                </p>
                            </ScrollSpy>
                        </Column>
                        <Column sm={33}>
                            <ScrollSpy>
                                <h3>5 Years Later</h3>
                                <p>
                                    Within 5 years, Dalin, Chris and their team
                                    built a data-driven markting engine that
                                    propelled this account to surpass 20 million
                                    dollars a year in revenue and garner
                                    official partnerships with the NFL, MLB,
                                    Marvel, and Disney.
                                </p>
                            </ScrollSpy>
                        </Column>
                        <Column sm={33}>
                            <ScrollSpy>
                                <h3>Social Hustle Now</h3>
                                <p>{`We've partnered with companies around the world, generated over $200 million in sales for our partners and built an award winning creative team from the ground up... and every day we grow more.`}</p>
                            </ScrollSpy>
                        </Column>
                    </Row>
                </Container>
                <Container>
                    <Styled.Image>
                        <Image
                            src="https://socialhustle-blitz-static.s3.amazonaws.com/our-company/story.jpg"
                            alt="The Story of Social Hustle"
                            layout="fill"
                        />
                    </Styled.Image>
                </Container>
            </Section>
        </>
    )
}
