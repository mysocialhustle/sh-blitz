import styled from 'styled-components'

export namespace Styled {
    export const Image = styled.div`
        position: relative;
        width: 100%;
        padding-bottom: 40%;
        margin-top: 50px;
        margin-bottom: 50px;
    `
}
