import styled from 'styled-components'
import { Column } from 'app/styles/grid'
import mediaQueries from 'app/core/constants/mediaQueries'
import { motion } from 'framer-motion'
import { StyledProps } from 'types'

export const QuoteColumn = styled(Column)`
    text-align: center;
    line-height: 0;
    margin-bottom: 30px;

    ${mediaQueries.sm} {
        margin-bottom: 0;
    }
`

export const Description = styled.div`
    font-style: italic;
`

export const Author = styled.h5`
    text-transform: uppercase;
`

export const LogoWrapper = styled.div<StyledProps>`
    width: 100px;
    margin: 20px 0px;
    position: relative;
    height: 50px;
    ${(props) =>
        props.inverted &&
        `
        filter: invert(1);
    `};
`

export const Pager = styled.div`
    height: 4px;
    background-color: #6b1111;
    margin: 15px 10px 15px 0px;
    display: inline-block;
    transform-origin: left center;
    transition: width 1s cubic-bezier(0.16, 1, 0.3, 1);
`

export const Wrapper = styled.div`
    overflow: hidden;
    width: 100%;
`

export const Testimonials = styled.div`
    display: flex;
    flex-flow: row nowrap;
`

export const Testimonial = styled(motion.div)`
    flex: 1 1 0%;
    display: flex;
    flex-flow: column wrap;
    justify-content: center;
`
