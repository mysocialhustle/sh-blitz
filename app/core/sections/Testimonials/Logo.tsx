import React from 'react'
import * as Styled from './Testimonials.styles'
import { Image } from 'blitz'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'

interface Props {
    src?: null | string
    alt?: string
    inverted?: boolean
}

export default function Logo({ src = null, alt, inverted = false }: Props) {
    if (src !== null) {
        return (
            <ScrollSpy>
                <Styled.LogoWrapper inverted={inverted}>
                    <Image
                        src={src}
                        alt={alt}
                        layout="fill"
                        objectFit="contain"
                        objectPosition="left center"
                    />
                </Styled.LogoWrapper>
            </ScrollSpy>
        )
    } else {
        return null
    }
}
