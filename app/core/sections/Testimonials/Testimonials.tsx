import React, { useState, useEffect } from 'react'
import { TestimonialItem } from 'types'
import Section from 'app/core/components/Section/Section'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import Quote from 'app/svg/quote.inline.svg'
import { wrap } from 'popmotion'
import { Container, Row, Column } from 'app/styles/grid'
import * as Styled from './Testimonials.styles'
import Logo from './Logo'
import { useAnimation } from 'framer-motion'
import { ThemeType } from 'types'

interface Props {
    nodes: TestimonialItem[]
    theme?: ThemeType
}

export default function Testimonials({ nodes, theme = 'dark' }: Props) {
    const [page, setPage] = useState(0)
    const wrapped = (index) => wrap(0, nodes.length, index)
    const activeIndex = wrapped(page)

    const wrapperWidth = `${nodes.length * 100}%`

    const controls = useAnimation()

    useEffect(() => {
        const scrollPercent = activeIndex * -100
        controls.start({
            x: `${isNaN(scrollPercent) ? 0 : scrollPercent}%`,
        })

        if (nodes.length > 1) {
            const timer = setTimeout(() => {
                if (page + 1 < nodes.length) {
                    setPage(page + 1)
                } else {
                    setPage(0)
                }
            }, 7000)
            return () => clearTimeout(timer)
        }
    }, [activeIndex, controls, nodes.length, page])

    const isInverted = (theme: ThemeType) =>
        theme === 'light' || theme === 'grey' || theme === 'transparentLight'

    return (
        <Section theme={theme}>
            <Container>
                <Row alignX="space-between" alignY="center" reverse>
                    <Styled.QuoteColumn xs={20} sm={30}>
                        <Quote style={{ width: '100%', height: 'auto' }} />
                    </Styled.QuoteColumn>
                    <Column sm={75} md={70}>
                        <Styled.Wrapper>
                            <Styled.Testimonials
                                style={{ width: wrapperWidth }}
                            >
                                {nodes.length &&
                                    nodes.map((item, index) => (
                                        <Styled.Testimonial
                                            key={index}
                                            animate={controls}
                                            transition={{
                                                ease: [0.16, 1, 0.3, 1],
                                                duration: 1,
                                            }}
                                        >
                                            <ScrollSpy>
                                                <Styled.Description>
                                                    <p
                                                        dangerouslySetInnerHTML={{
                                                            __html: item.body,
                                                        }}
                                                    />
                                                </Styled.Description>
                                            </ScrollSpy>
                                            <ScrollSpy>
                                                <Styled.Author>
                                                    {item?.author}
                                                </Styled.Author>
                                            </ScrollSpy>
                                            <Logo
                                                src={item?.images?.featured}
                                                alt={item?.title}
                                                inverted={isInverted(theme)}
                                            />
                                        </Styled.Testimonial>
                                    ))}
                            </Styled.Testimonials>
                        </Styled.Wrapper>
                        {nodes.length &&
                            nodes.map((item, index) =>
                                index === activeIndex ? (
                                    <Styled.Pager
                                        key={index}
                                        style={{ width: 70 }}
                                    />
                                ) : (
                                    <Styled.Pager
                                        key={index}
                                        style={{
                                            width: 30,
                                            cursor: 'pointer',
                                        }}
                                        onClick={() => setPage(index)}
                                    />
                                )
                            )}
                    </Column>
                </Row>
            </Container>
        </Section>
    )
}
