import styled from 'styled-components'
import { Column as GridColumn } from 'app/styles/grid'
import Headings from 'app/styles/headings'

export namespace Styled {
    export const PageTitle = styled(Headings.H3).attrs({ as: 'h1' })`
        font-weight: 400;
        text-align: center;

        span {
            font-size: 0.8em;
        }
    `

    export const Image = styled.a`
        flex: 2 0 80px;
        position: relative;
        margin-bottom: 20px;
        filter: grayscale(1) brightness(0.75);
        transition: all 0.5s;
        display: block;
    `

    export const Info = styled.div`
        flex: 1 0 0%;
        opacity: 0;
        transition: all 0.5s;
    `

    export const Name = styled(Headings.H4).attrs({ as: 'h2' })`
        margin-bottom: 10px;
        letter-spacing: 1px;
    `

    export const Tagline = styled(Headings.H5).attrs({ as: 'h3' })`
        font-weight: 300;
    `

    export const Column = styled(GridColumn)`
        text-align: center;
        display: flex;
        flex-direction: column;

        &:hover {
            ${Image} {
                filter: grayscale(0) brightness(1);
            }

            ${Info} {
                opacity: 1;
            }
        }
    `
}
