import { Styled } from './Partners.styles'
import partners from 'app/data/partners'
import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import Headings from 'app/styles/headings'
import { Image } from 'blitz'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'

export default function Partners() {
    return (
        <Section>
            <Container>
                <ScrollSpy>
                    <Styled.PageTitle>
                        <span>
                            The Social Hustle Partnership Network provides you
                            access to{' '}
                            <strong>
                                <em>world-class</em>
                            </strong>{' '}
                            business solutions.
                        </span>
                    </Styled.PageTitle>
                </ScrollSpy>
                <Row alignX="space-around">
                    {partners.map((item, index) => {
                        const { name, link, imageSrc, tagline } = item
                        return (
                            <Styled.Column key={index} sm={33}>
                                <Styled.Image href={link} target="_blank">
                                    <Image
                                        src={imageSrc}
                                        alt={name}
                                        layout="fill"
                                        objectFit="contain"
                                        objectPosition="center center"
                                    />
                                </Styled.Image>
                                <Styled.Info>
                                    <Styled.Name>{name}</Styled.Name>
                                    <Styled.Tagline>{tagline}</Styled.Tagline>
                                </Styled.Info>
                            </Styled.Column>
                        )
                    })}
                </Row>
            </Container>
        </Section>
    )
}
