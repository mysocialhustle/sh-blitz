import { Styled } from './ProductFeed.styles'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { Image } from 'blitz'

export default function SingleProduct({ node }) {
    const { featuredImage, onlineStoreUrl, title, priceRange } = node
    const { altText, url } = featuredImage
    const { minVariantPrice, maxVariantPrice } = priceRange

    const price = (min, max) => {
        if (min !== max) {
            return `$${min} - $${max}`
        } else {
            return `$${min}`
        }
    }

    return (
        <ScrollSpy>
            <Styled.Wrapper>
                <Styled.Image>
                    <Styled.ImageWrapper href={onlineStoreUrl} target="_blank">
                        <Image
                            src={url}
                            alt={altText ? altText : title}
                            layout="fill"
                            objectFit="cover"
                            objectPosition={`center center`}
                        />
                    </Styled.ImageWrapper>
                </Styled.Image>
                <Styled.Title>{title}</Styled.Title>
                <p>{price(minVariantPrice?.amount, maxVariantPrice?.amount)}</p>
            </Styled.Wrapper>
        </ScrollSpy>
    )
}
