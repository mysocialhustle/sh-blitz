import Section from 'app/core/components/Section/Section'
import { Container, Row, Column } from 'app/styles/grid'
import SingleProduct from './SingleProduct'

export default function ProductFeed({ nodes }) {
    return (
        <Section size="small">
            <Container>
                <Row>
                    {nodes?.map(({ node }, index) => {
                        return (
                            <Column sm={33} key={index}>
                                <SingleProduct node={node} />
                            </Column>
                        )
                    })}
                </Row>
            </Container>
        </Section>
    )
}
