import styled from 'styled-components'

export namespace Styled {
    export const Wrapper = styled.div`
        height: 100%;
        display: flex;
        flex-flow: column wrap;
        justify-content: space-evenly;
    `

    export const Image = styled.div`
        overflow: hidden;
        margin-bottom: 20px;
        display: block;
        padding-bottom: 100%;
        position: relative;
    `

    export const Title = styled.h4`
        margin-bottom: 10px;
    `

    export const ImageWrapper = styled.a`
        position: absolute;
        transition: transform 0.5s ease;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;

        ${Image}:hover & {
            transform: scale(1.05);
        }
    `
}
