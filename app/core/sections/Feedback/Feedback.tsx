import { ComponentType, useState } from 'react'
import { Styled } from './Feedback.styles'
import { Container, Row } from 'app/styles/grid'
import FeedbackForm from 'app/core/blocks/Forms/Feedback'
import ReviewButtons from './ReviewButtons'
import SadFace from 'app/svg/sadface.inline.svg'
import MehFace from 'app/svg/mehface.inline.svg'
import HappyFace from 'app/svg/happyface.inline.svg'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import { motion, AnimatePresence } from 'framer-motion'
import variants from './Feedback.variants'

type FeedbackState = 'bad' | 'meh' | 'great'

interface State {
    name: FeedbackState
    Component: ComponentType
    Icon: ComponentType
}

const states: State[] = [
    {
        name: 'bad',
        Component: FeedbackForm,
        Icon: SadFace,
    },
    {
        name: 'meh',
        Component: FeedbackForm,
        Icon: MehFace,
    },
    {
        name: 'great',
        Component: ReviewButtons,
        Icon: HappyFace,
    },
]

export default function Feedback(props) {
    const [feedback, setFeedback] = useState<FeedbackState | null>(null)

    const opacity = (state) =>
        feedback !== null && feedback !== state ? 0.3 : 1

    return (
        <Container>
            <Row alignX="center">
                {states.map((item) => {
                    const { Icon, name } = item
                    return (
                        <Styled.Emoji
                            key={name}
                            sm={25}
                            style={{
                                opacity: opacity(name),
                            }}
                            onClick={() => setFeedback(name)}
                        >
                            <ScrollSpy>
                                <Icon />
                                <Styled.Heading>{name}</Styled.Heading>
                            </ScrollSpy>
                        </Styled.Emoji>
                    )
                })}
            </Row>
            <AnimatePresence>
                {states.map((item) => {
                    const { Component, name } = item
                    return feedback === name ? (
                        <motion.div
                            key={feedback}
                            variants={variants}
                            layout
                            initial="initial"
                            animate="animate"
                            exit="initial"
                            transition={{
                                duration: 0.75,
                                ease: [0.04, 0.62, 0.23, 0.98],
                            }}
                        >
                            <Component {...props} />
                        </motion.div>
                    ) : null
                })}
            </AnimatePresence>
        </Container>
    )
}
