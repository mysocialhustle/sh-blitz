import { Styled } from './Feedback.styles'
import { Row, Column } from 'app/styles/grid'
import Icon from 'app/core/components/Icon/Icon'

export default function ReviewButtons() {
    return (
        <>
            <h3 style={{ textAlign: 'center', marginTop: 50 }}>
                Leave Us a Review
            </h3>
            <Row alignX="center" style={{ textAlign: 'center' }}>
                <Column sm={25}>
                    <Styled.ShareButton
                        style={{ backgroundColor: '#1877f2' }}
                        href="https://www.facebook.com/mysocialhustle/reviews/?ref=page_internal"
                        target="_blank"
                    >
                        <Icon name="facebook" /> Facebook
                    </Styled.ShareButton>
                </Column>
                <Column sm={25}>
                    <Styled.ShareButton
                        style={{ backgroundColor: '#db4437' }}
                        href="https://g.page/Social-Hustle-ID/review?rc"
                        target="_blank"
                    >
                        <Icon name="googlePlus" /> Google+
                    </Styled.ShareButton>
                </Column>
            </Row>
        </>
    )
}
