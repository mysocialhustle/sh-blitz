import styled from 'styled-components'
import { Column } from 'app/styles/grid'
import Headings from 'app/styles/headings'

export namespace Styled {
    export const Heading = styled(Headings.H3)`
        text-transform: uppercase;
    `

    export const Emoji = styled(Column)`
        text-align: center;
        transition: opacity 0.3s ease-in;

        svg {
            cursor: pointer;
            max-width: 75px;
            margin: 0px auto 20px;
            display: block;
        }
    `

    export const ShareButton = styled.a`
        padding: 10px 20px;
        display: inline-block;
        transition: opacity 0.25s ease;
        color: white;
        border-radius: 3px;

        &:hover {
            opacity: 0.7;
        }
    `
}
