import { Styled } from './Item.styles'
import { Image, Link } from 'blitz'
import useDimensions from 'app/core/hooks/useDimensions'
import { useTransform, useSpring } from 'framer-motion'
import useWindowSize from 'app/core/hooks/useWindowSize'
import colors from 'app/core/constants/colors'

export default function Item({ item, xVal }) {
    const { slug, images, title, tagline } = item
    const [ref, dimensions] = useDimensions()
    const [imgRef, imgDimensions] = useDimensions()
    const { width: windowWidth } = useWindowSize()

    const inputStart = windowWidth - dimensions.x
    const inputEnd = -dimensions.x - dimensions.width

    const outputStart = 0
    const outputEnd = dimensions.width - imgDimensions.width

    const xRange = useTransform(
        xVal,
        [inputStart, inputEnd],
        [outputStart, outputEnd]
    )
    const x = useSpring(xRange, { stiffness: 400, damping: 90 })

    return (
        <Styled.Item>
            <Styled.Image ref={ref}>
                <Styled.ImageWrap ref={imgRef} style={{ x }}>
                    <Image src={images?.featured} alt={title} layout="fill" />
                </Styled.ImageWrap>
            </Styled.Image>
            <Styled.Content>
                <Styled.HeadingWrap>
                    <Styled.Heading>
                        <Styled.Tagline>
                            {tagline ? tagline : title}
                        </Styled.Tagline>
                        <Styled.Title>{title}</Styled.Title>
                    </Styled.Heading>
                    <Styled.ArrowWrap>
                        <svg
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            className="arrow-1"
                        >
                            <path
                                d="M13.25 1.5C13.25 1.08579 12.9142 0.749999 12.5 0.749999L5.75 0.749999C5.33579 0.749999 5 1.08579 5 1.5C5 1.91421 5.33579 2.25 5.75 2.25H11.75V8.25C11.75 8.66421 12.0858 9 12.5 9C12.9142 9 13.25 8.66421 13.25 8.25L13.25 1.5ZM2.03033 13.0303L13.0303 2.03033L11.9697 0.969669L0.96967 11.9697L2.03033 13.0303Z"
                                fill="currentColor"
                            ></path>
                        </svg>
                        <svg
                            width="14"
                            height="14"
                            viewBox="0 0 14 14"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                            className="arrow-2"
                        >
                            <path
                                d="M13.25 1.5C13.25 1.08579 12.9142 0.749999 12.5 0.749999L5.75 0.749999C5.33579 0.749999 5 1.08579 5 1.5C5 1.91421 5.33579 2.25 5.75 2.25H11.75V8.25C11.75 8.66421 12.0858 9 12.5 9C12.9142 9 13.25 8.66421 13.25 8.25L13.25 1.5ZM2.03033 13.0303L13.0303 2.03033L11.9697 0.969669L0.96967 11.9697L2.03033 13.0303Z"
                                fill={colors.red}
                            ></path>
                        </svg>
                    </Styled.ArrowWrap>
                </Styled.HeadingWrap>
            </Styled.Content>
        </Styled.Item>
    )
}
