import styled from 'styled-components'
import { motion } from 'framer-motion'
import mediaQueries from 'app/core/constants/mediaQueries'

export namespace Styled {
    export const Heading = styled.h3`
        font-size: 1.5rem;
        font-weight: 400;
        overflow: hidden;
        position: relative;
        margin-bottom: 0;
        flex: 1 1 0%;

        span {
            display: inline-block;
            transition: transform 0.8s cubic-bezier(0.16, 1, 0.3, 1);
        }
    `

    export const Title = styled.span`
        position: absolute;
        left: 0;
        top: 100%;
        width: 100%;
        height: 100%;
    `

    export const Tagline = styled.span``

    export const HeadingWrap = styled.div`
        display: flex;
        flex-flow: row wrap;
        justify-content: space-between;
        align-items: center;
        width: 100%;
    `

    export const ArrowWrap = styled.div`
        position: relative;
        height: 14px;
        width: 14px;
        overflow: hidden;

        svg {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 100%;
            transition: transform 0.8s cubic-bezier(0.16, 1, 0.3, 1);

            &.arrow-1 {
                transform: translate(0, 0);
            }
            &.arrow-2 {
                transform: translate(-100%, 100%);
            }
        }
    `

    export const ImageWrap = styled(motion.div)`
        width: 75vh;

        img {
            bottom: 0;
            height: 100%;
            left: 0;
            margin: 0;
            max-width: none;
            padding: 0;
            position: absolute;
            right: 0;
            top: 0;
            width: 100%;
            object-fit: cover;
            transform-origin: center center;
            transition: transform 0.8s cubic-bezier(0.16, 1, 0.6, 1);
        }

        ${mediaQueries.sm} {
            pointer-events: none;
            user-select: none;
            will-change: transform;
        }
        ${mediaQueries.md} {
            width: 90vh;
        }
    `

    export const Item = styled.div`
        position: relative;
        display: flex;
        align-items: stretch;
        justify-content: stretch;
        will-change: transform;
        width: 80vw;
        flex-direction: column;

        &:hover {
            ${Heading} span {
                transform: translate3d(0, -100%, 0);
            }
            ${ArrowWrap} {
                .arrow-1 {
                    transform: translate(100%, -100%);
                }
                .arrow-2 {
                    transform: translate(0%, 0%);
                }
            }
            ${ImageWrap} {
                img {
                    transform: scale(1.1);
                }
            }
        }

        ${mediaQueries.sm} {
            width: 60vw;
        }

        ${mediaQueries.md} {
            width: 40vw;
        }

        ${mediaQueries.lg} {
            width: 25vw;
        }
    `

    export const Image = styled.div`
        background: black;
        width: 100%;
        position: relative;
        height: 50vh;
        overflow: hidden;

        ${mediaQueries.md} {
            height: 60vh;
        }

        ${ImageWrap} {
            overflow: hidden;
            position: absolute;
            top: 0px;
            left: 0px;
            height: 100%;
        }
    `

    export const Content = styled.div`
        width: 100%;
        padding: 20px 0;
    `

    export const Button = styled.a`
        margin: 4.167vw auto 0px;
        color: rgb(255, 255, 255);
        display: flex;
        text-transform: uppercase;
        text-decoration: none;
        -webkit-box-align: center;
        align-items: center;
        height: 11.111vw;
        background: transparent;
        border: 0px;
        appearance: none;
        font-weight: 600;
        letter-spacing: 0.5px;

        ${mediaQueries.sm} {
            margin: 1.389vw auto;
            height: 2.778vw;
        }
    `
}
