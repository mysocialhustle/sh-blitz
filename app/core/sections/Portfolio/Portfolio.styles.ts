import styled from 'styled-components'
import { motion } from 'framer-motion'
import mediaQueries from 'app/core/constants/mediaQueries'
import { maxWidth, gutter } from 'app/core/constants/grid'
import colors from 'app/core/constants/colors'

export namespace Styled {
    export const Slider = styled.div`
        top: 0px;
        left: 0px;
        bottom: 0px;
        z-index: 9;
        will-change: transform;
        display: flex;
        align-items: center;
        width: 100%;
        overflow: hidden;
        position: relative;

        ${mediaQueries.md} {
            padding-bottom: 10vh;
        }
    `

    export const Grid = styled(motion.div)`
        display: grid;
        column-gap: 2vw;
        align-items: stretch;
        /**padding: 0 calc((100vw - ${maxWidth}px) / 2 + ${gutter}px);
        height: 100vh;**/
        padding: 0 2vw;
        grid-auto-columns: 1fr;
        grid-auto-flow: column;

        ${mediaQueries.sm} {
            cursor: grab;
        }
    `

    export const ProgressWrap = styled.div`
        ${mediaQueries.md} {
            display: block;
            position: absolute;
            left: 16.6667vw;
            bottom: 0;
            width: 66.6667vw;
            height: 2px;
            overflow: hidden;
            background-color: rgb(214, 214, 214);
            transform-origin: center center;
            will-change: transform;
        }
    `

    export const Progress = styled(motion.div)`
        transform: scaleX(0);
        ${mediaQueries.md} {
            display: block;
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 2px;
            background-color: ${colors.red};
            transform-origin: left center;
            will-change: transform;
        }
    `
}
