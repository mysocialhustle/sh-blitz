import { PortfolioItem, ThemeType } from 'types'
import { Styled } from './Portfolio.styles'
import useDimensions from 'app/core/hooks/useDimensions'
import useWindowSize from 'app/core/hooks/useWindowSize'
import { useMotionValue, useTransform } from 'framer-motion'
import Item from './Item/Item'
import Section from 'app/core/components/Section/Section'
import { Container } from 'app/styles/grid'

interface Props {
    nodes: PortfolioItem[]
    title?: string
    theme?: ThemeType
}

export default function Portfolio({ nodes, title, theme = 'light' }: Props) {
    const [ref, dimensions] = useDimensions()
    const { width: windowWidth } = useWindowSize()

    const rightLimit = 0
    const leftLimit = windowWidth - dimensions.width

    const x = useMotionValue(0)

    const progress = useTransform(x, [rightLimit, leftLimit], [0, 1])

    return (
        <Section theme={theme}>
            {title && <h2 style={{ textAlign: `center` }}>{title}</h2>}
            <Styled.Slider>
                <Styled.Grid
                    ref={ref}
                    drag="x"
                    dragConstraints={{
                        right: rightLimit,
                        left: leftLimit,
                    }}
                    dragTransition={{ bounceStiffness: 50 }}
                    style={{ x }}
                >
                    {nodes.map((item, index) => (
                        <Item key={index} item={item} xVal={x} />
                    ))}
                </Styled.Grid>
                <Styled.ProgressWrap>
                    <Styled.Progress style={{ scaleX: progress }} />
                </Styled.ProgressWrap>
            </Styled.Slider>
        </Section>
    )
}
