import React from 'react'
import { ServiceCategory } from 'types'

export interface ServicesContextProps {
    category: ServiceCategory
    setCategory: React.Dispatch<React.SetStateAction<ServiceCategory>>
}

export const ServicesContext = React.createContext<ServicesContextProps>({
    category: 'Marketing',
    setCategory: () => {},
})
