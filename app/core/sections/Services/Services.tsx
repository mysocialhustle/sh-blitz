import React, { useState, useMemo } from 'react'
import { ServiceCategory } from 'types'
import Section from 'app/core/components/Section/Section'
import { ServicesContext } from './Services.context'
import Toggle from './components/Toggle'
import List from './components/List'
import services from 'app/data/services'

export default function Services() {
    const [category, setCategory] = useState<ServiceCategory>('Marketing')
    const value = useMemo(() => ({ category, setCategory }), [category])

    return (
        <Section>
            <ServicesContext.Provider value={value}>
                <Toggle />
                <List nodes={services} />
            </ServicesContext.Provider>
        </Section>
    )
}
