import React, { useState, useContext, useMemo, useRef, useEffect } from 'react'
import { ServicesContext } from '../Services.context'
import useWindowSize from 'app/core/hooks/useWindowSize'
import * as Styled from '../Services.styles'
import Card from './Card'
import { ServiceItem } from 'types'

interface Props {
    nodes: ServiceItem[]
}

export default function List({ nodes }: Props) {
    const { category } = useContext(ServicesContext)
    const [dimensions, setDimensions] = useState({
        width: 0,
        height: 0,
        top: 0,
        left: 0,
        x: 0,
        y: 0,
        right: 0,
        bottom: 0,
    })
    const ref = useRef<any>(null)

    const { width: windowWidth } = useWindowSize()

    const filteredNodes = useMemo(
        () => nodes.filter((node) => node.category === category),
        [category, nodes]
    )

    useEffect(() => {
        if (ref) {
            const el = ref.current
            if (el) {
                const resize = () => {
                    const rect = el.getBoundingClientRect()
                    window.requestAnimationFrame(() => setDimensions(rect))
                }
                resize()
                window.addEventListener('resize', resize)

                return () => {
                    window.removeEventListener('resize', resize)
                }
            }
        }
    }, [ref])

    return (
        <Styled.ListWrapper>
            <Styled.Grid
                ref={ref}
                drag="x"
                dragConstraints={{
                    right: 0,
                    left: -dimensions.width + windowWidth,
                }}
                dragTransition={{ bounceStiffness: 50 }}
            >
                {filteredNodes.length > 0 &&
                    filteredNodes.map((node, index) => (
                        <Card node={node} key={index} />
                    ))}
            </Styled.Grid>
        </Styled.ListWrapper>
    )
}
