import React, { useContext } from 'react'
import { ServicesContext } from '../Services.context'
import { Container } from 'app/styles/grid'
import ScrollSpy from 'app/core/components/ScrollSpy/ScrollSpy'
import * as Styled from '../Services.styles'

export default function Toggle() {
    const { category, setCategory } = useContext(ServicesContext)
    return (
        <Container style={{ textAlign: 'center' }}>
            <ScrollSpy>
                <Styled.ToggleHeading
                    active={category === 'Marketing'}
                    style={{ textAlign: 'right' }}
                    onClick={() => setCategory('Marketing')}
                >
                    Marketing
                </Styled.ToggleHeading>
                <Styled.ToggleDivider>/</Styled.ToggleDivider>
                <Styled.ToggleHeading
                    active={category === 'Creative'}
                    onClick={() => setCategory('Creative')}
                >
                    Creative
                </Styled.ToggleHeading>
            </ScrollSpy>
        </Container>
    )
}
