import React from 'react'
import * as Styled from '../Services.styles'
import { Link, Image } from 'blitz'
import { ServiceItem } from 'types'

interface Props {
    node: ServiceItem
}

export default function Card({ node }: Props) {
    return (
        <Styled.Card>
            {/**<Link href={`/${node?.slug}/`} passHref>
                <a />
    </Link>**/}
            <Styled.CardImg>
                {node?.images?.transparent && (
                    <Image
                        src={node?.images?.transparent}
                        alt={node?.title}
                        layout="fill"
                    />
                )}
            </Styled.CardImg>
            <Styled.CardHeading>
                <span>{node?.title}</span>
                <svg
                    width="14"
                    height="14"
                    viewBox="0 0 14 14"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M13.25 1.5C13.25 1.08579 12.9142 0.749999 12.5 0.749999L5.75 0.749999C5.33579 0.749999 5 1.08579 5 1.5C5 1.91421 5.33579 2.25 5.75 2.25H11.75V8.25C11.75 8.66421 12.0858 9 12.5 9C12.9142 9 13.25 8.66421 13.25 8.25L13.25 1.5ZM2.03033 13.0303L13.0303 2.03033L11.9697 0.969669L0.96967 11.9697L2.03033 13.0303Z"
                        fill="currentColor"
                    ></path>
                </svg>
            </Styled.CardHeading>
        </Styled.Card>
    )
}
