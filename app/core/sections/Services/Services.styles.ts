import styled from 'styled-components'
import colors from 'app/core/constants/colors'
import mediaQueries from 'app/core/constants/mediaQueries'
import { StyledProps } from 'types'
import Headings from 'app/styles/headings'
import { motion } from 'framer-motion'

export const Card = styled.div`
    width: 100vw;
    border-right: 1px solid ${colors.lightGrey};
    border-bottom: 1px solid ${colors.lightGrey};
    position: relative;
    overflow: hidden;

    a {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        z-index: 2;
    }

    ${mediaQueries.sm} {
        width: 60vw;
    }

    ${mediaQueries.md} {
        width: 30vw;
    }

    &:before {
        content: '';
        display: inline-block;
        position: absolute;
        left: 0;
        top: 0;
        z-index: 0;
        width: 100%;
        height: 100%;
        background-color: ${colors.lightGrey};
        opacity: 0.5;
        transform: translateZ(0) scaleY(0);
        transform-origin: top center;
        transition: transform 1.2s cubic-bezier(0.351, 0.311, 0, 0.995);
    }

    &:hover {
        &:before {
            transition: transform 0.8s cubic-bezier(0.351, 0.311, 0, 0.995) 0.2s;
            transform: translateZ(0) scaleY(1);
            transform-origin: bottom center;
            -webkit-backface-visibility: hidden;
        }
        .gatsby-image-wrapper {
            transform: scale(1.1);
        }
    }
`

export const Grid = styled(motion.div)`
    display: grid;
    grid-auto-columns: 1fr;
    grid-auto-flow: column;
    border-top: 1px solid ${colors.lightGrey};
    margin-top: 20px;
    cursor: grab;

    ${Card}:last-child {
        border-right-color: transparent;
    }
`

export const CardImg = styled.div`
    margin-bottom: 20px;
    padding-bottom: 100%;
    position: relative;
    overflow: hidden;
    user-select: none;
    pointer-events: none;

    img,
    picture {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        transition: transform 1.2s cubic-bezier(0.351, 0.311, 0, 0.995);
        object-fit: contain;
        object-position: center center;
    }
`

export const CardHeading = styled(Headings.H3)`
    font-size: 1.5rem;
    font-weight: 400;
    position: relative;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 0;
    padding: 20px;
`

export const ToggleDivider = styled.span`
    display: inline-block;
    color: ${colors.lightGrey};
    font-weight: 800;
    font-size: 1.25rem;
    ${mediaQueries.md} {
        font-size: 4rem;
    }
`

export const ToggleHeading = styled(Headings.H2)<StyledProps>`
    margin: 30px 10px;
    transition: color 0.25s;
    display: inline-block;

    ${mediaQueries.sm} {
        margin: 30px;
    }

    @media (max-width: 959px) {
        font-size: 1.25rem;
    }

    ${(props) =>
        !props.active &&
        `
    color: ${colors.lightGrey};
    cursor: pointer;

    &:hover{
      color: black
    }

  `}
`

export const Navigation = styled.div`
    display: flex;
    flex-flow: row wrap;
    justify-content: center;
    margin-bottom: 10px;
    transition: all 0.15s ease-in-out;
`

export const NavArrow = styled.button`
    -webkit-appearance: none;
    border: 1px solid ${colors.lightGrey};
    padding: 10px 15px;
    border-radius: 30px;
    background-color: transparent;
    line-height: 0;
    margin: 0px 10px 20px;
    color: ${colors.darkGrey};
    transition: all 0.2s ease;

    ${(props) =>
        props.disabled &&
        `
      pointer-events: none;
    `}

    &:hover {
        background-color: ${colors.darkGrey};
        color: white;
    }

    svg {
        width: auto;
        height: 1em;
        stroke: currentColor;
    }
`

export const ListWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    overflow-x: unset;
    position: relative;
`
