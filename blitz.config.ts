import { BlitzConfig, sessionMiddleware, simpleRolesIsAuthorized } from 'blitz'
import { withGoogleFonts } from 'nextjs-google-fonts'

const config: BlitzConfig = {
    middleware: [
        sessionMiddleware({
            cookiePrefix: 'social-hustle-blitz',
            isAuthorized: simpleRolesIsAuthorized,
        }),
    ],
    images: {
        domains: [
            'socialhustle-blitz-static.s3.amazonaws.com',
            'scontent.cdninstagram.com',
            'cdn.shopify.com',
        ],
    },
    compiler: {
        styledComponents: true,
    },

    /* Uncomment this to customize the webpack config
  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    // Note: we provide webpack above so you should not `require` it
    // Perform customizations to webpack config
    // Important: return the modified config
    return config
  },
  */
    webpack: (config) => {
        config.module.rules.push({
            test: /\.svg$/,
            use: [
                {
                    loader: '@svgr/webpack',
                    options: {
                        prettierConfig: {
                            trailingComma: 'es5',
                            tabWidth: 4,
                            semi: false,
                            singleQuote: true,
                        },
                    },
                },
            ],
        })

        return config
    },
}

/**const googleFonts = withGoogleFonts({
    googleFonts: {
        fonts: [
            'https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap',
        ],
    },
})**/

module.exports = { ...config }
